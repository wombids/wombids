INSERT INTO `service_quote_statuses` (`code`, `name`) 
VALUES
	('new', 'New'), 
	('accepted', 'Accepted'), 
	('shortlist', 'Shortlisted'), 
	('eliminate', 'Eliminated'),
	('deleted', 'Deleted');