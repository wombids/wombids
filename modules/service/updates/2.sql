INSERT INTO `service_statuses` (`id`, `code`, `name`) 
VALUES
	(1, 'draft', 'Draft'),
	(2, 'pending', 'Pending Approval'),
	(3, 'active', 'Active'),
	(4, 'expired', 'Expired'),
	(5, 'closed', 'Closed'),
	(6, 'cancelled', 'Cancelled'),
	(7, 'archived', 'Archived');