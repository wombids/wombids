-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 10, 2014 at 11:19 PM
-- Server version: 5.5.39-36.0
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rajivban_wombids`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_groups`
--

CREATE TABLE IF NOT EXISTS `admin_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_groups`
--

INSERT INTO `admin_groups` (`id`, `name`, `description`, `code`) VALUES
(1, 'Administrator', 'Administrators can manage user accounts, system settings and any other objects in the system.', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `admin_groups_users`
--

CREATE TABLE IF NOT EXISTS `admin_groups_users` (
  `admin_user_id` int(11) DEFAULT NULL,
  `admin_group_id` int(11) DEFAULT NULL,
  KEY `admin_user_id` (`admin_user_id`),
  KEY `admin_group_id` (`admin_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_groups_users`
--

INSERT INTO `admin_groups_users` (`admin_user_id`, `admin_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_rights`
--

CREATE TABLE IF NOT EXISTS `admin_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `module_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_permission` (`user_id`,`module_id`,`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `admin_rights`
--

INSERT INTO `admin_rights` (`id`, `name`, `value`, `user_id`, `module_id`) VALUES
(1, 'access_dashboard', '1', 2, 'admin'),
(2, 'manage_users', '1', 2, 'admin'),
(3, 'manage_updates', NULL, 2, 'admin'),
(4, 'manage_settings', NULL, 2, 'admin'),
(5, 'notify_blog_comments', '1', 2, 'blog'),
(6, 'manage_posts_and_categories', '1', 2, 'blog'),
(7, 'manage_settings', '1', 2, 'blog'),
(8, 'manage_comments', '1', 2, 'blog'),
(9, 'manage_menus', '1', 2, 'builder'),
(10, 'manage_forms', '1', 2, 'builder'),
(11, 'manage_content', NULL, 2, 'cms'),
(12, 'manage_themes', NULL, 2, 'cms'),
(13, 'manage_files', NULL, 2, 'cms'),
(14, 'manage_templates', NULL, 2, 'cms'),
(15, 'manage_pages', NULL, 2, 'cms'),
(16, 'manage_partials', NULL, 2, 'cms'),
(17, 'manage_fees', '1', 2, 'payment'),
(18, 'manage_types', '1', 2, 'payment'),
(19, 'manage_invoices', '1', 2, 'payment'),
(20, 'manage_taxes', '1', 2, 'payment'),
(21, 'manage_requests', '1', 2, 'service'),
(22, 'manage_providers', '1', 2, 'service'),
(23, 'manage_categories', '1', 2, 'service'),
(24, 'manage_users', '1', 2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `time_zone` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `password_reset_hash` varchar(150) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `email` (`email`),
  KEY `password_reset_hash` (`password_reset_hash`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `login`, `first_name`, `middle_name`, `last_name`, `password`, `email`, `phone`, `mobile`, `time_zone`, `status`, `last_login`, `password_reset_hash`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Rajiv', NULL, 'Banga', '52d695bb53d14c86806cea55a30b8d29', 'banga.rajiv@gmail.com', NULL, NULL, NULL, NULL, '2014-09-08 19:35:00', NULL, NULL, NULL, '2014-07-08 18:17:57', NULL),
(2, 'tyangelo', 'Tyler', NULL, 'Angelo', '49f527787a216e42912e1bf5bde27b1d', 'tyangelo@gmail.com', NULL, NULL, NULL, 0, '2014-09-09 04:14:59', NULL, 1, NULL, '2014-08-11 18:30:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url_name` varchar(255) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` text,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `url_name` (`url_name`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `url_name`, `code`, `description`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Uncategorized', 'uncategorized', 'uncategorized', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE IF NOT EXISTS `blog_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(255) DEFAULT NULL,
  `author_email` varchar(50) DEFAULT NULL,
  `author_url` varchar(255) DEFAULT NULL,
  `author_ip` varchar(15) DEFAULT NULL,
  `content` text,
  `content_html` text,
  `is_owner_comment` tinyint(4) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author_ip` (`author_ip`),
  KEY `post_id` (`post_id`),
  KEY `status_id` (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog_comments`
--

INSERT INTO `blog_comments` (`id`, `author_name`, `author_email`, `author_url`, `author_ip`, `content`, `content_html`, `is_owner_comment`, `post_id`, `status_id`, `created_at`) VALUES
(1, 'tyler', 'tyangelo@gmail.com', 'wombids.com', '50.174.115.105', 'test post', '<p>test post</p>', NULL, 1, 1, '2014-08-13 04:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comment_statuses`
--

CREATE TABLE IF NOT EXISTS `blog_comment_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blog_comment_statuses`
--

INSERT INTO `blog_comment_statuses` (`id`, `code`, `name`) VALUES
(1, 'new', 'New'),
(2, 'approved', 'Approved'),
(3, 'deleted', 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comment_subscribers`
--

CREATE TABLE IF NOT EXISTS `blog_comment_subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `subscriber_name` varchar(255) DEFAULT NULL,
  `email_hash` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `email` (`email`),
  KEY `email_hash` (`email_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url_title` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `keywords` text,
  `published_at` date DEFAULT NULL,
  `is_published` tinyint(4) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `comments_allowed` tinyint(4) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `url_title` (`url_title`),
  KEY `category_id` (`category_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `title`, `url_title`, `description`, `content`, `keywords`, `published_at`, `is_published`, `category_id`, `comments_allowed`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'test title', 'test', 'laksjdflakjsdflakjsdflkajsdflkajsdf', '<p>Test, test</p>', NULL, '2014-08-13', 1, NULL, 1, 2, NULL, '2014-08-13 04:01:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_categories`
--

CREATE TABLE IF NOT EXISTS `blog_posts_categories` (
  `blog_post_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`blog_post_id`,`blog_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_posts_categories`
--

INSERT INTO `blog_posts_categories` (`blog_post_id`, `blog_category_id`, `code`) VALUES
(1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bluebell_directory_cities`
--

CREATE TABLE IF NOT EXISTS `bluebell_directory_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `url_name` varchar(100) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `is_seed` tinyint(4) DEFAULT NULL,
  `is_processed` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `zip_country_city` (`zip`,`state_id`,`country_id`,`url_name`),
  KEY `zip` (`zip`),
  KEY `country_id` (`country_id`),
  KEY `state_id` (`state_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bluebell_directory_cities`
--

INSERT INTO `bluebell_directory_cities` (`id`, `name`, `url_name`, `zip`, `country_id`, `state_id`, `is_seed`, `is_processed`) VALUES
(1, 'San Francisco', 'san-francisco', '94101', 1, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bluebell_provider_zip`
--

CREATE TABLE IF NOT EXISTS `bluebell_provider_zip` (
  `provider_id` int(11) NOT NULL,
  `zip` varchar(20) NOT NULL,
  PRIMARY KEY (`provider_id`,`zip`),
  KEY `provider_id` (`provider_id`),
  KEY `zip` (`zip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bluebell_provider_zip`
--

INSERT INTO `bluebell_provider_zip` (`provider_id`, `zip`) VALUES
(15, '94101');

-- --------------------------------------------------------

--
-- Table structure for table `braintree_log`
--

CREATE TABLE IF NOT EXISTS `braintree_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_data` text CHARACTER SET utf8,
  `created_at` datetime DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `builder_forms`
--

CREATE TABLE IF NOT EXISTS `builder_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `builder_forms`
--

INSERT INTO `builder_forms` (`id`, `name`, `code`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(5, 'Plumber', 'plumber', 2, NULL, '2014-08-30 00:08:20', NULL),
(4, 'Pool', 'PoolCode', 2, NULL, '2014-08-29 21:44:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `builder_form_fields`
--

CREATE TABLE IF NOT EXISTS `builder_form_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `element_id` varchar(100) DEFAULT NULL,
  `element_class` varchar(100) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `class_name` varchar(100) DEFAULT NULL,
  `config_data` text,
  `sort_order` int(11) DEFAULT NULL,
  `is_enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `form_id` (`form_id`),
  KEY `class_name` (`class_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `builder_form_fields`
--

INSERT INTO `builder_form_fields` (`id`, `label`, `code`, `comment`, `element_id`, `element_class`, `form_id`, `class_name`, `config_data`, `sort_order`, `is_enabled`) VALUES
(5, 'Plumber', 'plumber', 'What kind of plumber', NULL, NULL, 5, 'Builder_Radio_Field', '<?xml version="1.0"?>\n<data><field><id>options</id><value><![CDATA[s:39:"rough\nsoft \nfinish\nrepairs\nmaintenance\n";]]></value></field><field><id>allow_custom</id><value><![CDATA[s:0:"";]]></value></field><field><id>display_inline</id><value><![CDATA[s:0:"";]]></value></field></data>\n', 5, 1),
(4, 'Type of Pool Work', 'type-of-pool-work', 'Tell us what type of work', NULL, NULL, 4, 'Builder_Checkbox_Field', '<?xml version="1.0"?>\n<data><field><id>options</id><value><![CDATA[s:30:"Design\nBuild\nMaintenance\nCare ";]]></value></field><field><id>allow_custom</id><value><![CDATA[s:1:"1";]]></value></field><field><id>display_inline</id><value><![CDATA[s:0:"";]]></value></field></data>\n', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `builder_menus`
--

CREATE TABLE IF NOT EXISTS `builder_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `builder_menus`
--

INSERT INTO `builder_menus` (`id`, `name`, `code`, `short_description`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(5, 'Plumber', 'Plumber', 'Plumber', 2, NULL, '2014-08-30 00:07:03', NULL),
(4, 'Air conditioning', '1', NULL, 2, NULL, '2014-08-29 23:45:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `builder_menu_items`
--

CREATE TABLE IF NOT EXISTS `builder_menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `url_suffix` varchar(100) DEFAULT NULL,
  `element_id` varchar(100) DEFAULT NULL,
  `element_class` varchar(100) DEFAULT NULL,
  `config_data` text,
  `class_name` varchar(100) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `builder_menu_items`
--

INSERT INTO `builder_menu_items` (`id`, `menu_id`, `parent_id`, `label`, `title`, `url`, `url_suffix`, `element_id`, `element_class`, `config_data`, `class_name`, `sort_order`) VALUES
(1, 4, NULL, 'womBids helps with', NULL, '/about', NULL, NULL, NULL, '<?xml version="1.0"?>\n<data><field><id>page_id</id><value><![CDATA[s:1:"2";]]></value></field></data>\n', 'Builder_Page_Menu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_content`
--

CREATE TABLE IF NOT EXISTS `cms_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `type` varchar(10) DEFAULT 'html',
  `content` mediumtext,
  `is_global` tinyint(4) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `theme_id` varchar(64) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `page_id` (`page_id`),
  KEY `theme_id` (`theme_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Dumping data for table `cms_content`
--

INSERT INTO `cms_content` (`id`, `name`, `code`, `type`, `content`, `is_global`, `page_id`, `theme_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'FAQs', 'about_faq', 'html', '<h3>Provider FAQ</h3>\n\n<h5>How much does {site_name} cost?</h5>\n<p>It is <em>FREE</em> to sign up and to receive job leads. To respond to jobs you''re interested in, you will be charged a once off credit which cost as low as 75cents each. (credit amount varies by request type)</p>\n\n<h5>When does {site_name} charge me for a job?</h5>\n<p>When you find a job that you are interested in, you will need to buy some Pay-as-you-go credits to make contact with the client.</p>\n\n<h5>How does {site_name} match me to a job?</h5>\n<p>We match you based on the categories and skills you select in your freelance profile when you register. You can update this criteria at any time.</p>\n\n<h5>How long do I have to submit a price quote for jobs I''m offered?</h5>\n<p>It depends. Usually you have up to 30 hours from the time you receive the job offer. However, sometimes a client needs the service performed immediately. In that case, you''ll have up to 24 hours to respond.</p>\n\n<hr />\n\n<h3>Customer FAQ</h3>\n\n<h5>How much does {site_name} cost?</h5>\n<p>It is <em>FREE</em> to sign up and to contact providers. To respond to providers you''re interested in, you will be charged a once off credit which cost as low as 75cents each.</p>\n\n<h5>When does {site_name} charge me to find a provider?</h5>\n<p>When you find a provider that meets your requirements, you will need to buy some Pay-as-you-go credits to make contact with the provider.</p>\n\n<h5>How does {site_name} match me to a provider?</h5>\n<p>We match you based on the categories and skills you select in your freelance profile when you register. You can update this criteria at any time.</p>\n', NULL, 15, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(2, 'About us', 'about_us', 'html', NULL, NULL, 2, 'custom', NULL, 1, '2014-07-08 18:18:03', '2014-08-08 16:30:26'),
(3, 'Directory no providers found', 'directory_not_found', 'html', '<h3>It looks like we don''t have any providers in your area at the moment</h3>\n<p>Not to worry, we are dedicating to finding someone for you! So fill out the form to the right and tell us what you are looking for...</p>', NULL, NULL, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(4, 'Directory request submitted', 'directory_request_submit', 'html', '<h2>Your request has been received!</h2>\n<p class="lead">{site_name} will send the details of your request to this provider. Interested Providers will respond with their best price quotes.</p>\n<p>In the meantime, we have sent a confirmation message to your email address to verify your account for any updates about this request.</p>', NULL, NULL, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(6, 'Our Guarantee', 'our_guarantee', 'html', '<div class="row-fluid">\n	<div class="span9 mobile-span9">\n		<h3>Home Service Guarantee</h3>\n		<p>We take your trust and safety very seriously. We know your home is your most valuable asset. More importantly, your personal safety and the safety of your family is priceless.</p>\n		<p>Our Trust &amp; Safety team has developed some important features to ensure you find a trustworthy professional, and get your job done with peace of mind.</p>\n	</div>\n	<div class="span3 mobile-span3">\n		<p><img src="{theme_url}assets/images/intro/our_guarantee.png" alt="Our Gaurantee" /></p>\n	</div>\n</div>', NULL, 17, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(7, 'Page not found', 'page_not_found', 'html', '<p>\n	Please check the link and try again, or alternatively navigate to one of the links below:\n	<ul class="disc">\n		<li><a href="{root_url}">Return to the homepage</a></li>\n		<li><a href="{root_url}contact">Contact us for help</a></li>\n	</ul>\n</p>', NULL, NULL, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(8, 'Privacy page content', 'page_privacy_content', 'html', '<h4>PLEASE READ THE FOLLOWING CAREFULLY. THE AFOREMENTIONED IS THE PRIVACY POLICY THAT GOVERNS YOUR USE OF ANY {site_name} SERVICE.</h4>\n<p>This Privacy Policy ("**Privacy Policy**") applies to **{site_name}**</p><p><br></p>\n<!-- START PRIVACY POLICY CODE --><div style="font-family:arial"><strong>What information do we collect?</strong> <br><br>We collect information from you when you register on our site, place an order, subscribe to our newsletter or fill out a form.  <br><br>When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address, mailing address, phone number or credit card information. You may, however, visit our site anonymously.<br><br><strong>What do we use your information for?</strong> <br><br>Any of the information we collect from you may be used in one of the following ways: <br><br>; To personalize your experience<br><blockquote>(your information helps us to better respond to your individual needs)</blockquote><br><br>; To improve our website<br><blockquote>(we continually strive to improve our website offerings based on the information and feedback we receive from you)</blockquote><br><br>; To improve customer service<br><blockquote>(your information helps us to more effectively respond to your customer service requests and support needs)</blockquote><br><br>; To process transactions<br><blockquote>Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.</blockquote><br>; To send periodic emails<br><blockquote>The email address you provide for order processing, will only be used  to send you information and updates pertaining to your order.</blockquote><br><br><strong>How do we protect your information?</strong> <br><br>We implement a variety of security measures to maintain the safety of your personal information when you place an order or enter, submit, or access your personal information. <br> <br>We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Payment gateway providers database only to be accessible by those authorized with special access rights to such systems, and are required to?keep the information confidential.<br><br>After a transaction, your private information (credit cards, social security numbers, financials, etc.) will not be stored on our servers.<br><br><strong>Do we use cookies?</strong> <br><br>We do not use cookies.<br><br><strong>Do we disclose any information to outside parties?</strong> <br><br>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.<br><br><strong>Childrens Online Privacy Protection Act Compliance</strong> <br><br>We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.<br><br><strong>Terms and Conditions</strong> <br><br>Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website at <a href="/terms/">our terms and conditions page.</a><br><br><strong>Your Consent</strong> <br><br>By using our site, you consent to our <a style="text-decoration:none; color:#3C3C3C;" href="/privacy/" target="_blank">websites privacy policy</a>.<br><br><strong>Changes to our Privacy Policy</strong> <br><br>If we decide to change our privacy policy, we will update the Privacy Policy modification date below. <br><br>This policy was last modified on 6/13/14<br><br><strong>Contacting Us</strong> <br><br>If there are any questions regarding this privacy policy you may contact us using the information below. <br><br>wombids.com<br>3rd and Madison <br>Oakland, California 94607<br>USA<br>info@wombids.com<span></span><!-- END PRIVACY POLICY CODE --></div>', NULL, 25, 'custom', NULL, 1, '2014-07-08 18:18:03', '2014-08-15 20:00:14'),
(9, 'Terms page content', 'page_terms', 'html', '<p></p><p><b style="line-height: 1.45em; background-color: initial;">Introduction</b><br></p>\n\n<p></p>\n\n<p>These terms of use govern your use of our website;\nby using our website, you accept these terms of use in full.&nbsp; If you disagree with these terms of use or\nany part of these terms of use, you must not use our website. </p>\n<p>If you register with our website, we will ask you\nto expressly agree to these terms of use.</p>\n<p>We provide a forum for users to advertise, browse,\nbuy and sell products and services.&nbsp; We\nare not party to the sales or purchases of products via this website, and\nsubject to the first paragraph of Section 18 we will not be liable to any\nperson in relation to any contract for the sale and purchase of products.&nbsp; See Section 11 for more details of our role.</p><p><br></p>\n<p></p>\n\n<p><b>Licence to use\nwebsite</b></p>\n<p>Use of the Website\nis available only to individuals who are at least 18 years old and can form\nlegally binding contracts under applicable law. You represent, acknowledge and\nagree that you are at least 18 years of age, and that: </p>\n<p>All registration\ninformation that you submit is truthful and accurate, (b) you will maintain the\naccuracy of such information, and (c) your use of the Website and Services\noffered through this Website do not violate any applicable law or regulation.\nYour username and account may be terminated without warning if we believe that\nyou are under the age of 18 or that you are not complying with any applicable\nfederal, state or local laws, rules or regulations.</p><p><br></p>\n<p><b>Acceptable use and legal compliance</b></p>\n<p>You must comply with all applicable laws in\nrelation to any proposed or actual sale or purchase of products or service via\nour website.&nbsp; </p><p><br></p>\n<p><b>Homeowner and / or\nConsumer registration</b></p>\n<p>Providing you are at least 18 years of age and\nlegally allowed to do business, you may register with the websites as a buyer\nby: setting up a consumer profile. </p>\n<p>You will have the opportunity to identify and\ncorrect input errors prior to registering.</p><p><br></p>\n<p><b>Contractor and / or\nService Provider registration</b></p>\n<p>Providing you are at least 18 years of age, are\nacting in the course of a business and you represent that you have authority to claim and\nrepresent the business’s womBids account. </p>\n<p>You will have the opportunity to identify and\ncorrect input errors prior to registering. </p><p><br></p>\n<p><b>Listing rules</b></p>\n\n<p></p>\n\n<p>Be truthful, be ethical. </p><p><br></p>\n<p><b>Product and service rules</b></p>\n\n<p></p>\n<p>You must not use this website to advertise, buy,\nsell or supply services or downloads, or other intangible products.</p>\n<p>You must not sell or offer to sell via our website\nany product that:</p>\n<p>breaches any applicable laws, regulations or codes,\nor infringes any third party intellectual property rights or other rights, or\ngives rise to a cause of action against any person in any jurisdiction;</p><p><br></p>\n<p><b>The selling and\nbuying process</b></p>\n<p>You agree that a contract for the sale and purchase\nof a product or products will come into force between you and another\nregistered website user (and accordingly that you commit to buying or selling\nthe relevant product or products) in the following circumstances: </p>\n<p>(a) &nbsp; you\nmust add the products you wish to purchase to the shopping cart, and then\nproceed to the checkout; </p>\n<p>(b) &nbsp; if\nyou are a new user, you must create a buyer account with the website and log\nin; if you already have a buyer account with the website, your must enter your\nlogin details; </p>\n<p>(f) &nbsp; once\nthe seller has checked whether it is able to meet the buyer''s order, the seller\nwill send to the buyer an order confirmation (at which point the order will\nbecome a binding contract between the seller and the buyer) or the seller will\nconfirm by email that it is unable to meet the buyer''s order.</p><p><br></p>\n<p><b>Terms\nand conditions of sale</b></p>\n<p>A seller must use the website interface to create\nlegal notices that will apply to the relationship between that seller and that\nseller''s customers.&nbsp; Sellers are\nresponsible for ensuring that such legal notices are sufficient to meet the\nseller''s legal disclosure obligations and other legal obligations.&nbsp; Sellers must comply with all laws applicable\nto their product listings and sales, including where applicable the Consumer\nProtection (Distance Selling) Regulations 2000 and the Electronic Commerce (EC\nDirective) Regulations 2002.</p>\n<p>Except to the extent that a buyer and seller\nexpressly agree otherwise / Notwithstanding any terms agreed between a buyer\nand a seller, the following provisions will be incorporated into the contract\nof sale and purchase between the buyer and the seller: </p>\n<p>(a)&nbsp; the\nprice for a product will be as stated in the relevant product listing;</p>\n<p>(b)&nbsp; delivery\ncharges, packaging charges, handling charges, administrative charges, insurance\ncosts, other ancillary costs and charges, and VAT and other taxes will only be\npayable by the buyer if this is expressly and clearly stated in the product\nlisting;</p>\n<p>(c)&nbsp; deliveries\nof products must be made within 30 days of date the contract of sale coming\ninto force; </p><p><br></p>\n<p><b>Payments</b></p>\n<p>Sellers must pay to us the charges specified in\nthis Section 12.</p>\n<p>We will process all payments in respect of products\nmade by buyers to sellers, and will account to sellers for such payments in\nrespect of a calendar month on or before the end of the following calendar\nmonth.&nbsp; Payments to sellers will be made\nby Braintree in the form of direct deposit or Venmo transaction. </p>\n<p>The applicable seller charges in respect of stores\n/ product listings / product sales will be as set out on our website from time to\ntime.&nbsp; </p>\n<p>We will be entitled to charge interest on any\noverdue amount under these terms of use at the annual rate of 8% above the base\nrate of HSBC Bank Plc from time to time.&nbsp;\nSuch interest will accrue daily and be compounded quarterly.</p>\n<p>If a buyer makes any credit card, debit card or\nother charge-back then the seller will be liable to pay us, within 7 days\nfollowing the date we give notice to the buyer, an amount equal to the\ncommission due to us in respect of the transaction, plus any third party costs\nincurred by us arising directly out of the charge-back.</p><p><br></p>\n<p><b>User IDs and passwords</b></p>\n<p>It is the accountholder’s responsibility to protect\nall login credentials. </p><p><br></p>\n<p><b>User content</b></p>\n\n<p></p>\n\n<p>Our Right\nto Use Your Content<b>: &nbsp;</b>You acknowledge and agree that any\nContent you post or provide may be viewed by the general public and will not be\ntreated as private, proprietary or confidential. You authorize us and our\naffiliates, licensees and sub-licensees, without compensation to you or others,\nto copy, adapt, create derivative works of, reproduce, incorporate, distribute,\npublicly display or otherwise use or exploit such Content throughout the world\nin any format or media (whether now known or hereafter created) for the\nduration of any copyright or other rights in such Content, and such permission\nshall be perpetual and may not be revoked for any reason. Further, to the\nextent permitted under applicable law, you waive and release and covenant not\nto assert any moral rights that you may have in any Content posted or provided\nby you. Grant of License<b>:&nbsp;</b>You\nhereby grant WomBids and its users a perpetual, non-exclusive, royalty-free,\ntransferable, assignable, sub-licensable, worldwide license to use, store,\ndisplay, reproduce, modify, create derivative works, perform, distribute,\nprint, publish, disseminate and place advertising near and adjacent to your\nContent in any format or media (whether now know or hereafter created) on the\nWomBids and apps in any manner that we deem appropriate or necessary,\nincluding, if submitted, your name, voice and likeness throughout the world,\nand such permission shall be perpetual and cannot be revoked for any reason. Representation of Ownership and Right to Use\nContent: By posting or providing any Content to WomBids,\nyou represent and warrant to WomBids that you own or have all necessary rights\nto use the Content, and grant to WomBids the rights granted below. The forgoing\nrepresentation includes, but is not limited to a representation and warranty\nthat you own or have the necessary rights (including any necessary releases) to\ngrant all rights granted below in relation to any persons, places or\nintellectual property pictured in any photographic Content that you provide. In\naddition, if you post or otherwise provide any Content that is protected by\ncopyright, you represent that you have obtained any necessary permissions or\nreleases from the applicable copyright owner. Content Guidelines:&nbsp;WomBids reserves the right, but not the\nobligation, to edit or abridge, or to refuse to post, or to remove any content\nthat you or any other users post on any WomBids owned or operated websites or\napps if WomBids determines (in its sole discretion) that such content contains\nor features any of the following:</p>\n\n<p>Offensive, harmful and/or abusive\nlanguage, including without limitation: expletives, profanities, obscenities,\nharassment, vulgarities, sexually explicit language and hate speech (e.g.,\nracist/discriminatory speech.) References to illegal activity. Language that\nviolates the standards of good taste or the standards of this Site. Statements\nthat are or appear to be false. Comments that disparage WomBids.</p>\n\n<p>With respect to Ratings and Reviews\nof service professionals, all of the above and in addition the following:</p>\n\n<p>Reviews that do not address the\ngoods and services of the business or reviews with no qualitative value (e.g.,\n"work has not started yet"). Comments concerning a different Service\nProfessional: Information not related to work requested in the service request.\nIf a dispute arises between a consumer and professional, the rating submitted\nmay be held in pending status until resolution is reached. You represent and\nwarrant that any Rating and Review provided by you is accurate and truthful,\nand that your will only provide a Rating and Review for a service professional\nthat has performed services for you pursuant to your applicable service\nrequests.</p>\n\n<p></p>\n<p>Upon using\nWomBids, you will be prompted to disclose certain information about yourself and your\nservice requirements, and you will be able to store information, such as home services\nrecords, on our website or apps. Some of this information will be sent to\nservice professionals who will need this information to respond to your\nrequest. By providing this information to us, or by submitting a service\nrequest, you are requesting, and you expressly consent to being contacted by us\nand by our service professionals and providers via phone, fax, email, mail or\nother reasonable means, at any of your contact numbers or addresses, even if\nyou are listed on any federal, state, provincial or other applicable "Do\nNot Call" list, in order that we may provide the services set forth on our\nsite, to service your account, to reasonably address matters pertaining to your\naccount or for other purposes reasonably related to your service request and our\nbusiness, including marketing related emails. For complete details on our use\nof your information, please see our privacy policy. You agree that by\ncompleting a service request, you are entering into a business relationship\nwith WomBids and its partners and thus agree to be contacted by WomBids and/or\nits partners. You promise that all information you provide (including but not\nlimited to your contact information, and any Ratings and Reviews of service\nprofessionals that you provide) will be accurate, current and truthful to the\nbest of your knowledge. If you provide any information that is untrue, not\ncurrent or incomplete, or WomBids has reasonable grounds to suspect that such\ninformation is untrue, inaccurate, not current or incomplete, WomBids has the\nright to refuse any current or future use of the WomBids services (or any\nportion thereof) by you. You are responsible for any use of the WomBids\nservices by persons to whom you intentionally or negligently allow access to\nyour password.</p>\n<p>TO KNOWINGLY INPUT FALSE\nINFORMATION, INCLUDING BUT NOT LIMITED TO NAME, PHONE NUMBER, ADDRESS OR E-MAIL\nADDRESS IS A VERY SERIOUS AND FRAUDULENT MATTER THAT COULD RESULT IN\nSIGNIFICANT COSTS AND DAMAGES INCLUDING INVASION OF PRIVACY RIGHTS, TO WOMBIDS\nAND ITS SERVICE PROFESSIONAL PARTNERS, AND TO CONSUMERS, AS WELL AS THE LOSS OF\nTIME, EFFORT AND EXPENSE RESPONDING TO AND PURSUING SUCH FALSE INFORMATION AND\nREQUEST, AND FURTHER, COULD RESULT IN REGULATORY FINES AND PENALTIES.\nACCORDINGLY, IF YOU KNOWINGLY INPUT FALSE INFORMATION IN A SERVICE REQUEST,\nINCLUDING BUT NOT LIMITED TO SOME ONE ELSE''S NAME, E-MAIL ADDRESS, PHYSICAL\nADDRESS OR PHONE NUMBER OR A RANDOM OR MADE UP NAME, ADDRESS, E-MAIL OR PHONE\nNUMBER YOU AGREE TO FULLY INDEMNIFY AND BE LIABLE TO WomBids AND EACH WomBids\nSERVICE PROVIDER WHO ACCEPTS SUCH SERVICE REQUESTS, FOR THE GREATER OF: (1) A\nMINIMUM AMOUNT OF $11,000 TO EACH OF WomBids AND EACH OF THE AFFECTED SERVICE\nPROFESSIONALS AND FOR EACH OF THE ACTUAL PERSON(S) AFFECTED BY ANY OF THE\nIMPROPER, INCORRECT OR FRAUDULENT INFORMATION YOU ENTER (FOR EXAMPLE THE ACTUAL\nOWNER OF THE E-MAIL ADDRESS OR PHONE NUMBER, ETC.), PER IMPROPER SUBMISSION,\nPLUS ANY ATTORNEYS FEES COSTS AND EXPENSE RELATING THERETO, IF APPLICABLE, OR\n(2) THE ACTUAL DAMAGES, DIRECT, PUNITIVE AND CONSEQUENTIAL, AND ANY REGULATORY\nOR JUDICIAL FINES OR PENALTIES THAT MAY ARISE FROM SUCH INTENTIONAL,\nMISLEADING, HARMFUL AND FRAUDULENT ACTIVITY, PLUS REASONABLE LEGAL FEES, COST\nAND EXPENSES RELATING THERETO, WHICH EVER IS GREATER.</p><p><br></p>\n<p><b>Limitations and exclusions of liability</b></p>\n<p>No Guarantees or\nEndorsements:&nbsp;Although we take certain steps to examine the credentials of our\nlisted service professionals, we make no guarantees or representations\nregarding the skills or representations of such service professional or the\nquality of the job that he or she may perform for you if you elect to retain\ntheir services.&nbsp;WomBids does not\nendorse or recommend the services of any particular service professional.&nbsp;It\nis entirely up to you to evaluate a service professional''s qualifications, and\nto enter into a direct contract or otherwise reach agreement with a service\nprofessional. We do not guarantee or warrant any service professional''s\nperformance on the job or the outcome or quality of the services performed. The\nservice professionals are not employees or agents of WomBids, nor is WomBids an\nagent of the service professionals.</p>\n\n<p></p>\n\n<p>No Contracting via\nthe WomBids Website:&nbsp;WomBids may inform you of certain offers or discounts provided\nby a service professional. Such offers or discounts are made solely by the\nservice professional, and WomBids does not guarantee or warrant the pricing or\ndiscounts that a service professional may offer you. Any quotes provided by\nService Professionals via the WomBids or apps, or which you find on the WomBids\nor apps, are not contractually binding offers, are for informational purposes\nonly, and cannot be accepted on or via WomBids. No contractual arrangement is\ncreated based upon the quotes provided to you from Service Professionals (or\nyour scheduling of an appointment with a Service Professional) via WomBids. To\ncontract with a Service Professional, you must work directly with the Service\nProfessional. WomBids does not perform, and is not responsible for, any of the\nservices requested by you in your service request. Your rights under contracts\nyou enter into with service professionals are governed by the terms of such\ncontracts and by applicable federal, state, provincial and local laws.</p>\n\n<p>Release from Damages\nor Claims:&nbsp;Should you have a dispute with any service professional, you must\naddress such dispute with the service professional directly. YOU HEREBY AGREE\nTO RELEASE WomBids (AND OUR OFFICERS, DIRECTORS, AFFILIATES, EMPLOYEES AND\nAGENTS) FROM ANY DAMAGES OR CLAIMS (INCLUDING CONSEQUENTIAL AND INCIDENTAL\nDAMAGES) OF EVERY KIND OR NATURE, SUSPECTED AND UNSUSPECTED, KNOWN AND UNKNOWN,\nAND DISCLOSED OR UNDISCLOSED, ARISING OUT OF OR IN ANY WAY CONNECTED WITH SUCH\nDISPUTES AND YOUR DEALINGS WITH SERVICE PROFESSIONALS.</p><p><br></p>\n\n<p></p>\n\n<p><b>Indemnity</b></p>\n\n<p></p>\n\n<p>Anyone who operates on\nWOMBIDS AGREEs TO INDEMNIFY WOMBIDS, AND its AFFILIATES, OFFICERS, EMPLOYEES,\nAGENTS, CO-BRANDERS, AND OTHER PARTNERS AND HOLD THEM EACH HARMLESS FROM ANY\nAND ALL CLAIMS OR DEMANDS, INCLUDING ATTORNEY''S FEES, MADE BY ANY THIRD PARTY\nDUE TO OR ARISING FROM YOUR USE OF THE WOMBIDS SERVICES, IN CONNECTION WITH THE\nWOMBIDS WEB SITE, WITH REGARD TO ANY DISPUTE BETWEEN YOU AND A SERVICE\nPROFESSIONAL, OR YOUR VIOLATION OF THESE TERMS AND CONDITIONS, OR ARISING FROM\nYOUR VIOLATION OF ANY RIGHTS OF A THIRD PARTY.</p><p><br></p>\n\n<p></p>\n<p><b>Termination</b></p>\n<p>You may terminate the Terms\nat any time by closing your account, discontinuing your use of the Site, and\nproviding Wombids with a notice of termination. </p>\n<p>We may close your account,\nsuspend your ability to use certain portions of the Site, and/or ban you\naltogether from the Site for any or no reason, and without notice or liability\nof any kind. Any such action could prevent you from accessing your account, the\nSite, Your Content, Site Content, or any other related information.</p>\n', NULL, 36, 'custom', NULL, 1, '2014-07-08 18:18:03', '2014-08-15 19:46:21'),
(10, 'Sidebar content', 'provider_create_sidebar', 'html', '<h3>Advice and Tips</h3>\n<ul>\n<li><a href="/diy-guides">DIY Guides</a></li>\n<li><a href="/cost-guides">Cost Guides</a></li>\n<li><a href="/trade-insights">Trade Insights</a></li>\n<li><a href="/regulations">Regulations</a></li>\n<li><a href="/ask-an-expert">Ask an Expert</a></li>\n<li><a href="/blog">Blog</a></li>\n<li><a href="/social-media">Social Media</a></li>\n</ul>', NULL, 28, 'custom', NULL, 1, '2014-07-08 18:18:03', '2014-08-05 00:08:22'),
(12, 'Profile has been set up', 'provider_success', 'html', '<h2>Congrats, Your {role_name} profile has been set up!</h2>\n<p class="lead">We will now send you email or sms alerts with any requests during your work hours.</p>\n<p><a href="/intro">Learn more about {site_name}</a></p>\n<p><a href="/provide/create">Create another profile</a></p>\n<p><a href="/account/notifications">Change notification preferences</a></p>\n<p><a href="/dashboard">Go to your Dashboard</a></p>', NULL, 29, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(13, 'Why get testimonials?', 'provider_testimonials', 'html', '<div class="span6">\n<h5>Why get testimonials?</h5>\n<ul class="circle">\n<li>Helps build your credibility and repuatation</li>\n<li>Displays for talents and work ethic</li>\n<li>Assists if you have little or no ratings</li>\n</ul>\n</div>\n<div class="span6">\n<h5>Who should I ask?</h5>\n<ul class="circle">\n<li>Anyone you have worked with before</li>\n<li>Anyone who hired you for a job</li>\n</ul>\n</div>', NULL, 29, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(14, 'Why use us', 'provider_why_us', 'html', '<h3>Earn extra income!</h3>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt vehicula feugiat. Integer vulputate tempor consectetur. Etiam sed nunc arcu. Fusce tempus iaculis orci. Morbi dignissim eros id urna semper at porta sapien pretium. In ultricies tortor quis sem dapibus at posuere sem pulvinar. Quisque auctor sollicitudin nisi ac auctor. Etiam tincidunt tempus magna non vulputate. Sed lorem lorem, accumsan a bibendum eu, tincidunt non lectus. Duis quis sapien eget leo ultricies tristique sit amet sed diam. Nulla eget lorem metus. Donec nec pretium odio. Integer dictum erat quis nibh euismod pharetra. Nullam sed est quam. Nam augue purus, suscipit a sollicitudin non, venenatis id ligula.</p>\n<h3>Save time!</h3>\n<p>Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut gravida tempus lectus faucibus volutpat. Nulla facilisi. Mauris vel lorem vel turpis dignissim egestas. Suspendisse vel elit ac nisi feugiat consectetur in in lorem. Vivamus luctus turpis ac dui iaculis imperdiet. Donec purus quam, placerat at varius quis, accumsan sed libero. Fusce eu neque quis justo feugiat porttitor. Etiam accumsan lectus purus. Sed sagittis, arcu fermentum suscipit accumsan, elit eros eleifend nisl, ut lobortis ipsum arcu eu dui. Praesent malesuada risus id magna sagittis commodo condimentum ac lacus.</p>\n<h3>Get noticed online!</h3>\n<p>Duis a nulla eu metus commodo fermentum. Curabitur sit amet venenatis dolor. Curabitur blandit est ac enim venenatis sed ultrices turpis vehicula. Aliquam molestie vestibulum consectetur. Sed non neque mauris. Maecenas lobortis, tortor id consectetur fermentum, nisi orci pulvinar justo, at sodales dui magna consequat nulla. Pellentesque a turpis sit amet nulla laoreet laoreet quis non tellus. Sed ac ligula nulla, ut rhoncus est. Ut sit amet magna justo, ac imperdiet dui.</p>', NULL, 27, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(15, 'Quality Providers', 'quality_providers', 'html', '\n<div class="row-fluid">\n    <div class="span3 mobile-span3">\n        <p><img src="{theme_url}assets/images/intro/quality_providers.png" alt="Quality Providers" /></p>\n    </div>\n    <div class="span9 mobile-span9">\n        <h3>Target the best providers</h3>\n        <p>We pre-screen and pre-approve all the pros in our community of providers so you can be sure your job will get done right. During our pre-screening process we:</p>\n        <ul class="circle">\n            <li>Interview each professional</li>\n            <li>Verify their profile information</li>\n            <li>Check licenses, background, and service history</li>\n            <li>Look at reviews by customers like you, to see results from completed projects</li>\n        </ul>\n        <p>When a pro wows us during this process, we''ll ask them to take an oath.</p>\n    </div>\n</div>', NULL, 17, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:03'),
(17, 'Request Right Panel', 'request_right_panel', 'html', '<h4>How it Works</h4>\n<ul>\n	<li>Save time and money by submitting one request</li>\n	<li>Hire verified and trusted businesses</li>\n	<li>Stay protected with Our Guarantee</li>\n</ul>', NULL, 32, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:04'),
(18, 'Testimonial Introduction', 'testimonial_intro', 'html', '<h3>Testimonials will help improve {business_name}''s chance of winning jobs!</h3>\n<p>{site_name} is a website that helps connect providers, like {business_name} to local customers.</p>\n<p>By adding your testimonial you help {business_name} attract more customers and book more jobs.</p>', NULL, 31, 'custom', NULL, NULL, '2014-07-08 18:18:03', '2014-07-08 18:18:04'),
(106, 'Request submitted', 'request_complete', 'html', '<ul class="unstyled">\n    <li>\n        <h4 class="text-success"><span class="ring-badge ring-badge-success">1</span> We notify the right {title_plural} about your request</h4>\n        <p>We send your request to qualified {title_plural} who are available to work on your job at the time and place you requested.</p>\n    </li>\n    <li>\n        <h4 class="text-success"><span class="ring-badge ring-badge-success">2</span> You''ll receive price quotes</h4>\n        <p>You''ll receive real-time price quotes from {title_plural}.</p>\n    </li>\n    <li>\n        <h4 class="text-success"><span class="ring-badge ring-badge-success">3</span> Choose the right person for the job</h4>\n        <p>Pick the {title} you like best and schedule the work to be completed.</p>\n    </li>\n</ul>\n', NULL, NULL, 'custom', 1, 1, '2014-08-16 22:30:57', '2014-08-16 22:33:47'),
(26, 'Directory request submitted', 'directory_request_submit', 'html', NULL, NULL, 26, 'custom', 1, NULL, '2014-07-26 11:12:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE IF NOT EXISTS `cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `head` text,
  `description` text,
  `keywords` text,
  `content` mediumtext,
  `sort_order` int(11) DEFAULT NULL,
  `sitemap_visible` tinyint(4) DEFAULT '1',
  `published` tinyint(4) DEFAULT '1',
  `action_code` varchar(100) DEFAULT NULL,
  `code_post` text,
  `code_pre` text,
  `code_ajax` text,
  `parent_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `security_id` varchar(15) DEFAULT 'everyone',
  `security_page_id` int(11) DEFAULT NULL,
  `unique_id` varchar(25) DEFAULT NULL,
  `theme_id` varchar(64) DEFAULT NULL,
  `module_id` varchar(30) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `url` (`url`),
  KEY `title` (`title`),
  KEY `sort_order` (`sort_order`),
  KEY `action_code` (`action_code`),
  KEY `template_id` (`template_id`),
  KEY `theme_id` (`theme_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `name`, `file_name`, `url`, `title`, `head`, `description`, `keywords`, `content`, `sort_order`, `sitemap_visible`, `published`, `action_code`, `code_post`, `code_pre`, `code_ajax`, `parent_id`, `template_id`, `security_id`, `security_page_id`, `unique_id`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Page not found', '404', '/404', NULL, NULL, NULL, NULL, '<div id="p_site_404">\r\n	<?=$this->display_partial(''site:404'')?>\r\n</div>', NULL, 1, 1, 'Custom', NULL, NULL, NULL, NULL, 2, 'everyone', NULL, '4f72b29abb48e0.70787123', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:01'),
(2, 'womBids helps with', 'about', '/about', NULL, NULL, NULL, NULL, '<div class="image-container" style="background:url(''http://dev1.wombids.com/themes/custom/assets/images/about/about-us-1.jpg'');">\n	<div class="inner-panel">\n			<p>Founded in 2013 and based in San Francisco,<b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>is trusted marketplace where homeowners can find the most respected local\n			contractors and home service pros. </p>\n			\n			<p>Our customers use <b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>to find small businesses that specialize in projects\n			ranging from air conditioning to zinc roofing.</p>\n	</div>\n</div>\n<div class="container">\n	<div class="row-fluid">\n		<div class="span12">\n			<div class="page-header">\n			    <h1><?=$this->page->title_name?></h1>\n			    <h4 class="subheader"><?=$this->page->description?></h4>\n			</div>\n			<table class="contentTable"><tbody><tr><td>Air Conditioning &amp; Cooling</td><td>Driveways, Patios, Walks, Steps &amp; Floors</td><td>Junk Removal</td></tr><tr><td>Cabinets &amp; Countertops<br></td><td>Drywall &amp; Insulation<br></td><td>Landscape, Decks &amp; Fences<br></td></tr><tr><td>Carpentry<br></td><td>Fences<br></td><td>Maintenance of Lawn, Trees &amp; Shrubs<br></td></tr><tr><td>Carpet<br></td><td>Flooring<br></td><td>Painting &amp; Staining<br></td></tr><tr><td>Cleaning Services<br></td><td>Garages, Doors, Openers<br></td><td>Plumbing<br></td></tr><tr><td>Concrete, Brick &amp; Stone<br></td><td>Gutters<br></td><td>Swimming Pools, Spas, Hot Tubs<br></td></tr><tr><td>Decks, Porches, Gazebos &amp; Play Equipment<br></td><td>Handyman Services<br></td><td>Tile &amp; Stone<br></td></tr><tr><td>Decorators &amp; Designers<br></td><td>Heating &amp; Cooling<br></td><td>Miscellaneous<br></td></tr></tbody></table>\n\n			<div class="sub-text">Whether it is an enormous remodel or a job as simple as removing debris, <b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>can help! Don’t\n			believe us, give us a call toll free at 1(800)574-6501 and we’ll walk you\n			through the process.</p></div>\n			<div class="sub-header">At <b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>we have two\n			main missions:</div>\n			\n			<div class="dynamic-row step-1">\n				<div class="image">\n				    <img src="/themes/custom/assets/images/about/rating.jpg">\n				</div>\n				<div class="text">\n				    <h4>1. Simpler & Transparent</h4>\n				    <p>Make home projects simpler and more efficient.<br>We achieve this by providing transparency, as well as easier access to the best local pros.\n				    </p>\n				</div>\n			</div>\n			\n			<div class="dynamic-row step-2">\n				<div class="image">\n				    <img src="/themes/custom/assets/images/about/american_dream.jpg">\n				</div>\n				<div class="text">\n				    <h4>2. We <i class="fa fa-heart"></i> small businesses</h4>\n				    <p>We want to do our part to help small businesses grow.<br>Not only are they the back bone of the economy, they are living proof that the American Dream is alive and well.<br>Whether it’s through finding new customers or providing a platform to showcase your amazing reputation, womBids is here to help.</p>\n				</div>\n			</div>\n			\n			<div class="pageLinks">Next steps? Start an account are you <a class="homeowner" href="/request">homeowner</a>&nbsp;or a <a class="pro" href="/provide">service pro</a></div>\n			\n			<?=content_block(''about_us'', ''About us'')?>\n		</div>\n	</div>\n</div>', NULL, 1, 1, 'Custom', NULL, '$this->data[''current_menu''] = ''about'';\n\n\n\n', NULL, NULL, 8, 'everyone', NULL, '4fed995de453f1.01201997', 'custom', NULL, NULL, 1, '2014-07-08 18:17:58', '2014-08-26 17:46:54'),
(3, 'Account', 'account', '/account', 'My Account', '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/account_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/account_page.js'',\n)) ?>', NULL, NULL, '<?\n	$selected_tab = $this->request_param(0, ''account'');\n	if ($selected_tab == ''offers'' || $selected_tab == ''requests'')\n		$selected_tab = ''work_history'';\n?>\n<div class="row-fluid">\n	<div class="span9">\n\n		<div class="tabbable">\n\n			<ul class="nav nav-tabs">\n				<li class="<?=$selected_tab==''account''?''active'':''''?>"><a href="#account" data-toggle="tab"><?=__(''Account'')?></a></li>\n				<li class="<?=$selected_tab==''finances''?''active'':''''?>"><a href="#finances" data-toggle="tab"><?=__(''Finances'')?></a></li>\n				<? if ($this->user->is_provider): ?>\n					<li class="<?=$selected_tab==''skill_profiles''?''active'':''''?>"><a href="#skill_profiles" data-toggle="tab"><?=__(''Skill Profiles'')?></a></li>\n					<li class="<?=$selected_tab==''withdraw_profiles''?''active'':''''?>"><a href="#withdraw_profiles" data-toggle="tab"><?=__(''Withdraw Profiles'')?></a></li>\n				<? endif ?>\n				<li class="<?=$selected_tab==''notifications''?''active'':''''?>"><a href="#notifications" data-toggle="tab"><?=__(''Notifications'')?></a></li>\n				<li class="<?=$selected_tab==''work_history''?''active'':''''?>"><a href="#work_history" data-toggle="tab"><?=__(''Work History'')?></a></li>\n			</ul>\n			<div class="tab-content">\n				<div class="tab-pane <?=$selected_tab==''account''?''active'':''''?> account" id="account">\n					<div class="content"><?=$this->display_partial(''account:details'')?></div>\n				</div>\n\n				<div class="tab-pane <?=$selected_tab==''finances''?''active'':''''?> finances" id="finances">\n					<div class="content"><?=$this->display_partial(''account:finances'')?></div>\n				</div>\n\n				<? if ($this->user->is_provider): ?>\n					<div class="tab-pane <?=$selected_tab==''skill_profiles''?''active'':''''?> profiles" id="skill_profiles">\n						<div class="content"><?=$this->display_partial(''account:skill_profiles'')?></div>\n					</div>\n					<div class="tab-pane <?=$selected_tab==''withdraw_profiles''?''active'':''''?> profiles" id="withdraw_profiles">\n						<div class="content"><?=$this->display_partial(''account:withdraw_profiles'')?></div>\n					</div>\n				<? endif ?>\n\n				<div class="tab-pane <?=$selected_tab==''notifications''?''active'':''''?> notifications" id="notifications">\n					<div class="content"><?=$this->display_partial(''account:notifications'')?></div>\n				</div>\n\n				<div class="tab-pane <?=$selected_tab==''work_history''?''active'':''''?> work-history" id="work_history">\n					<div class="content"><?=$this->display_partial(''account:work_history'')?></div>\n				</div>\n			</div>\n		</div>\n\n	</div>\n</div>', NULL, 1, 1, 'bluebell:account', NULL, NULL, NULL, NULL, 2, 'users', 5, '4f792ee75c2481.67961677', 'custom', NULL, NULL, 1, '2014-07-08 18:17:58', '2014-09-07 01:07:20'),
(4, 'Reset Password', 'account_reset', '/account/reset', 'Forgot your password?', '<?=$this->js_include(array(\n    ''@/assets/scripts/js/layouts/user_layout/pages/account_reset_page.js'',\n)) ?>', NULL, NULL, '<div class="row-fluid">\n	<div class="span6 offset3">\n		<div class="well">\n\n			<!-- Reset password -->\n			<div id="reset_pass_form">\n				<h1><?=$this->page->title_name?></h1>\n				<h4><?=__("No problem! We can help if you can access your email...")?></h4>\n				<?=form_open(array(''id'' => ''form_reset_pass''))?>\n					<input type="hidden" name="password_reset" value="1" />\n					<div class="control-group">\n						<label for="user_email" class="control-label"><?= __(''Enter your Email Address'', true) ?></label>\n						<div class="controls">\n							<input type="text" name="User[email]" value="" id="user_email" class="span12" />\n						</div>\n					</div>\n					<?=form_submit(''submit'', __(''Reset Password''), ''class="btn btn-primary btn-large btn-block"'')?>\n				<?=form_close()?>\n			</div>\n\n			<!-- Success message -->\n			<div id="reset_pass_success" style="display:none">\n				<h1><?=__(''Check your mailbox'')?></h1>\n				<h4><?=__(''Instructions have been sent to your email address, please check your Email for confirmation!'')?></h4>\n\n				<?=form_open(array(''id'' => ''form_login'', ''class''=>'''', ''onsubmit'' => "return $(this).phpr().post(''user:on_login'').send()"))?>\n					<input type="hidden" name="redirect" value="<?= root_url(''dashboard'') ?>" />\n					\n					<?=$this->display_partial(''site:login_form'')?>\n					\n					<?=form_submit(''submit'', __(''Log in''), ''class="btn btn-primary btn-large btn-block"'')?>\n				<?=form_close()?>\n			</div>\n\n		</div>\n	</div>\n</div>\n\n<script>\n\nPage.forgotPasswordFormFields = $.phpr.form().defineFields(function() {\n	this.defineField(''User[email]'').required("<?=__(''Please specify your email address'',true)?>").email("<?=__(''Please specify a valid email address'',true)?>");\n});\n\n</script>', NULL, 1, 1, 'user:reset_password', NULL, NULL, NULL, 3, 2, 'guests', 13, '4f72ad706021d9.68655677', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(5, 'Sign in', 'account_signin', '/account/signin', 'Sign in', '<?=$this->js_include(array(\n    ''@/assets/scripts/js/layouts/user_layout/pages/account_signin_page.js'',\n)) ?>', NULL, NULL, '<div class="row-fluid">\n	<div class="span6 offset3">\n		<div class="well">\n		\n			<h1><?=$this->page->title_name?></h1>\n\n			<!-- Login form -->\n			<div class="form">\n				<?=form_open(array(''id'' => ''form_login''))?>\n					<input type="hidden" name="redirect" value="<?= (isset($redirect))?$redirect:root_url(''dashboard'') ?>" />\n					\n					<?=$this->display_partial(''site:login_form'')?>\n					\n					<p><input type="submit" name="submit" value="<?= __(''Log in'') ?>" class="btn btn-primary btn-large btn-block" /></p>\n					<p><a href="<?=root_url(''account/reset'')?>"><?=__(''Forgot your password?'')?></a></p>\n				<?=form_close()?>\n			</div>\n\n			<!-- Social Authentication -->\n			<? if (Social_Config::can_authenticate()): ?>\n				<hr />\n				<h4><?= __(''Connect using'') ?></h4>\n				<? foreach (Social_Provider::find_all_active_providers() as $provider): ?>\n					<p>\n						<a href="<?=$provider->get_login_url()?>" class="btn btn-large btn-block">\n							<i class="icon-<?=$provider->code?>"></i> Sign in with <?=$provider->provider_name?>\n						</a>\n					</p>\n				<? endforeach ?>\n			<? endif ?>\n\n		</div>\n\n		<p><a href="<?= root_url(''account/signup'') ?>"><?= __(''Register a new account'') ?></a></p>\n		\n	</div>\n</div>', NULL, 1, 1, 'user:login', NULL, '$this->data[''current_menu''] = ''login'';\n\n\n\n', NULL, 3, 2, 'guests', 13, '4f70511904b848.15165327', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(6, 'Sign out', 'account_signout', '/account/signout', 'Sign out', NULL, NULL, NULL, '<p><?=__("Logging out...")?></p>', NULL, 1, 1, 'Custom', 'user_logout(root_url(''/''));\n\n\n\n', NULL, NULL, 3, 2, 'everyone', NULL, '4f72ae09a604b7.15517912', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(7, 'Sign up', 'account_signup', '/account/signup', 'Register New Account', '<?=$this->js_include(array(\n    ''@/assets/scripts/js/layouts/user_layout/pages/account_signup_page.js'',\n)) ?>', NULL, NULL, '<div class="row-fluid">\n	<div class="span6 offset3">\n		<div class="well">\n			<h1><?=$this->page->title_name?></h1>\n			<h4><?= __(''Already have an account?'') ?> <a href="<?= root_url(''account/signin'') ?>"><?= __(''Sign in'') ?></a></h4>\n\n			<!-- Register form -->\n			<?=form_open(array(''id'' => ''form_register''))?>\n				<input type="hidden" name="redirect" value="<?= root_url(''account/signin'') ?>" />\n\n				<?=$this->display_partial(''site:register_form'')?>\n\n				<p><input type="submit" name="submit" value="<?= __(''Create Account'') ?>" class="btn btn-primary btn-large btn-block" /></p>\n				<p>\n						<?=__(''By pressing create account you agree to the %s.'', array(\n						''<a href="''.root_url(''terms'').''">''.__(''terms of service'', true).''</a>''\n					))?>\n				</p>\n			<?=form_close()?>\n\n		</div>\n	</div>\n</div>', NULL, 1, 1, 'user:register', NULL, '$this->data[''current_menu''] = ''register'';\n\n\n', NULL, 3, 2, 'guests', 13, '4f704934c7f028.15518777', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(8, 'Blog', 'blog', '/blog', NULL, NULL, NULL, NULL, '<div class="row-fluid">\n	<div class="span8">\n		<div class="page-header">\n			<h1><?=$this->page->title_name?></h1>\n			<h4 class="subheader"><?=$this->page->description?></h4>\n		</div>\n\n		<ul class="post_list">\n			<? foreach ($post_list as $post): ?>\n			<li>\n				<h3><a href="<?=root_url(''blog/post/''.$post->url_title) ?>"><?=h($post->title) ?></a></h3>\n				<p class="light">\n					<?=__(''Published by %s on %s.'', array(\n						h($post->author_first_name.'' ''.substr($post->author_last_name, 0, 1).''.''),\n						$post->published_at->format(''%F'')\n					))?>\n					<?=__(''Comments'')?>: <?=$post->approved_comment_num ?>\n				</p>\n				<p><?=h($post->description) ?> <a href="<?=root_url(''blog/post/''.$post->url_title) ?>"><?=__(''Show more'', true)?></a></p>\n			</li>\n		<? endforeach ?>\n		</ul>\n\n		<div class="view_controls">\n			<? $this->display_partial(''site:pagination'', array(''pagination''=>$pagination, ''base_url''=>root_url(''blog''))); ?>\n		</div>\n\n	</div>\n	<div class="span4">\n		<div id="blog_sidebar">\n			<?=$this->display_partial(''blog:sidebar'')?>\n		</div>\n	</div>\n</div>', NULL, 1, 1, 'blog:archive', '$this->data[''pagination''] = $posts->paginate($this->request_param(0, 1)-1, 5);\n$this->data[''post_list''] = $posts->find_all();\n\n', '$this->data[''current_menu''] = ''blog'';\n\n', NULL, NULL, 1, 'everyone', NULL, '4ff11fc39a0f21.03879450', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(9, 'Blog Category', 'blog_category', '/blog/category', NULL, NULL, NULL, NULL, '<? if ($category): ?>\n\n	<div class="page-header">\n		<h1><?=$this->page->title_name?></h1>\n		<h4 class="subheader"><?=$this->page->description?></h4>\n	</div>\n\n	<ul class="post_list">\n		<? foreach ($post_list as $post): ?>\n		<li>\n			<h3><a href="<?=root_url(''blog/post/''.$post->url_title) ?>"><?=h($post->title) ?></a></h3>\n			<p class="light">\n				<?=__(''Published by %s on %s.'', array(\n					h($post->author_first_name.'' ''.substr($post->author_last_name, 0, 1).''.''), \n					$post->published_at->format(''%F''))\n				)?>\n				<?=__(''Comments'')?>: <?=$post->approved_comment_num ?>\n			</p>\n			<p><?=h($post->description) ?> <a href="<?=root_url(''blog/post/''.$post->url_title) ?>"><?=__(''Show more'', true)?></a></p>\n		</li>\n	<? endforeach ?>\n	</ul>\n	\n	<div class="view_controls">\n		<? $this->display_partial(''site:pagination'', array(''pagination''=>$pagination, ''base_url''=>root_url(''blog/category/''.$category->url_name))); ?>\n	</div>\n\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that category could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'blog:category', 'if (isset($posts)){\n    $this->data[''pagination''] = $posts->paginate($this->request_param(1, 1)-1, 5);\n    $this->data[''post_list''] = $posts->find_all(); \n  }\n  \n  if ($category)\n    $this->page->title = $category->name;\n\n', '$this->data[''current_menu''] = ''blog'';\n\n', NULL, 8, 1, 'everyone', NULL, '4ff1216dcc8593.05478295', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(10, 'Blog Post', 'blog_post', '/blog/post', NULL, NULL, NULL, NULL, '<? if ($post): ?>\n\n	<div class="page-header">\n		<h1><?=$this->page->title_name?></h1>\n		<h4 class="subheader"><?=$this->page->description?></h4>\n	</div>\n\n	<div class="blog-post-content">\n		<?=$post->content ?>\n	</div>\n\n	<p class="blog-post-intro">\n		<?\n			$category_string = '''';\n			foreach ($post->categories as $index=>$category)\n				$category_string .= ''<a href="''.root_url(''blog/category/''.$category->url_name).''">''.h($category->name).''</a>''.($index < $post->categories->count-1 ? '', '' : ''.'');\n		?>            \n\n		<?=__(''Published by %s on %s in %s'', array(\n			h($post->author_first_name.'' ''.substr($post->author_last_name, 0, 1).''.''),                 \n			$post->published_at->format(''%F''),\n			$category_string\n		))?>\n\n		<?=__(''Comments'')?>: <?=$post->approved_comment_num ?>\n	</p>\n\n	<h4><?=__(''Comments'')?></h4>\n	\n	<? if (!$post->approved_comment_num): ?>\n		<p><?=__(''No comments have been posted yet'')?></p>\n	<? else: ?>\n		<ul class="blog-comment-list">\n		<? foreach ($post->approved_comments as $comment):\n			$site_url_specified = strlen($comment->author_url);\n		?>\n			<li class="<?=$comment->is_owner_comment ? ''owner_comment'' : null  ?>">\n				<p>\n					<?\n						$author_string = '''';\n						if ($site_url_specified)\n							$author_string .= ''<a href="''.$comment->get_url_formatted().''">'';\n\n						$author_string .= h($comment->author_name);\n\n						if ($site_url_specified)\n							$author_string .= ''</a>'';\n					?>\n					<?=__(''Posted by %s on %s'', array(\n						$author_string, \n						$comment->created_at->format(''%F'')\n					))?>\n				</p>\n				<p><?=nl2br(h($comment->content)) ?></p>\n			</li>\n		<? endforeach ?>\n		</ul>\n	<? endif ?>\n	\n	<? if ($post->comments_allowed): ?>\n		<div id="comment_form"><? $this->display_partial(''blog:comment_form'') ?></div>\n	<? else: ?>\n		<p class="comments_closed"><?=__(''Comments are not permitted for this post'')?></p>\n	<? endif ?>\n\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that post could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'blog:post', NULL, '$this->data[''current_menu''] = ''blog'';\n', NULL, 8, 1, NULL, NULL, '4ff1210b6e16a1.73542399', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(11, 'Blog RSS', 'blog_rss', '/blog/rss', NULL, NULL, NULL, NULL, '<?=Blog_Post::get_rss(\n	__(''Blog''),\n	__(''News and updates''),\n	root_url(''rss'', true),\n	root_url(''blog/post'', true),\n	root_url(''blog/category'', true),\n	root_url(''blog'', true),\n	20\n)?>', NULL, 1, 1, 'blog:rss', NULL, NULL, NULL, 8, NULL, NULL, NULL, '4ff1227db3a156.78628378', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(12, 'Contact', 'contact', '/contact', 'Contact us', NULL, 'How to get in touch with us', NULL, '<div class="image-container" style="background:url(''http://dev1.wombids.com/themes/custom/assets/images/about/contact-us.jpg'');">\n	<div class="inner-panel">\n			<h3>SEND US AN EMAIL</h3>\n			<p>We love hearing from you. Whether its questions, suggestions, potential partnerships, or even just to say hi, the best way to get in contact with us is through email.</p>\n	</div>\n</div>\n<div class="container">\n	<div class="row-fluid">\n		<div class="span12">\n			<div class="page-header">\n			    <h1><?=$this->page->title_name?></h1>\n			    <h4 class="subheader"><?=$this->page->description?></h4>\n			</div>\n\n		<div class="row-fluid">\n			<div class="span8">\n				<p>If you have questions or need help with anything regarding our website, please email: <?=mailto_encode(''help@wombids.com'')?></p>\n\n				<p>For non-help related inquiries, like suggestions, press inquiries, partnership opportunities, please direct all messages to: <?=mailto_encode(''general@wombids.com'')?></p>\n\n				<p>If you are a contractor or home service professional and would like to speak to an account executive, please email: <?=mailto_encode(''prosteam@wombids.com'')?></p>\n				</br>\n				</br>\n				<h3>GIVE US A CALL</h3>\n				<p>We would be happy to hear from you over the phone as well. Give us a call toll free at 1(800)574-6501. Our hours of operation are Monday through Saturday 8 am to 5pm (PST).</p>\n			</div>\n			<div class="span4">\n				<div class="well">\n					<h2><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span></h2>\n					<p>\n						Some street, some place <br />\n						1111 Country\n					</p>\n					<h5>Support</h5>\n					<p><?=mailto_encode(''support@email.com'')?></p>\n		\n					<h5>Sales</h5>\n					<p><?=mailto_encode(''sales@email.com'')?></p>\n				</div>\n			</div>\n			\n		</div>\n		</div>\n	</div>\n</div>', NULL, 1, 1, 'Custom', NULL, '$this->data[''current_menu''] = ''contact'';\n\n\n\n\n', NULL, 2, 8, 'everyone', NULL, '4ffbcf5b29fc49.23209793', 'custom', NULL, NULL, 1, '2014-07-08 18:17:58', '2014-08-28 18:14:19'),
(13, 'Dashboard', 'dashboard', '/dashboard', NULL, '<?=$this->css_include(array(\r\n	''@/assets/stylesheets/css/layouts/application_layout/pages/dashboard_page.css'',\r\n)) ?>\r\n<?=$this->js_include(array(\r\n	''@/assets/scripts/js/layouts/application_layout/pages/dashboard_page.js'',\r\n)) ?>', NULL, NULL, '<?\n	$is_newuser = !$this->user || ($this->user && !$this->user->is_requestor && !$this->user->is_provider);\n?>\n\n<? if ($is_newuser): ?>\n	<?=$this->display_partial(''dash:new_user'')?>\n<? else: ?>\n	<div class="row-fluid">\n\n		<div class="span3">\n\n			<div id="p_dash_booking_panel">\n				<?=$this->display_partial(''dash:booking_panel'')?>\n			</div>\n\n			<div id="p_dash_message_panel">\n				<?=$this->display_partial(''dash:message_panel'')?>\n			</div>\n\n			<div id="p_dash_review_panel">\n				<?=$this->display_partial(''dash:review_panel'')?>\n			</div>\n\n		</div>\n\n		<div class="span9">\n			<div id="p_dash_welcome">\n				<?=$this->display_partial(''dash:welcome'')?>\n			</div>\n			<? if ($this->user->is_requestor): ?>\n				<div id="p_requests" class="box">\n					<div class="box-header">\n						<h6><?=__(''My Requests'')?></h6>\n					</div>\n					<div class="box-content offer-request-control">\n						<div id="p_dash_requests">\n							<?=$this->display_partial(''dash:requests'')?>\n						</div>\n					</div>\n					<div class="box-footer">\n						<a href="<?=root_url(''account/requests'')?>">\n							<i class="icon-plus-sign"></i> \n							<?=__(''View my service requests'')?>\n						</a>\n					</div>\n				</div>\n			<? endif ?>\n\n			<? if ($this->user->is_provider): ?>\n				<div id="p_offers" class="box">\n					<div class="box-header">\n						<h6><?=__(''Job Offers'')?></h6>\n					</div>\n					<div class="box-content offer-request-control">\n						<div id="p_dash_offers">\n							<?=$this->display_partial(''dash:offers'')?>\n						</div>\n						<? if (!$jobs->count): ?>\n							<p><?=__(''There are no active service requests'')?></p>\n						<? endif ?>\n					</div>\n					<? if ($jobs->count): ?>\n						<div class="box-footer">\n							<a href="<?=root_url(''account/offers'')?>">\n								<i class="icon-plus-sign"></i> \n								<?=__(''View more job offers'')?>\n							</a>\n						</div>\n					<? endif ?>\n				</div>\n			<? endif ?>\n\n		</div>\n\n	</div>\n<? endif ?>', NULL, 1, 1, 'bluebell:dashboard', NULL, '$this->data[''current_menu''] = ''dash'';\n\n', NULL, 16, 2, 'users', 5, '4f739ac4748779.55959719', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:58', '2014-07-08 18:18:02'),
(14, 'Directory', 'directory', '/directory', 'Provider Directory', '<?=$this->css_include(array(\r\n	''@/assets/stylesheets/css/layouts/application_layout/pages/directory_page.css'',\r\n)) ?>\r\n<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>\r\n<?=$this->js_include(array(\r\n    ''@/assets/scripts/js/layouts/application_layout/pages/directory_page.js'',\r\n)) ?>', NULL, NULL, '<div id="p_directory">\n	<?=$this->display_partial(''directory:''.$mode)?>\n</div>', NULL, 1, 1, 'bluebell:directory', NULL, NULL, NULL, NULL, 2, 'everyone', NULL, '4fed9ac893cb04.02841985', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(15, 'FAQ', 'faq', '/faq', 'Frequently Asked Questions', NULL, NULL, NULL, '<div class="page-header">\n	<h1><?=$this->page->title_name?></h1>\n	<h4 class="subheader"><?=$this->page->description?></h4>\n</div>\n\n<?=content_block(''about_faq'', ''FAQs'')?>', NULL, 1, 1, 'Custom', NULL, '$this->data[''current_menu''] = ''faq'';\n\n\n\n', NULL, 2, 1, 'everyone', NULL, '4ffbd316bc0d05.56706501', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(16, 'Home', 'home', '/', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/home_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/home_page.js'',\n)) ?>', NULL, NULL, '<div class="row-fluid">\n	<div class="span7 columns hide-for-small">\n		<h4 class="separated"><?=__(''Happening on %s now'', c(''site_name''))?></h5>\n		<div id="p_activity_feed">\n			<?=$this->display_partial(''home:activity_feed'')?>\n		</div>\n	</div>\n	<div class="span5 columns">\n\n		<div class="box">\n			<div class="box-header">\n				<h6><?=__(''Reasons to use %s'', c(''site_name''))?></h6>\n			</div>\n			<div class="box-content">\n				<ol class="usage-reasons no-bullet">\n					<li><span class="label round">1</span> Connect with local service providers</li>\n					<li><span class="label round">2</span> Review provider credentials online</li>\n					<li><span class="label round">3</span> Save time and dramatically cut costs</li>\n					<li><span class="label round">4</span> Leverage buying power</li>\n					<li><span class="label round">5</span> Completely FREE to use!</li>\n				</ol>\n			</div>\n		</div>\n\n		<p>\n			<a href="<?=root_url(''provide'')?>" class="btn btn-primary btn-large btn-block">\n				<i class="icon-user"></i>\n				<?=__(''I want to provide my services'', true)?> \n			</a>\n		</p>\n\n		<div class="box popular-categories">\n			<div class="box-header">\n				<h6><?=__(''Popular categories'')?></h6>\n			</div>\n			<div class="box-content">\n				<ul class="block-grid grid-span2">\n					<? foreach ($categories as $category): ?>\n						<li><i class="icon-angle-right"></i> <a href="<?=Bluebell_Directory::category_url($category)?>"><?=$category->name?></a></li>\n					<? endforeach ?>\n				</ul>\n			</div>\n		</div>\n\n		<h4><?=__(''From the blog'')?></h4>\n		<div class="latest_blog">\n			<? if ($blog_post): ?>\n				<div class="title">\n					<i class="icon-pencil"></i> <a href="<?=root_url(''blog/post/''.$blog_post->url_title) ?>"><?=$blog_post->title?> - <span class="date"><?=$blog_post->published_at->format(''%F'')?><span></a>\n				</div>\n				<p><?=h($blog_post->description) ?> <a href="<?=root_url(''blog/post/''.$blog_post->url_title) ?>" class="link-button radius"><?=__(''Read more'', true)?></a></p>\n			<? endif ?>\n		</div>\n	</div>\n</div>', NULL, 1, 1, 'bluebell:home', NULL, '$this->data[''current_menu''] = ''home'';\n\n', NULL, NULL, 3, 'everyone', NULL, '4f670ded115c69.70706303', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-07-08 18:44:44'),
(68, 'How It Works Providers', 'how_it_works_providers', '/how-it-works-providers', 'How it Works', NULL, NULL, NULL, '<div class="page-header">\n    <h1><?=$this->page->title_name?></h1>\n    <h4 class="subheader"><?=$this->page->description?></h4>\n</div>\n<div class="row-fluid">\n	<div class="span12">\n	\n		<div class="dynamic-row step-1">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/comp_g.png">\n			    <div class="imageText">\n			    	<p>Create an account. It''s easy.</p>\n			    </div>\n			</div>\n			<div class="text">\n			    <h3>1. Sign Up</h3>\n			    <p>You can create a profile through womBids or use an existing Facebook or Google account. Don’t worry, it’s extremely easy.  By signing up, you represent that you have authority to claim the business account on behalf of this business. \n			    </p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-2">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/profile_g.png">\n			    <div class="imageText">\n			    	<p>List your skills and talents. Make it pop with photos and more.</p>\n			    </div>\n			</div>\n			<div class="text">\n			    <h3>2. Create a Profile</h3>\n			    <p>Think of this as an opportunity to put your best foot forward. This webpage is solely designed to showcase your business’ portfolio, core competencies, experience level, licenses, insurance, bonding, and references. It also gives your past customers an opportunity to say something nice about you in the form a rating and short review.</p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-3">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/search_g.png">\n			</div>\n			<div class="text">\n			    <h3>3. Search for Jobs</h3>\n			    <p>After you’ve made your profile, you can search through potential projects. Millions homeowners search for contractors every year, here’s where you can find the jobs.  You’ll have an opportunity to talk to the homeowner regarding the project and if it’s a good fit put in a bid for the project.</p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-4">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/arrow_g.png">\n			</div>\n			<div class="text">\n			    <h3>4. Get Hired</h3>\n			    <p>\n				After ironing the details, the homeowner will proceed with accepting a bid, hopefully, it’s your bid! This is homeowner picking a pro to proceed with the project.\n			    </p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-1">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/dollar_g.png">\n			</div>\n			<div class="text">\n			    <h3>5. Get Paid</h3>\n			    <p>\n				Don’t worry - it’s simple and easy. We have payment integration built in and it easily disburses to you. We use a safe and secure third party processor, Braintree (a division of Paypal, Inc. and the same company used by heavyweights such as Airbnb and Uber).\n			    </p>\n			</div>\n		</div>\n			\n	</div>\n</div>', NULL, NULL, 1, 'Custom', NULL, NULL, NULL, NULL, 7, 'everyone', NULL, '53e145a89750a0.81433049', 'custom', NULL, 1, 1, '2014-08-05 20:59:20', '2014-08-06 19:26:50'),
(18, 'Job', 'job', '/job', 'Work Request', '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/job_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/shared/job_quote_behavior.js'',\n	''@/assets/scripts/js/layouts/application_layout/pages/job_page.js'',\n)) ?>', NULL, NULL, '<ul class="breadcrumb">\n	<li><a href="<?=root_url(''dashboard'')?>"><?=__(''My Jobs'')?></a>  <i class="icon-chevron-right divider"></i></li>\n	<li class="current"><?=$this->page->title_name?></li>\n</ul>	\n\n<? if ($request): ?>\n	<div class="page-header">\n		<h1><?=__(''%s Request from %s'', array($request->title, $request->user->username), true)?></h1>\n		<? if ($request->status_code!=Service_Status::status_active): ?>\n			<h4 class="subheader"><?=__(''Sorry, this request has closed for bidding'')?></h4>\n		<? elseif ($request_max_bids = c(''request_max_bids'', ''service'')): ?>\n			<h4 class="subheader"><?=__(''Only the first %s quote(s) are accepted, so quote now!'', $request_max_bids)?></h4>\n		<? endif ?>\n	</div>\n\n	<div class="row-fluid">\n\n		<div class="span4">\n			<div class="well job-details">\n\n				<?= $this->display_partial(''job:details'') ?>\n\n				<div class="row-fluid">\n					<div class="span4 mobile-span3"><p class="detail"><?=__(''Time'')?>:</p></div>\n					<div class="span8 mobile-span9"><p><?=Bluebell_Request::required_by($request)?></p></div>\n				</div>\n\n				<div class="row-fluid">\n					<div class="span4 mobile-span3"><p class="detail"><?=__(''Location'')?>:</p></div>\n					<div class="span8 mobile-span9">\n						<p><?=Bluebell_Request::location($request)?>\n						<? if ($request->location_string): ?>\n							<br />\n							<a href="<?=Location_Map::get_directions($request, $this->user)?>" target="_blank" class="small"><?=__(''Get directions'')?></a></p>\n						<? endif ?>\n					</div>\n				</div>\n\n				<hr />\n\n				<div id="p_ask_question">\n					<?=$this->display_partial(''job:ask_question'', array(''can_ask''=>$request->status_code==Service_Status::status_active))?>\n				</div>\n\n			</div>\n			<? if ($this->user): ?>\n			<div class="job_ignore">\n				<a href="javascript:;" id="link_request_ignore">\n					<i class="icon-ban-circle"></i> \n					<?=__(''Ignore this job offer'')?>\n				</a>\n			</div>\n			<? endif ?>\n\n		</div>\n\n		<div class="span8">\n			<? if ($request->status_code!=Service_Status::status_active): ?>\n				<div class="well align-center"><?=__(''Sorry, this request has closed for bidding'')?></div>\n				<? if ($quote): ?>\n					<div id="p_quote_panel"><?=$this->display_partial(''job:quote_summary'', array(''is_editable''=>false))?></div>\n				<? endif ?>\n			<? elseif (!$quote): ?>\n				<? if ($request->provider_has_link($provider, Service_Request::link_type_banned)): ?>\n					<div class="well align-center"><?=__(''Sorry, you have ignored this request and cannot bid'')?></div>\n				<? else: ?>\n					<div id="p_quote_panel"><?=$this->display_partial(''job:quote_submit'')?></div>\n				<? endif ?>\n			<? else: ?>\n				<div id="p_quote_panel"><?=$this->display_partial(''job:quote_summary'')?></div>\n			<? endif ?>\n		</div>\n\n	</div>\n\n	<div id="popup_request_ignore" class="modal hide fade" tabindex="-1" role="dialog">\n		<div class="modal-header"><h2><?=__(''Ignore this job offer?'')?></h2></div>\n		<div class="modal-body">\n			<p class="lead"><?=__(''This job offer will no longer be available to you. This can not be undone.'')?></p>\n		</div>\n		<div class="modal-footer">\n			<div class="pull-left">\n				<a href="javascript:;" class="popup-close"><?=__(''I changed my mind'')?></a>\n			</div>\n			<div class="pull-right">\n				<a href="javascript:;" class="btn btn-danger btn-large popup-close" id="button_request_ignore" data-request-id="<?=$request->id?>"><?=__(''Confirm ignore'')?></a>\n			</div>\n		</div>\n	</div>\n\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that job could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'service:request', 'if ($request && $this->user && $request->user_id == $this->user->id)\n    Phpr::$response->redirect($request->get_url(''request/manage''));\n\nif ($request && $quote && $quote->status->code == Service_Quote_Status::status_accepted)\n    Phpr::$response->redirect($request->get_url(''job/booking''));\n\n', NULL, NULL, NULL, 2, 'everyone', NULL, '4f867f6cb92374.20689952', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(19, 'Job Booking', 'job_booking', '/job/booking', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/job_booking_page.css'',\n)) ?>\n<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/job_booking_page.js'',\n)) ?>', NULL, NULL, '<? if ($request && $provider): ?>\n\n	<ul class="breadcrumb">\n		<li><a href="<?=root_url(''dashboard'')?>"><?=__(''My Appointments'')?></a> <i class="icon-chevron-right divider"></i></li>\n		<li class="current"><a href="javascript:;"><?=$this->page->title_name?></a></li>\n	</ul>\n	<div class="inner_panel">\n\n		<div class="row-fluid">\n\n			<div class="span8">\n\n				<div class="page-header">\n					<h1><?=__(''Booking with %s'', $opp_user_name)?></h1>\n					<? if (!$is_cancelled): ?>\n						<h4 class="subheader"><?=__("Congrats! You''ve booked this %s appointment", $request->title)?></h4>\n					<? else: ?>\n						<h4 class="subheader"><?=__("Appointment cancelled")?></h4>\n					<? endif ?>\n				</div>\n\n				<? if ($can_rate): ?>\n					<div class="well">\n						<?=form_open(array(''id'' => ''form_rating''))?>\n							<?=form_hidden(''quote_id'', $quote->id)?>\n							<div id="p_job_rating_form"><?=$this->display_partial(''job:rating_form'')?></div>\n						<?=form_close()?>\n					</div>\n				<? endif ?>\n				\n				<div class="well">\n					<div id="p_booking_summary"><?=$this->display_partial(''job:booking_summary'')?></div>\n					<hr />\n					<?=form_open(array(''id'' => ''form_conversation'', ''class''=>''control_conversation''))?>\n						<?=$this->display_partial(''control:conversation'', array(''to_user_id'' => $opp_user_id, ''quote'' => $quote))?>\n					<?=form_close()?>                    \n				</div>\n\n			</div>\n			<div class="span4">\n				<div class="panel job-details radius">\n\n					<?= $this->display_partial(''job:details'') ?>\n					\n					<? if ($questions->count): ?>\n						<hr />\n						<h4><?=__(''Job Questions'')?></h4>\n						<ul id="job_questions">\n							<? foreach ($questions as $question): ?>\n							<li>\n								<? if ($question->answer): ?>\n									<a href="javascript:;" class="question"><?=$question->description?></a>\n									<span class="answer"><?=$question->answer->description?></span>\n								<? else: ?>\n									<span class="question"><?=$question->description?></span>\n								<? endif ?>\n							</li>\n							<? endforeach ?>\n						</ul>\n					<? endif ?>\n\n				</div>\n				<? if (!$is_cancelled): ?>\n					<hr />\n					<div class="job_cancel">\n						<a href="javascript:;" id="link_job_cancel">\n							<i class="icon-remove-sign"></i> <?=__(''Cancel this appointment'')?>\n						</a>\n					</div>\n				<? endif ?>\n			</div>\n			<div class="escrow">\n					<? if($escrow->escrow_status == Payment_Escrow::status_funded && $is_provider):?>\n					This escrow have funded on <?=$escrow->funded_at?>\n						<?if(!$escrow->is_requested):?>\n						<a href="javascript:;" id="request_escrow">\n							<i class="icon-remove-sign"></i> <?=__(''Request release Escrow'')?>\n						</a>\n						<? endif ?>\n					<? elseif(!$is_provider): ?>\n					This escrow have funded on <?=$escrow->funded_at?>\n					<?if($escrow->is_requested):?>\n						<a href="javascript:;" id="request_escrow">\n							<i class="icon-remove-sign"></i> <?=__(''Approve Request'')?>\n						</a>\n						<? endif ?>\n					<? endif ?>\n			</div>\n\n		</div>\n\n	</div>\n\n	<? if ($is_success): ?>\n		<div id="popup_success_message" class="modal hide fade" tabindex="-1" role="dialog">\n			<div class="modal-header">\n				<h2><?=__(''Your %s job has been booked!'', $request->title)?></h2>\n			</div>\n			<div class="modal-body">\n				<p class="lead"><?=__(''Help us grow by sharing this with your friends.'')?></p>\n				<div class="row-fluid">\n					<div class="span6 mobile-span2">\n						<p>\n							<img src="<?=theme_url(''assets/images/social/facebook.png'')?>" alt="Facebook" class="social_icon" />\n							<a href="<?=Social_Share::facebook($request->get_url(''job/booking'',true), __(''Just booked a %s on %s!'', array($request->title, c(''site_name''))), c(''site_name''))?>" target="_blank">\n								<?=__(''Share on Facebook'')?>\n							</a>\n						</p>\n					</div>\n					<div class="span6 mobile-span2">\n						<p>\n							<img src="<?=theme_url(''assets/images/social/twitter.png'')?>" alt="Twitter" class="social_icon" />\n							<a href="<?=Social_Share::twitter($request->get_url(''job/booking'',true), __(''Just booked a %s on %s!'', array($request->title, c(''site_name''))))?>" target="_blank">\n								<?=__(''Share on Twitter'')?>\n							</a>\n						</p>\n					</div>\n				</div>\n			</div>\n			<div class="modal-footer">\n				<a href="javascript:;" class="popup-close"><?=__(''No thanks'')?></a>\n			</div>\n		</div>\n		<script> $(''#popup_success_message'').popup({ autoReveal:true }); </script>\n	<? endif ?>\n\n	<div id="popup_job_cancel" class="modal hide fade" tabindex="-1" role="dialog">\n		<div class="modal-header">\n			<h2><?=__(''Cancel this booking?'')?></h2>\n		</div>\n		<div class="modal-body">\n			<p class="lead"><?=__(''Are you sure you want to cancel this booking? The other party will be notified of your decision.'')?></p>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="popup-close pull-left"><?=__(''I changed my mind'')?></a>\n			<a href="javascript:;" class="btn btn-danger popup-close" id="button_job_cancel" data-quote-id="<?=$quote->id?>"><?=__(''Cancel booking'')?></a>\n		</div>\n	</div>\n		<!--Escrow release-->\n	<?if($is_provider):?>\n	<div id="popup_request_release" class="modal hide fade" tabindex="-1" role="dialog">\n		<div class="modal-header">\n			<h2><?=__(''Request Release?'')?></h2>\n		</div>\n		<div class="modal-body">\n			<p class="lead"><?=__(''Are you sure you want to Request Release? The other party will be notified of your decision.'')?></p>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="popup-close pull-left"><?=__(''I changed my mind'')?></a>\n			<a href="javascript:;" class="btn btn-danger popup-close" id="button_request_release" data-escrow-id="<?=$escrow->id?>"><?=__(''Request Release'')?></a>\n		</div>\n	</div>\n	<? else: ?>\n		<div id="popup_request_release" class="modal hide fade" tabindex="-1" role="dialog">\n			<div class="modal-header">\n				<h2><?=__(''Approve Request?'')?></h2>\n			</div>\n			<div class="modal-body">\n				<p class="lead"><?=__(''Are you sure you want to Approve Request? The amount will be released to other party.'')?></p>\n			</div>\n			<div class="modal-footer">\n				<a href="javascript:;" class="popup-close pull-left"><?=__(''I changed my mind'')?></a>\n				<a href="javascript:;" class="btn btn-danger popup-close" id="button_request_release" data-escrow-id="<?=$escrow->id?>"><?=__(''Approve Request'')?></a>\n			</div>\n		</div>\n	<? endif ?>\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that job booking could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'bluebell:booking', NULL, NULL, NULL, 18, 2, 'everyone', NULL, '4ff8db43a30b25.17811678', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-09-06 22:23:59'),
(20, 'Message', 'message', '/message', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/user_layout/pages/message_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/user_layout/pages/message_page.js'',\n)) ?>', NULL, NULL, '<? if ($messages): ?>\n\n	<ul class="breadcrumb">\n		<li><a href="<?=root_url(''messages'')?>"><?=__(''My Messages'')?></a> <i class="icon-chevron-right divider"></i></li>\n		<li class="active"><?=$this->page->title_name?></li>\n	</ul>\n\n	<div class="page-header">\n		<h1><?=$this->page->title_name?></h1>\n		<? if ($related_request): ?><h4 class="subheader"><?=__(''Related to %s request'', ''<a href="''.$related_request->get_url(''job'').''">''.$related_request->title.''</a>'')?></h4><? endif ?>\n	</div>\n\n	<div id="p_messages_thread">\n		<?=$this->display_partial(''messages:thread'', array(''messages''=>$messages))?>\n	</div>\n\n	<div class="row-fluid">\n		<div class="span10 offset2">\n			<?=form_open(array(''id''=>''form_message_reply''))?>\n				<input type="hidden" name="Message[thread_id]" value="<?= $message->message_thread_id ?>" />\n				\n				<div id="p_messages_reply_form"><?=$this->display_partial(''messages:reply_form'')?></div>\n\n				<div class="form-actions align-right">\n					<?=form_submit(''submit'', __(''Send message''), ''id="button_reply" class="btn btn-primary btn-large"'')?>\n				</div>\n			<?=form_close()?>\n		</div>\n	</div>\n\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that message could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'user:message', '$this->data[''related_request''] = null;\n\nif ($message && $message->master_object_class == ''Service_Quote'')\n{\n    $quote = Service_Quote::create()->find($message->master_object_id);\n    if ($quote)\n    {\n        $this->data[''related_request''] = $quote->request;\n    }\n}\n\n', NULL, NULL, 21, 2, 'users', 5, '4ff61f67e22c48.55996728', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(21, 'Messages', 'messages', '/messages', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/user_layout/pages/messages_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/user_layout/pages/messages_page.js'',\n)) ?>', NULL, NULL, '<div class="page-header">\n	<div class="row-fluid">\n		<div class="span7">\n			<h1><?=$this->page->title_name?></h1>\n		</div>\n		<div class="span5">\n			<div class="main-controls">\n				<div class="row-fluid">\n					<?=form_open(array(''id''=>''form_message_search''))?>\n						<div class="input-append pull-right">\n							<input type="text" name="search" placeholder="<?= __(''Search messages'') ?>" />\n							<?=form_submit(''submit'', __(''Search''), ''class="btn"'')?>\n						</div>\n					<?=form_close()?>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n<div id="p_message_panel" class="all-messages">\n	<?=$this->display_partial(''messages:message_panel'', array(''messages''=>$messages))?>\n</div>', NULL, 1, 1, 'user:messages', NULL, NULL, NULL, NULL, 2, 'users', 5, '4ff61eb57044b5.77417930', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(22, 'Credits', 'payment_credits', '/payment/credits', 'Purchase Credits', NULL, NULL, NULL, '<div class="page-header">\n	<h1><?=$this->page->title_name?></h1>\n	<h4 class="subheader"><?=$this->page->description?></h4>\n</div>\n<?=form_open(array(''id''=>''form_credits'', ''class''=>''''))?>\n	<input type="hidden" name="redirect" value="<?=root_url(''payment/pay/%s'')?>" />\n	<input type="hidden" name="item_name" value="<?=__(''%s Credits'', c(''site_name''))?>" />\n	<table class="table">\n		<thead>\n			<tr>\n				<th><?=__(''Credits'')?></th>\n				<th><?=__(''Price'')?></th>\n			</tr>\n		</thead>\n		<tbody>\n			<? foreach (Payment_Credits::get_table() as $table): ?>\n			<tr>\n				<td>\n					<label class="radio">\n						<input type="radio" name="credits" value="<?=$table->credit?>" />\n						<?= $table->credit ?>\n					</label>\n				</td>\n				<td>\n					<?=format_currency($table->cost)?> \n					<?=__(''(%s/credit)'', format_currency($table->cost/$table->credit))?>\n				</td>\n			</tr>\n			<? endforeach ?>\n		</tbody>\n	</table>\n	<div class="form-actions">\n		<input type="submit" class="btn btn-success btn-large" value="<?=__(''Purchase Credits'')?>" />\n	</div>\n<?=form_close()?>\n<script>\n	$.phpr.form().validate(''#form_credits'').action(''payment:on_buy_credits'');\n</script>', NULL, 1, 1, 'payment:credits', NULL, NULL, NULL, 24, 2, 'everyone', NULL, '5038a9bfdfd847.95633586', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02');
INSERT INTO `cms_pages` (`id`, `name`, `file_name`, `url`, `title`, `head`, `description`, `keywords`, `content`, `sort_order`, `sitemap_visible`, `published`, `action_code`, `code_post`, `code_pre`, `code_ajax`, `parent_id`, `template_id`, `security_id`, `security_page_id`, `unique_id`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(23, 'Invoice', 'payment_invoice', '/payment/invoice', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/payment_layout/pages/payment_pay_page.css'',\n)) ?>', NULL, NULL, '<? if ($invoice): ?>\n	<div class="page-header">\n		<h1><?=$this->page->title_name?></h1>\n		<h4 class="subheader"><?=$this->page->description?></h4>\n	</div>\n\n	<!-- Invoice header -->\n	<div class="row-fluid">\n		<div class="span6">\n\n			<h4><?=__(''Bill to'')?></h4>\n\n			<p>\n				<?= $invoice->billing_name ?><br />\n				<? if ($invoice->billing_street_addr): ?><?= $invoice->billing_street_addr ?><br /><? endif ?>\n				<? if ($invoice->billing_city||$invoice->billing_zip): ?><?= $invoice->billing_city ?> <?= $invoice->billing_zip ?><br /><? endif ?>\n				<? if ($invoice->billing_state): ?><?= $invoice->billing_state->name ?>, <? endif ?>\n				<? if ($invoice->billing_country): ?><?= $invoice->billing_country->name ?><? endif ?>\n				<? if ($invoice->billing_phone): ?><br /><?= $invoice->billing_phone ?><? endif ?>\n			</p>\n		</div>\n		<div class="span6 align-right">\n			<h2><?=__(''Invoice %s'', $invoice->id)?></h2>\n			<p><?=__(''Date: %s'', $invoice->sent_at->to_long_date_format())?></p>\n		</div>\n	</div>\n\n	<!-- Invoice table -->\n	<div id="p_payment_invoice"><? $this->display_partial(''payment:invoice_table'', array(''invoice''=>$invoice)) ?></div>\n\n	<!-- Status -->\n	<? if ($invoice->payment_type): ?>\n		<? if ($invoice->payment_type->has_payment_form() && !$invoice->is_payment_processed()): ?>\n			<div class="align-right">\n				<div class="alert alert-error"><i class="icon-exclamation-sign"></i> <?=__(''This invoice has not been paid'')?></div>\n			</div>\n			<div class="form-actions align-right">\n				<a class="btn btn-success btn-large btn-icon" href="<?= root_url(''payment/pay/''.$invoice->hash) ?>">\n					<i class="icon-ok"></i> \n					<?=__(''Pay this Invoice'')?>\n				</a>\n			</div>\n		<? else: ?>\n			<div class="align-right">\n				<div class="alert alert-success"><i class="icon-ok"></i> <?=__(''This invoice has been paid!'')?></div>\n			</div>\n			<div class="form-actions align-left">\n				<p><a href="<?=root_url(''/dashboard'')?>">Back to Dashboard</a></p>\n			</div>\n			<div class="form-actions align-right">\n				<p><?=__(''Thank you for your business.'')?></p>\n			</div>\n		<? endif ?>\n	<? else: ?>\n		<div class="alert alert-error"><?=__(''Sorry there are no payment gateways available to use!'')?></div>\n	<? endif ?>\n\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that invoice could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'payment:invoice', NULL, NULL, NULL, 24, 2, 'everyone', NULL, '5088c499bf62e8.60104789', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-09-08 00:18:19'),
(24, 'Pay', 'payment_pay', '/payment/pay', 'Submit Payment', '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/payment_layout/pages/payment_pay_page.css'',\n)) ?>\n<?=$this->js_include(array(\n    ''@/assets/scripts/js/layouts/payment_layout/pages/payment_pay_page.js'',\n)) ?>', NULL, NULL, '<? if ($invoice): ?>\n	<div class="page-header">\n		<h1><?=$this->page->title_name?></h1>\n		<h4 class="subheader"><?=$this->page->description?></h4>\n	</div>\n\n	<? if ($invoice->is_payment_processed()): ?>\n		<h4 class="text-success">\n			<span class="ring-badge ring-badge-success"><i class="icon-ok"></i></span> <?=__(''This invoice has already been paid!'')?>\n		</h4>\n		<p>View <a href="<?= $invoice->get_receipt_url() ?>">invoice <?=$invoice->id?></a></p>\n	<? else: ?>\n\n		<!-- Invoice header -->\n		<div class="row-fluid">\n			<div class="span6">\n				<?=form_open(array(''id''=>''form_invoice_details''))?>\n\n					<h4><?=__(''Bill to'')?></h4>\n					<div class="form-field name">\n						<label><?=__(''Your name'')?></label>\n						<div class="edit row-fluid">\n							<div class="span6">\n								<input type="text" name="Invoice[billing_first_name]" value="<?= form_value($invoice, ''billing_first_name'') ?>" placeholder="<?=__(''Your name'')?>" id="invoice_billing_first_name" class="span12" />\n							</div>\n							<div class="span6">\n								<input type="text" name="Invoice[billing_last_name]" value="<?= form_value($invoice, ''billing_last_name'') ?>" placeholder="<?=__(''Your surname'')?>" id="invoice_billing_last_name" class="span12" />\n							</div>\n						</div>\n					</div>        \n\n					<div class="form-field address" id="profile_details_address">\n						<label for="invoice_billing_address"><?=__(''Business address'')?></label>\n						<div class="edit">\n							<input type="text" name="Invoice[billing_street_addr]" value="<?= form_value($invoice, ''billing_street_addr'') ?>" placeholder="<?= __(''Business address'') ?>" class="span12" id="invoice_billing_address" />\n							<div class="row-fluid">\n								<div class="span6">\n									<input type="text" name="Invoice[billing_city]" value="<?= form_value($invoice, ''billing_city'') ?>" placeholder="<?= __(''City'', true) ?>" class="span12" id="invoice_billing_city" />\n								</div>\n								<div class="span6">\n									<input type="text" name="Invoice[billing_zip]" value="<?= form_value($invoice, ''billing_zip'') ?>" placeholder="<?= __(''Zip / Postal Code'', true) ?>" class="span12" id="invoice_billing_zip" />\n								</div>\n							</div>\n							<div class="row-fluid">\n								<div class="span6">\n									<?=form_dropdown(''Invoice[billing_country_id]'', Location_Country::get_name_list(), form_value($invoice, ''billing_country_id''), ''id="invoice_billing_country_id" class="span12"'',  __(''-- Select --'', true))?>\n								</div>\n								<div class="span6">\n									<?=form_dropdown(''Invoice[billing_state_id]'', Location_State::get_name_list(form_value($invoice, ''billing_country_id'')), form_value($invoice, ''billing_state_id''), ''id="invoice_billing_state_id" class="span12"'', __(''-- Select --'', true));?>\n								</div>\n							</div>\n						</div>\n					</div>\n\n					<div class="form-field phone">\n						<label for="provide_phone"><?= __(''Phone number'', true) ?></label>\n						<div class="edit">\n							<input type="text" name="Invoice[billing_phone]" value="<?= form_value($invoice, ''billing_phone'') ?>" placeholder="<?= __(''Phone number'') ?>" class="span12" id="invoice_billing_phone" />\n						</div>\n					</div>                              \n\n				<?=form_close()?>\n			</div>\n			<div class="span6 align-right">\n				<h2><?=__(''Invoice %s'', $invoice->id)?></h2>\n				<p><?=__(''Date: %s'', $invoice->sent_at->to_long_date_format())?></p>\n			</div>\n		</div>\n\n		<!-- Invoice table -->\n		<div id="p_payment_invoice"><? $this->display_partial(''payment:invoice_table'', array(''invoice''=>$invoice)) ?></div>\n\n		<!-- Payment method -->\n		<h5><?=__(''Please choose a payment method'')?></h5>\n\n		<?=form_open(array(''class''=>''''))?>\n			<? \n				$payment_types = Payment_Type::list_applicable($invoice->country_id); \n			?>\n			<? foreach ($payment_types as $type): ?>\n				<label for="<?=''type''.$type->id?>" class="radio">\n					<input type="radio" <?=($type->id == $invoice->payment_type_id)?''checked="checked"'':''''?> \n						value="<?=$type->id?>" name="payment_type" id="<?=''type''.$type->id?>" \n						onchange="$(this).phpr().post(''payment:on_update_payment_type'').update(''#p_payment_form'', ''payment:form'').send()" />\n					<?= h($type->name) ?>\n				</label>\n			<? endforeach ?>\n		<?=form_close()?>\n\n		<!-- Payment form -->\n		<div id="p_payment_form"><?=$this->display_partial(''payment:form'') ?></div>\n\n	<? endif ?>\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that payment could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'payment:pay', NULL, NULL, NULL, NULL, 2, 'everyone', NULL, '5024bfdf61e221.05869600', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(25, 'Privacy', 'privacy', '/privacy', 'Privacy Policy', NULL, 'Website privacy policy', NULL, '<div class="page-header">\n	<h1><?=$this->page->title_name?></h1>\n	<h4 class="subheader">Website privacy policy</h4>\n</div>\n\n<?=content_block(''page_privacy_content'', ''Privacy page content'') ?>', NULL, 1, 1, 'Custom', NULL, '$this->data[''current_menu''] = ''privacy'';\n\n\n\n\n', NULL, 2, 1, 'everyone', NULL, '4f7942db0ea568.73803073', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(26, 'Profile', 'profile', '/profile', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/profile_page.css'',\n)) ?>\n<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/profile_page.js'',\n)) ?>', NULL, NULL, '<? if ($provider): ?>\n<!--ul class="breadcrumb">\n	<li><a href="<?=root_url(''directory'')?>"><?=__(''Directory'',true)?></a></li>\n	<?  \n	$dir_url = root_url(''directory/a'');\n	?>\n	<li><a href="<?=$dir_url.=''/''.strtolower($provider->country->code)?>"><?=$provider->country->name?></a> <i class="icon-chevron-right divider"></i></li>\n	<li><a href="<?=$dir_url.=''/''.strtolower($provider->state->code)?>"><?=$provider->state->name?></a> <i class="icon-chevron-right divider"></i></li>\n	<li><a href="<?=$dir_url.=''/''.Phpr_Inflector::slugify($provider->city)?>"><?=$provider->city?></a> <i class="icon-chevron-right divider"></i></li>\n	<li><a href="<?=$dir_url.=''/''.$provider->categories->first->url_name?>"><?=$provider->role_name?></a> <i class="icon-chevron-right divider"></i></li>\n	<li class="active"><?= $provider->business_name ?></li>\n</ul-->\n<div class="page-header">\n	<h1><?=$this->page->title_name?></h1>\n	<h4 class="subheader"><?=$this->page->description?></h4>\n</div>\n<div class="row-fluid">\n	<div class="span12">\n	<div class="span8 secLeft">\n	  \n		<? if ($portfolio): ?>\n			<div id="portfolio-images" class="carousel slide" data-ride="carousel">\n				<!-- Carousel items -->\n				<div class="carousel-inner">\n					<?\n						$count = 0;\n					?>\n					<? foreach ($portfolio as $item): ?>\n						<div class="<?=$count==1?''active'':''''?> item">\n							<img src="<?=$item->image?>" alt="" />\n						</div>\n					<? $count++; endforeach ?>\n				</div>\n				\n				<div class="controlls-sec">\n					<ul class="carousel-indicators">\n						<?\n							$count = 0;\n						?>\n						<? foreach ($portfolio as $item): ?>\n							<li data-target="#portfolio-images" data-slide-to="<?=$count?>" class="<?=$count==1?''active'':''''?>"><img src="<?=$item->image?>" alt="" /></li>\n						<? $count++; endforeach ?>\n					</ul>\n				</div>\n			\n			</div>\n			\n		<? endif ?>\n\n		<h4 class="hborder title"><?=__(''About %s'', $provider->business_name)?></h4>\n		<div class="businessDesc"><?= $provider->description_html ?></div>\n		\n	\n		\n		<div class="reviews_mvs row-fluid">\n			<div class="span12">\n				<h4 class="hborder title"><?=__(''leave a review for %s'', $provider->business_name)?></h4>\n				<?=form_open(array(''id'' => ''form_rating''))?>\n				<input type="hidden" name="Provider[id]" value="<?= $provider->id ?>" />\n				<? if(isset($jobs->first)){ ?>\n				<input type="hidden" name="job" value="<?= $jobs->first->id;?>" /><? } ?>\n				<div class="span4 reset">\n					<div class="rate-box">\n						<div class="round-li">1</div>\n						<span class="rate">Rate This Contractor</span>\n						<div class="controls">\n							<div class="rating-selector" id="rating_rating">\n								<?=form_dropdown(''Rating[rating]'', array(0,1,2,3,4,5))?>\n							</div>\n						</div>\n					</div>\n					<br />\n					<div class="rate-dis">\n						<p>Click stars to rate</p>\n					</div>	\n				</div>\n				<script type="text/javascript"> jQuery(document).ready(function($) { $(''#rating_rating'').starRating(); }); </script>\n				<div class="span8">\n				<div class="rate-box">\n					<div class="round-li">2</div>\n					<span class="rate">Provide a detailed review of this contrator</span>\n					<textarea class="span10 reviewBoxTextarea" name="Rating[comment]" placeholder="What was you experience?"></textarea>\n					<button id= "button_rating_provider" data-provider-id="<?=$provider->id?>" class="btn btn-large btn-primary btn-icon" type="submit" name="submit">\n					Submit Your Review\n				</button>\n				</div>\n				<?=form_close()?>\n			</div></div>\n		</div>\n\n	</div>\n	<div class="span4">\n		<div class="generalinfo">\n			<h4 class="hborder title textright"><?=__(''General Information'')?></h4>\n			<div id="p_control_badge" class="hide-for-small">\n				<? if ($user): ?>\n					<?//= $user->fetched[''name''] ?>\n				<? endif ?>\n				<?=$this->display_partial(''control:badge'', array(''provider''=>$provider))?>\n			</div>\n		</div>\n\n		<div class="call_to_action">\n			<a href="javascript:;" class="btn btn-primary btn-large btn-block" id="button_contact_provider"><?=__(''Contact this provider'')?></a>\n		</div>\n		\n		<div class="contactinfo">\n			<h4 class="hborder2 title textright"><?=__(''Contact Information'')?></h4>\n			<div class="ct-div info-mobile"><span class="text-ct"><?= $provider->mobile ?></span></div>\n			<div class="ct-div info-phone"><span class="text-ct"><?= $provider->phone ?></span></div>\n			<div class="ct-div info-email"><span class="text-ct"><?//= $provider->email ?></span></div>\n			<div class="ct-div info-web"><span class="text-ct"><?= $provider->url ?></span></div>\n			<div class="map-ct">\n				<iframe width="350" height="200" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDr09FTrMAuDL0IkyP70qx8ffsbf01uGPg&q=<?= urlencode($provider->address_string); ?>"></iframe>\n			</div>\n			<div class="address">\n				<p><?= $provider->address_string ?></p>\n			</div>\n		</div>\n		\n		\n		\n		\n		<div class="ratings">\n		<p class="overall star-rating">\n				<i class="rating-<?=$provider->rating*10?>"></i> \n				<span class="hborder2 title pull-right"><?=__(''Ratings'')?></span>\n			</p>	\n		</div>\n		<p class="rate-comment"><?=__(''Avg rating '')?><?=$provider->rating?>/5 Stars</p>\n			<? if ($provider->rating_num > 0): ?>\n			<?=$this->display_partial(''provide:ratings_small'', array(\n				''ratings''=>$ratings, \n				''base_url''=>$provider->get_url(''profile'')\n			))?>\n		<? endif ?>\n		<button name="submit" type="submit" class="btn btn-large btn-primary pull-right btn-icon" id="button_rating_full">\n					Show All\n				</button>\n	</div>\n	</div>\n</div>\n<hr />\n<!-- rating pop up -->\n<div id="popup_rating_provider" class="modal hide fade" tabindex="-1" role="dialog">\n	<?=form_open(array(''id''=>''form_rating_pop''))?>\n		<?=form_hidden(''invite_providers[]'', $provider->id)?>\n		<div class="modal-header">\n			<h3><?=__(''Get a Quote from %s'', $provider->business_name)?></h3>\n		</div>\n		<div class="modal-body">\n			\n				<?=__(''Send this to other %s in %s and let them provide quotes as well'', array(Phpr_Inflector::pluralize($provider->role_name), $provider->location_string))?>\n			</label>\n		</div>\n		<div class="modal-footer">\n			<?=form_submit(''submit'', __(''Send request''), ''class="btn btn-primary" id="button_submit_contact_provider" data-provider-id="''.$provider->id.''"'')?>\n		</div>\n	<?=form_close()?>\n</div>\n<!-- contact pop up -->\n<div id="popup_contact_provider" class="modal hide fade" tabindex="-1" role="dialog">\n	<?=form_open(array(''id''=>''form_request''))?>\n		<?=form_hidden(''invite_providers[]'', $provider->id)?>\n		<div class="modal-header">\n			<h3><?=__(''Get a Quote from %s'', $provider->business_name)?></h3>\n		</div>\n		<div class="modal-body">\n			<?=$this->display_partial(''request:quick_form'', array(''role''=>$provider->categories->first))?>\n			<label class="checkbox open_request" for="open_request">\n				<?=form_checkbox(''open_request'', true, true, ''id="open_request"'')?> \n				<?=__(''Send this to other %s in %s and let them provide quotes as well'', array(Phpr_Inflector::pluralize($provider->role_name), $provider->location_string))?>\n			</label>\n		</div>\n		<div class="modal-footer">\n			<?=form_submit(''submit'', __(''Send request''), ''class="btn btn-primary" id="button_submit_contact_provider" data-provider-id="''.$provider->id.''"'')?>\n		</div>\n	<?=form_close()?>\n</div>\n<!-- full rating-->\n<div id="popup_full_ratings" class="modal hide fade" tabindex="-1" role="dialog">\n	\n		<div class="modal-header">\n			<h3><?=__(''Ratings'')?></h3>\n		</div>\n		<div class="modal-body">\n			<? if ($ratings): ?>\n				\n			<?endif?>\n		</div>\n		<div class="modal-footer">\n			<?=form_submit(''submit'', __(''Close''), ''class="btn btn-primary" id="button_submit_contact_provider" data-provider-id="''.$provider->id.''"'')?>\n		</div>\n	\n</div>\n<div id="popup_contact_provider_success" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-body">\n		<?=global_content_block(''directory_request_submit'', ''Directory request submitted'')?>\n	</div>\n	<div class="modal-footer">\n		<?=form_button(''close'', __(''Close'', true), ''class="btn popup-close"'')?>\n	</div>\n</div>\n\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that profile could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'bluebell:profile', '$this->data[''ratings''] = $ratings = $provider->find_ratings();\n$this->data[''pagination''] = $ratings->paginate($this->request_param(1, 1)-1, 3);\n\n\n\n\n\n\n\n', NULL, NULL, 14, 2, 'everyone', NULL, '4fed9a444bcc33.87482899', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-09-08 11:29:21'),
(27, 'Provide', 'provide', '/provide', 'Provide Services', '<?=$this->css_include(array(\r\n	''@/assets/stylesheets/css/layouts/application_layout/pages/provide_page.css'',\r\n)) ?>\r\n<?=$this->js_include(array(\r\n    ''@/assets/scripts/js/layouts/application_layout/pages/provide_page.js'',\r\n)) ?>', NULL, NULL, '<?\n	$selected_tab = $this->request_param(0, ''provide-services'');\n?>\n\n\n<div class="tabbable">\n\n	<div class="row-fluid">\n		<div class="span3">\n			<ul class="nav nav-pills nav-stacked">\n				<li class="<?=$selected_tab==''why-us''?''active'':''''?>"><a data-toggle="tab" href="#why-us"><?=__(''Why %s'', c(''site_name''))?></a></li>\n				<li class="<?=$selected_tab==''go-mobile''?''active'':''''?>"><a data-toggle="tab" href="#go-mobile"><?=__(''Go mobile'')?></a></li>\n				<li class="<?=$selected_tab==''how-it-works''?''active'':''''?>"><a data-toggle="tab" href="#how-it-works"><?=__(''How it works'')?></a></li>\n				<li class="<?=$selected_tab==''provide-services''?''active'':''''?>"><a data-toggle="tab" href="#provide-services"><?=__(''Provide Services'')?></a></li>\n			</ul>\n		</div>\n		<div class="span9">\n\n			<div class="tab-content">\n				<div class="tab-pane <?=$selected_tab==''why-us''?''active'':''''?>" id="why-us">\n					<div class="page-header">\n						<h1><?=__(''Why %s'', c(''site_name''))?></h1>\n						<h4 class="subheader"><?=__(''Find the jobs you want and do it your way!'')?></h4>\n					</div>\n					<?=content_block(''provider_why_us'', ''Why use us'')?>\n				</div>\n				<div class="tab-pane <?=$selected_tab==''go-mobile''?''active'':''''?>" id="go-mobile">\n					<div class="page-header">\n						<h1><?=__(''Go mobile'')?></h1>\n						<h4 class="subheader"><?=__(''Find jobs on the go with your mobile'')?></h4>\n					</div>\n					<?=content_block(''provider_why_us'', ''Why use us'')?>\n				</div>\n				<div class="tab-pane <?=$selected_tab==''how-it-works''?''active'':''''?>" id="how-it-works">\n					<div class="page-header">\n						<h1><?=__(''How it works'')?></h1>\n						<h4 class="subheader"><?=__(''%s will send customer requests to local businesses, companies and professionals who can do the job at the location and time they request'', c(''site_name''))?></h4>\n					</div>\n					<?=content_block(''provider_how_it_works'', ''How it works'')?>\n				</div>\n				<div class="tab-pane <?=$selected_tab==''provide-services''?''active'':''''?>" id="provide-services">\n					<div class="page-header">\n						<h1><?=__(''Register as a Provider'', c(''site_name''))?></h1>\n						<h4 class="subheader"><?=__(''Recieve free leads from customers who are looking for your services'')?></h4>\n					</div>\n\n					<? if (!$this->user): ?>\n\n						<?=form_open(array(''id'' => ''form_register'', ''class''=>''custom nice''))?>\n							<fieldset class="form-horizontal">\n								<?=form_hidden(''redirect'', root_url(''provide/create''))?>\n								<?=$this->display_partial(''site:register_form'')?>\n								<div class="controls">\n									<span class="help-block">\n										<?=__(''By pressing create account you agree to the %s.'', array(\n											''<a href="''.root_url(''terms'').''">''.__(''terms of service'', true).''</a>''\n										))?>\n									</span>\n								</div>\n								<div class="form-actions">\n									<button name="submit" type="submit" class="btn btn-large btn-success btn-icon">\n										<?= __(''Create Service Profile'') ?> <i class="icon-ok"></i>\n									</button>\n								</div>\n							</fieldset>\n						<?=form_close()?>\n\n					<? else: ?>\n						<a href="<?=root_url(''provide/create'')?>" class="btn btn-large btn-primary"><?=__(''Create Service Profile'')?></a>\n					<? endif?>\n\n				</div>\n			</div>\n\n		</div>\n	</div>\n</div>', NULL, 1, 1, 'Custom', NULL, '$this->data[''current_menu''] = ''provide'';\n\n', NULL, NULL, 2, 'everyone', NULL, '4f794284837740.63490800', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(28, 'Provide Create', 'provide_create', '/provide/create', 'Create Service Provider', '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/provide_create_page.css'',\n)) ?>\n<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>\n<?=$this->js_include(array(\n    ''@/assets/scripts/js/layouts/application_layout/pages/provide_create_page.js'',\n)) ?>', NULL, NULL, '<div class="row-fluid">\n	<div class="span9">\n		<?=form_open(array(''id'' => ''form_provide_create'', ''class''=>''nice''))?>\n\n			<div class="box">\n				<div class="box-header">\n					<h3><?=__(''Create your profile'')?></h3>\n				</div>\n				<div class="box-content">\n\n					<div class="row-fluid">\n						<div class="span8">\n							<div class="well">\n								\n								<div id="p_profile_form">\n									<?=$this->display_partial(''provide:profile_form'')?>\n								</div>\n\n							</div>\n						</div>\n					</div>\n\n					<h4><?=__(''Your hours'')?> <a href="javascript:;" id="link_work_hours" data-toggle-text="<?=__(''Show less options'')?>"><?=__(''Show more options'')?></a></h4>\n					<div id="p_work_hours_form">\n						<?=$this->display_partial(''provide:work_hours_form'')?>\n					</div>\n\n					<hr />\n\n					<h4><?=__(''Your work area'')?></h4>\n					<div id="p_work_radius_form">\n						<?=$this->display_partial(''provide:work_radius_form'')?>\n					</div>\n\n					<hr />\n\n					<p><label for="agree_terms" class="checkbox">\n						<input id="agree_terms" type="checkbox"> <?=__(''I agree to the Terms and Conditions'')?>\n					</label></p>\n\n				</div>\n			</div>\n\n			<div class="form-actions align-center">\n				<?=form_submit(''save'', __(''Save profile''), ''class="btn btn-primary btn-large"'')?>\n			</div>\n\n		<?=form_close()?>		\n\n	</div>\n	<div class="span3">\n		<?=content_block(''provider_create_sidebar'', ''Sidebar content'') ?>\n		<?=Social_Facebook::facepile()?>\n	</div>\n</div>', NULL, 1, 1, 'service:create_provider', NULL, '$this->data[''current_menu''] = ''provide'';\n\n\n\n\n\n\n\n', NULL, 27, 2, 'everyone', NULL, '4f8d58e6c5acf9.32683598', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(29, 'Provide Manage', 'provide_manage', '/provide/manage', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/provide_manage_page.css'',\n)) ?>\n<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/provide_manage_page.js'',\n)) ?>', NULL, NULL, '<? if ($provider): ?>\n<div class="row-fluid">\n	<div class="span9">\n		<a href="<?=root_url(''provide/create'')?>" class="pull-right btn btn-primary">\n			<i class="icon-plus"></i> \n			<?=__(''Create another profile'')?>\n		</a>\n\n		<div class="page-header">\n			<h1><?=__(''%s Profile'', $provider->role_name)?> <small><a href="javascript:;" id="link_delete_profile"><?=__(''delete this profile'')?></a></small></h1>\n			<h4 class="subheader"><?=__(''Below is how %s customers will see your profile. Research shows that service providers who fill out their profiles entirely and more likely to win bids.'', c(''site_name''))?></h4>\n		</div>\n\n		<div class="box">\n			<div class="box-header">\n				<h3><?=__(''Manage your profile'')?></h3>\n			</div>\n			<div class="box-content">\n\n				<div class="row-fluid">\n					<div class="span8">\n						<div class="well">\n							<div id="p_profile_form">\n								<?=form_open(array(''id'' => ''form_provide_details'', ''class''=>''nice''))?>\n									<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n									<?=$this->display_partial(''provide:profile_form'')?>\n								<?=form_close()?>\n							</div>\n						</div>\n					</div>\n				</div>\n				\n				<!--add Membership-->\n				<div class="row-fluid">\n					<div class="span8 membershipPanel">\n						<div class="well">\n							<div id="p_profile_form">\n								<?=$this->display_partial(''provide:membership_form'')?>\n							</div>\n						</div>\n					</div>\n				</div>\n				<!--end Membership-->\n				\n				<hr />\n				<div class="row-fluid">\n					<div class="span12">\n						<a href="javascript:;" id="button_profile_portfolio" class="button_profile_portfolio pull-right btn btn-small"><?=__(''Add / Remove Photos'')?></a>\n						<h4><?=__(''Portfolio'')?></h4>\n						<div id="p_provide_portfolio"><?=$this->display_partial(''provide:portfolio'', array(''images''=>$provider->get_portfolio(), ''is_manage''=>true))?></div>\n					</div>\n				</div>\n				<hr />\n				<div class="row-fluid">\n					<div class="span12">\n						<h4><?=__(''About %s'', $provider->business_name)?></h4>\n						<div id="p_provide_description"><?=$this->display_partial(''provide:description'')?></div>\n					</div>\n				</div>\n				<? /* @todo \n				<hr />\n				<div class="row-fluid">\n					<div class="span12">\n						<h4><?=__(''Services'')?></h4>\n					</div>\n				</div>\n				*/ ?>\n				<hr />\n				<div class="row-fluid">\n					<div class="span12">\n						<a href="javascript:;" id="button_profile_testimonials" class="pull-right btn btn-small"><?=__(''Get testimonials'')?></a>\n						<h4><?=__(''Testimonials'')?></h4>\n						<? if ($provider->testimonials->count): ?>\n							<div id="p_provide_testimonials"><?=$this->display_partial(''provide:testimonials'')?></div>\n						<? else: ?>\n							<div class="well testimonials">\n								<div class="row-fluid"><?=content_block(''provider_testimonials'', ''Why get testimonials?'')?></div>\n							</div>\n						<? endif ?>\n					</div>\n				</div>\n\n			</div>\n		</div>\n		<div class="form-actions align-center">\n			<?=form_button(''done'', __(''Done'', true), ''class="btn btn-primary btn-large" id="button_done"'')?>\n		</div>\n	</div>\n	<div class="span3">\n\n		<div class="well">\n			<h4><?=__(''Modify'')?></h4>\n			<ul class="square">\n				<li><a href="javascript:;" id="link_work_hours"><?=__(''My hours'')?></a></li>\n				<li><a href="javascript:;" id="link_work_area"><?=__(''My work area'')?></a></li>\n				<li><a href="<?=root_url(''account/notifications'')?>"><?=__(''My notifications'')?></a></li>\n			</ul>\n		</div>\n\n		<div id="p_dash_top_provider_panel"><?=$this->display_partial(''dash:top_provider_panel'')?></div>\n\n		<a href="<?=$provider->get_url(''profile'')?>" class="btn btn-block"><?=__(''View my profile page'')?></a>\n\n	</div>\n</div>\n\n<!-- Profile Complete -->\n<div id="popup_profile_success" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-body">\n		<?=content_block(''provider_success'', ''Profile has been set up'', array(''role_name''=>$provider->role_name, ''site_name''=>c(''site_name'')))?>\n	</div>\n	<div class="modal-footer">\n		<a href="javascript:;" class="btn popup-close" onclick=""><?=__(''Close'',true)?></a>\n	</div>\n</div>\n\n<!-- Delete Profile -->\n<div id="popup_profile_delete" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-header"><h2><?=__(''Delete your %s profile'', $provider->role_name)?></h2></div>\n	<div class="modal-body">\n		<p class="lead"><?=__(''Are you sure you want to delete this profile? This action cannot be reversed.'')?></p>\n	</div>\n	<div class="modal-footer">\n		<a href="javascript:;" class="popup-close pull-left"><?=__(''I changed my mind'')?></a>\n		<a href="javascript:;" class="btn btn-danger btn-large popup-close" id="button_profile_delete" data-provider-id="<?=$provider->id?>"><?=__(''Delete this profile'')?></a>\n	</div>\n</div>\n\n\n<!-- Membership Profile -->\n<div id="popup_profile_membership" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-header"><h2><?=__(''Update your %s membership'', $provider->role_name)?></h2></div>\n	<div class="modal-body">\n	<?=form_open(array(''id'' => ''form_provide_membership'', ''class''=>''nice''))?>\n	<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n	<input type="hidden" name="redirect" value="<?=root_url(''payment/pay/%s'')?>" />\n		<?\n		$has_membership = ($provider&&$provider->plan_id);\n		\n		$pops = Service_Plan::get_object_list();\n		foreach($pops as $pop){ ?>\n			<div class="membershipSinglePanel mBoxLight">\n				<div class="mBoxDark">\n					<div class="mBoxTitle"><? echo $pop->name; ?></div>\n					<div class="mBoxPrice"><span>$</span><? echo $pop->price + 0; ?></div>\n				</div>\n				<div class="mBoxSubDesc"><? if($pop->credits == 2500) { echo ''Unlimited''; } else { echo $pop->credits; } ?> Bid Intros per Month</div>\n				<div class="mBoxDesc"></div>\n				<label class="membershipLinks">\n					select plan\n					<? echo form_radio(''Provider[plan_id]'',$pop->id , ($pop->id == form_value($provider, ''plan_id'') ? true : false)); ?>\n				</label>\n			</div>\n		<? } ?>\n	</div>\n	<div class="modal-footer">\n		<a href="javascript:;" class="popup-close pull-left"><?=__(''I changed my mind'')?></a>\n		<!--a href="javascript:;" class="btn btn-danger btn-large popup-close" id="button_profile_membership" data-provider-id="<?=$provider->id?>"><?=__(''Update Membership'')?></a-->\n		<input type="submit" class="btn btn-success btn-large" value="Done" name="done">\n	</div>\n	<?=form_close()?>\n</div>\n\n<!-- Portfolio -->\n<div id="popup_profile_portfolio" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-header"><h2><?=__(''Add photos or prior work'')?></h2></div>\n	<?=form_open(array(''id'' => ''form_provide_portfolio'', ''class''=>''nice''))?>\n		<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n		<div class="modal-body">\n			<div id="p_provide_portfolio_form"><?=$this->display_partial(''provide:portfolio_form'')?></div>\n		</div>\n		<div class="modal-footer">\n			<?=form_submit(''done'', __(''Done'', true), ''class="btn btn-primary btn-large popup-close" onclick="return false" id="button_profile_portfolio_close"'')?>\n		</div>\n	<?=form_close()?>\n</div>\n\n<!-- Profile Description -->\n<div id="popup_profile_description" class="modal hide fade" tabindex="-1" role="dialog">\n	<?=form_open(array(''id'' => ''form_provide_decription'', ''class''=>''nice''))?>\n		<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n		<div class="modal-header"><h2><?=__(''About'')?></h2></div>\n		<div class="modal-body">\n			<div id="p_provide_description_form"><?=$this->display_partial(''provide:description_form'')?></div>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="popup-close pull-left"><?=__(''Cancel'', true)?></a>\n			<?=form_submit(''done'', __(''Done'', true), ''class="btn btn-success btn-large" id="button_profile_description_submit"'')?>\n		</div>\n	<?=form_close()?>\n</div>\n\n<!-- Testimonials -->\n<div id="popup_profile_testimonials" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-header"><h2><?=__(''Get Testimonials'')?></h2></div>\n	<?=form_open(array(''id'' => ''form_provide_testimonials'', ''class''=>''nice''))?>\n		<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n		<div class="modal-body">\n			<p><?=__(''Message will be sent from %s and is not a bulk email, the recipient will only see their email address.'', $this->user->email)?></p>\n			<fieldset>\n				<div id="p_profile_testimonials"><?=$this->display_partial(''provide:testimonials_form'')?></div>\n			</fieldset>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="popup-close pull-left"><?=__(''Cancel'', true)?></a>\n			<?=form_submit(''send'', __(''Send'', true), ''class="btn btn-success btn-large" id="button_profile_testimonials_submit"'')?>\n		</div>\n	<?=form_close()?>\n\n	<div id="profile_testimonials_success" style="display:none">\n		<div class="modal-body">\n			<p class="lead"><?=__(''Your request has been sent successfully'')?></p>\n			<p><?=__(''We will send you an email telling you when someone submits a testimonial about you.'')?></p>\n		</div>\n		<div class="modal-footer">\n			<?=form_button(''close'', __(''Close'', true), ''class="large btn btn-primary popup-close"'')?>\n		</div>\n	</div>\n</div>\n\n<!-- Work Radius -->\n<div id="popup_work_radius" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-header"><h2><?=__(''My work area'')?></h2></div>\n	<?=form_open(array(''id'' => ''form_work_radius'', ''class''=>''nice''))?>\n		<input type="hidden" name="Provider[zip]" value="<?= $provider->zip ?>" />\n		<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n		<div class="modal-body">\n			<div id="p_work_radius_form"><?=$this->display_partial(''provide:work_radius_form'')?></div>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="popup-close pull-left"><?=__(''Cancel'', true)?></a>\n			<?=form_submit(''done'', __(''Done'', true), ''class="btn btn-success btn-large"'')?>\n		</div>\n	<?=form_close()?>\n</div>\n\n<!-- Work Hours -->\n<div id="popup_work_hours" class="modal hide fade" tabindex="-1" role="dialog">\n	<div class="modal-header"><h2><?=__(''My work hours'')?></h2></div>\n	<?=form_open(array(''id'' => ''form_work_hours'', ''class''=>''nice''))?>\n		<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n		<div class="modal-body">\n			<div id="p_work_hours_form"><?=$this->display_partial(''provide:work_hours_form'')?></div>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="popup-close pull-left"><?=__(''Cancel'', true)?></a>\n			<?=form_submit(''done'', __(''Done'', true), ''class="btn btn-success btn-large"'')?>\n		</div>\n	<?=form_close()?>\n</div>\n\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that profile could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'service:update_provider', NULL, '$this->data[''current_menu''] = ''provide'';\n\n\n\n\n\n\n\n', NULL, 27, 2, 'users', 5, '4f9a7204e0d7c2.67132592', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-08-22 22:27:51'),
(30, 'Provide Profiles', 'provide_profiles', '/provide/profiles', NULL, '<?=$this->css_include(array(\r\n	''@/assets/stylesheets/css/layouts/application_layout/pages/provide_profiles_page.css'',\r\n)) ?>', NULL, NULL, '<div class="row-fluid">\n	<div class="span3 hide-for-small">\n		<h4><?=__(''Profile summaries'')?></h4>\n\n		<ul class="nav nav-tabs nav-stacked">\n			<? foreach ($profiles as $profile): ?>\n				<li><a href="<?=root_url(''provide/manage/''.$profile->id)?>"><?=$profile->role_name?></a></li>\n			<? endforeach ?>\n		</ul>\n\n		<a href="<?=root_url(''provide/create'')?>" class="btn btn-primary btn-block">\n			<i class="icon-plus"></i>\n			<?=__(''Create Skill Profile'')?>\n		</a>\n\n		<ul class="quick-links">\n			<li><a href="<?=root_url(''account/notifications'')?>"><?=__(''Notification preferences'')?></a></li>\n			<li><a href="<?=root_url(''account/work_history'')?>"><?=__(''Work history'')?></a></li>\n		</ul>\n	</div>\n	<div class="span9">\n		<div class="row-fluid">\n			<div class="span8">\n\n				<div class="provide-panel">\n					<? foreach ($profiles as $index=>$profile): ?>\n						<? if ($index != 0): ?><hr /><? endif ?>\n						<?=$this->display_partial(''dash:provide_panel'', array(''provider_profile''=>$profile))?>\n					<? endforeach?>\n				</div>\n\n			</div>\n\n			<div class="span4">\n				<div id="p_dash_booking_panel">\n					<?=$this->display_partial(''dash:booking_panel'')?>\n				</div>\n\n				<div id="p_dash_message_panel">\n					<?=$this->display_partial(''dash:message_panel'')?>\n				</div>\n			</div>\n\n		</div>\n	</div>\n</div>', NULL, 1, NULL, 'service:update_provider', NULL, '$this->data[''current_menu''] = ''provide'';\n\n\n\n\n\n\n', NULL, 27, 2, 'everyone', NULL, '4fe7ea880ed184.04896682', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(31, 'Provide Testimonial', 'provide_testimonial', '/provide/testimonial', NULL, '<?=$this->js_include(array(\r\n    ''@/assets/scripts/js/layouts/application_layout/pages/provide_testimonials_page.js'',\r\n)) ?>', NULL, NULL, '<? if ($provider): ?>\n	<div class="page-header">\n		<h1><?=$this->page->title_name?></h1>\n		<h4 class="subheader"><?=__("Write a testimonial for %s''s %s service", array($provider->business_name, $provider->role_name))?></h4>\n	</div>\n	<div class="row-fluid">\n		<div class="span6">\n			<div id="p_provide_testimonial_write_form">\n				<?=form_open(array(''id'' => ''form_provide_testimonial_write'', ''class''=>''nice''))?>			\n					<input type="hidden" name="provider_id" value="<?= $provider->id ?>" />\n					<input type="hidden" name="hash" value="<?= $testimonial->hash ?>" />\n					<?=$this->display_partial(''provide:testimonial_write_form'')?>\n					<?=form_submit(''submit'', __(''Submit testimonial''), ''class="btn btn-primary btn-large"'')?>\n				<?=form_close()?>\n			</div>\n			<div id="provide_testimonial_write_success" style="display:none">\n				<div class="alert alert-success"><?=__(''Thank you for submitting a testimonial for %s.'', $provider->business_name)?></div>\n			</div>\n		</div>\n		<div class="span6">\n			<div class="well">\n				<?=content_block(''testimonial_intro'', ''Testimonial Introduction'', array(''business_name''=>$provider->business_name))?>\n			</div>\n		</div>\n	</div>\n<? else: ?>\n	<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that provider could not be found'')))?>\n<? endif ?>', NULL, 1, 1, 'service:testimonial', NULL, NULL, NULL, 27, 2, 'everyone', NULL, '4faa0825e26539.15034291', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(32, 'Request', 'request', '/request', 'Request a Service', '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/request_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/request_page.js'',\n)) ?>', NULL, NULL, '<?=form_open(array(''id'' => ''request_form'', ''class''=>''''))?>\n\n	<input type="hidden" name="redirect" value="<?=root_url(''request/review'')?>" />\n	<input type="hidden" name="redirect_assist" value="<?=root_url(''request/assist'')?>" />\n\n	<div class="page-header">\n		<h1><?=$this->page->title_name?></h1>\n		<h4 class="subheader"><?=__(''Submit your request to our pool of service providers'')?></h4>\n	</div>\n	<div class="row-fluid">\n		<div class="span8">\n			<div id="p_request_form">\n				<?=$this->display_partial(''request:request_form'')?>\n			</div>\n			<div class="form-actions">\n				<button name="submit" type="submit" class="btn btn-large btn-primary btn-icon">\n					<?=__(''Submit Request'')?> <i class="icon-chevron-right"></i>\n				</button>\n			</div>\n		</div>\n		\n		<div class="span4">\n			<div class="well hide-for-small">\n				<?=content_block(''request_right_panel'', ''Request Right Panel'')?>\n			</div>\n		</div>\n	</div>\n<?=form_close()?>', NULL, 1, 1, 'bluebell:create_request', NULL, '$this->data[''current_menu''] = ''request'';\n\n\n\n\n', NULL, NULL, 2, 'everyone', NULL, '4f6b14d3b16e46.76157627', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(33, 'Request Assist', 'request_assist', '/request/assist', NULL, '<?=$this->css_include(array(\r\n	''@/assets/stylesheets/css/layouts/application_layout/pages/request_assist_page.css'',\r\n)) ?>\r\n<?=$this->js_include(array(\r\n	''@/assets/scripts/js/layouts/application_layout/pages/request_assist_page.js'',\r\n)) ?>', NULL, NULL, '<? if ($request): ?>\n	<?=form_open(array(''id''=> ''assist_form''))?>\n\n		<input type="hidden" name="redirect" value="<?=root_url(''request/review'')?>" />\n		<input type="hidden" name="redirect_assist" value="<?=root_url(''request/assist'')?>" />\n		<input type="hidden" name="Request[category_id]" value="" id="request_category_id" />\n		<input type="hidden" name="Request[title]" value="" id="request_title" />\n		\n		<div class="page-header">\n			<h1><?=__(''Browse providers'')?></h1>\n			<h4 class="subheader"><?=__(''Please select the service professional who can best meet your needs'')?></h4>\n		</div>\n		<div class="row-fluid">\n			<div class="span8">\n				<div id="p_category_form"><?= $this->display_partial(''request:category_form'') ?></div>\n				<div class="form-actions align-right">\n					<?= form_submit(''assist'', __(''Continue to Review''), ''class="btn btn-success btn-large"'') ?>\n				</div>\n			</div>\n		</div>\n	<?=form_close()?>\n\n	<div id="popup_suggest_category_success" class="modal hide fade" tabindex="-1" role="dialog">\n		<div class="modal-body">\n			<p class="lead"><?=__(''Thanks! Your suggestion has been submitted. We will let you know the outcome via Email.'')?></p>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="btn popup-close"><?=__(''Close'', true)?></a>\n		</div>\n	</div>\n\n	<div id="popup_suggest_category" class="modal hide fade" tabindex="-1" role="dialog">\n		<?=form_open(array(''id''=> ''assist_suggest_form''))?>\n			<div class="modal-header">\n				<h3><?=__(''Suggest a new service for %s'', c(''site_name''))?></h3>\n			</div>\n			<div class="modal-body">\n				<?= $this->display_partial(''request:suggest_form'') ?>	    \n			</div>\n			<div class="modal-footer">\n				<input type="submit" name="submit" value="<?=__(''Submit suggestion'')?>" class="btn btn-success" />\n			</div>\n		<?=form_close()?>\n	</div>\n\n<script>\n\nPage.requestAssistFormFields = $.phpr.form().defineFields(function() {\n	this.defineField(''select_category'').required();\n	this.defineField(''select_alpha'').required();\n});\n\n</script>\n\n<? else: ?>\n	<?= $this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, something went wrong with your request. Please check that your browser has cookies enabled or change your browser settings to default.''))) ?>\n<? endif ?>', NULL, 1, 1, 'bluebell:review_request', NULL, NULL, NULL, 32, 2, 'everyone', NULL, '514ce672b5cf67.21892208', 'custom', NULL, NULL, NULL, '2014-07-08 18:17:59', '2014-07-08 18:18:02'),
(34, 'Request Manage', 'request_manage', '/request/manage', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/request_manage_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/request_manage_page.js'',\n)) ?>', NULL, NULL, '<ul class="breadcrumb">\n	<li><a href="<?=root_url(''dashboard'')?>"><?=__(''My Service Requests'')?></a> <i class="icon-chevron-right divider"></i></li>\n	<li class="active"><?=$this->page->title_name?></li>\n</ul>\n\n<div class="inner_panel">\n	<? if ($request): ?>\n\n	<div class="row-fluid">\n\n		<div class="span8">\n\n			<? if ($request->is_new && $request->status_code != Service_Status::status_cancelled): ?>\n				<div class="page-header">\n					<h1><?=__(''%s request submitted'', ''<strong>''.$request->title.''</strong>'')?></h1>\n					<h4 class="subheader"><?=__(''Your request has been submitted. What happens next?'')?></h4>\n				</div>\n				<div class="request-complete">\n					<?=content_block(''request_complete'', ''Request submitted'', array(\n						''title''=>$request->title, \n						''title_plural''=>Phpr_Inflector::pluralize($request->title)\n					))?>\n				</div>\n				<hr />\n			<? else: ?>\n				<div class="page-header">\n					<h1><?=__(''%s Request'', ''<strong>''.$request->title.''</strong>'', true)?></h1>\n				</div>\n			<? endif ?>\n\n			<div id="p_details">\n				<?=$this->display_partial(''request:manage_details'')?>\n			</div>\n			\n			<? if ($show_questions): ?>\n				<a name="questions"></a>\n				<h4><?=__(''Questions'')?></h4>\n				<div id="p_questions">\n					<?=$this->display_partial(''request:questions'')?>\n				</div>\n			<? endif ?>\n\n		</div>\n\n		<div class="span4">\n			<div id="p_status_panel" class="box">\n				<?=$this->display_partial(''request:status_panel'')?>\n			</div>\n			<? if ($can_edit): ?>\n				<div class="request_cancel">\n					<?=__(''%s this service request'', ''<a href="javascript:;" id="link_request_cancel"><i class="icon-remove-sign"></i> ''.__(''Cancel'', true).''</a>'')?>\n				</div>\n			<? endif ?>\n		</div>\n\n	</div>\n\n	<? if ($show_quotes): ?>\n		<a name="quotes"></a>\n		<h4><?=__(''Quotes'')?></h4>\n		<div id="p_quotes">\n			<?=$this->display_partial(''request:quotes'')?>\n		</div>\n	<? endif ?>\n\n	<div id="popup_request_cancel" class="modal hide fade" tabindex="-1" role="dialog">\n		<div class="modal-header"><h2><?=__(''Cancel this service request?'')?></h2></div>\n		<div class="modal-body">\n			<p class="lead"><?=__(''Are you sure you want to cancel this request? This request and all quotes will be lost!'')?></p>\n		</div>\n		<div class="modal-footer">\n			<a href="javascript:;" class="btn btn-danger popup-close" id="button_request_cancel" data-request-id="<?=$request->id?>"><?=__(''Cancel this request'')?></a>\n			<a href="javascript:;" class="pull-left popup-close"><?=__(''Continue request'')?></a>\n		</div>\n	</div>\n\n	<? else: ?>\n		<?=$this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, that request could not be found'')))?>\n	<? endif ?>\n\n</div>', NULL, 1, 1, 'service:update_request', 'if (!$request)\n    return;\n\nif ($request->status_code == Bluebell_Request::status_booked || $request->status_code == Service_Status::status_closed || $request->status_code == Bluebell_Request::status_booked_cancelled)\n    Phpr::$response->redirect($request->get_url(''job/booking''));\n\n$show_quotes = $show_questions = $can_edit = false;\n\nif ($request->status_code == Service_Status::status_active || $request->status_code == Service_Status::status_expired)\n    $show_quotes = $show_questions = $can_edit = true;\n\nif ($request->quotes->count == 0)\n    $show_quotes = false;\n\nif ($request->questions->count == 0)\n    $show_questions = false;\n\n$this->data[''show_quotes''] = $show_quotes;\n$this->data[''can_edit''] = $can_edit;\n$this->data[''show_questions''] = $show_questions;\n\n\n\n', NULL, NULL, 32, 2, 'users', 5, '4f86c27e08e7c7.02122910', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-08-16 22:34:00');
INSERT INTO `cms_pages` (`id`, `name`, `file_name`, `url`, `title`, `head`, `description`, `keywords`, `content`, `sort_order`, `sitemap_visible`, `published`, `action_code`, `code_post`, `code_pre`, `code_ajax`, `parent_id`, `template_id`, `security_id`, `security_page_id`, `unique_id`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(35, 'Request Review', 'request_review', '/request/review', NULL, '<?=$this->css_include(array(\n	''@/assets/stylesheets/css/layouts/application_layout/pages/request_review_page.css'',\n)) ?>\n<?=$this->js_include(array(\n	''@/assets/scripts/js/layouts/application_layout/pages/request_review_page.js'',\n)) ?>', NULL, NULL, '<? if ($request): ?>\n	<?= form_open(array(''id'' => ''review_form'', ''class''=>'''')) ?>\n		<input type="hidden" name="redirect_request" value="request/manage" />\n\n		<div class="page-header">\n			<h1><?=__(''Review and submit your request'')?></h1>\n			<h4 class="subheader"><?=__(''%s will send your request to Home Service Professionals available to do your work at the time you specified. Interested Pros will respond with their best price quotes.'', array(c(''site_name'')))?></h4>\n		</div>\n\n		<div id="p_review_form">\n			<?=$this->display_partial(''request:review_form'', array(''request''=>$request))?>\n		</div>\n\n		<? if (!$this->user): ?>\n			<fieldset class="form-horizontal">\n				<div class="row-fluid" id="title_register">\n					<div class="span8">\n						<h3><?=__(''Last step - Create account and submit request!'')?></h3>\n					</div>\n					<div class="span4">\n						<a href="javascript:;" id="toggle_login" \n							onclick="Page.toggleAccountRequest()" \n							class="pull-right btn">\n							<?= __(''Already have an account? Log in'') ?>\n						</a>\n					</div>\n				</div>\n				\n				<div class="row-fluid" id="title_login" style="display:none">\n					<div class="span8">\n						<h3><?=__(''Last step - Log in and submit request!'')?></h3>\n					</div>\n					<div class="span4">\n						<a href="javascript:;" id="toggle_register" \n							onclick="Page.toggleAccountRequest()" \n							class="pull-right btn">\n							<?= __("Don''t have an account yet? Sign up") ?>\n						</a>\n					</div>\n				</div>\n				<div class="row-fluid">\n					<div class="span8">\n						<div id="p_account">\n							<?=$this->display_partial(''site:register_form'')?>\n						</div>\n					</div>\n				</div>\n			<? endif ?>\n			<div class="controls">\n				<p>\n					<?=__(''By pressing submit you agree to the %s.'', array(\n						''<a href="''.root_url(''terms'').''" target="_blank">''.__(''terms of service'', true).''</a>''\n					))?>\n				</p>\n			</div>\n			<div class="form-actions">\n				<button name="review" type="submit" class="btn btn-large btn-success btn-icon">\n					<?=__(''Confirm Request'')?> <i class="icon-ok"></i>\n				</button>\n			</div>\n\n	<? if (!$this->user): ?>\n		</fieldset>\n	<? endif ?>\n\n	<?= form_close() ?>\n<? else: ?>\n	<?= $this->display_partial(''site:404'', array(''error_message''=>__(''Sorry, something went wrong with your request. Please check that your browser has cookies enabled or change your browser settings to default.''))) ?>\n<? endif ?>', NULL, 1, 1, 'bluebell:review_request', NULL, NULL, NULL, 32, 2, 'everyone', NULL, '514ce548eaad44.04340181', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-07-26 13:56:38'),
(36, 'Terms', 'terms', '/terms', 'Terms of use', NULL, 'Website usage terms and conditions', NULL, '<div class="page-header">\n	<h1><?=$this->page->title_name?></h1>\n	<h4 class="subheader"><?=$this->page->description?></h4>\n</div>\n<?=content_block(''page_terms'', ''Terms page content'') ?>', NULL, 1, 1, 'Custom', NULL, '$this->data[''current_menu''] = ''terms'';\n\n\n\n\n', NULL, 2, 1, 'everyone', NULL, '4f7942e9da5860.64151282', 'custom', NULL, NULL, 1, '2014-07-08 18:17:59', '2014-08-15 19:44:53'),
(37, 'Social Capture Email', 'social_capture_email', '/social/capture-email', NULL, NULL, NULL, NULL, '<div id="email_confirmation">\n	<?= form_open() ?>\n	\n		<input type="hidden" name=''flynsarmysocialmedia_email_confirmation'' value="1" />\n		\n		<?= flash_message() ?>\n\n		<div class="page-header">\n			<h2><?=__(''Confirm email'')?></h2>\n			<p><?=__("You''re almost done. Please confirm your email address.")?></p>\n		</div>\n\n		<fieldset class="form-horizontal">\n			<div class="control-group">\n				<label for="signup_email" class="control-label">Email</label>\n				<div class="controls">\n					<input id="signup_email" type="text" name="email" value="<? post(''email'', '''') ?>" class="text" />\n				</div>\n			</div>\n\n			<div class="form-actions">\n				<a href="javascript:;" class="btn btn-large btn-success"\n					onclick="return $(this).getForm().sendRequest(''flynsarmysociallogin:on_email_confirmation'', {update: {email_confirmation: ''email_confirmation''}})">Submit</a>\n			</div>\n		</fieldset>\n\n	<?= form_close() ?>\n</div>', NULL, 1, 1, 'Custom', NULL, NULL, NULL, NULL, NULL, 'everyone', NULL, '51dcd8013cfe94.53302981', NULL, 'social', 1, NULL, '2014-07-08 18:36:59', NULL),
(70, 'Request Provider', 'request_provider', '/request/providers', NULL, NULL, NULL, NULL, '<?=$this->display_partial(''request:providers'')?>', NULL, 1, 1, 'service:request_provider', 'if (!$request)\n    return;\n', NULL, NULL, 32, 2, 'users', 6, '53eea9f9dba996.48380523', 'custom', NULL, 1, 1, '2014-08-16 00:46:49', '2014-08-17 20:27:10'),
(72, 'braintree', 'braintree', '/braintree', NULL, NULL, NULL, NULL, '<?print_r($this->data)?>', NULL, NULL, 1, 'payment:braintree_response', NULL, NULL, NULL, NULL, 9, 'everyone', NULL, '540a58892586b9.74051873', 'custom', NULL, 1, 1, '2014-09-06 00:42:49', '2014-09-06 00:54:35'),
(69, 'How it Works HomeOwner', 'how_it_works_homeowner', '/how-it-works-homeowner', 'How it Works', NULL, NULL, NULL, '<div class="page-header">\n    <h1><?=$this->page->title_name?></h1>\n    <h4 class="subheader"><?=$this->page->description?></h4>\n</div>\n<div class="row-fluid">\n	<div class="span12">\n	\n		<div class="dynamic-row step-1">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/comp_b.png">\n			    <div class="imageText">\n			    	<p>Create an account. It''s easy.</p>\n			    </div>\n			</div>\n			<div class="text">\n			    <h3>1. Sign Up</h3>\n			    <p>\n			    	You can create a profile through womBids or use an existing Facebook or Google account. Once you’ve done this, you’ll be able to proceed with posting a project or you can search through local contractors in your area. You’ll be able to see contractor’s licenses, insurance and bonding limits, reputation and even a pictures from previous projects.\n			    </p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-2">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/pencil_b.png">\n			    <div class="imageText">\n			    	<p>List your skills and talents. Make it pop with photos and more.</p>\n			    </div>\n			</div>\n			<div class="text">\n			    <h3>2. Post a Project</h3>\n			    <p>Now this is the fun part, homeowners have the opportunity to post a project, otherwise known as a Request for Proposal. Don’t be discouraged if you don’t know the lingo, this is Step #1 in the process and contractors know that part of their job is to educate customers. The key to success is getting as much information as possible into the request form, i.e. in the form detailed descriptions, pictures, rough sketches, blue prints, etc. – whatever you have womBids and the contractors will appreciate it.</p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-3">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/star_b.png">\n			</div>\n			<div class="text">\n			    <h3>3. Review Profiles</h3>\n			    <p>We give customers the option to either wait for contractors to come to them or seek out contractors themselves on the womBids platform.</p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-4">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/arrow_b.png">\n			</div>\n			<div class="text">\n			    <h3>4. Hire a Professional</h3>\n			    <p>\n				After ironing out the details of the project, sifting through reviews, checking references and agreeing on terms, customers can hire a professional simply by accepting the pro’s bid.\n			    </p>\n			</div>\n		</div>\n		\n		<div class="dynamic-row step-1">\n			<div class="image">\n			    <img src="/themes/custom/assets/images/dollar_b.png">\n			</div>\n			<div class="text">\n			    <h3>5. Pay for your Project</h3>\n			    <p>\n				Paying for the project is simple and easy as we have payment integration built into the site. We use a safe and secure third party processor, Braintree (a division of Paypal, Inc. and the same company used by heavyweights such as Airbnb and Uber).\n			    </p>\n			</div>\n		</div>\n			\n	</div>\n</div>', NULL, NULL, 1, 'Custom', NULL, NULL, NULL, NULL, 7, 'everyone', NULL, '53e4ae4f83e147.89671786', 'custom', NULL, 1, 1, '2014-08-08 11:02:39', '2014-08-08 11:31:36');

-- --------------------------------------------------------

--
-- Table structure for table `cms_page_visits`
--

CREATE TABLE IF NOT EXISTS `cms_page_visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date_ip` (`visit_date`,`ip`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=143 ;

--
-- Dumping data for table `cms_page_visits`
--

INSERT INTO `cms_page_visits` (`id`, `url`, `visit_date`, `ip`, `page_id`) VALUES
(1, '/', '2014-09-08', '14.96.193.190', 16),
(2, '/', '2014-09-08', '14.96.193.190', 16),
(3, '/how-it-works-providers', '2014-09-08', '14.96.193.190', 68),
(4, '/', '2014-09-08', '148.251.11.148', 16),
(5, '/', '2014-09-08', '99.167.201.59', 16),
(6, '/account/signin', '2014-09-08', '99.167.201.59', 5),
(7, '/', '2014-09-08', '107.77.75.20', 16),
(8, '/', '2014-09-08', '99.167.201.59', 16),
(9, '/request', '2014-09-08', '99.167.201.59', 32),
(10, '/request/review', '2014-09-08', '99.167.201.59', 35),
(11, '/request/review', '2014-09-08', '99.167.201.59', 35),
(12, '/', '2014-09-08', '99.167.201.59', 16),
(13, '/', '2014-09-08', '50.174.115.105', 16),
(14, '/', '2014-09-09', '50.174.115.105', 16),
(15, '/', '2014-09-09', '50.174.115.105', 16),
(16, '/', '2014-09-09', '66.249.84.93', 16),
(17, '/', '2014-09-09', '54.92.222.212', 16),
(18, '/', '2014-09-09', '192.99.39.79', 16),
(19, '/', '2014-09-09', '50.174.115.105', 16),
(20, '/', '2014-09-09', '50.174.115.105', 16),
(21, '/', '2014-09-09', '50.174.115.105', 16),
(22, '/', '2014-09-09', '66.249.84.66', 16),
(23, '/', '2014-09-09', '66.249.85.118', 16),
(24, '/', '2014-09-09', '66.249.85.118', 16),
(25, '/', '2014-09-09', '66.249.85.118', 16),
(26, '/', '2014-09-09', '66.249.85.118', 16),
(27, '/', '2014-09-09', '50.174.115.105', 16),
(28, '/', '2014-09-09', '66.249.65.111', 16),
(29, '/blog', '2014-09-09', '66.249.65.119', 8),
(30, '/about', '2014-09-09', '66.249.65.119', 2),
(31, '/terms', '2014-09-09', '66.249.65.119', 36),
(32, '/provide', '2014-09-09', '66.249.65.119', 27),
(33, '/privacy', '2014-09-09', '66.249.65.103', 25),
(34, '/contact', '2014-09-09', '66.249.65.103', 12),
(35, '/request', '2014-09-09', '66.249.65.111', 32),
(36, '/testimonials', '2014-09-09', '66.249.65.119', 1),
(37, '/advice-and-tips', '2014-09-09', '66.249.65.119', 1),
(38, '/blog/feed', '2014-09-09', '66.249.65.111', 8),
(39, '/account/signin', '2014-09-09', '66.249.65.103', 5),
(40, '/how-it-works-homeowner', '2014-09-09', '66.249.65.111', 69),
(41, '/how-it-works-providers', '2014-09-09', '66.249.65.111', 68),
(42, '/themes/custom/assets', '2014-09-09', '66.249.65.111', 1),
(43, '/faq', '2014-09-09', '66.249.65.111', 15),
(44, '/blog/rss', '2014-09-09', '66.249.65.103', 11),
(45, '/account/reset', '2014-09-09', '66.249.65.119', 4),
(46, '/account/signup', '2014-09-09', '66.249.65.119', 7),
(47, '/blog/post/test', '2014-09-09', '66.249.65.111', 10),
(48, '/blog/category/uncategorized', '2014-09-09', '66.249.65.119', 9),
(49, '/blog/posttest', '2014-09-09', '66.249.65.111', 8),
(50, '/blog/categoryuncategorized', '2014-09-09', '66.249.65.103', 8),
(51, '/', '2014-09-09', '97.74.104.6', 16),
(52, '/', '2014-09-09', '50.174.115.105', 16),
(53, '/contact', '2014-09-09', '50.174.115.105', 12),
(54, '/', '2014-09-09', '59.177.65.120', 16),
(55, '/', '2014-09-09', '99.167.201.59', 16),
(56, '/dashbard', '2014-09-09', '99.167.201.59', 1),
(57, '/', '2014-09-09', '99.167.201.59', 16),
(58, '/dashbard', '2014-09-09', '99.167.201.59', 1),
(59, '/', '2014-09-09', '99.167.201.59', 16),
(60, '/', '2014-09-09', '198.199.66.231', 16),
(61, '/', '2014-09-09', '54.224.27.110', 16),
(62, '/blog', '2014-09-09', '66.249.69.199', 8),
(63, '/about', '2014-09-09', '66.249.69.199', 2),
(64, '/terms', '2014-09-09', '66.249.69.199', 36),
(65, '/provide', '2014-09-09', '66.249.69.215', 27),
(66, '/privacy', '2014-09-09', '66.249.69.231', 25),
(67, '/contact', '2014-09-09', '66.249.69.199', 12),
(68, '/request', '2014-09-09', '66.249.69.231', 32),
(69, '/themes/custom/assets', '2014-09-09', '66.249.69.199', 1),
(70, '/testimonials', '2014-09-09', '66.249.69.215', 1),
(71, '/', '2014-09-09', '66.249.69.231', 16),
(72, '/', '2014-09-09', '97.74.104.7', 16),
(73, '/', '2014-09-09', '99.167.201.59', 16),
(74, '/account/signin', '2014-09-09', '99.167.201.59', 5),
(75, '/account/signin', '2014-09-09', '99.167.201.59', 5),
(76, '/account/signin', '2014-09-09', '99.167.201.59', 5),
(77, '/', '2014-09-09', '99.167.201.59', 16),
(78, '/', '2014-09-09', '59.177.65.120', 16),
(79, '/', '2014-09-09', '59.177.65.120', 16),
(80, '/advice-and-tips', '2014-09-09', '66.249.69.215', 1),
(81, '/', '2014-09-09', '64.134.239.68', 16),
(82, '/contact', '2014-09-09', '64.134.239.68', 12),
(83, '/', '2014-09-10', '150.70.97.116', 16),
(84, '/blog/feed', '2014-09-10', '66.249.69.231', 8),
(85, '/account/signin', '2014-09-10', '66.249.69.231', 5),
(86, '/how-it-works-homeowner', '2014-09-10', '66.249.69.199', 69),
(87, '/how-it-works-providers', '2014-09-10', '66.249.69.215', 68),
(88, '/request', '2014-09-10', '66.249.69.199', 32),
(89, '/privacy', '2014-09-10', '66.249.69.199', 25),
(90, '/about', '2014-09-10', '66.249.69.231', 2),
(91, '/blog', '2014-09-10', '66.249.69.231', 8),
(92, '/request', '2014-09-10', '66.249.69.215', 32),
(93, '/privacy', '2014-09-10', '66.249.69.215', 25),
(94, '/provide', '2014-09-10', '66.249.69.231', 27),
(95, '/how-it-works-homeowner', '2014-09-10', '66.249.69.231', 69),
(96, '/terms', '2014-09-10', '66.249.69.199', 36),
(97, '/', '2014-09-10', '66.249.79.103', 16),
(98, '/how-it-works-providers', '2014-09-10', '66.249.79.103', 68),
(99, '/request', '2014-09-10', '66.249.79.111', 32),
(100, '/contact', '2014-09-10', '66.249.79.119', 12),
(101, '/provide', '2014-09-10', '66.249.79.103', 27),
(102, '/blog', '2014-09-10', '66.249.79.119', 8),
(103, '/request', '2014-09-10', '66.249.79.119', 32),
(104, '/', '2014-09-10', '72.14.199.167', 16),
(105, '/', '2014-09-10', '50.174.115.105', 16),
(106, '/about', '2014-09-10', '50.174.115.105', 2),
(107, '/contact', '2014-09-10', '50.174.115.105', 12),
(108, '/', '2014-09-10', '66.249.84.93', 16),
(109, '/', '2014-09-10', '66.249.88.90', 16),
(110, '/privacy', '2014-09-10', '66.249.79.103', 25),
(111, '/', '2014-09-10', '66.2.49.14', 16),
(112, '/', '2014-09-10', '150.70.173.56', 16),
(113, '/terms', '2014-09-10', '66.249.79.111', 36),
(114, '/about', '2014-09-10', '66.2.49.14', 2),
(115, '/contact', '2014-09-10', '66.2.49.14', 12),
(116, '/account/signin', '2014-09-10', '66.2.49.14', 5),
(117, '/', '2014-09-10', '150.70.173.37', 16),
(118, '/', '2014-09-10', '66.2.49.14', 16),
(119, '/provide', '2014-09-10', '66.249.79.103', 27),
(120, '/blog', '2014-09-10', '66.249.79.103', 8),
(121, '/', '2014-09-10', '97.74.104.6', 16),
(122, '/request', '2014-09-10', '66.249.79.103', 32),
(123, '/account/signin', '2014-09-10', '66.249.79.103', 5),
(124, '/', '2014-09-10', '99.167.201.59', 16),
(125, '/provide', '2014-09-10', '66.249.79.119', 27),
(126, '/', '2014-09-10', '99.167.201.59', 16),
(127, '/request', '2014-09-10', '99.167.201.59', 32),
(128, '/', '2014-09-10', '99.167.201.59', 16),
(129, '/', '2014-09-10', '99.167.201.59', 16),
(130, '/request', '2014-09-10', '99.167.201.59', 32),
(131, '/terms', '2014-09-10', '66.249.69.199', 36),
(132, '/', '2014-09-11', '172.56.39.103', 16),
(133, '/', '2014-09-11', '172.56.39.103', 16),
(134, '/', '2014-09-11', '50.174.115.105', 16),
(135, '/', '2014-09-11', '50.174.115.105', 16),
(136, '/', '2014-09-11', '50.174.220.250', 16),
(137, '/request', '2014-09-11', '50.174.220.250', 32),
(138, '/', '2014-09-11', '50.174.220.250', 16),
(139, '/about', '2014-09-11', '50.174.220.250', 2),
(140, '/contact', '2014-09-11', '50.174.220.250', 12),
(141, '/', '2014-09-11', '50.174.220.250', 16),
(142, '/', '2014-09-11', '97.74.104.7', 16);

-- --------------------------------------------------------

--
-- Table structure for table `cms_partials`
--

CREATE TABLE IF NOT EXISTS `cms_partials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `theme_id` varchar(64) DEFAULT NULL,
  `module_id` varchar(30) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `theme_id` (`theme_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=146 ;

--
-- Dumping data for table `cms_partials`
--

INSERT INTO `cms_partials` (`id`, `name`, `file_name`, `content`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'account:detail_block', 'account;detail_block', '<?\n\n	$object = $this->user;\n\n	if ($block_key = post(''detail_block_key''))\n	{\n		$object = User::create()->find($this->user->id);\n		$block_key = unserialize(base64_decode($block_key));\n		extract($block_key);\n	}\n	\n	if (!isset($title))\n		$title = "Unknown";\n\n	if (!isset($edit_fields))\n		$edit_fields = array();\n\n	if (!isset($display_fields))\n		$display_fields = $edit_fields;\n\n	$block_key = base64_encode(serialize(array(\n		''block_id''=>$block_id,\n		''title''=>$title, \n		''edit_fields''=>$edit_fields, \n		''display_fields''=>$display_fields\n	)));\n\n?>\n<div class="block">\n	<div class="edit-link">\n		<a href="javascript:;" onclick="Page.toggleEdit(this)" class="label label-important">Edit</a>\n	</div>\n	<h5 class="block-title"><?=$title?></h5>\n	<div class="row-fluid view">\n		<div class="span12">\n			<? foreach ($display_fields as $name => $field): ?>\n				<?\n					$field = (object)$field;\n					$value = isset($field->value) \n						? $field->value \n						: (strlen($object->$name) \n							? $object->$name \n							: ''&nbsp;'');\n				?>\n				<div class="block-summary"><?=$field->label?></div>\n				<div class="block-value"><?=$value?></div>\n			<? endforeach ?>\n		</div>\n	</div>\n	<div class="row-fluid edit" style="display:none">\n		<div class="span7">\n			\n			<?=form_open(array(''id''=>''form_''.$block_id))?>\n			<input type="hidden" name="detail_block_key" value="<?= $block_key ?>" />\n			\n				<? foreach ($edit_fields as $name => $field): ?>\n					<?\n						$field = (object)$field;\n						$field_name = "User[".$name."]";\n\n						if (!isset($field->type))\n							$field->type = null;\n\n						$align = isset($field->align) ? $field->align : null;\n						$class = ($align) ? ''span6'' : ''span12'';\n\n					?>				\n				<? if ($align != "right") { ?><div class="row-fluid"><? } ?>\n					<div class="control-group <?=$name?> <?=$class?>">\n						<label for="<?=$name?>" class="control-label"><?=$field->label?></label>\n						<div class="controls">\n							<?\n								switch ($field->type)\n								{\n									default: echo form_input($field_name, $object->$name, ''id="''.$name.''" class="span12"''); break;\n									case "country": \n										echo form_dropdown($field_name, Location_Country::get_name_list(), $object->$name, ''id="''.$name.''" class="span12"'',  __(''-- Select --'', true)); \n										echo "<script> jQuery(document).ready(function() { Page.initCountrySelect(''#".$name."''); }); </script>";\n									break;\n									case "state": echo form_dropdown($field_name, Location_State::get_name_list($object->country_id), $object->$name, ''class="span12"'', __(''-- Select --'', true)); break;\n									case "password": echo form_password($field_name, '''', ''id="''.$name.''" class="span12"''); break;\n								}\n							?>\n						</div>\n					</div>\n				<? if ($align != "left") { ?></div><? } ?>\n				<? endforeach ?>\n				<div class="row-fluid control_panel">\n					<div class="span12">\n						<?=form_submit(''submit'', __(''Save'', true), ''class="btn btn-primary"'')?> \n						&nbsp; <a href="javascript:;" class="cancel-text" onclick="Page.toggleEdit(this)"><?=__(''Cancel'', true)?></a>\n					</div>\n				</div>\n			<?=form_close()?>\n		</div>\n	</div>\n</div>\n<script>\n	jQuery(document).ready(function($) { \n\n		// Throw-away var\n		var accountDetailBlockFormFields = $(''#form_<?=$block_id?>'').phpr().form();\n		accountDetailBlockFormFields.defineFields(function(){\n			<? foreach ($edit_fields as $name => $field): ?>\n			<? \n				$field = (object)$field; \n				$field_name = "User[".$name."]";\n			?>\n			<? if (isset($field->validate_rules) && isset($field->validate_messages)): ?>\n				this.defineField(''<?=$field_name?>'')\n					.setRules(<?=$field->validate_rules?>)\n					.setMessages(<?=$field->validate_messages?>);\n			<? endif ?>\n			<? endforeach ?>\n		});\n		accountDetailBlockFormFields.validate()\n			.action(''user:on_update_account'')\n			.beforeSend(function(){ Page.toggleEdit($(''#form_<?=$block_id?>'')); })\n			.update(''#<?=$block_id?>'', ''account:detail_block'');\n	});\n\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(2, 'account:details', 'account;details', '<div class="block" id="block_name">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_name'',\n		''title'' => __(''Name'',true),\n		''display_fields'' => array(\n			''name'' => array(''label''=>__(''Your real name''))\n		),\n		''edit_fields'' => array(\n			''first_name''=> array(\n				''label''=>__(''First Name'',true),\n				''validate_rules''=>''{ required: true }'', \n				''validate_messages''=>''{ required: "''.__(''Please specify your first name'', true).''" }''\n			),\n			''last_name''=> array(''label''=>__(''Last Name'',true))\n		)\n	))?>\n</div>\n\n<div class="block" id="block_email">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_email'',\n		''title'' => __(''Email'',true),\n		''display_fields'' => array(\n			''email'' =>array(''label''=>__(''Your email address''))\n		),\n		''edit_fields'' => array(\n			''email''=>array(\n				''label''=>__(''Email Address'',true),\n				''validate_rules''=>''{ required: true, email: true }'', \n				''validate_messages''=>''{ \n					required: "''.__(''Please specify your email address'',true).''", \n					email: "''.__(''Please specify a valid email address'',true).''" \n				}''\n			)\n		)\n	))?>\n</div>\n\n<div class="block" id="block_password">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_password'',\n		''title'' => __(''Password''),\n		''display_fields'' => array(\n			''password'' => array(''label''=>__(''Your password''), ''value''=>''**********'')\n		),\n		''edit_fields'' => array(\n			''old_password''=>array(\n				''label''=>__(''Enter current password'', true), \n				''type''=>''password'', \n				''validate_rules''=>''{ \n					required: true,  \n					phprRemote: {\n						action: "user:on_validate_password"\n					}\n				}'', \n				''validate_messages''=>''{ \n					required: "''.__(''Please specify the old password'',true).''",\n					phprRemote: "''.__(''Password entered is invalid'',true).''"\n				}''\n			),\n			''password''=> array(\n				''label''=>__(''Create a Password'', true), \n				''type''=>''password'', ''validate_rules''=>''{ required: true }'', \n				''validate_messages''=>''{ required: "''.__(''Please specify a new password'',true).''" }''\n			),\n			''password_confirm''=> array(\n				''label''=>__(''Confirm Password'', true), \n				''type''=>''password'', \n				''validate_rules''=>''{ required: true, equalTo:"#password" }'', \n				''validate_messages''=>''{ \n					required: "''.__(''Please specify a new password'',true).''", \n					equalTo: "''.__(''Password and confirmation password do not match.'',true).''" \n				}''\n			)\n		)\n	))?>\n</div>\n\n<div class="block" id="block_address">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_address'',\n		''title'' => __(''Contact Details''),\n		''display_fields'' => array(\n			''phone'' =>array(''label''=>__(''Your contact phone'')),\n			''mobile'' => array(''label''=>__(''Your mobile phone'')),\n			''address_string'' => array(''label''=>__(''Your address''))\n\n		),\n		''edit_fields'' => array(\n			''phone''=> array(''label''=>__(''Phone number'', true) ),\n			''mobile''=> array(''label''=>__(''Mobile number'', true)),\n			''street_addr''=> array(''label''=>__(''Address'', true)),\n			''city''=> array(''label''=>__(''City'', true), ''align''=>''left''),\n			''zip''=> array(''label''=>__(''Zip / Postal Code'', true), ''align''=>''right''),\n			''country_id''=> array(''label''=>__(''Country''), ''type''=>''country'', ''align''=>''left''),\n			''state_id''=> array(''label''=>__(''State''), ''type''=>''state'', ''align''=>''right''),\n		)\n	))?>\n</div>\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(3, 'account:finances', 'account;finances', '\n<hr />\n<h4><?=__(''Invoices'')?></h4>\n\n<? if (!$invoices || !$invoices->count): ?>\n	<p>You have no invoices yet</p>\n<? else: ?>\n\n	<table class="license_table table table-striped table-hover">\n		<thead>\n			<tr>\n				<th>#</th>\n				<th>Date</th>\n				<th>Status</th>\n				<th class="align-right last">Total</th>\n				<th class="last"></th>\n			</tr>\n		</thead>\n		<tbody>\n			<? foreach ($invoices as $invoice): ?>\n				<?\n					$url = root_url(''payment/invoice/''.$invoice->id);\n				?>\n				<tr>\n					<td><a href="<?= $url ?>"><?= $invoice->id ?></a></td>\n					<td><a href="<?= $url ?>"><?= $invoice->sent_at->format(''%x'') ?></a></td>\n					<td><a href="<?= $url ?>"><strong><?= h($invoice->status->name) ?></strong> since <?= $invoice->status_updated_at->format(''%x'') ?></a></td>\n					<td class="total align-right"><a href="<?= $url ?>"><?= format_currency($invoice->total) ?></a></td>\n					<td class="align-right last"><a href="<?= $url ?>" class="btn btn-small btn-block"><i class="icon-search"></i> View</a></td>\n				</tr>\n			<? endforeach ?>\n		</tbody>\n	</table>\n\n<? endif ?>\n', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-09-07 01:30:32'),
(4, 'account:notification_block', 'account;notification_block', '<?\r\n	$sms_code = (isset($sms_code)) ? $sms_code : null;\r\n	$email_code = (isset($email_code)) ? $email_code : null;\r\n?>\r\n<div class="row-fluid notification_block">\r\n	<div class="span8 columns"><p><?=$title?></p></div>\r\n	<div class="span4 columns">\r\n		<ul class="block-grid grid-span2">\r\n			<? if ($email_code): ?><li><label class="checkbox"><span class="visible-phone"><?=__(''Email'', true)?></span> <?=form_checkbox(''User[''.$email_code.'']'', true, $this->user->$email_code)?></label></li><? endif ?>\r\n			<? if ($sms_code): ?><li><label class="checkbox"><span class="visible-phone"><?=__(''SMS'', true)?></span> <?=form_checkbox(''User[''.$sms_code.'']'', true, $this->user->$sms_code)?></label></li><? endif ?>\r\n		</ul>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(5, 'account:notifications', 'account;notifications', '<?=form_open(array(''id''=>''form_user_notifications'', ''class''=>''nice custom''))?>\r\n\r\n	<div class="row-fluid notification_title">\r\n		<div class="span8"><h4><?=__(''General Notifications'')?></h4></div>\r\n		<div class="span4">\r\n			<ul class="block-grid grid-span2 hidden-phone">\r\n				<li><?=__(''Email'', true)?></li>\r\n				<li><?=__(''SMS'', true)?></li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Job scheduled''), ''email_code'' => ''service_email_job_booked''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Appointment time reminder''), ''email_code'' => ''service_email_booking_reminder'', ''sms_code'' => ''service_sms_booking_reminder''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Job completed''), ''email_code'' => ''service_email_job_complete'', ''sms_code'' => ''service_sms_job_complete''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Rating/review left about you''), ''email_code'' => ''service_email_rating_submit'', ''sms_code'' => ''service_sms_rating_submit''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Job cancelled''), ''email_code'' => ''service_email_job_cancel'', ''sms_code'' => ''service_sms_job_cancel''))?>\r\n\r\n	<div class="row-fluid notification_title">\r\n		<div class="span8"><h4><?=__(''Provide Services Notifications'')?></h4></div>\r\n		<div class="span4">\r\n			<ul class="block-grid grid-span2 hidden-phone">\r\n				<li><?=__(''Email'', true)?></li>\r\n				<li><?=__(''SMS'', true)?></li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Service request received''), ''email_code'' => ''service_email_request_submit''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Question about your request''), ''email_code'' => ''service_email_request_question'', ''sms_code'' => ''service_sms_request_question''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Price quote on your request''), ''email_code'' => ''service_email_request_quote''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Quoting period ended''), ''email_code'' => ''service_email_request_expired'', ''sms_code'' => ''service_sms_request_expired''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''12 hours remaining to select Provider''), ''email_code'' => ''service_email_request_reminder''))?>\r\n\r\n	<div class="row-fluid notification_title">\r\n		<div class="span8"><h4><?=__(''Request a Provider Notifications'')?></h4></div>\r\n		<div class="span4">\r\n			<ul class="block-grid grid-span2 hidden-phone">\r\n				<li><?=__(''Email'', true)?></li>\r\n				<li><?=__(''SMS'', true)?></li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''New job offer''), ''email_code'' => ''service_email_job_offer'', ''sms_code'' => ''service_sms_job_offer''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Answer to your question''), ''email_code'' => ''service_email_job_answer'', ''sms_code'' => ''service_sms_job_answer''))?>\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __("Answer to another Provider''s question"), ''email_code'' => ''service_email_job_answer_other'', ''sms_code'' => ''service_sms_job_answer_other''))?>\r\n\r\n	<div class="row-fluid notification_title">\r\n		<div class="span8"><h4><?=__(''Stop All Notifications'')?></h4></div>\r\n		<div class="span4">\r\n			<ul class="block-grid grid-span2 hidden-phone">\r\n				<li><?=__(''Email'', true)?></li>\r\n				<li><?=__(''SMS'', true)?></li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n\r\n	<?=$this->display_partial(''account:notification_block'', array(''title'' => __(''Block all notitications''), ''email_code'' => ''email_block_email'', ''sms_code'' => ''sms_block_sms''))?>\r\n\r\n	<div class="row-fluid">\r\n		<div class="span8">&nbsp;</div>\r\n		<div class="span4 align-center">\r\n			<?=form_submit(''ask'', __(''Save changes''), ''class="btn btn-primary"'')?>\r\n		</div>\r\n	</div>\r\n\r\n<?=form_close()?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(6, 'account:skill_profiles', 'account;skill_profiles', '<? foreach ($profiles as $profile): ?>\r\n<div class="profile">\r\n	<div class="row-fluid">\r\n		<div class="span6 columns mobile-two">\r\n			<a href="<?=root_url(''provide/manage/''.$profile->id)?>" class="role_name"><?=$profile->role_name?></a>\r\n		</div>\r\n		<div class="span6 columns mobile-two align_right">\r\n			<div class="status active">Active</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n<? endforeach ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(7, 'account:work_history', 'account;work_history', '<div class="offer-request-control">\r\n	<?=form_open(array(''id''=>''form_work_history''))?>\r\n		<input type="hidden" name="mode" value="<?= $mode ?>" />\r\n		<input type="hidden" name="submode" value="<?= $submode ?>" />\r\n		<input type="hidden" name="page" value="<?= $page ?>" />\r\n\r\n		<? if ($this->user->is_provider && $this->user->is_requestor): ?>\r\n			<ul class="nav nav-tabs" id="mode_filter">\r\n				<li class="disabled"><a href="javascript:;"><?=__(''Filter'', true)?>:</a></li>\r\n				<li class="<?=$mode==''offers''?''active'':''''?>"><a href="javascript:;" data-mode="offers"><?=__(''Jobs Offered'')?></a></li>\r\n				<li class="<?=$mode==''requests''?''active'':''''?>"><a href="javascript:;" data-mode="requests"><?=__(''Services Requested'')?></a></li>\r\n			</ul> \r\n		<? endif ?>\r\n		<? if ($mode=="offers"): ?>\r\n			<ul class="nav nav-pills" id="submode_filter_offers">\r\n				<li class="<?=$submode==''open''?''active'':''''?>"><a href="javascript:;" data-submode="open"><?=__(''Open'')?></a></li>\r\n				<li class="<?=$submode==''performed''?''active'':''''?>"><a href="javascript:;" data-submode="performed"><?=__(''Performed'')?></a></li>\r\n				<li class="<?=$submode==''lost''?''active'':''''?>"><a href="javascript:;" data-submode="lost"><?=__(''Lost'')?></a></li>\r\n				<li class="<?=$submode==''ignored''?''active'':''''?>"><a href="javascript:;" data-submode="ignored"><?=__(''Ignored'')?></a></li>\r\n				<li class="<?=$submode==''cancelled''?''active'':''''?>"><a href="javascript:;" data-submode="cancelled"><?=__(''Cancelled'')?></a></li>\r\n			</ul>\r\n		<? endif ?>\r\n\r\n		<? if ($mode=="offers"): ?>\r\n\r\n			<? if ($activity): ?>\r\n				<div class="row-fluid titles hide-for-small">\r\n					<div class="span8"><strong><?=__(''Job'',true)?></strong></div>\r\n					<div class="span4"><strong><?=__(''Status'',true)?></strong></div>\r\n				</div>\r\n\r\n				<? foreach ($activity as $index=>$job): ?>\r\n					<? if ($index != 0): ?><hr /><? endif ?>\r\n					<?=$this->display_partial(''dash:job_panel'', array(''job''=>$job))?>\r\n				<? endforeach ?>\r\n			<? else: ?>\r\n				<div class="well align-center">\r\n					<?=__(''No job offers found'')?>\r\n				</div>\r\n			<? endif ?>\r\n\r\n		<? else: ?>\r\n\r\n			<? if ($activity): ?>\r\n				<? foreach ($activity as $index=>$request): ?>\r\n					<? if ($index != 0): ?><hr /><? endif ?>\r\n					<?=$this->display_partial(''dash:request_panel'', array(''request''=>$request))?>\r\n				<? endforeach ?>\r\n			<? else: ?>\r\n				<p><?=__(''No service requests found'')?></p>\r\n			<? endif ?>\r\n\r\n		<? endif ?>\r\n\r\n		<? if ($pagination): ?>\r\n			<hr />\r\n			<div id="p_site_pagination"><? $this->display_partial(''site:pagination'', array(''pagination''=>$pagination)); ?></div>\r\n		<? endif?>\r\n	<?=form_close()?>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(8, 'blog:comment_form', 'blog;comment_form', '<? if (!isset($comment_success) || !$comment_success): ?>\r\n	<h4><?=__(''Comment on this post'')?></h4>\r\n	<?=form_open(array(''onsubmit''=>"return $(this).phpr().post(''blog:on_create_comment'').update(''#comment_form'', ''blog:comment_form'').send()")) ?>\r\n		<div class="blog_comment_form">\r\n\r\n			<div class="row-fluid">\r\n				<div class="row-fluid">\r\n					<div class="span6 mobile-span2">\r\n						<div class="control-group">\r\n							<label for="author_name" class="control-label"><?=__(''Name'', true)?></label>\r\n							<div class="controls">\r\n								<input id="author_name" type="text" name="author_name" class="span12" /><br/>\r\n							</div>\r\n						</div>\r\n					</div>\r\n					<div class="span6 mobile-span2">\r\n						<div class="control-group">\r\n							<label for="author_email" class="control-label"><?=__(''Email Address'', true)?></label>\r\n							<div class="controls">\r\n								<input type="text" id="author_email" name="author_email" class="span12" /><br/>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n\r\n				<div class="control-group">\r\n					<label for="author_url" class="control-label"><?=__(''Website URL'')?></label>\r\n					<div class="controls">\r\n						<input type="text" id="author_url" name="author_url" class="span12" /><br/>\r\n					</div>\r\n				</div>\r\n\r\n				<div class="control-group">\r\n					<label for="comment_content" class="control-label"><?=__(''Comment'')?></label>\r\n					<div class="controls">\r\n						<textarea id="comment_content" name="content" class="span12"></textarea>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<div class="form-actions">\r\n				<?=form_submit(''submit'', __(''Submit Comment''), ''class="btn btn-primary"'')?>\r\n			</div>\r\n		</div>\r\n\r\n	<?=form_close()?>\r\n<? else: ?>\r\n	<div class="alert-box success">\r\n		<?=__(''Your comment has been posted. Thank you!'')?>\r\n		<a href="" class="close">&times;</a>\r\n	</div>\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(9, 'blog:sidebar', 'blog;sidebar', '<h5><?=__(''Blog categories'')?></h5>\n<ul class="category_list">\n	<?\n		$categories = Blog_Category::create()->find_all();\n	?>\n	<? foreach ($categories as $category): ?>\n		<li>\n			<a href="<?=root_url(''blog/category/''.$category->url_name) ?>"><?=h($category->name) ?></a>\n		</li>\n	<? endforeach ?>\n</ul>\n\n<div class="panel">\n	<h5><?=__(''RSS channel'')?></h5>\n	<p><?=__(''Subscribe to our %s to stay updated with our latest blog news.'', ''<a href="''.root_url(''blog/rss'').''">''.__(''RSS feed'').''</a>'')?></p>\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(10, 'control:badge', 'control;badge', '<?\r\n	$badge_mode = (isset($badge_mode)) ? $badge_mode : ''normal'';\r\n?>\r\n<div class="row-fluid <?=$badge_mode?>">\r\n<? if ($badge_mode == ''detailed''): ?>\r\n	<div class="span8 mobile-span9">\r\n		<div class="name"><?=$provider->business_name?></div>\r\n		<div class="location"><?=$provider->location_string?></div>\r\n		<div class="rating star-rating"><i class="rating-<?=$provider->rating*10?>"></i></div>\r\n		<div class="link"><a href="<?=$provider->get_url(''profile'')?>" class="btn btn-small"><?=__(''View Profile'')?></a></div>\r\n	</div>\r\n	<div class="span4 mobile-span3">\r\n		<div class="avatar"><img src="<?=Bluebell_Provider::avatar($provider)?>" alt="<?=$provider->business_name?>" class="img-circle" /></div>          \r\n	</div>\r\n<? else: ?>\r\n	<div class="span8 mobile-span9">\r\n		<div class="name"><?=$provider->business_name?></div>\r\n		<div class="location"><?=$provider->location_string?></div>\r\n	</div>\r\n	<div class="span4 mobile-span3">\r\n		<div class="avatar"><img src="<?=Bluebell_Provider::avatar($provider)?>" alt="<?=$provider->business_name?>" /></div>\r\n	</div>\r\n<? endif ?>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(11, 'control:conversation', 'control;conversation', '<?\r\n	$has_messages = false;\r\n	$to_user_id = (isset($to_user_id)) ? $to_user_id : $quote->provider->user_id;\r\n?>\r\n<? foreach ($quote->messages as $index=>$message): ?>\r\n	<? \r\n		$is_self = $message->from_user->id==$this->user->id; \r\n	?>\r\n	<? if (!$has_messages): $has_messages = true; ?>\r\n		<input type="hidden" name="Message[thread_id]" value="<?= $message->message_thread_id ?>" />\r\n		<h4 class="conv_header"><?=__(''Your conversation with %s'', $quote->provider->business_name)?></h4>\r\n	<? endif ?>\r\n\r\n	<div class="conv_message <?=$is_self ? ''left'' : ''right''?>">\r\n		<div class="message">\r\n			<p><?=Phpr_String::show_more_link($message->message, 250, __(''Show more'', true))?><p>\r\n			<p class="time"><?=__(''%s ago'', Phpr_DateTime::interval_to_now($message->sent_at), true)?></p>\r\n		</div>\r\n		<div class="user">\r\n			<span class="arrow"></span>\r\n			<?=$is_self ? __(''You'', true) : $quote->provider->business_name?>\r\n		</div>\r\n	</div>\r\n<? endforeach ?>\r\n\r\n\r\n<div class="send_message">\r\n	<div class="row-fluid">\r\n		<div class="span10">\r\n			<div class="control-group">\r\n				<input type="text" name="Message[message]" value="" class="span12" placeholder="<?= __(''Type your message or question and click Send'') ?>" />\r\n			</div>\r\n		</div>\r\n		<div class="span2">\r\n			<?=form_submit(''send'', ''Send'', ''class="send_message_button btn btn-block"'')?>\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n<p class="terms"><?=__(''Sharing your contact details is against our terms and conditions'')?></p>\r\n\r\n<input type="hidden" name="Message[to_user_id]" value="<?= $to_user_id ?>" />\r\n<input type="hidden" name="Message[object_id]" value="<?= $quote->id ?>" />\r\n<input type="hidden" name="Message[object_class]" value="<?= get_class($quote) ?>" />\r\n\r\n<script>\r\n\r\nPage.controlConversationFormFields = $.phpr.form().defineFields(function() {\r\n	this.defineField(''Message[message]'').required("<?=__(''Please specify a message'',true)?>");\r\n});\r\n\r\n</script>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(12, 'dash:booking_panel', 'dash;booking_panel', '<?\r\n	$bookings = Bluebell_Request::get_bookings($this->user)->find_all();\r\n?>\r\n<div class="well">\r\n	<h5><?=__(''Upcoming Appointments'')?></h5>\r\n	<ul class="square">\r\n		<? if (count($bookings)): ?>\r\n			<? foreach ($bookings as $booking): ?>\r\n				<li>\r\n					<a href="<?=$booking->get_url(''job/booking'')?>">\r\n						<?=__(''%s Request'', ''<strong>''.$booking->title.''</strong>'', true)?> - \r\n						<?=Bluebell_Request::location($booking)?>\r\n					</a>\r\n				</li>\r\n			<? endforeach ?>\r\n		<? else: ?>\r\n			<li><?=__(''You have no upcoming appointments'',true)?></li>\r\n		<? endif ?>\r\n	</ul>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(13, 'dash:job_panel', 'dash;job_panel', '<div class="row-fluid job">\r\n	<div class="span8">\r\n		<div class="title"><a href="<?=$job->get_url(''job'')?>"><?=$job->title?></a></div>\r\n		<div class="info"><?=Bluebell_Request::required_by($job)?> | <?=Bluebell_Request::location($job)?></div>\r\n		<div class="description"><?=$job->description_summary?></div>\r\n	</div>\r\n	<div class="span4">\r\n		<div class="status">\r\n			<div class="time">\r\n				<? if ($job->status_code==Service_Status::status_active): ?>\r\n					<?=__(''Quoting closes in'')?>\r\n					<span><?=$job->get_remaining_time(true)?></span>\r\n				<? endif ?>\r\n			</div>\r\n			<? if ($job->status_code==Service_Status::status_cancelled): ?>\r\n				<div><?=__(''Job cancelled'')?></div>\r\n			<? elseif ($job->status_code==Service_Status::status_active): ?>\r\n				<div><a href="<?=$job->get_url(''job'')?>" class="btn btn-primary btn-small"><?=__(''View Details'')?></a></div>\r\n			<? else: ?>\r\n				<div><?=__(''Quoting closed'')?></div>\r\n			<? endif ?>\r\n		</div>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(14, 'dash:message_panel', 'dash;message_panel', '<?\r\n	$new_messages = User_Message::check_new_messages($this->user);\r\n?>\r\n<div class="well">\r\n	<h5><?=__(''Messages'',true)?></h5>\r\n	<ul class="square">\r\n		<li>\r\n			<a href="<?=root_url(''messages'')?>">\r\n				<? if ($new_messages > 0): ?>\r\n					<?=__(''You have %s new message(s)'', $new_messages, true)?>\r\n				<? else: ?>\r\n					<?=__(''No new messages'',true)?>\r\n				<? endif ?>\r\n			</a>\r\n		</li>\r\n	</ul>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(15, 'dash:new_user', 'dash;new_user', '<div class="row">\r\n	<div class="span9">\r\n\r\n		<h3><?=__(''How would you like to start using %s?'', c(''site_name''))?></h3>\r\n\r\n		<div class="tabbable">\r\n			 <ul class="nav nav-tabs">\r\n				 <li class="active"><a href="#request_services" data-toggle="tab"><?=__(''Request Services'')?></a></li>\r\n				 <li><a href="#provide_services" data-toggle="tab"><?=__(''Provide Services'')?></a></li>\r\n			 </ul>\r\n			<div class="tab-content">\r\n				<div class="tab-pane active" id="request_services">\r\n					<?=form_open(array(''action''=>root_url(''request''), ''id''=>''submit_request''))?>\r\n						<div class="row-fluid">\r\n							<div class="span5 align-center">\r\n								<?=form_label(__(''What service are you looking for?''))?>\r\n							</div>\r\n							<div class="span7">\r\n								\r\n									<div class="input-append">\r\n										<input type="text" name="role_name" value="" class="oversize" id="request_title" />\r\n										<?=form_submit(''submit'', ''Get started'', ''class="btn btn-primary"'')?>\r\n									</div>\r\n\r\n								<div class="small"><?=__(''eg: Web Developer, Computer Programmer'')?></div>\r\n								<script> Page.bindRoleSelect(''#request_title''); </script>\r\n							</div>\r\n						</div>\r\n					<?=form_close()?>\r\n				</div>\r\n				<div class="tab-pane" id="provide_services">\r\n					<?=form_open(array(''id''=>''create_profile'', ''onsubmit''=>"window.location = ''".root_url(''provide/create'')."'';return false"))?>\r\n						<div class="row-fluid">\r\n							<div class="span6 align-center">\r\n								<?=form_label(__(''Create a profile and get free job leads!''))?>\r\n							</div>\r\n							<div class="span6">\r\n								<?=form_submit(''submit'', ''Create profile'', ''class="btn btn-primary"'')?>\r\n							</div>\r\n						</div>\r\n					<?=form_close()?>\r\n				</div>\r\n			</div>\r\n		</div>\r\n\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(16, 'dash:offers', 'dash;offers', '<?\r\n	$filter = (isset($filter)) ? $filter : ''active'';\r\n?>\r\n<div class="row-fluid">\r\n	<div class="span6">\r\n		<ul class="nav nav-pills offer-filter">\r\n			<li class="disabled"><a href="javascript:;"><?=__(''Filter'', true)?>:</a></li>\r\n			<li class="<?=($filter=="active")?''active'':''''?>"><a href="javascript:;" data-filter-type="active"><?=__(''Offers'')?></a></li>\r\n			<li class="<?=($filter=="booked")?''active'':''''?>"><a href="javascript:;" data-filter-type="booked"><?=__(''Won'')?></a></li>\r\n			<li class="<?=($filter=="ended")?''active'':''''?>"><a href="javascript:;" data-filter-type="ended"><?=__(''Lost'')?></a></li>\r\n		</ul>\r\n	</div>\r\n	<div class="span6">\r\n		<? if ($request_max_bids = c(''request_max_bids'', ''service'')): ?>\r\n			<div class="request_max_bids"><span class="secondary label radius"><?=__(''Quoting closes when a job gets %s quotes'', $request_max_bids)?></span></div>\r\n		<? endif ?>\r\n	</div>\r\n</div>\r\n\r\n<div class="row-fluid titles hide-for-small">\r\n	<div class="span8"><strong><?=__(''Job'',true)?></strong></div>\r\n	<div class="span4 align-center"><strong><?=__(''Status'',true)?></strong></div>\r\n</div>\r\n\r\n<? foreach ($jobs as $index=>$job): ?>\r\n	<? if ($index != 0): ?><hr /><? endif ?>\r\n	<?=$this->display_partial(''dash:job_panel'', array(''job''=>$job))?>\r\n<? endforeach ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(17, 'dash:provide_panel', 'dash;provide_panel', '<?\r\n	$provider = (isset($provider_profile)) ? $provider_profile : null;\r\n?>\r\n<? if ($provider): ?>\r\n	<h4><?=$provider->role_name?> <?=__(''profile'', true)?></h4>\r\n	<div class="row-fluid">\r\n		<div class="span3">\r\n			<div class="header"><?=__(''Earned'')?></div>\r\n			<div class="large align-center"><?=format_currency($provider->stat_earned)?></div>\r\n		</div>\r\n		<div class="span5">\r\n			<div class="header"><?=__(''Jobs'')?></div>\r\n			<ul class="block-grid grid-span3">\r\n				<li class="align-center"><div class="large"><?=$provider->stat_offers?></div><div><?=__(''offers'')?></div></li>\r\n				<li class="align-center"><div class="large"><?=$provider->stat_quotes?></div><div><?=__(''quotes'')?></div></li>\r\n				<li class="align-center"><div class="large"><?=$provider->stat_wins?></div><div><?=__(''wins'')?></div></li>\r\n			</ul>\r\n		</div>\r\n		<div class="span4">\r\n			<div class="header"><?=__(''Rating'')?></div>\r\n			<p class="star-rating align-center">\r\n				<i class="rating-<?=$provider->rating*10?>">&nbsp;</i>\r\n				<br />\r\n				<? if ($provider->rating_num > 0): ?>\r\n					(<?=__(''%s reviews'', $provider->rating_num)?>)\r\n				<? else: ?>\r\n					(<?=__(''No ratings yet'')?>)\r\n				<? endif?>\r\n			</p>\r\n		\r\n		</div>\r\n	</div>\r\n	<div class="align-right">\r\n		<a href="<?=root_url(''provide/manage/''.$provider->id)?>" class="btn btn-small"><?=__(''View/Edit Profile'')?></a>\r\n	</div>\r\n<? endif?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(18, 'dash:request_panel', 'dash;request_panel', '<div class="row-fluid request">\r\n	<div class="span7 columns">\r\n		<div class="title"><a href="<?=$request->get_url(''request/manage'')?>"><?=__(''%s Request'', $request->title)?></a></div>\r\n		<div class="info"><?=Bluebell_Request::required_by($request)?> | <?=Bluebell_Request::location($request)?></div>\r\n		<div class="description"><?=$request->description_summary?></div>\r\n	</div>\r\n	<div class="span5 columns">\r\n		<ul class="block-grid grid-span2">\r\n			<li class="time">\r\n				<? if ($request->status_code == Service_Status::status_active): ?>\r\n					<span><?=$request->get_remaining_time(true)?></span>\r\n					<?=__(''to get quotes'')?>\r\n				<? endif ?>\r\n			</li>\r\n			<li class="status">\r\n				<? \r\n					$show_button = true; \r\n				?>\r\n				<p>\r\n					<? if ($request->status_code == Service_Status::status_active): ?>\r\n						<span class="quotes">\r\n							<a href="<?=$request->get_url(''request/manage'')?>#quotes"><?=$request->total_quotes?></a>\r\n						</span> \r\n						<span>\r\n							<?=__(''quote received'')?>\r\n						</span>\r\n\r\n						<a href="<?=$request->get_url(''request/manage'')?>" class="btn btn-small btn-primary">\r\n							<?=__(''View Details'')?>\r\n						</a>\r\n\r\n					<? elseif ($request->status_code == Service_Status::status_cancelled): ?>\r\n						<span>\r\n							<?=__(''Request cancelled by consumer'')?>\r\n						</span>\r\n					\r\n					<? elseif ($request->status_code == Service_Status::status_expired): ?>\r\n						<span class="label label-warning">\r\n							<?=__(''Quote period has ended'')?>\r\n						</span>\r\n					\r\n					<? elseif ($request->status_code == Bluebell_Request::status_booked): ?>\r\n						<span class="label label-warning">\r\n							<i class="icon-calendar-empty"></i> <?=__(''Job scheduled'')?>\r\n						</span>\r\n\r\n						<a href="<?=$request->get_url(''request/manage'')?>" class="btn btn-small">\r\n							<?=__(''View Booking'')?>\r\n						</a>\r\n\r\n					<? elseif ($request->status_code == Service_Status::status_closed || $request->status_code == Service_Status::status_archived): ?>\r\n						<span class="label label-success">\r\n							<i class="icon-ok"></i> <?=__(''Job completed'')?>\r\n						</span>\r\n\r\n						<a href="<?=$request->get_url(''request/manage'')?>" class="btn btn-small">\r\n							<?=__(''View Booking'')?>\r\n						</a>\r\n					\r\n					<? endif ?>\r\n				</p>\r\n			</li>\r\n		</ul>\r\n	</div>\r\n</div>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(19, 'dash:requests', 'dash;requests', '<? foreach ($requests as $index=>$request): ?>\r\n	<? if ($index != 0): ?><hr /><? endif ?>\r\n	<?=$this->display_partial(''dash:request_panel'', array(''request''=>$request))?>\r\n<? endforeach ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(20, 'dash:review_panel', 'dash;review_panel', '<?\r\n	$bookings = Bluebell_Request::get_need_review($this->user)->find_all();\r\n?>\r\n<div class="well">\r\n	<h5><?=__(''Write Reviews'')?></h5>\r\n	<ul class="square">\r\n		<? if (count($bookings)): ?>\r\n			<? foreach ($bookings as $booking): ?>\r\n				<li>\r\n					<a href="<?=$booking->get_url(''job/booking'')?>">\r\n						<?=__(''%s Request'', ''<strong>''.$booking->title.''</strong>'', true)?> - \r\n						<?=Bluebell_Request::location($booking)?>\r\n					</a>\r\n				</li>\r\n			<? endforeach ?>\r\n		<? else: ?>\r\n			<li><?=__(''No reviews required'',true)?></li>\r\n		<? endif ?>\r\n	</ul>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(21, 'dash:top_provider_panel', 'dash;top_provider_panel', '<?\r\n	$top_providers = Service_Provider::create()->find_top_providers($this->user)->limit(3)->find_all();\r\n?>\r\n<? if ($top_providers->count > 0): ?>\r\n	<h5><?=__(''View top profiles'')?></h5>\r\n	<? foreach ($top_providers as $top_provider): ?>\r\n		<p>\r\n			<a href="<?=$top_provider->get_url(''profile'')?>"><?=$top_provider->business_name?></a>\r\n			<br /><?=$top_provider->role_name?>\r\n		</p>\r\n	<? endforeach ?>\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(22, 'dash:welcome', 'dash;welcome', '<? if ($this->user->is_provider): ?>\r\n<div class="box">\r\n	<div class="box-header">\r\n		<h6><?=__(''Welcome to %s'', c(''site_name''))?></h6>\r\n	</div>\r\n	<div class="box-content">\r\n		\r\n		<div class="row-fluid">\r\n			<div class="span6">\r\n				<div class="row-fluid">\r\n					<div class="span5 mobile-span2">\r\n						<div class="avatar">\r\n							<img src="<?=Bluebell_Provider::avatar($latest_profile)?>" alt="<?=$latest_profile->business_name?>" class="img-polaroid" />\r\n						</div>\r\n					</div>\r\n					<div class="span7 mobile-span2">\r\n						<div class="business_name">\r\n							<p class="name"><a href="<?=root_url(''provide/manage/''.$latest_profile->id)?>"><?=$latest_profile->business_name?></a></p>\r\n							<p class="title"><strong><?=$latest_profile->role_name?></strong></p>\r\n							<p class="location"><?=$latest_profile->location_string?></p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<div class="span6">\r\n				<ul class="block-grid grid-span3 welcome_icons">\r\n					<li>\r\n						<a href="<?= root_url(''provide/profiles'') ?>">\r\n							<i class="icon-edit"></i> \r\n							<span class="link-button"><?=__(''Profile'')?></a>\r\n						</a>\r\n					</li>\r\n					<li>\r\n						<a href="<?= root_url(''account'') ?>">\r\n							<i class="icon-user"></i> \r\n							<span class="link-button"><?=__(''Account'')?></span>\r\n						</a>\r\n					</li>\r\n					<li>\r\n						<a href="<?= root_url(''provide/how-it-works'') ?>">\r\n							<i class="icon-book"></i> \r\n							<span class="link-button"><?=__(''Learn More'')?></span>\r\n						</a>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n\r\n	</div>\r\n</div>\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(23, 'directory:breadcrumb', 'directory;breadcrumb', '<ul class="breadcrumb">\r\n	<li><a href="<?=root_url(''directory'')?>"><?=__(''Directory'', true)?></a> <i class="icon-chevron-right divider"></i></li>\r\n	\r\n	<? // Browse ?>\r\n	<? if ($parent_mode==''browse''): ?><li><a href="<?=root_url(''directory/browse'')?>"><?=__(''Browse'', true)?></a> <i class="icon-chevron-right divider"></i></li><? endif ?>\r\n	<? if (isset($category->name)): ?><li><a href="<?=$dir_url.=''/''.strtolower($category->url_name)?>"><?=$category->name?></a> <i class="icon-chevron-right divider"></i></li><? endif ?>\r\n\r\n	<? // Location ?>\r\n	<? if (isset($country->name)): ?><li><a href="<?=$dir_url.=''/''.strtolower($country->code)?>"><?=$country->name?></a> <i class="icon-chevron-right divider"></i></li><? endif ?>\r\n	<? if (isset($state->name)): ?><li><a href="<?=$dir_url.=''/''.strtolower($state->code)?>"><?=$state->name?></a> <i class="icon-chevron-right divider"></i></li><? endif ?>\r\n	<? if (isset($city->name)): ?><li><a href="<?=$dir_url.=''/''.$city->url_name?>"><?=$city->name?></a> <i class="icon-chevron-right divider"></i></li><? endif ?>\r\n\r\n	<? // Role ?>\r\n	<? if (isset($role->name)): ?><li><a href="javascript:;"><?=$role->name?></a> <i class="icon-chevron-right divider"></i></li><? endif ?>\r\n</ul>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(24, 'directory:browse', 'directory;browse', '\r\n<?=$this->display_partial(''directory:breadcrumb'')?>\r\n\r\n<div class="page-header">\r\n	<h1><?=$this->page->title_name?></h1>\r\n	<h4 class="subheader"><?=$this->page->description?></h4>\r\n</div>\r\n\r\n<div class="row">\r\n	<div class="span3">\r\n		<? foreach ($categories as $key=>$category): ?>\r\n			<? if (($key) % 4 != 0) continue; // 1/4 ?>\r\n			<h4><a href="<?=root_url(''directory/b/''.$category->url_name)?>"><?=$category->name?></a></h4>\r\n			<ul>\r\n				<? foreach ($category->list_children() as $subcategory): ?>\r\n					<li><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$subcategory->url_name)?>"><?=$subcategory->name?></a>\r\n				<? endforeach ?>\r\n			</ul>\r\n		<? endforeach ?>\r\n	</div>\r\n	<div class="span3">\r\n		<? foreach ($categories as $key=>$category): ?>\r\n			<? if (($key+1) % 4 != 0) continue; // 2/4 ?>\r\n			<h4><a href="<?=root_url(''directory/b/''.$category->url_name)?>"><?=$category->name?></a></h4>\r\n			<ul>\r\n				<? foreach ($category->list_children() as $subcategory): ?>\r\n					<li><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$subcategory->url_name)?>"><?=$subcategory->name?></a>\r\n				<? endforeach ?>\r\n			</ul>\r\n		<? endforeach ?>\r\n	</div>\r\n	<div class="span3">\r\n		<? foreach ($categories as $key=>$category): ?>\r\n			<? if (($key+2) % 4 != 0) continue; // 3/4 ?>\r\n			<h4><a href="<?=root_url(''directory/b/''.$category->url_name)?>"><?=$category->name?></a></h4>\r\n			<ul>\r\n				<? foreach ($category->list_children() as $subcategory): ?>\r\n					<li><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$subcategory->url_name)?>"><?=$subcategory->name?></a>\r\n				<? endforeach ?>\r\n			</ul>\r\n		<? endforeach ?>        \r\n	</div>\r\n	<div class="span3">\r\n		<? foreach ($categories as $key=>$category): ?>\r\n			<? if (($key+3) % 4 != 0) continue; // 4/4 ?>\r\n			<h4><a href="<?=root_url(''directory/b/''.$category->url_name)?>"><?=$category->name?></a></h4>\r\n			<ul>\r\n				<? foreach ($category->list_children() as $subcategory): ?>\r\n					<li><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$subcategory->url_name)?>"><?=$subcategory->name?></a>\r\n				<? endforeach ?>\r\n			</ul>\r\n		<? endforeach ?>           \r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(25, 'directory:category', 'directory;category', '\n<?=$this->display_partial(''directory:breadcrumb'')?>\n\n<div class="page-header">\n	<h1><?=$this->page->title_name?></h1>\n	<h4 class="subheader"><?=$this->page->description?></h4>\n</div>\n\n<? if (count($roles)): ?>\n	<div class="row">\n		<div class="span3">\n			<? foreach ($roles as $key=>$role): ?>\n				<? if (($key) % 4 != 0) continue; // 1/4 ?>\n				<h5><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$role->url_name)?>"><?=$role->name?></a></h5>\n			<? endforeach ?>\n		</div>\n		<div class="span3">\n			<? foreach ($roles as $key=>$role): ?>\n				<? if (($key+1) % 4 != 0) continue; // 2/4 ?>\n				<h5><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$role->url_name)?>"><?=$role->name?></a></h5>\n			<? endforeach ?>\n		</div>\n		<div class="span3">\n			<? foreach ($roles as $key=>$role): ?>\n				<? if (($key+2) % 4 != 0) continue; // 3/4 ?>\n				<h5><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$role->url_name)?>"><?=$role->name?></a></h5>\n			<? endforeach ?>\n		</div>\n		<div class="span3">\n			<? foreach ($roles as $key=>$role): ?>\n				<? if (($key+3) % 4 != 0) continue; // 4/4 ?>\n				<h5><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$role->url_name)?>"><?=$role->name?></a></h5>\n			<? endforeach ?>\n		</div>\n	</div>\n<? else: ?>\n	<?=global_content_block(''directory_not_found'', ''Directory no providers found'')?>\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(26, 'directory:city', 'directory;city', '\r\n<?=$this->display_partial(''directory:breadcrumb'')?>\r\n\r\n<div class="page-header">\r\n	<h1><?=$this->page->title_name?></h1>\r\n	<h4 class="subheader"><?=$this->page->description?></h4>\r\n</div>\r\n<div class="row-fluid">\r\n	<div class="span8">\r\n\r\n		<? if ($city): ?>\r\n			<div id="p_directory_search_form">\r\n				<?=$this->display_partial(''directory:search_form'', array(''placeholder''=>__(''Search for providers in %s. Eg: Plumber, Gardener, etc.'', $city->name . '', '' . $state->code)))?>\r\n			</div>\r\n			<hr />\r\n		<? endif ?>\r\n\r\n\r\n		<? if ($roles): ?>\r\n			<h4><?=__(''Browse roles'')?></h4>\r\n			<ul class="block-grid grid-span3">\r\n				<? foreach ($roles as $role): ?>\r\n				<?\r\n					$link = strtolower(root_url(''directory/a/''.$country->code.''/''.$state->code.''/''.$city->url_name.''/''.Phpr_Inflector::slugify($role->name).''/''.$role->id.''/top''));\r\n				?>\r\n					<li><a href="<?=$link?>"><?=$role->name?></a></li>\r\n				<? endforeach ?>\r\n			</ul>\r\n		<? else: ?>\r\n			<?=global_content_block(''directory_not_found'', ''Directory no providers found'')?>\r\n		<? endif ?>\r\n	</div>\r\n	<div class="span4">\r\n		<div class="well">\r\n			<div id="p_directory_request_panel"><?=$this->display_partial(''directory:request_panel'')?></div>\r\n		</div>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(27, 'directory:country', 'directory;country', '\r\n<?=$this->display_partial(''directory:breadcrumb'')?>\r\n\r\n<div class="page-header">\r\n	<h1><?=$this->page->title_name?></h1>\r\n	<h4 class="subheader"><?=$this->page->description?></h4>\r\n</div>\r\n<div class="row-fluid">\r\n	<div class="span8">\r\n\r\n		<?=form_open(array(''id''=>''form_directory_search''))?>\r\n			<div id="p_directory_search_form">\r\n				<?=$this->display_partial(''directory:search_form'', array(''placeholder''=>__(''Search for providers: Enter your address or zip/postal code'')))?>\r\n			</div>\r\n		<?=form_close()?>\r\n\r\n		<hr />\r\n\r\n		<? if ($country): ?>\r\n			<h5><?=__(''Browse %s Areas'', $country->code)?></h5>\r\n			<ul class="block-grid grid-span3">\r\n				<? foreach ($country->states as $state): ?>\r\n					<li><a href="<?=strtolower(root_url(''directory/a/''.$country->code.''/''.$state->code))?>"><strong><?=$state->code?></strong> - <?=$state->name?></a></li>\r\n				<? endforeach ?>\r\n			</ul>\r\n		<? else: ?>\r\n			<?=global_content_block(''directory_not_found'', ''Directory no providers found'')?>\r\n		<? endif ?>\r\n	</div>\r\n    <div class="span4">\r\n        <div class="well">\r\n            <div id="p_directory_request_panel"><?=$this->display_partial(''directory:request_panel'')?></div>\r\n        </div>\r\n    </div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(28, 'directory:letter', 'directory;letter', '<?\r\n	$providers = $providers->find_all();\r\n?>\r\n<div class="page-header">\r\n	<h1><?=$this->page->title_name?></h1>\r\n	<h4 class="subheader"><?=$this->page->description?></h4>\r\n</div>\r\n<div class="pagination">\r\n	<ul>\r\n		<? foreach (range(''a'', ''z'') as $letter): ?>\r\n		<li<?=$letter==$current_letter?'' class="current"'':''''?>><a href="<?=root_url(''directory/l/''.$letter)?>" onclick="return Page.selectLetter(''<?=$letter?>'')"><?=strtoupper($letter)?></a></li>\r\n		<? endforeach ?>\r\n	</ul>\r\n</div>\r\n<div class="row-fluid">\r\n	<div class="span6 columns">\r\n		<? foreach ($providers as $index=>$provider): ?>\r\n			<? if (!($index & 1)):?>\r\n				<div class="business_name"><a href="<?=$provider->get_url(''profile'')?>"><?=$provider->business_name?> - <?=$provider->role_name?></a></div>\r\n			<? endif ?>\r\n		<? endforeach ?>\r\n	</div>\r\n	<div class="span6 columns">\r\n		<? foreach ($providers as $index=>$provider): ?>\r\n			<? if ($index & 1):?>\r\n				<div class="business_name"><a href="<?=$provider->get_url(''profile'')?>"><?=$provider->business_name?> - <?=$provider->role_name?></a></div>\r\n			<? endif ?>\r\n		<? endforeach ?>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(29, 'directory:request_panel', 'directory;request_panel', '<?\r\n	$request = (isset($request)) ? $request : null;\r\n?>\r\n<? if (!$request): ?>\r\n	<h4><?=__(''Request a free quote!'')?></h4>\r\n	<?=form_open(array(''id''=>''form_request_panel''))?>\r\n		<input type="hidden" name="open_request" value="1" />\r\n		<?=$this->display_partial(''request:quick_form'')?>\r\n		<?=form_submit(''submit'', __(''Send request''), ''class="btn btn-primary btn-block popup-close" id="submit_request_panel"'')?>\r\n	<?=form_close()?>\r\n<? else: ?>\r\n	<?=global_content_block(''directory_request_submit'', ''Directory request submitted'')?>\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL);
INSERT INTO `cms_partials` (`id`, `name`, `file_name`, `content`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(30, 'directory:role', 'directory;role', '\r\n<?=$this->display_partial(''directory:breadcrumb'')?>\r\n\r\n<?\r\n	$providers = $providers->find_all();\r\n\r\n	if ($parent_mode == ''area'')\r\n		$link = strtolower(root_url(''directory/a/''.$country->code.''/''.$state->code.''/''.$city->url_name.''/''.$role->url_name));\r\n	else \r\n		$link = strtolower(root_url(''directory/b/''.$category->url_name.''/''.$role->url_name));\r\n?>\r\n<div class="page-header">\r\n	<h1><?=$this->page->title_name?></h1>\r\n	<h4 class="subheader"><?=$this->page->description?></h4>\r\n</div>\r\n\r\n<div class="row">\r\n	<div class="span9">\r\n		<? if ($providers->count): ?>\r\n			<ul class="nav nav-pills">\r\n				<li class="disabled"><a href="javascript:;"><?=__(''Filter'', true)?>:</a></li>\r\n				<li class="<?=($filter==''top'')?''active'':''''?>"><a href="<?=$link.''/top''?>"><?=__(''Top Rated'')?></a></li>\r\n				<li class="<?=($filter==''cheap'')?''active'':''''?>"><a href="<?=$link.''/cheap''?>"><?=__(''Cheapest'')?></a></li>\r\n				<li class="<?=($filter==''reliable'')?''active'':''''?>"><a href="<?=$link.''/reliable''?>"><?=__(''Most Reliable'')?></a></li>\r\n			</ul>\r\n\r\n			<div class="provider_list">\r\n				<? foreach ($providers as $provider): ?>\r\n					<div class="provider">\r\n						<div class="row-fluid">\r\n							<div class="span6">\r\n								<div class="badge-control">\r\n									<?=$this->display_partial(''control:badge'', array(''provider''=>$provider, ''badge_mode''=>''detailed''))?>\r\n								</div>\r\n							</div>\r\n							<div class="span6">\r\n								<? if ($provider->ratings->count): ?>\r\n								<? $rating = $provider->ratings->first(); ?>\r\n								<p>\r\n									<strong><?=__(''Recent job for a %s'', $rating->request_title)?>:</strong><br />\r\n									<?=$rating->comment?>\r\n								</p>\r\n								<? else: ?>\r\n									<p><?=__(''This provider has not been rated yet'')?></p>\r\n								<? endif ?>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				<? endforeach ?>\r\n			</div>\r\n		<? else: ?>\r\n			<?=global_content_block(''directory_not_found'', ''Directory no providers found'')?>\r\n		<? endif ?>\r\n	</div>\r\n	<div class="span3">\r\n		<div class="well">\r\n			<h4><?=__(''Request a free quote from a %s'', $role->name)?></h4>\r\n			<p><?=__(''Been searching high and low for the best %s to meet your needs? Look no further...'', $role->name)?></p>\r\n			<p><a href="javascript:;" id="button_request_popup" class="btn btn-primary btn-block"><?=__(''Get a free quote!'')?></a></p>\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n<div id="popup_request" class="modal hide fade" tabindex="-1" role="dialog">\r\n	<?=form_open(array(''id''=>''form_request''))?>\r\n		<div class="modal-header">\r\n			<h3><?=__(''Get Quotes from a %s'', $role->name)?></h3>\r\n		</div>\r\n		<div class="modal-body">\r\n			<?=$this->display_partial(''request:quick_form'', array(''role''=>$role))?>\r\n		</div>\r\n		<div class="modal-footer">\r\n			<?=form_submit(''submit'', __(''Send request''), ''class="btn btn-primary" id="button_submit_request"'')?>\r\n		</div>\r\n	<?=form_close()?>\r\n</div>\r\n\r\n<div id="popup_request_success" class="modal hide fade" tabindex="-1" role="dialog">\r\n	<div class="modal-body">\r\n		<?=global_content_block(''directory_request_submit'', ''Directory request submitted'')?>\r\n	</div>\r\n	<div class="modal-footer">\r\n		<?=form_button(''close'', __(''Close'', true), ''class="btn popup-close"'')?>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(31, 'directory:search_form', 'directory;search_form', '<?\r\n	$placeholder = isset($placeholder) ? $placeholder : '''';\r\n?>\r\n<div class="control-group">\r\n	<div class="controls">\r\n		<div class="input-append span12">\r\n			<input type="text" id="address" \r\n				name="address" \r\n				class="span10"\r\n				placeholder="<?=$placeholder?>">\r\n			<?=form_submit(''submit'', __(''Search''), ''class="btn"'')?>\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n<div class="clearfix"></div>\r\n\r\n<script>\r\n\r\nPage.directorySearchFormFields = $.phpr.form().defineFields(function(){\r\n	this.defineField(''address'', ''Address'').required("<?=__(''Please provide a valid location'',true)?>").action(''location:on_validate_address'', "<?=__(''Please provide a valid location'', true)?>");\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(32, 'directory:state', 'directory;state', '\r\n<?=$this->display_partial(''directory:breadcrumb'')?>\r\n\r\n<div class="page-header">\r\n	<h1><?=$this->page->title_name?></h1>\r\n	<h4 class="subheader"><?=$this->page->description?></h4>\r\n</div>\r\n<div class="row-fluid">\r\n	<div class="span8">\r\n		\r\n		<?=form_open(array(''id''=>''form_directory_search''))?>\r\n			<div id="p_directory_search_form">\r\n				<?=$this->display_partial(''directory:search_form'', array(''placeholder''=>__(''Search for providers: Enter your address or zip/postal code'')))?>\r\n			</div>\r\n		<?=form_close()?>\r\n\r\n		<hr />\r\n\r\n		<? if ($cities->count && $state): ?>\r\n			<h4><?=__(''Browse %s Areas'', $state->code)?></h4>\r\n			<ul class="block-grid grid-span3">\r\n				<? foreach ($cities as $city): ?>\r\n					<li><a href="<?=strtolower(root_url(''directory/a/''.$country->code.''/''.$state->code.''/''.$city->url_name))?>"><?=$city->name?></a></li>\r\n				<? endforeach ?>\r\n			</ul>\r\n		<? else: ?>\r\n			<?=global_content_block(''directory_not_found'', ''Directory no providers found'')?>\r\n		<? endif ?>\r\n\r\n	</div>\r\n	<div class="span4">\r\n		<div class="well">\r\n			<div id="p_directory_request_panel"><?=$this->display_partial(''directory:request_panel'')?></div>\r\n		</div>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(33, 'home:activity_feed', 'home;activity_feed', '<? if ($activity): ?>\n	<ul class="small-block-grid-1">\n		<? foreach ($activity as $key=>$feed_item): ?>\n			<? \n				if (!$feed_item->user) \n					continue;\n				\n				$is_last = !($key==($activity->count - 1));\n			?>\n			<li class="item">\n				<div class="<?=$is_last?''separator bottom'':''''?>">\n					<?\n						$class_name = get_class($feed_item);\n					?>\n					<? if ($class_name == "Service_Request"): ?>\n						<div class="avatar"><img src="<?=Bluebell_User::avatar($feed_item->user)?>" alt="" /></div>\n						<div class="title">\n							<a href="<?=$feed_item->get_url(''job'')?>"><?=$feed_item->user->username?></a> \n							<small><?=__(''has a new request'')?></small>\n						</div>\n						<div class="description">\n							<i class="icon-bullhorn"></i>\n							<strong><?=$feed_item->title?></strong> \n							<?=Phpr_String::limit_words($feed_item->description, 20)?>\n						</div>\n						<div class="info"><?=__(''%s ago'', Phpr_DateTime::interval_to_now($feed_item->created_at), true)?> – <?=Bluebell_Request::location($feed_item, true)?></div>    \n					<? elseif ($class_name == "Service_Provider"): ?>\n						<div class="avatar"><img src="<?=Bluebell_Provider::avatar($feed_item)?>" alt="" /></div>\n						<div class="title">\n							<a href="<?=$feed_item->get_url(''profile'')?>"><?=$feed_item->business_name?></a> \n							<small><?=__(''has a new service'')?></small>\n						</div>\n						<div class="description">\n							<i class="icon-user"></i>\n							<strong><?=$feed_item->role_name?></strong> \n							<?=Phpr_String::limit_words($feed_item->description, 20)?>\n						</div>\n						<div class="info"><?=__(''%s ago'', Phpr_DateTime::interval_to_now($feed_item->created_at), true)?> – <?=$feed_item->location_string?></div>\n					<? endif ?>\n				</div>\n			</li>\n		<? endforeach ?>\n	</ul>\n<? else: ?>\n	<p>No activity here yet! Check back again soon...</p>\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(34, 'job:ask_question', 'job;ask_question', '<?\r\n	$can_ask = (isset($can_ask)) ? $can_ask : true;\r\n	if (!$this->user) $can_ask = false;    \r\n?>\r\n<? if ($questions->count): ?>\r\n	<h5><?=__(''Job Questions'')?></h5>\r\n	<ul id="job_questions">\r\n		<? foreach ($questions as $question): ?>\r\n		<li>\r\n			<? if ($question->answer): ?>\r\n				<a href="javascript:;" class="question"><?=$question->description?></a>\r\n				<span class="answer"><?=$question->answer->description?></span>\r\n			<? else: ?>\r\n				<span class="question"><?=$question->description?></span>\r\n			<? endif ?>\r\n		</li>\r\n		<? endforeach ?>\r\n	</ul>\r\n<? endif ?>\r\n\r\n<? if ($can_ask): ?>\r\n	<p><a href="javascript:;" id="link_ask_question" class="btn btn-primary btn-block"><?=__(''Ask a question'')?></a></p>\r\n	<div id="ask_question" style="display:none">\r\n		<?=form_open(array(''id''=>''ask_question_form''))?>\r\n\r\n			<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n			<input type="hidden" name="Question[is_public]" value="1" />\r\n\r\n			<div class="control-group">\r\n				<div class="controls">\r\n					<textarea name="Question[description]" class="span12" placeholder="<?= __("Enter your question here") ?>"></textarea>\r\n				</div>\r\n			</div>\r\n			<?=form_submit(''ask'', __(''Submit'', true), ''class="btn btn-success btn-block"'')?>\r\n\r\n		<?=form_close()?>\r\n	</div>\r\n\r\n	<script>\r\n		Page.askQuestionFormFields = $.phpr.form().defineFields(function(){\r\n			this.defineField(''Question[description]'', ''Question Description'').required("<?=__(''Please enter your question'')?>")\r\n		});\r\n	</script>    \r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(35, 'job:booking_summary', 'job;booking_summary', '<div class="row-fluid">\r\n	<div class="span8">\r\n\r\n		<? if (!$is_cancelled): ?>\r\n			<div id="p_job_booking_time"><?=$this->display_partial(''job:booking_time'', array(''quote''=>$quote))?></div>\r\n		<? else: ?>\r\n			<div class="row-fluid">\r\n				<div class="span4"><p class="detail_label"><?=__(''Start time'')?></p></div>\r\n				<div class="span8">\r\n					<p class="detail_text">\r\n						<?=__(''Booking cancelled'')?>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		<? endif ?>\r\n\r\n		<div class="row-fluid">\r\n			<div class="span4"><p class="detail_label"><?=__(''Contact info'')?></p></div>\r\n			<div class="span8">\r\n				<p class="detail_text">\r\n					<?=$provider->business_name?><br />\r\n					<?=$provider->phone?> <?=($provider->phone&&$provider->mobile)?''/'':'''' ?> <?=$provider->mobile?><br />\r\n					<?=mailto_encode($provider->user->email)?>\r\n				</p>\r\n			</div>\r\n		</div>\r\n\r\n		<div class="row-fluid">\r\n			<div class="span4"><p class="detail_label"><?=__(''Price quoted'')?></p></div>\r\n			<div class="span8">\r\n				<p class="detail_price">\r\n					<?=format_currency($quote->price)?>\r\n					<span class="detail_price_summary">(<?=Bluebell_Quote::price_summary($quote)?>)</span>\r\n					<? if (Bluebell_Quote::price_terms($quote)): ?>\r\n						<span class="detail_price_terms">* <?=Bluebell_Quote::price_terms($quote)?></span>\r\n					<? endif ?>\r\n				</p>\r\n			</div>\r\n		</div>\r\n\r\n		<div class="row-fluid">\r\n			<div class="span4"><p class="detail_label"><?=__(''Personal Note'')?></p></div>\r\n			<div class="span8"><p class="detail_text"><?=Phpr_String::show_more_link($quote->comment, 150, __(''Show more'', true))?></p></div>\r\n		</div>\r\n\r\n	</div>\r\n	<div class="span4">\r\n		<div class="map" id="map_booking_address"><?=__(''Loading map...'')?></div>\r\n		<div class="address" id="booking_address"><?=$request->user->address_string?></div>\r\n	</div>    \r\n</div>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(36, 'job:booking_time', 'job;booking_time', '<div class="row-fluid">\r\n	<div class="span4"><p class="detail_label"><?=__(''Start time'')?></p></div>\r\n	<div class="span8">\r\n		<p class="detail_text">\r\n			<? if ($quote->start_at): ?>\r\n				<?=$quote->start_at->to_time_format()?> <?=$quote->start_at->to_long_date_format()?> \r\n				<a href="javascript:;" id="link_booking_time_suggest"><?=__(''Suggest another time'')?></a>\r\n			<? else: ?>\r\n				<?=__(''No appointment time yet'')?> \r\n				<a href="javascript:;" id="link_booking_time_suggest"><?=__(''Suggest a time'')?></a>\r\n			<? endif ?>\r\n		</p>\r\n	</div>\r\n</div>\r\n\r\n<div class="booking_time_suggest" id="booking_time_suggest_container" style="display:none">\r\n	<?=form_open(array(''id'' => ''form_booking_time'', ''class'' => ''form_booking_time''))?>\r\n		<input type="hidden" name="quote_id" value="<?= $quote->id ?>" />\r\n		<div class="row-fluid">\r\n			<div class="span8">\r\n				<div class="control-group select_booking_time_time">\r\n					<?=form_dropdown(''start_time'', Phpr_DateTime::time_array(), ''09:00:00'', ''class="booking_time_time span12" id="booking_time_time"'')?>\r\n				</div>\r\n				<div class="control-group field_booking_time_date">\r\n					<?=form_widget(''start_date'', array(\r\n						''class'' => ''Db_DatePicker_Widget'',\r\n						''field_id'' => ''booking_time_date'',\r\n						''field_name'' => ''start_date'',\r\n						''on_select'' => ''$(this).closest("form").validate().element(this)'',\r\n						''css_class'' => ''span12''\r\n					))?>\r\n				</div>\r\n			</div>\r\n			<div class="span4"><?=form_submit(''suggest_time'', __(''Suggest''), ''class="btn btn-primary btn-block"'')?></div>\r\n		</div>\r\n	<?=form_close()?>\r\n</div>\r\n\r\n<script>\r\n\r\n Page.jobBookingTimeFormFields = $.phpr.form().defineFields(function(){\r\n	this.defineField(''start_date'').required("<?=__(''Please provide a start date'',true)?>");\r\n	this.defineField(''start_time'').required("<?=__(''Please provide a start time'',true)?>");\r\n });\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(37, 'job:details', 'job;details', '<h4><?=__(''Job Details'')?></h4>\n\n<? if ($custom_form_fields = Bluebell_Request::get_custom_form_fields($request)): ?>\n	<!-- Custom form details -->\n	<? foreach ($custom_form_fields as $field): ?>\n		<div class="row-fluid">\n			<div class="span4 mobile-span3"><p class="detail"><?= h($field->label) ?>:</p></div>\n			<div class="span8 mobile-span9"><p><?= h($field->current_value) ?></p></div>\n		</div>\n	<? endforeach ?>\n<? endif ?>\n\n<!-- Primary Job Descriptions -->\n<div class="job-description expander" data-slice-point="250" data-expand-text="<?=__(''Show more'', true)?>">\n	<?= $request->description_html ?>\n</div>\n\n<!-- Extra Job Descriptions -->\n<? foreach ($request->get_extra_description() as $extra): ?>\n	<? \n		$extra_date = new Phpr_DateTime($extra->created_at); \n	?>\n	<blockquote>\n		<p>\n			<div class="job-description expander" data-slice-point="250" data-expand-text="<?=__(''Show more'', true)?>">\n				<?= h($extra->description) ?>\n			</div>\n			<small><?=__(''Additional information added %s ago'', Phpr_DateTime::interval_to_now($extra_date))?></small>\n		</p>\n	</blockquote>\n<? endforeach ?>\n\n<? if ($request->files->count): ?>\n	<h4><?=__(''Job Photos'')?></h4>\n\n	<!-- Photos -->\n	<ul class="thumbnails">\n		<? foreach ($request->files as $file): ?>\n			<li class="span4">\n				<div class="thumbnail">\n					<a href="#full-image" data-toggle="modal" \n						onclick="$(''#full-image-element'').attr(''src'', ''<?=$file->getThumbnailPath(990, 660, true, array(''mode''=>''crop''))?>'')">\n						<img src="<?=$file->getThumbnailPath(100, 75, true, array(''mode''=>''crop''))?>" alt="" />\n					</a>\n				</div>\n			</li>		\n		<? endforeach ?>\n	</ul>\n\n	<!-- Modal for viewing photos -->\n	<div class="modal hide fade in" id="full-image">\n		<div class="modal-body align-center">\n			<a class="close" data-dismiss="modal">×</a>\n			<img id="full-image-element" src="<?= theme_url(''assets/images/ajax_loading.gif'') ?>" alt="">\n		</div>\n	</div>\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(38, 'job:quote_flat_rate_form', 'job;quote_flat_rate_form', '<?\r\n	$config = Service_Config::create();\r\n?>\r\n<table class="table labor_table">\r\n	<thead>\r\n		<tr>\r\n			<th><?=__(''Description of work'')?></th>\r\n			<th><?=__(''Price for labor'')?></th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td class="line_item">\r\n				<textarea name="Quote[flat_labor_description]" class="span12" placeholder="<?= __(''Briefly describe what is and is not included in your labor price'') ?>"><?= form_value($quote, ''flat_labor_description'')?></textarea>\r\n			</td>\r\n			<td class="price">\r\n				<span class="currency"><?=Core_Locale::currency_symbol()?></span>\r\n				<input type="text" name="Quote[flat_labor_price]" value="<?= form_value($quote, ''flat_labor_price'') ?>" class="input-mini" />\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<? if (!$config->quote_hide_materials): ?>\r\n	<table class="table item_table">\r\n		<thead>\r\n			<tr>\r\n				<th><?=__(''Item or material description'')?></th>\r\n				<th><?=__(''Price for material'')?></th>\r\n			</tr>\r\n		</thead>\r\n		<tbody>\r\n			<tr class="shell">\r\n				<td class="line_item">\r\n					<input type="text" name="Quote[flat_item_description][]" value="" class="span12" placeholder="<?= __(''List any materials or supplies not included in labor price'') ?>" />\r\n				</td>\r\n				<td class="price">\r\n					<span class="currency"><?=Core_Locale::currency_symbol()?></span>\r\n					<input type="text" name="Quote[flat_item_price][]" value="" class="input-mini" />\r\n				</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n	<div class="add_line_item"><a href="javascript:;" id="link_add_line_item" onclick="Page.clickFlatRateAddItem(); return false"><?=__(''Add another line item'')?></a></div>\r\n	<textarea name="Quote[flat_items]" style="display:none" id="quote_flat_items"><?= form_value($quote, ''flat_items'') ?></textarea>\r\n<? endif ?>\r\n\r\n<div class="row total_cost">\r\n	<div class="span6 offset6 align-center">\r\n		<ul>\r\n			<li><span class="total_cost_label"><?=__(''Total Cost'')?></span></li>\r\n			<li><span class="total_cost_value" id="total_cost_value"><?=$quote ? format_currency($quote->price) : format_currency(0)?></span></li>\r\n		</ul>\r\n	</div>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.jobQuoteFlatRateFormFields = $.phpr.form().defineFields(function() {\r\n	this.defineField(''Quote[flat_labor_price]'').required("<?=__(''Please enter your cost of labor (Min: %s)'', format_currency($config->min_labor_price))?>").number("<?=__(''Cost of labor entered must be a number'')?>").min(<?=$config->min_labor_price?>,"<?=__(''Minimum cost of labor must be %s'', format_currency($config->min_labor_price))?>");\r\n	<? if (!$config->quote_hide_materials && $config->quote_materials_required): ?>\r\n		this.defineField(''Quote[flat_item_price][]'').required("<?=__(''Please enter your cost of materials'')?>").number("<?=__(''Cost of materials entered must be a number'')?>");\r\n	<? endif ?>\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(39, 'job:quote_message_form', 'job;quote_message_form', '<? if (!$quote): ?>\n<?\n	$greeting_array = array(\n		__(''Dear %s'', $request->user->username) => __(''Dear %s'', $request->user->username),\n		__(''Hello %s'', $request->user->username) => __(''Hello %s'', $request->user->username),\n		__(''Hi %s'', $request->user->username) => __(''Hi %s'', $request->user->username)\n	);\n	$closer_array = array(\n		__(''today'') => __(''today''),\n		__(''tomorrow'') => __(''tomorrow''),\n		__(''in a few days'') => __(''in a few days''),\n		__(''next week'') => __(''next week''),\n		__(''in two weeks'') => __(''in two weeks'')\n	);\n	$parting_array = array(\n		__(''Thanks'') => __(''Thanks'')\n	);\n?>\n<div class="message_greeting">\n	<?=form_dropdown(''message_greeting'', $greeting_array)?>,\n</div>\n\n<div class="control-group message_description">\n	<div class="controls">	\n		<textarea name="message_description" class="small span12" placeholder="<?= __(''Use this as an opportunity to sell your services. Tell the customer some reasons they should pick you.'') ?>"></textarea>\n		<!--[ ] Save this personal message as default for my future quotes-->\n	</div>\n</div>\n\n<div class="message_closer">\n<? if ($type=="onsite"): ?>\n	<?=__("I could visit onsite as soon as %s. If you''d like a price quote, click ''Set Appointment'' on %s so we can share contact details and schedule a time.",\n		array(form_dropdown(''Quote[message_start]'', $closer_array), c(''site_name''))\n	)?>\n<? else: ?>\n	<?=__("I could start %s. If you''d like to move forward, click ''Set Appointment'' on %s so we can share contact details and schedule a time.",\n		array(form_dropdown(''Quote[message_start]'', $closer_array), c(''site_name''))\n	)?>\n<? endif ?>\n</div>\n<div class="message_parting">\n<?=form_dropdown(''message_parting'', $parting_array)?>, <br />\n<? if ($provider): ?><?=$provider->business_name?>\n<? elseif ($this->user): ?><?=$this->user->name ?>\n<? else: ?><?=__(''Your name'')?>\n<? endif ?>\n</div>\n\n<textarea name="Quote[comment]" class="final_comment use_message_builder" style="display:none"></textarea>\n\n<script>\n\nPage.jobQuoteMessageFormFields = $.phpr.form().defineFields(function() {\n	this.defineField(''message_description'').required("<?=__(''Please enter a message'')?>");\n});\n\n</script>\n\n<? else: ?>\n	<textarea name="Quote[comment]" class="span12 large final_comment"><?= $quote->comment ?></textarea>\n<? endif ?>', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-09-08 07:58:38'),
(145, 'provide:ratings_small', 'provide;ratings_small', '<?\n	$ratings = (isset($ratings)) ? $ratings : null;\n	$pagination = (isset($pagination)) ? $pagination : false;\n?>\n<? if ($ratings): ?>\n	<? \n		$ratings = $ratings->limit(2)->find_all();\n	?>\n	<? foreach ($ratings as $rating): ?>\n		<? if ($rating instanceof Service_Rating): ?>\n			<div>\n				<p class="star-rating single"><?=$rating->user_from->name?><i class="pull-right rating-<?=$rating->rating*10?>">&nbsp;</i></p>\n				"<?=Phpr_String::show_more_link($rating->comment, 125, __(''Show more'', true))?>"\n				\n			</div>\n		<? elseif ($rating instanceof Service_Testimonial): ?>\n			<blockquote>\n				"<?=$rating->comment?>"\n				<small><?=$rating->name?>, <?=$rating->location?></small>\n			</blockquote>\n		<? endif ?>\n	<? endforeach ?>\n	\n<? endif ?>', 'custom', NULL, 1, 1, '2014-09-08 07:59:46', '2014-09-08 08:05:02'),
(40, 'job:quote_onsite_form', 'job;quote_onsite_form', '<div class="row-fluid">\r\n	<div class="span8"><?=__(''What is the estimated price range for this work?'')?></div>\r\n	<div class="span2 price_column">\r\n		<span class="currency"><?=Core_Locale::currency_symbol()?></span>\r\n		<input type="text" name="Quote[onsite_price_start]" value="<?= form_value($quote, ''onsite_price_start'') ?>" class="onsite_price span1" />\r\n		<span class="divider">-</span>\r\n	</div>\r\n	<div class="span2 price_column">\r\n		<span class="currency"><?=Core_Locale::currency_symbol()?></span>\r\n		<input type="text" name="Quote[onsite_price_end]" value="<?= form_value($quote, ''onsite_price_end'') ?>" class="onsite_price span1" />\r\n	</div>\r\n</div>\r\n<div class="row-fluid">\r\n	<div class="span8"><?=__(''Do you charge a fee to travel to the work site?'')?></div>\r\n	<div class="span2">\r\n		<label class="radio" for="quote_onsite_travel_required_yes">\r\n			<?=form_radio(''Quote[onsite_travel_required]'', true, form_value_boolean($quote, ''onsite_travel_required'', true), ''id="quote_onsite_travel_required_yes"'')?> Yes\r\n		</label>\r\n	</div>\r\n	<div class="span2">\r\n		<label class="radio" for="quote_onsite_travel_required_no">\r\n			<?=form_radio(''Quote[onsite_travel_required]'', false, form_value_boolean($quote, ''onsite_travel_required'', false, true), ''id="quote_onsite_travel_required_no"'')?> No\r\n		</label>\r\n	</div>\r\n</div>\r\n<div id="panel_travel_fee" style="display:none">\r\n	<div class="row-fluid">\r\n		<div class="span8"><?=__(''How much do you charge for travel?'')?></div>\r\n		<div class="span2">&nbsp;</div>\r\n		<div class="span2 price_column">\r\n			<span class="currency"><?=Core_Locale::currency_symbol()?></span>\r\n			<?=form_input(''Quote[onsite_travel_price]'', form_value($quote, ''onsite_travel_price''), ''class="span1"'')?>\r\n		</div>\r\n	</div>\r\n	<div class="row-fluid">\r\n		<div class="span8"><?=__(''Will you waive the travel fee if you are picked for this job?'')?></div>\r\n		<div class="span2">\r\n			<label class="radio" for="quote_onsite_travel_waived_true">\r\n				<?=form_radio(''Quote[onsite_travel_waived]'', true, form_value_boolean($quote, ''onsite_travel_waived'', true), ''id="quote_onsite_travel_waived_true"'')?> Yes\r\n			</label>\r\n		</div>\r\n		<div class="span2">\r\n			<label class="radio" for="quote_onsite_travel_waived_false">\r\n				<?=form_radio(''Quote[onsite_travel_waived]'', false, form_value_boolean($quote, ''onsite_travel_waived'', false, true), ''id="quote_onsite_travel_waived_false"'')?> No\r\n			</label>\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.jobQuoteOnSiteFormFields = $.phpr.form().defineFields(function() {\r\n	this.defineField(''Quote[onsite_price_end]'').requiredMulti(1, ''.onsite_price'', "<?=__(''Please enter an estimated price'')?>");\r\n	this.defineField(''Quote[onsite_travel_price]'').required("<?=__(''Please enter how much you charge for travel'')?>");\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(41, 'job:quote_submit', 'job;quote_submit', '<?\r\n	$quote_type = ($quote&&$quote->quote_type==''onsite'') ? ''onsite'' : ''flat_rate'';\r\n	$config = Service_Config::create();\r\n?>\r\n<div class="tabbable">\r\n	<ul class="nav nav-tabs" id="quote_submit_tabs">\r\n		<li class="<?=($quote_type==Bluebell_Quote::quote_type_flat_rate)?''active'':''''?>">\r\n			<a href="#flat_rate" data-toggle="tab"><?=__(''Flat rate'')?></a>\r\n		</li>\r\n		<? if (!$config->quote_hide_onsite): ?>\r\n			<li class="<?=($quote_type==Bluebell_Quote::quote_type_onsite)?''active'':''''?>">\r\n				<a href="#onsite_visit" data-toggle="tab"><?=__(''Onsite visit required'')?></a>\r\n			</li>\r\n		<? endif ?>\r\n	</ul>\r\n\r\n	<div class="tab-content">\r\n		<div class="tab-pane <?=($quote_type==Bluebell_Quote::quote_type_flat_rate)?''active'':''''?>" id="flat_rate">\r\n			<?=form_open(array(''id''=>''form_quote_flat_rate''))?>\r\n				<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n				<input type="hidden" name="Quote[quote_type]" value="<?= Bluebell_Quote::quote_type_flat_rate ?>" />\r\n\r\n				<h4><?=__(''Price'')?></h4>\r\n				<div id="p_quote_flat_rate"><?=$this->display_partial(''job:quote_flat_rate_form'')?></div>\r\n\r\n				<hr />\r\n\r\n				<h4><?=__(''Personal Message'', true)?></h4>\r\n				<div id="p_personal_message_flat"><?=$this->display_partial(''job:quote_message_form'', array(''type''=>Bluebell_Quote::quote_type_flat_rate))?></div>\r\n\r\n				<div class="form-actions">\r\n					<? if ($this->user && $provider): ?>\r\n						<div class="align-center">\r\n							<?=form_submit(''review'', __(''Submit Quote''), ''class="btn btn-primary btn-large"'')?>\r\n						</div>\r\n						<div class="final_note align-center">\r\n							<?=__(''If this customer books you for this job, we will send you their contact details and guarantee you the job for a charge of 25% your price for labor.'')?>\r\n						</div>\r\n					<? else:?>\r\n						<div class="align-center">\r\n							<a href="<?=root_url(''provide'')?>"><?=__(''Create a provider profile to quote on this job!'')?></a>\r\n						</div>\r\n					<? endif ?>\r\n				</div>\r\n\r\n			<?=form_close()?>\r\n		</div>\r\n		<? if (!$config->quote_hide_onsite): ?>\r\n			<div class="tab-pane <?=($quote_type==Bluebell_Quote::quote_type_onsite)?''active'':''''?>" id="onsite_visit">\r\n				<?=form_open(array(''id''=>''form_quote_onsite''))?>\r\n					<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n					<input type="hidden" name="Quote[quote_type]" value="<?= Bluebell_Quote::quote_type_onsite ?>" />\r\n\r\n					<h4><?=__(''Price'')?></h4>\r\n					<div id="p_quote_onsite"><?=$this->display_partial(''job:quote_onsite_form'')?></div>\r\n\r\n					<hr />\r\n\r\n					<h4><?=__(''Personal Message'', true)?></h4>\r\n					<div id="p_personal_message_onsite"><?=$this->display_partial(''job:quote_message_form'', array(''type''=>Bluebell_Quote::quote_type_onsite))?></div>\r\n\r\n					<div class="form-actions">\r\n						<? if ($this->user && $provider): ?>\r\n							<div class="align-center">\r\n								<?=form_submit(''review'', __(''Submit Quote''), ''class="btn btn-primary btn-large"'')?>\r\n							</div>\r\n							<div class="final_note align-center">\r\n								<?=__(''If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.'')?>\r\n							</div>\r\n						<? else:?>\r\n							<div class="align-center">\r\n								<a href="<?=root_url(''provide'')?>"><?=__(''Create a provider profile to quote on this job!'')?></a>\r\n							</div>\r\n						<? endif ?>\r\n					</div>\r\n\r\n				<?=form_close()?>\r\n			</div>\r\n		<? endif ?>\r\n	</div>\r\n</div>\r\n<script>\r\n	jQuery(document).ready(function($) {\r\n		Page.initQuoteForms();\r\n	});\r\n</script>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(42, 'job:quote_summary', 'job;quote_summary', '<?\r\n	$is_editable = (isset($is_editable)) ? $is_editable : true;\r\n?>\r\n<div class="row-fluid quote_title">\r\n	<div class="span6"><h4><?=__(''Quote Details'')?></h4></div>\r\n	<div class="span6 align-right">\r\n		<? if ($is_editable): ?>\r\n			<a href="javascript:;" id="link_edit_quote">\r\n				<i class="icon-pencil"></i> <?=__(''Modify quote'')?>\r\n			</a>\r\n			 &nbsp; &nbsp; \r\n			<a href="javascript:;" id="link_delete_quote">\r\n				<i class="icon-ban-circle"></i> \r\n				<?=__(''Cancel quote'')?>\r\n			</a>\r\n		<? endif ?>\r\n	</div>\r\n</div>\r\n<div class="row-fluid">\r\n	<div class="span6">\r\n		<?=$quote->comment_html?>\r\n	</div>\r\n	<div class="span6">\r\n		\r\n		<div id="p_control_badge" class="badge-control">\r\n			<?=$this->display_partial(''control:badge'', array(''provider''=>$provider))?>\r\n		</div>\r\n\r\n	</div>\r\n</div>\r\n<table class="table table-bordered quote_summary">\r\n	<thead>\r\n		<tr>\r\n			<th class="details"><?=__(''Quote Details'')?></th>\r\n			<th class="price"><?=__(''Price'')?></th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<? if ($quote->flat_labor_price): ?>\r\n		<tr>\r\n			<td><em><?=__(''Labor'')?></em>: <?=$quote->flat_labor_description?></td>\r\n			<td><?=format_currency($quote->flat_labor_price)?></td>\r\n		</tr>\r\n		<? endif ?>\r\n\r\n		<? if ($quote->flat_items): ?>\r\n			<? foreach (json_decode($quote->flat_items) as $item): ?>\r\n			<tr>\r\n				<td><?=$item->description?></td>\r\n				<td><?=format_currency($item->price)?></td>\r\n			</tr>\r\n			<? endforeach ?>\r\n		<? endif ?>\r\n\r\n		<? if ($quote->onsite_price_start||$quote->onsite_price_end): ?>\r\n		<tr>\r\n			<td><?=__(''Onsite visit is required to give a price quote. Estimated cost provided.'')?></td>\r\n			<td>\r\n				<?=(!$quote->onsite_price_start||!$quote->onsite_price_end)\r\n					? format_currency($quote->onsite_price_end + $quote->onsite_price_start)\r\n					: format_currency($quote->onsite_price_start) \r\n					. '' - '' \r\n					. format_currency($quote->onsite_price_end)?>\r\n			</td>\r\n		</tr>\r\n		<? endif ?>\r\n		<? if ($quote->onsite_travel_required): ?>\r\n		<tr>\r\n			<td>\r\n				<?=($quote->onsite_travel_waived)\r\n					? __(''Travel fee (waived if chosen)'')\r\n					: __(''Travel fee'') ?>\r\n			</td>\r\n			<td>\r\n				<?=($quote->onsite_travel_price)\r\n					? format_currency($quote->onsite_travel_price)\r\n					: __(''Free'',true) ?>\r\n			</td>\r\n		</tr>\r\n		<? endif ?>\r\n	</tbody>\r\n</table>\r\n\r\n<? if ($quote->flat_labor_price): ?>\r\n<div class="row-fluid">\r\n	<div class="span5 offset7">\r\n		<table class="table quote_summary">\r\n			<tbody>\r\n				<tr>\r\n					<th class="total"><div class="align-right"><?=__(''Total'')?></div></th>\r\n					<td class="price"><?=format_currency($quote->price)?></td>\r\n				</tr>\r\n			</tbody>\r\n		</table>\r\n	</div>\r\n</div>\r\n<? endif ?>\r\n<?=form_open(array(''id''=>''form_quote_summary''))?>\r\n	<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n<?=form_close()?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(43, 'job:rating_form', 'job;rating_form', '<? if ($rating): ?>\r\n	<h4><?=__(''Review of %s'', $opp_user_name)?></h4>\r\n	<blockquote>\r\n		<div class="rating star-rating"><i class="rating-<?=$rating->rating*10?>"></i></div>\r\n		<p>"<?=Phpr_String::show_more_link($rating->comment, 125, __(''Show more'', true))?>"</p>\r\n		<small><cite><?=$rating->user_from->username?></cite>, <?=$rating->user_from->location_string?></small>\r\n	</blockquote>\r\n<? else: ?>\r\n	<h4><?=__(''Submit a review about %s'', $opp_user_name)?></h4>\r\n\r\n	<div class="control-group">\r\n		<label for="rating_comment" class="control-label"><?=__(''Write something about %s'', $opp_user_name)?></label>\r\n		<div class="controls">\r\n			<?=form_textarea(''Rating[comment]'', '''', ''id="rating_comment" class="span12"'')?>\r\n		</div>\r\n	</div>\r\n\r\n	<div class="row-fluid">\r\n		<div class="span6">\r\n			<div class="control-group">\r\n				<label for="rating_rating" class="control-label"><?=__(''Select a rating'')?></label>\r\n				<div class="controls">\r\n					<div class="rating-selector" id="rating_rating">\r\n						<?=form_dropdown(''Rating[rating]'', array(0,1,2,3,4,5))?>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="span6 align-right">\r\n			<button type="submit" name="submit" class="btn btn-primary btn-large"><?= __(''Submit this rating'') ?></button>\r\n		</div>\r\n	</div>\r\n	<script type="text/javascript"> jQuery(document).ready(function($) { $(''#rating_rating'').starRating(); }); </script>\r\n\r\n<script>\r\n\r\n	Page.jobRatingFormFields = $.phpr.form().defineFields(function(){\r\n	   this.defineField(''Rating[comment]'').required("<?=__(''Please write a short review comment'')?>");\r\n	});\r\n\r\n</script>\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(44, 'messages:message_panel', 'messages;message_panel', '<? foreach ($messages->find_all() as $message): ?>\r\n	<div class="message <?=($message->is_new) ? ''unread'' : ''''?>">\r\n		<div class="row-fluid">\r\n			<div class="span9 mobile-span9">\r\n				<div class="row-fluid message-link" data-url="<?=root_url(''message/''.$message->id)?>">\r\n					<div class="span2 mobile-span3">\r\n						<div class="avatar align_right"><img src="<?=Bluebell_User::avatar($message->from_user)?>" alt="" /></div>\r\n					</div>\r\n					<div class="span10 mobile-span9">\r\n						<div class="recipient"><?= $message->recipients_string ?></div>\r\n						<div class="subject"><?= $message->message_summary ?></div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<div class="span3 mobile-span3">\r\n				<div class="controls">\r\n					<span class="date"><?=__(''%s ago'', Phpr_DateTime::interval_to_now($message->sent_at), true)?></span>\r\n					<a href="javascript:;" data-message-id="<?=$message->id?>" class="link-delete">\r\n						<i class="icon-remove-sign"></i> \r\n						<?=__(''Delete'', true)?>\r\n					</a>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n<? endforeach ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(45, 'messages:reply_form', 'messages;reply_form', '<div class="control-group">\r\n	<label class="control-label"><?=__(''Your Message'')?></label>\r\n	<div class="controls">\r\n		<textarea name="Message[message]" id="message_reply_message" class="span12"></textarea>\r\n	</div>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.messagesReplyFormFields = $.phpr.form().defineFields(function(){\r\n	this.defineField(''Message[message]'', ''Message'').required("<?=__(''Please enter a reply to this message'')?>");\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(46, 'messages:thread', 'messages;thread', '<? foreach ($messages as $message): ?>\r\n<div class="message">\r\n	<div class="row-fluid">\r\n		<div class="span2 columns mobile-span3 align-right">\r\n			<img src="<?=Bluebell_User::avatar($message->from_user)?>" alt="" />\r\n		</div>\r\n		<div class="span10 columns mobile-span9">\r\n			<div class="row-fluid">\r\n				<div class="span6 columns mobile-span2">\r\n					<div class="username"><?=$message->from_user->username?></div>\r\n				</div>\r\n				<div class="span6 columns mobile-span2">\r\n					<div class="date"><?=__(''%s ago'', Phpr_DateTime::interval_to_now($message->sent_at), true)?></div>\r\n				</div>\r\n			</div>\r\n			<div class="row-fluid">\r\n				<div class="span12">\r\n					<div class="message_content"><?=$message->message_html?></div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n<? endforeach ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(47, 'payment:form', 'payment;form', '<? if ($payment_type): ?>\n	<? if ($payment_type->has_payment_form()): ?>\n		<div class="payment_form">\n			<? $payment_type->display_payment_form($this) ?>\n		</div>\n	<? else: ?>\n		<p><?=__(''Payment method %s has no payment form. Please pay and notify us of your action'', $payment_type->name)?></p>\n		<ul class="disc">\n			<li><a href="<?=root_url()?>"><?=__(''Return to the homepage'')?></a></li>\n		</ul>\n	<? endif ?>\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(48, 'payment:invoice_table', 'payment;invoice_table', '<table class="table table-striped table-invoice">\n	<thead>\n		<tr>\n			<th class="item-description"><?=__(''Item'')?></th>\n			<th class="numeric"><?=__(''Price'')?></th>\n			<th class="numeric"><?=__(''Discount'')?></th>\n			<th class="numeric"><?=__(''Tax'')?></th>\n			<th class="numeric last"><?=__(''Total'')?></th>\n		</tr>\n	</thead>\n	<tbody>\n		<? foreach ($invoice->items as $index=>$item): ?>\n		<tr>\n			<td>\n				<div class="product_description">\n					<?=$item->quantity?>x <?=$item->description?>\n				</div>\n			</td>\n			<td class="numeric"><?=format_currency($item->price)?></td>\n			<td class="numeric"><?=format_currency($item->discount)?></td>\n			<td class="numeric"><?=format_currency($item->tax)?></td>\n			<td class="numeric last total"><?=format_currency($item->total)?></td>\n		</tr>\n		<? endforeach ?>\n	</tbody>\n	<tfoot>\n		<tr>\n			<td colspan="4" class="align-right"><?=__(''Subtotal'')?></td>\n			<td class="numeric total"><?=format_currency($invoice->subtotal) ?></td>\n		</tr>\n		<? foreach ($invoice->list_item_taxes() as $tax): ?>\n			<tr>\n				<td class="numeric" colspan="4"><?=__(''Sales tax'')?> (<?=($tax->name) ?>)</td>\n				<td class="numeric total"><?=format_currency($tax->total) ?></td>\n			</tr>\n		<? endforeach ?>\n		<tr class="grand-total">\n			<td class="blank">&nbsp;</td>\n			<td class="numeric" colspan="3"><?=__(''Total'')?></td>\n			<td class="numeric"><span class="product_price"><?=format_currency($invoice->total) ?></span></td>\n		</tr>\n	</tfoot>\n</table>​', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(49, 'payment:offline', 'payment;offline', '\n<?=$payment_type->get_payment_instructions($payment_type, $invoice)?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(50, 'profile:other_providers', 'profile;other_providers', '<div id="map_other_providers" class="google_map hide-for-small">\r\n</div>\r\n<div id="other_providers">\r\n	<ul>\r\n		<? foreach ($other_providers as $o_provider): ?>\r\n\r\n			<li data-id="info_provider<?=$o_provider->id?>" data-latlng="<?=$o_provider->latitude?>,<?=$o_provider->longitude?>" data-address="<?=$o_provider->address_string?>">\r\n				<div class="avatar"><img src="<?=Bluebell_Provider::avatar($o_provider)?>" alt="<?=$o_provider->business_name?>" /></div>\r\n				<div class="name"><?=$o_provider->business_name?></div>\r\n				<div class="rating star-rating">\r\n					<i class="rating-<?=$o_provider->rating*10?>"></i>\r\n				</div>\r\n			</li>\r\n		<? endforeach ?>\r\n	</ul>\r\n</div>\r\n<div style="display:none">\r\n<? foreach ($other_providers as $o_provider): ?>\r\n	<div id="location_info_provider<?=$o_provider->id?>">\r\n		<div class="page_profile_map_bubble">\r\n\r\n			<div class="box">\r\n				<div class="box-header">\r\n					<h3><?=$o_provider->business_name?> <small><?=$o_provider->role_name?></small></h3>\r\n				</div>\r\n				<div class="box-content">\r\n					<div class="row-fluid">\r\n						<div class="span4 align-center">\r\n							<img src="<?=Bluebell_Provider::avatar($o_provider)?>" alt="<?=$o_provider->business_name?>" class="rounded" />                            \r\n							<p><a href="<?=$o_provider->get_url(''profile'')?>" class="btn"><?=__(''View Profile'')?></a></p>\r\n						</div>\r\n						<div class="span6">\r\n							<p><?=Phpr_Html::limit_characters($o_provider->description,100)?></p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class="box-footer">\r\n					<div class="row-fluid">\r\n						<div class="span5">\r\n							<div class="rating star-rating">\r\n								<i class="rating-<?=$o_provider->rating*10?>"></i>\r\n							</div>\r\n						</div>\r\n						<div class="span5 align-right">\r\n							<h6><?=$o_provider->location_string?></h6>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n		</div>\r\n	</div>\r\n<? endforeach ?>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(51, 'provide:description', 'provide;description', '<? if (!strlen($provider->description)): ?>\r\n	<p id="button_profile_description" class="form-field"><?=__(''Click to add a short description about your business'')?></p>\r\n<? else: ?>\r\n<div id="button_profile_description" class="form-field">\r\n	<p><?=$provider->description_html?></p>\r\n</div>\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(52, 'provide:description_form', 'provide;description_form', '<div class="row-fluid">\r\n	<div class="control-group">\r\n		<label for="provider_description_experience" class="control-label"><?= __(''Describe your work experience'') ?></label>\r\n		<textarea name="Provider[description_experience]" value="<?= form_value($provider, ''description_experience'') ?>" class="span12" id="provider_description_experience"></textarea>\r\n	</div>\r\n\r\n	<div class="control-group">\r\n		<label for="provider_description_speciality" class="control-label"><?= __(''What kind of projects do you work on?'') ?></label>\r\n		<textarea name="Provider[description_speciality]" value="<?= form_value($provider, ''description_speciality'') ?>" class="span12" id="provider_description_speciality"></textarea>\r\n	</div>\r\n\r\n	<div class="control-group">\r\n		<label for="provider_description_why_us" class="control-label"><?= __(''Why do your customers like working with you?'') ?></label>\r\n		<textarea name="Provider[description_why_us]" value="<?= form_value($provider, ''description_why_us'') ?>" class="span12" id="provider_description_why_us"></textarea>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(53, 'provide:portfolio', 'provide;portfolio', '<?\n	$container_id = (isset($container_id)) ? $container_id : ''provider_portfolio'';\n?>\n<? if (isset($images) && count($images) > 0): ?>\n<div id="<?=$container_id?>" class="carousel slide">\n	<ol class="carousel-indicators">\n		<?\n			$count = 0; \n		?>\n		<? foreach ($images as $item): $count++; ?>\n			<li data-target="#<?=$container_id?>" data-slide-to="<?=$count?>" class="<?=$count==1?''active'':''''?>"></li>\n		<? endforeach ?>\n	</ol>\n	<div class="carousel-inner">\n		<?\n			$count = 0; \n		?>	\n		<? foreach ($images as $item): $count++; ?>\n			<div class="<?=$count==1?''active'':''''?> item">\n				<img src="<?=$item->image?>" alt="" data-image-id="<?=$item->id?>" data-image-thumb="<?=$item->thumb?>" />\n			</div>\n		<? endforeach ?>\n	</div>\n	<a class="carousel-control left" href="#<?=$container_id?>" data-slide="prev">&lsaquo;</a>\n	<a class="carousel-control right" href="#<?=$container_id?>" data-slide="next">&rsaquo;</a>	\n</div>\n\n<? elseif (isset($is_manage) && $is_manage): ?>\n	<div class="well" id="panel_button_profile_portfolio">\n		<a href="javascript:;" class="button_profile_portfolio"><?=__(''Click to add portfolio photos'')?></a>\n	</div>\n	<div id="<?=$container_id?>"></div>\n<? endif ?>\n<script>\n	jQuery(document).ready(function($) {\n		$(''#<?=$container_id?>'').portfolio();\n	});\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(54, 'provide:portfolio_form', 'provide;portfolio_form', '<?\r\n	$has_portfolio = ($provider&&$provider->portfolio->count > 0);	\r\n?>\r\n<div id="panel_photos" style="<?=($has_portfolio)?'''':''display:none''?>">\r\n	<ul class="thumbnails">\r\n		<? if ($has_portfolio): ?>\r\n			<? foreach ($provider->get_portfolio() as $item): ?>\r\n				<li>\r\n					<div class="thumbnail" data-image-id="<?=$item->id?>">\r\n						<img src="<?=$item->thumb?>" alt="" />\r\n						<a href="javascript:;" class="remove">Remove</a>\r\n					</div>\r\n				</li>\r\n			<? endforeach ?>\r\n		<? endif ?>\r\n	</ul>\r\n</div>\r\n<p><?=__(''%s to your portfolio'', ''<a href="javascript:;" id="link_add_photos">''.__(''Attach photos'', true).''</a>'')?></a>\r\n<input id="input_add_photos" type="file" name="portfolio[]" multiple>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL);
INSERT INTO `cms_partials` (`id`, `name`, `file_name`, `content`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(55, 'provide:profile_form', 'provide;profile_form', '<div class="provider-form-control">\n	<div class="control-group business_name">\n		<label for="provide_business_name" class="control-label"><?= __(''Click to add company name'') ?></label>\n		<div class="controls edit"><?=form_input(''Provider[business_name]'', form_value($provider, ''business_name''), ''placeholder="''.__(''Click to add company name'').''" class="span12 oversize" id="provide_business_name"'')?></div>\n	</div>\n	<div class="business_logo">\n		<?\n			$has_logo = ($provider&&$provider->logo->count > 0);\n			$logo = $blank_logo = theme_url(''assets/images/avatar_thumb.jpg'');\n			if ($provider)\n				$logo = $provider->get_logo(''autox60'', $logo);\n		?>\n		<img src="<?=$logo?>" id="provide_logo" alt="" style="display:<?=($has_logo)?''block'':''none''?>" data-blank-src="<?=$blank_logo?>" />\n		<a href="javascript:;" id="link_provide_logo" style="display:<?=($has_logo)?''none'':''block''?>"><?=__(''Click here to add your business logo'')?></a>\n		<a href="javascript:;" id="link_provide_logo_remove" style="display:<?=($has_logo)?''block'':''none''?>"><?=__(''Remove'')?></a>\n		<input id="input_provide_logo" type="file" name="logo">\n	</div>\n	<div class="row-fluid">\n		<div class="span4">\n			<div class="profile_photo">\n				<?\n					$has_photo = ($provider&&$provider->photo->count > 0);\n					$photo = $blank_photo = theme_url(''assets/images/avatar_thumb.jpg'');\n					if ($provider)\n						$photo = $provider->get_photo(100, $photo);\n				?>\n				<img src="<?=$photo?>" id="provide_photo" alt="" data-blank-src="<?=$blank_photo?>" />\n				<a href="javascript:;" id="link_provide_photo" class="btn btn-block" style="display:<?=($has_photo)?''none'':''block''?>"><?=__(''Add Image'')?></a>\n				<a href="javascript:;" id="link_provide_photo_remove" style="display:<?=($has_photo)?''block'':''none''?>"><?=__(''Remove'')?></a>\n				<input id="input_provide_photo" type="file" name="photo">\n			</div>\n		</div>\n		<div class="span8">\n			<div class="control-group role_name">\n				<label for="provide_role_name" class="control-label"><?= __(''trade, speciality or skill'') ?></label>\n				<div class="controls edit"><?=form_input(''Provider[role_name]'', form_value($provider, ''role_name''), ''placeholder="''.__(''trade, speciality or skill'').''" class="span12" id="provide_role_name"'')?></div>\n			</div>\n			<div class="row-fluid lic">\n				<div class="span6">\n					<div class="license">\n						<label for="provide_license" class="control-label"><?= __(''License #'', true) ?></label>\n						<div class="controls edit"><?=form_input(''Provider[license]'', form_value($provider, ''license''), ''placeholder="''.__(''License #'').''" class="span12" id="provide_license"'')?></div>\n					</div>					\n				</div>\n				<div class="span6">\n					<div class="phone">\n						<label for="provide_lic_state" class="control-label"><?= __(''State'', true) ?></label>\n						<div class="controls edit"><?=form_input(''Provider[licn_state]'', form_value($provider, ''licn_state''), ''placeholder="''.__(''State'').''" class="span12" id="provide_lic_state"'')?></div>\n					</div>					\n				</div>\n			</div>\n			<div class="row-fluid lic">\n				<div class="span6">\n					<div class="phone">\n						<label for="provide_lic_type" class="control-label"><?= __(''Type'', true) ?></label>\n						<div class="controls edit"><?=form_input(''Provider[type]'', form_value($provider, ''type''), ''placeholder="''.__(''Type'').''" class="span12" id="provide_lic_type"'')?></div>\n					</div>					\n				</div>\n				<? $date = form_value($provider, ''date_issue'');\n					$dt = new DateTime($date);\n			?>\n				<div class="span6">\n					<div class="phone">\n						<label for="provide_lic_date" class="control-label"><?= __(''Date Issued'', true) ?></label>\n						<div class="controls edit"><?=form_widget(''date_issue'', array(\n												''class'' => ''Db_DatePicker_Widget'',\n												''css_class'' => ''date_issue span3'',\n												''field_id'' => ''provide_lic_date'',\n												''field_value'' => $dt->format(''d-m-Y''),\n												''field_name'' => ''Provider[date_issue]'',\n												''allow_past_dates'' => true,\n												''on_select'' => ''$(this).closest("form").validate().element(this)''\n											))?></div>\n					</div>					\n				</div>\n			</div>\n			<div class="control-group Bonded">\n				<label for="provide_lic_bonded" class="control-label"><?= __(''Bonded (Y/N Amount)'', true) ?></label>\n				<div class="controls edit"><?=form_input(''Provider[bonded]'', form_value($provider, ''bonded''), ''placeholder="''.__(''Bonded (Y/N Amount)'').''" class="span12" id="provide_lic_bonded"'')?></div>\n			</div>					\n		</div>\n	</div>\n	<div class="control-group phone_info" id="profile_details_phone">\n			<div class="row-fluid">\n				<div class="span6">\n					<div class="phone">\n						<label for="provide_phone" class="control-label"><?= __(''Phone number'', true) ?></label>\n						<div class="controls edit"><?=form_input(''Provider[phone]'', form_value($provider, ''phone''), ''placeholder="''.__(''Phone number'').''" class="span12" id="provide_phone"'')?></div>\n					</div>					\n				</div>\n				<div class="span6">\n					<div class="mobile">\n						<label for="provide_mobile" class="control-label"><?= __(''Mobile number'', true) ?></label>\n						<div class="controls edit"><?=form_input(''Provider[mobile]'', form_value($provider, ''mobile''), ''placeholder="''.__(''Mobile number'').''" class="span12" id="provide_mobile"'')?></div>\n					</div>\n				</div>\n			</div>\n	</div>\n	<div class="control-group address" id="profile_details_address">\n		<label for="provide_address" class="control-label"><?= __(''Business address'') ?></label>\n		<div class="controls edit">\n			<div class="controls">\n				<?=form_input(''Provider[street_addr]'', form_value($provider, ''street_addr''), ''placeholder="''.__(''Business address'').''" class="span12" id="provide_address"'')?>\n			</div>\n			<div class="row-fluid">\n				<div class="span6">\n					<div class="controls"><?=form_input(''Provider[city]'', form_value($provider, ''city''), ''placeholder="''.__(''City'', true).''" class="span12" id="provide_city"'')?></div>\n				</div>\n				<div class="span6">\n					<div class="controls"><?=form_input(''Provider[zip]'', form_value($provider, ''zip''), ''placeholder="''.__(''Zip / Postal Code'', true).''" class="span12" id="provide_zip"'')?></div>\n				</div>\n			</div>\n			<div class="row-fluid">\n				<div class="span6">\n					<div class="controls"><?=form_dropdown(''Provider[country_id]'', Location_Country::get_name_list(), form_value($provider, ''country_id''), ''id="provide_country_id"'',  __(''-- Select --'', true))?></div>\n				</div>\n				<div class="span6">\n					<div class="controls"><?=form_dropdown(''Provider[state_id]'', Location_State::get_name_list(form_value($provider, ''country_id'')), form_value($provider, ''state_id''), ''id="provide_state_id"'', __(''-- Select --'', true));?></div>\n				</div>\n			</div>\n		</div>\n	</div>\n	<div class="control-group url">\n		<label for="provide_url" class="control-label"><?= __(''Website link'') ?></label>\n		<div class="controls edit">\n			<?=form_input(''Provider[url]'', form_value($provider, ''url''), ''placeholder="''.__(''Website link'').''" class="span12" id="provide_url"'')?>\n		</div>\n	</div>\n</div>\n\n<script>\n\nPage.provideProfileDetailsFormFields = $.phpr.form().defineFields(function(){\n	this.defineField(''Provider[business_name]'').required("<?=__(''Please specify your business name'')?>");\n	this.defineField(''Provider[role_name]'').required("<?=__(''Please enter a skill or occupation'',true)?>").action(''service:on_validate_category_name'', "<?=__(''Please select a valid service form the list'', true)?>");\n	this.defineField(''Provider[phone]'').phone("<?=__(''Please specify a valid phone number'',true)?>");\n	this.defineField(''Provider[mobile]'').phone("<?=__(''Please specify a valid phone number'',true)?>");\n	this.defineField(''Provider[street_addr]'').required("<?=__(''You must provide your business address'')?>");\n	this.defineField(''Provider[zip]'').required("<?=__(''Please specify a zip / postal code'')?>");\n	this.defineField(''Provider[country_id]'').required("<?=__(''Please select your country'')?>");\n	this.defineField(''Provider[state_id]'').required("<?=__(''Please select your state'')?>");\n	this.defineField(''Provider[url]'').url("<?=__(''Please enter a valid website link'')?>");\n});\n\n\n</script>', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-08-19 04:17:48'),
(56, 'provide:ratings', 'provide;ratings', '<?\n	$ratings = (isset($ratings)) ? $ratings : null;\n	$pagination = (isset($pagination)) ? $pagination : false;\n?>\n<? if ($ratings): ?>\n	<? \n		$ratings = $ratings->find_all();\n	?>\n	<? foreach ($ratings as $rating): ?>\n		<? if ($rating instanceof Service_Rating): ?>\n			<div>\n				<p class="star-rating single"><?=$rating->user_from->name?><i class="pull-right rating-<?=$rating->rating*10?>">&nbsp;</i></p>\n				"<?=Phpr_String::show_more_link($rating->comment, 125, __(''Show more'', true))?>"\n				\n			</div>\n		<? elseif ($rating instanceof Service_Testimonial): ?>\n			<blockquote>\n				"<?=$rating->comment?>"\n				<small><?=$rating->name?>, <?=$rating->location?></small>\n			</blockquote>\n		<? endif ?>\n	<? endforeach ?>\n	<? if ($pagination): ?>\n		<? $this->display_partial(''site:pagination'', array(''pagination''=>$pagination, ''base_url''=>$base_url)); ?>\n	<? endif?>\n<? endif ?>', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-09-08 08:05:15'),
(57, 'provide:testimonial_write_form', 'provide;testimonial_write_form', '<div class="control-group">\r\n	<label for="testimonial_name" class="control-label"><?= __(''Your Name'') ?></label>\r\n	<div class="controls">\r\n		<input type="text" name="Testimonial[name]" value="" id="testimonial_name" class="span12" />\r\n	</div>\r\n</div>\r\n\r\n<div class="control-group">\r\n	<label for="testimonial_location" class="control-label"><?= __(''City, State'') ?></label>\r\n	<div class="controls">\r\n		<input type="text" name="Testimonial[location]" value="" id="testimonial_location" class="span12" />\r\n	</div>\r\n</div>\r\n\r\n<div class="control-group">\r\n	<label for="testimonial_comment" class="control-label"><?= __(''Your testimonial'') ?></label>\r\n	<div class="controls">\r\n		<textarea name="Testimonial[comment]" class="span12" id="testimonial_comment"></textarea>\r\n	</div>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.provideTestimonialWriteFormFields = $.phpr.form().defineFields(function() {\r\n	this.defineField(''Testimonial[name]'').required("<?=__(''Please provide your name'')?>");\r\n	this.defineField(''Testimonial[location]'').required("<?=__(''Please specify your location'')?>");\r\n	this.defineField(''Testimonial[comment]'').required("<?=__(''Please provide a testimonial'')?>");\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(58, 'provide:testimonials', 'provide;testimonials', '<? foreach ($provider->testimonials as $testimonial): ?>\r\n	<div class="row-fluid">\r\n		<div class="span10 columns">\r\n			<blockquote>\r\n				"<?=$testimonial->comment?>"\r\n				<cite><?=$testimonial->name?>, <?=$testimonial->location?></cite>\r\n			</blockquote>\r\n		</div>\r\n	</div>\r\n	<div class="pull-right">\r\n		<a href="javascript:;" class="button_delete_testimonial" \r\n			data-provider-id="<?=$provider->id?>" \r\n			data-testimonial-id="<?=$testimonial->id?>" \r\n			data-confirm="<?=__(''Are you sure you want to delete this testimonial?'')?>">\r\n			<?=__(''Delete this testimonial'')?>\r\n		</a>\r\n	</div>    \r\n<? endforeach ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(59, 'provide:testimonials_form', 'provide;testimonials_form', '<div class="row-fluid">\r\n	<div class="control-group">\r\n		<label for="testimonial_email" class="control-label"><?= __(''To Email'') ?></label>\r\n		<div class="controls">\r\n			<input type="text" name="Testimonial[email]" value="" id="testimonial_email" class="span12" />\r\n		</div>\r\n	</div>\r\n\r\n	<div class="control-group">\r\n		<label for="testimonial_subject" class="control-label"><?= __(''Subject'') ?></label>\r\n		<div class="controls">\r\n			<input type="text" name="Testimonial[subject]" value="<?= __(''Add a testimonial for me on %s'', c(''site_name'')) ?>" id="testimonial_subject" class="span12" />\r\n		</div>\r\n	</div>\r\n\r\n	<div class="control-group">\r\n		<label for="testimonial_message" class="control-label"><?= __(''Message'') ?></label>\r\n		<div class="controls">\r\n			<textarea name="Testimonial[message]" class="span12" id="testimonial_message"><?= __(''Would you add a brief recommendation of my work for my %s profile? Please let me know if you have any questions and thanks for your help!'', c(''site_name'')) ?></textarea>\r\n		</div>\r\n	</div>\r\n\r\n</div>\r\n\r\n<script>\r\n\r\nPage.provideTestimonialsFormFields = $.phpr.form().defineFields(function(){\r\n	this.defineField(''Testimonial[email]'').required("<?=__(''Please specify an email address'')?>");\r\n	this.defineField(''Testimonial[subject]'').required("<?=__(''Please specify a subject'')?>");\r\n	this.defineField(''Testimonial[message]'').required("<?=__(''Please specify message'')?>");\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(60, 'provide:work_hours_form', 'provide;work_hours_form', '<div id="work_hours_general">\r\n\r\n	<?=form_dropdown(''general_select_date'', array(\r\n		Core_Locale::date(''A_weekday_1'').'' - ''.Core_Locale::date(''A_weekday_7''),\r\n		Core_Locale::date(''A_weekday_1'').'' - ''.Core_Locale::date(''A_weekday_5''),\r\n		Core_Locale::date(''A_weekday_1'').'' - ''.Core_Locale::date(''A_weekday_6''),\r\n		__(''Weekends only''), __(''Specific days'')), '''', ''class="span3 general_select" '')?> \r\n\r\n	<?=__(''from %s to %s'', array(\r\n		form_dropdown(''general_select_start'', Phpr_DateTime::time_array(), ''08:00:00'', ''class="span3 general_select_start"''),\r\n		form_dropdown(''general_select_end]'', Phpr_DateTime::time_array(), ''18:00:00'', ''class="span3 general_select_end"'')\r\n	))?>\r\n\r\n</div>\r\n\r\n<div id="work_hours_specific" style="display:none">\r\n	<? for ($x = 0; $x < 7; $x++):  // Do seven times ?>\r\n	<div class="row-fluid" id="weekday_<?=$x+1?>">\r\n		<div class="span3 day_column">\r\n			<label class="checkbox"><?=form_checkbox(''schedule_''.$x.''_checkbox'', true)?><?=Core_Locale::date(''A_weekday_''.($x+1))?></label>\r\n		</div>\r\n		<div class="span9">\r\n			<div class="hide"><?=__(''Select to add your availability for %s'', Core_Locale::date(''A_weekday_''.($x+1)))?></div>\r\n			<div class="show">\r\n				<?=__(''from %s to %s'', array(\r\n					form_dropdown(''Provider[schedule_''.$x.''_start]'', Phpr_DateTime::time_array(array(null=>''--:--'')), form_value($provider, ''schedule_''.$x.''_start'', ''08:00:00''), ''class="span3"''),\r\n					form_dropdown(''Provider[schedule_''.$x.''_end]'', Phpr_DateTime::time_array(array(null=>''--:--'')), form_value($provider, ''schedule_''.$x.''_end'', ''18:00:00''), ''class="span3"'')\r\n				))?> \r\n				<? if ($x==0): ?>\r\n					<a href="javascript:;" id="link_apply_all"><?=__(''Apply to all'')?></a>\r\n				<? endif ?>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<? endfor ?>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(61, 'provide:work_radius_form', 'provide;work_radius_form', '<?\n	$radius_max = 100; // Maximum radius for work area\n	$radius_unit = Location_Config::create()->default_unit; // Miles (mi) or Kilometers (km)\n?>\n<?=form_hidden(''work_radius_max'', $radius_max, ''id="work_radius_max"'')?>\n<?=form_hidden(''work_radius_unit'', $radius_unit, ''id="work_radius_unit"'')?>\n<div class="row-fluid disabled" id="work_radius">\n	<div class="span8">\n		<div class="">\n			<div id="work_radius_radius"><span><?= form_value($provider, ''service_radius'', 25) ?></span> <?=$radius_unit?></div>\n			<?=form_label(__(''How far are you willing to travel?''))?>\n			<div class="radius_slider">\n				<div id="work_radius_slider"></div>\n				<ul id="work_radius_slider_legend">\n					<? for ($x = 0; $x < 11; $x++):  // Do eleven times ?>\n						<? if ($x==0): ?>\n							<li class="first">1</li>\n						<? elseif ($x==10): ?>\n							<li class="last"><?=$radius_max?></li>\n						<? else: ?>\n							<li><?=round(($radius_max/10)*($x))?></li>\n						<? endif ?>\n					<? endfor ?>\n				</ul>\n			</div>\n			<div id="work_radius_map"></div>\n		</div>\n	</div>\n	<div class="span4">\n		<div id="work_radius_areas">\n			<h5><?=__(''Your work area includes...'')?></h5>\n			<ul id="work_radius_area_list" data-empty-text="<?=__(''(No nearby areas)'')?>">\n				<li><?=__(''(No nearby areas)'')?></li>\n			</ul>\n		</div>\n	</div>\n</div>\n<div id="work_radius_disabled" class="well">\n	<?=__(''Please enter the address for your business'')?>\n</div>\n<input type="text" name="work_radius_address" value="" id="work_radius_address" style="display:none" />\n<input type="text" name="Provider[service_codes]" value="<?= form_value($provider, ''service_codes'') ?>" id="provider_service_codes" style="display:none" />\n<input type="text" name="Provider[service_radius]" value="<?= form_value($provider, ''service_radius'', 25) ?>" id="provider_service_radius" style="display:none" />\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(62, 'request:answer_form', 'request;answer_form', '<?\r\n	$question = (isset($question)) ? $question : null;\r\n?>\r\n<? if ($question): ?>\r\n<?\r\n	if ($question->answer)\r\n		$status = "answered";\r\n	else if ($question->is_public)\r\n		$status = "unanswered";\r\n	else\r\n		$status = "flagged";\r\n?>\r\n<div class="answer_form <?=$status?>">\r\n\r\n	<?=form_open(array(''id''=>''form_answer_question_''.$question->id))?>\r\n		<div class="row-fluid">\r\n			<div class="span11">\r\n				<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n				<input type="hidden" name="question_id" value="<?= $question->id ?>" />\r\n				<input type="hidden" name="Answer[is_public]" value="1" />\r\n\r\n				<!-- Question -->\r\n				<h5><?=$question->description?></h5>\r\n				<div class="author"><?=__(''Posted by'', true)?> <?=($question->provider) ? $question->provider->business_name : __(''Unknown'', true) ?></div>\r\n\r\n				<!-- Answer -->\r\n				<? if ($status == "answered"): ?>\r\n					<?=form_hidden(''answer_id'', $question->answer->id)?>\r\n					<div class="view">\r\n						<p><?=$question->answer->description?></p>\r\n					</div>\r\n					<div class="edit" style="display:none">\r\n						<div class="control-group">\r\n							<?=form_textarea(''Answer[description]'', $question->answer->description, ''class="span12" placeholder="''.__("Enter your answer here").''"'')?>\r\n						</div>\r\n						<?=form_submit(''answer'', __(''Update answer''), ''class="btn btn-success"'')?>\r\n						<div class="terms"><?=__(''Sharing your contact details is against our terms and conditions'')?></div>\r\n					</div>\r\n				<? elseif ($status == "unanswered"): ?>\r\n					<div class="control-group">\r\n						<?=form_textarea(''Answer[description]'', '''', ''class="span12" placeholder="''.__("Enter your answer here").''"'')?>\r\n					</div>\r\n					<?=form_submit(''answer'', __(''Answer'', true), ''class="btn"'')?>\r\n					<div class="terms"><?=__(''Sharing your contact details is against our terms and conditions'')?></div>\r\n				<? else: ?>\r\n					<p><span class="label label-important"><?=__(''You have flagged this question as inappropriate'')?></span></p>\r\n				<? endif ?>\r\n			</div>\r\n			<div class="span1 controls">\r\n				<? if ($status=="answered"): ?>\r\n					<a href="javascript:;" onclick="Page.questionEditToggle(<?=$question->id?>)">Edit</a>\r\n				<? elseif ($status=="unanswered"): ?>\r\n					<a href="javascript:;" onclick="Page.questionFlag(<?=$question->id?>)">Flag</a>\r\n				<? else: ?>\r\n					<a href="javascript:;" onclick="Page.questionFlag(<?=$question->id?>, true)">Unflag</a>\r\n				<? endif ?>\r\n			</div>\r\n		</div>\r\n	<?=form_close()?>\r\n\r\n</div>\r\n\r\n<script>\r\n\r\nPage.requestAnswerFormFields = $.phpr.form().defineFields(function() {\r\n	this.defineField(''Answer[description]'').required("<?=__(''Please enter an answer to this question'')?>");\r\n});\r\nPage.questionValidateForm(<?=$question->id?>, ''<?=$status?>'');\r\n\r\n</script>\r\n\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(63, 'request:booking_time', 'request;booking_time', '<? if ($quote->start_at): ?>\r\n	<div class="booking_time">\r\n		<?=$quote->start_at->to_time_format()?> <?=$quote->start_at->to_long_date_format()?> \r\n		<a href="javascript:;" id="link_booking_time_suggest_<?=$quote->id?>"><?=__(''Suggest another time'')?></a>\r\n	</div>\r\n<? else: ?>\r\n	<div class="booking_time"><?=__(''No appointment time yet'')?> <a href="javascript:;" id="link_booking_time_suggest_<?=$quote->id?>"><?=__(''Suggest a time'')?></a></div>\r\n<? endif ?>\r\n\r\n<div class="booking_time_suggest" id="booking_time_suggest_container_<?=$quote->id?>" style="display:none">\r\n	<?=form_open(array(''id'' => ''form_booking_time_''.$quote->id, ''class'' => ''form_booking_time''))?>\r\n		<input type="hidden" name="quote_id" value="<?= $quote->id ?>" />\r\n		<div class="row-fluid">\r\n			<div class="span8">\r\n				<div class="control-group select_booking_time_time">\r\n					<?=form_dropdown(''start_time'', Phpr_DateTime::time_array(), ''09:00:00'', ''class="booking_time_time span12" id="booking_time_time_''.$quote->id.''"'')?>\r\n				</div>\r\n				<div class="control-group field_booking_time_date">\r\n					<?=form_widget(''start_date'', array(\r\n						''class'' => ''Db_DatePicker_Widget'',\r\n						''field_id'' => ''booking_time_date_''.$quote->id,\r\n						''field_name'' => ''start_date'',\r\n						''on_select'' => ''$(this).closest("form").validate().element(this)'',\r\n						''css_class'' => ''span12''\r\n					))?>\r\n				</div>\r\n			</div>\r\n			<div class="span4"><?=form_submit(''suggest_time'', __(''Suggest''), ''class="btn btn-primary btn-block"'')?></div>\r\n		</div>\r\n	<?=form_close()?>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.requestBookingTimeFormFields = $.phpr.form().defineFields(function() {\r\n	this.defineField(''start_date'').required("<?=__(''Please provide a start date'',true)?>");\r\n	this.defineField(''start_time'').required("<?=__(''Please provide a start time'',true)?>");\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(64, 'request:category_form', 'request;category_form', '<div class="well">\r\n	<ul class="nav nav-pills" id="filter_navigation">\r\n		<li class="active"><a href="javascript:;" class="category"><?=__(''By Category'')?></a></li>\r\n		<li><a href="javascript:;" class="alpha"><?=__(''Browse A-Z'')?></a></li>\r\n	</ul>\r\n	<div class="row-fluid" id="category_form_select_category">\r\n		<div class="span6 columns">\r\n			<div class="control-group">\r\n				<label for="select_parent" class="control-label"><?= __(''Select a category:'') ?></label>\r\n				<div class="controls">\r\n					<?=form_dropdown(''select_parent'', array(), null, ''multiple="multiple" id="select_parent"'')?>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="span6 columns">\r\n			<div class="control-group">\r\n				<label for="select_category" class="control-label"><?= __(''Select a skill:'') ?></label>\r\n				<div class="controls">\r\n					<?=form_dropdown(''select_category'', array(), null, ''multiple="multiple" id="select_category"'')?>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class="row-fluid" id="category_form_select_alpha" style="display:none">\r\n		<div class="span6 columns">\r\n			<div class="control-group">\r\n				<label for="select_alpha" class="control-label"><?= __(''Select a specific skill:'') ?></label>\r\n				<div class="controls">\r\n					<?=form_dropdown(''select_alpha'', array(), null, ''multiple="multiple" id="select_alpha"'')?>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<p>\r\n		<?=__(''Still not finding what you are looking for?'')?>\r\n		<a href="javascript:;" id="link_suggest_category"><?=__(''Suggest a new service for %s'', c(''site_name''))?></a>\r\n	</p>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(65, 'request:custom_description', 'request;custom_description', '<?	\r\n	if (!isset($category) || !$category) {\r\n		$category = Bluebell_Request::get_category_from_role_name(post(''role_name'', $role_name));\r\n	}\r\n?>\r\n\r\n<? if ($category && $category->form): ?>\r\n	\r\n	<div class="row-fluid request-section">\r\n		<div class="span1 visible-desktop align-right">\r\n			<span class="ring-badge ring-badge-info"><i class="icon-info-sign"></i></span>\r\n		</div>\r\n		<div class="span10">\r\n			<?= $category->form->display_form(array(\r\n				''data'' => $request->custom_form_data,\r\n				''field_array_name'' => ''Custom'',\r\n				''field_classes'' => array(\r\n					''text'' => ''span12'',\r\n					''textarea'' => ''span12'',\r\n				)\r\n			)) ?>\r\n		</div>\r\n	</div>\r\n\r\n	<div class="row-fluid request-section">\r\n		<div class="span1 visible-desktop align-right">\r\n			<span class="ring-badge ring-badge-info"><i class="icon-pencil"></i></span>\r\n		</div>\r\n		<div class="span10">\r\n\r\n			<div class="control-group description">\r\n				<label for="request_description" class="control-label"><?= __(''Anything else the %s should know?'', $category->name) ?></label>\r\n				<textarea id="request_description" \r\n					name="Request[description]" \r\n					rows="5"\r\n					class="span12"><?=$request->description?></textarea>\r\n\r\n				<script> $(''#request_description'').autogrow(); </script>\r\n\r\n				<!-- Uploaded thumbnails -->\r\n				<?= $this->display_partial(''request:request_form_files'') ?>\r\n\r\n			</div>\r\n\r\n		</div>\r\n	</div>\r\n\r\n<? else: ?>\r\n\r\n	<div class="row-fluid request-section">\r\n		<div class="span1 visible-desktop align-right">\r\n			<span class="ring-badge ring-badge-info"><i class="icon-pencil"></i></span>\r\n		</div>\r\n		<div class="span10">\r\n\r\n			<div class="control-group description">\r\n				<label for="request_description" class="control-label"><?= __(''What do you need done?'') ?></label>\r\n				<textarea id="request_description" \r\n					name="Request[description]" \r\n					rows="5"\r\n					placeholder="<?= __(''Please describe your request. The more detail you provide, the better quality quotes.'') ?>"\r\n					class="span12"><?=$request->description?></textarea>\r\n\r\n				<script> $(''#request_description'').autogrow(); </script>\r\n\r\n				<!-- Uploaded thumbnails -->\r\n				<?= $this->display_partial(''request:request_form_files'') ?> \r\n\r\n			</div>\r\n\r\n		</div>\r\n	</div>\r\n\r\n<? endif ?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(66, 'request:manage_details', 'request;manage_details', '<?\r\n	$can_edit = isset($can_edit) ? $can_edit : true;\r\n?>\r\n<div class="row-fluid">\r\n	<div class="span6">\r\n		<div class="request_time">\r\n			<h4><?=__(''Requested appointment time(s)'')?></h4>\r\n			<p><?=Bluebell_Request::required_by($request)?></p>\r\n		</div>\r\n	</div>\r\n	<div class="span6">\r\n		<div class="request_location">\r\n			<h4><?=__(''Location'', true)?></h4>\r\n			<p>\r\n				<?=Bluebell_Request::location($request)?><br />				\r\n				<? if ($request->location_string): ?>\r\n					<a href="<?=Location_Map::get_map($request)?>" target="_blank" class="small">\r\n						<i class="icon-map-marker"></i> \r\n						<?=__(''Show map'', true)?>\r\n					</a>\r\n				<? endif ?>\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n<!-- Request Description -->\r\n<div class="request_description">\r\n\r\n	<? if ($custom_form_fields = Bluebell_Request::get_custom_form_fields($request)): ?>\r\n		\r\n		<!-- Custom form details -->\r\n		<? foreach ($custom_form_fields as $field): ?>\r\n			<h4><?= $field->label ?></h4>\r\n			<?= $field->current_value ?>\r\n		<? endforeach ?>\r\n		<h4><?= __(''Extra information'') ?></h4>\r\n		<?=$request->description_html?>\r\n\r\n	<? else: ?>\r\n\r\n		<!-- Regular form details -->\r\n		<h4><?=__(''What you need'')?></h4>\r\n		<?=$request->description_html?>\r\n\r\n	<? endif ?>\r\n	\r\n</div>\r\n\r\n\r\n<div class="added_info">\r\n	<? foreach ($request->get_extra_description() as $extra): ?>\r\n		<? \r\n			$extra_date = new Phpr_DateTime($extra->created_at); \r\n		?>\r\n		<blockquote>\r\n			<p>\r\n				<?=$extra->description?>\r\n				<small><?=__(''Additional information added %s ago'', Phpr_DateTime::interval_to_now($extra_date))?></small>\r\n			</p>\r\n		</blockquote>\r\n	<? endforeach ?>\r\n</div>\r\n<? if ($can_edit): ?>\r\n	<p>\r\n		<a href="javascript:;" id="link_add_description" \r\n			data-toggle-text="<?=__(''%s more information'', __(''Remove'',true))?>" \r\n			data-toggle-class="remove" \r\n			class="small">\r\n			<?=__(''%s more information'', __(''Add'',true))?>\r\n		</a>\r\n	</p>\r\n	<div id="panel_add_description" style="display:none">\r\n		<?=form_open(array(''id'' => ''add_description_form''))?>\r\n			<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n			<div class="control-group">\r\n				<div class="controls">\r\n					<textarea name="description" class="span12"></textarea>\r\n				</div>\r\n			</div>\r\n			<?=form_submit(''submit'', __(''Save'', true), ''class="btn btn-small"'')?>\r\n			<a href="javascript:;" id="link_add_description_cancel"><?=__(''Cancel'', true)?></a>\r\n		<?=form_close()?>\r\n		<script>\r\n			Page.addDescriptionFormFields = $.phpr.form().defineFields(function(){\r\n					this.defineField(''description'', ''Extra Description'').required("<?= __(''Please enter more request details'') ?>");\r\n				}).validate(''#add_description_form'')\r\n				.action(''service:on_describe_request'')\r\n				.update(''#p_details'', ''request:manage_details'');\r\n		</script>\r\n	</div>\r\n<? endif ?>\r\n\r\n<!-- Photos -->\r\n<h4><?=__(''Photos'', true)?></h4>\r\n<?=form_open(array(''id'' => ''form_add_photos''))?>\r\n	<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n	<div id="panel_photos" class="well" style="<?=(count($request->files)) ? '''' : ''display:none''?>">\r\n		<ul class="thumbnails">\r\n			<? foreach ($request->files as $file): ?>\r\n				<li>\r\n					<div class="thumbnail" data-image-id="<?=$file->id?>">\r\n						<img src="<?=$file->getThumbnailPath(100, 75, true, array(''mode''=>''crop''))?>" alt="" />\r\n						<a href="javascript:;" class="remove">Remove</a>\r\n					</div>\r\n				</li>\r\n			<? endforeach ?>\r\n		</ul>\r\n	</div>\r\n	<? if ($can_edit): ?>\r\n		<p><?=__(''%s about this job'', ''<a href="javascript:;" id="link_add_photos">''.__(''Attach photos'', true).''</a>'')?></a>\r\n		<input id="input_add_photos" type="file" name="files[]" multiple>\r\n	<? endif ?>\r\n<?=form_close()?>\r\n<script>\r\n	jQuery(document).ready(Page.bindAddPhotos);\r\n</script>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(67, 'request:questions', 'request;questions', '<ul class="nav nav-pills">\r\n	<li class="disabled"><a href="javascript:;"><?=__(''Filter'', true)?>:</a></li>\r\n	<li class="active"><a href="javascript:;" data-filter-type="all"><?=__(''All'')?></a></li>\r\n	<li><a href="javascript:;" data-filter-type="unanswered"><?=__(''Unanswered'')?></a></li>\r\n	<li><a href="javascript:;" data-filter-type="flagged"><?=__(''Flagged'')?></a></li>\r\n</ul>\r\n<div id="request_questions_all">\r\n	<? foreach ($request->questions as $question): ?>\r\n		<div id="p_request_answer_form_<?=$question->id?>">\r\n			<?=$this->display_partial(''request:answer_form'', array(''question''=>$question))?>\r\n		</div>\r\n	<? endforeach ?>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(68, 'request:quick_form', 'request;quick_form', '<?\r\n	$role = isset($role) ? $role : false;\r\n?>\r\n<div class="row-fluid">\r\n	<? if ($role): ?>\r\n		<input type="hidden" name="Request[category_id]" value="<?= $role->id ?>" />\r\n		<input type="hidden" name="Request[role_name]" value="<?= $role->name ?>" />\r\n	<? else: ?>\r\n		<section class="control-group title">\r\n			<label for="request_role_name" class="control-label"><?=__(''Who do you need?'')?></label>\r\n			<input type="text" name="Request[role_name]" value="" class="span12" id="request_role_name" placeholder="<?= __(''Handyman, Plumber, Cleaner, etc.'') ?>" />\r\n		</section>\r\n	<? endif ?>\r\n\r\n	<section class="control-group location">\r\n		<label for="request_location" class="control-label"><?=__(''Where do you need this service?'')?></label>\r\n		<input type="text" name="Request[address]" value="" class="span12" id="request_location" placeholder="<?= __(''Postal/zip code or full address'') ?>" />\r\n	</section>\r\n\r\n	<section class="control-group description" style="position:relative">\r\n		<label for="request_description" class="control-label"><?=__(''What do you need done?'')?></label>\r\n		<textarea name="Request[description]" class="span12" id="" id="request_description" placeholder="<?= __(''Please describe your request. The more detail you provide, the better quality quotes.'') ?>"></textarea>        \r\n	</section>\r\n\r\n	<? if ($this->user): ?>\r\n		<input type="hidden" name="Request[user_id]" value="<?= $this->user->id ?>" />\r\n	<? else: ?>\r\n	<section class="control-group email">\r\n		<label for="email_address" class="control-label"><?=__(''Enter your email address'')?></label>\r\n		<input type="text" name="User[email]" value="" class="span12" id="email_address" placeholder="<?= __(''Enter your email address for providers to contact you'') ?>" />\r\n	</section>\r\n	<? endif ?>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.requestQuickFormFields = $.phpr.form().defineFields(function(){\r\n<? if (!$role): ?>\r\n	this.defineField(''Request[role_name]'', ''Role Name'').required("<?=__(''Please enter a skill or occupation'',true)?>").action(''service:on_validate_category_name'', "<?=__(''Please select a valid service form the list'', true)?>");\r\n<? endif ?>\r\n	this.defineField(''Request[address]'', ''Address'').required("<?=__(''Please provide a valid location'',true)?>").action(''location:on_validate_address'', "<?=__(''Please provide a valid location'', true)?>");\r\n	this.defineField(''Request[description]'', ''Description'').required("<?=__(''Please describe your request'',true)?>").minWords(5, "<?=__(''Can you be more descriptive?'', true)?>");\r\n<? if (!$this->user): ?>\r\n	this.defineField(''User[email]'', ''Email'').required("<?=__(''Please specify your email address'',true)?>").email("<?=__(''Please specify a valid email address'',true)?>").action(''user:on_validate_email'', "<?=__(''Email address is already in use'', true)?>");\r\n<? endif ?>\r\n\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(69, 'request:quote_panel', 'request;quote_panel', '<div class="box">\r\n	<div class="box-header">\r\n		<h3><?=__(''Quote from %s'', $quote->provider->business_name)?></h3>\r\n	</div>\r\n	<div class="box-content">\r\n		<div class="row-fluid">\r\n			<div class="span5 push-seven mobile-span3">\r\n				<div class="logo align-center"><img src="<?=Bluebell_Provider::logo($quote->provider)?>" alt="" /></div>\r\n			</div>\r\n			<div class="span7 pull-five mobile-span9">\r\n				<div class="price"><?=format_currency($quote->price)?><span>00</span></div>\r\n				<div class="price_summary"><?=Bluebell_Quote::price_summary($quote)?></div>\r\n				<? if (Bluebell_Quote::price_terms($quote)): ?>\r\n					<div class="price_terms">* <?=Bluebell_Quote::price_terms($quote)?></div>\r\n				<? endif ?>\r\n				<div id="p_request_booking_time_<?=$quote->id?>"><?=$this->display_partial(''request:booking_time'', array(''quote''=>$quote))?></div>\r\n			</div>\r\n		</div>\r\n		<div class="quote_description">\r\n			<?=$quote->comment_html?>\r\n		</div>\r\n	</div>\r\n	<div class="box-footer">\r\n		<a href="javascript:;" class="btn btn-success btn-icon" id="button_quote_<?=$quote->id?>">\r\n			<i class="icon-ok"></i>\r\n			<?=__(''Accept quote'')?>\r\n		</a>\r\n		<span class="conversation_question">&nbsp; or <a href="javascript:;" id="button_question_<?=$quote->id?>"><?=__(''Ask a question'')?></a></span>\r\n		<p class="info"><?=__(''Click Accept Quote to accept the quote and schedule the job'')?></p>\r\n		<?=form_open(array(''id'' => ''form_conversation_''.$quote->id, ''class''=>''control_conversation''))?>\r\n			<?=$this->display_partial(''control:conversation'', array(''quote''=>$quote))?>\r\n		<?=form_close()?>\r\n	</div>\r\n</div>\r\n\r\n<div class="row-fluid">\r\n	<div class="span2">\r\n		<img src="<?=Bluebell_Provider::avatar($quote->provider)?>" width="75" height="75" alt="" />\r\n	</div>\r\n	<div class="span10">\r\n		\r\n		<h3><?=$quote->provider->business_name?></h3>\r\n		<? if ($quote->provider->url): ?><a href="<?=$quote->provider->url?>"><?=$quote->provider->url?></a><? endif ?>\r\n		<div class="location"><?=$quote->provider->location_string?></div>\r\n		<div class="summary">\r\n			<?=$quote->provider->description_html?>\r\n		</div>\r\n		\r\n		<? if ($quote_portfolio = $quote->provider->get_portfolio()): ?>\r\n			<h5><?=__(''Portfolio'')?></h5>\r\n			<div class="portfolio">\r\n				<?=$this->display_partial(''provide:portfolio'', array(''images''=>$quote_portfolio, ''container_id''=>''provider_portfolio_''.$quote->provider->id))?>\r\n			</div>\r\n		<? endif ?>\r\n\r\n		<h5><?=__(''Reviews'')?></h5>\r\n		<p class="overall star-rating">\r\n			<?=__(''Average rating'')?>: <i class="rating-<?=$quote->provider->rating*10?>"></i> \r\n			<span>\r\n				<? if ($quote->provider->rating_num > 0): ?>\r\n					(<?=__(''%s reviews'', $quote->provider->rating_num)?>)\r\n				<? else: ?>\r\n					(<?=__(''No ratings yet'')?>)\r\n				<? endif?>\r\n			</span>\r\n		</p>\r\n\r\n		<div class="tabbable"> \r\n			<ul class="nav nav-tabs">\r\n				<li class="active"><a href="#allreviews<?=$quote->provider->id?>" data-toggle="tab"><?=__(''All reviews'')?></a></li>\r\n				<li><a href="#fivestar<?=$quote->provider->id?>" data-toggle="tab"><?=__(''View 5 star reviews'')?></a></li>\r\n				<li><a href="#testimonials<?=$quote->provider->id?>" data-toggle="tab"><?=__(''Testimonials'')?></a></li>\r\n			</ul>\r\n			<div class="tab-content">\r\n				<div class="tab-pane active" id="allreviews<?=$quote->provider->id?>">\r\n					<?=$this->display_partial(''provide:ratings'', array(''ratings''=>$quote->provider->find_ratings()))?>\r\n				</div>\r\n				<div class="tab-pane" id="fivestar<?=$quote->provider->id?>">\r\n					<?=$this->display_partial(''provide:ratings'', array(''ratings''=>$quote->provider->find_ratings()->where(''rating=5'')))?>\r\n				</div>\r\n				<div class="tab-pane" id="testimonials<?=$quote->provider->id?>">\r\n					<?=$this->display_partial(''provide:ratings'', array(''ratings''=>$quote->provider->find_testimonials()))?>\r\n				</div>\r\n			</div>\r\n		</div>\r\n\r\n	</div>\r\n</div>\r\n\r\n<div id="popup_quote_<?=$quote->id?>" class="quote_popup modal hide fade" tabindex="-1" role="dialog">\r\n	<?=form_open(array(''id'' => ''form_quote_''.$quote->id))?>\r\n		<input type="hidden" name="quote_id" value="<?= $quote->id ?>" />\r\n		<input type="hidden" name="request_id" value="<?= $request->id ?>" />\r\n		<input type="hidden" name="redirect" value="<?= $request->get_url(''job/booking'').''/success'' ?>" />\r\n		<div class="modal-header">\r\n			<h2><?=__(''Accept this quote?'')?></h2>\r\n		</div>\r\n		<div class="modal-body">\r\n			<div class="quote_details">\r\n				<div class="row-fluid">\r\n					<div class="span2 mobile-span3"><p class="detail"><?=__(''Provider'')?></p></div>\r\n					<div class="span10 mobile-span9"><p><?=$quote->provider->business_name?></p></div>\r\n				</div>\r\n				<div class="row-fluid">\r\n					<div class="span2 mobile-span3"><p class="detail"><?=__(''Quote'')?></p></div>\r\n					<div class="span10 mobile-span9"><p><?=format_currency($quote->price)?></p></div>\r\n				</div>\r\n				<? if ($quote->start_at): ?>\r\n				<div class="row-fluid">\r\n					<div class="span2 mobile-span3"><p class="detail"><?=__(''Start time'')?></p></div>\r\n					<div class="span10 mobile-span9"><p><?=$quote->start_at->format(''%F %H:%M %p'')?></p></div>\r\n				</div>\r\n				<? endif ?>\r\n				<div class="row-fluid">\r\n					<div class="span2 mobile-span3"><p class="detail"><?=__(''Note'')?></p></div>\r\n					<div class="span10 mobile-span9"><p><?=Phpr_String::show_more_link($quote->comment, 150, __(''Show more'', true))?></p></div>\r\n				</div>\r\n			</div>\r\n			<hr />\r\n			<div class="customer_details">\r\n				<h4><?=__(''Provide your contact details'')?></h4>\r\n				<div class="row-fluid">\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><?= __(''First name'') ?></label> \r\n							<div class="controls">\r\n								<input type="text" name="User[first_name]" value="<?= $this->user->first_name ?>" class="span12" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><?= __(''Last name'') ?></label> \r\n							<div class="controls">\r\n								<input type="text" name="User[last_name]" value="<?= $this->user->last_name ?>" class="span12" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class="row-fluid">\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><?= __(''Phone number'') ?></label> \r\n							<div class="controls">\r\n								<input type="text" name="User[phone]" value="<?= $this->user->phone ?>" class="span12" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><?= __(''Mobile phone'') ?></label> \r\n							<div class="controls">\r\n								<input type="text" name="User[mobile]" value="<?= $this->user->mobile ?>" class="span12" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class="row-fluid">\r\n					<div class="span12">\r\n						<div class="control-group">\r\n							<label class="control-label"><?= __(''Street Address'') ?></label> \r\n							<div class="controls">\r\n								<input type="text" name="User[street_addr]" value="<?= $this->user->street_addr ?>" class="span12" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class="row-fluid">\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><?= __(''City'') ?></label> \r\n							<div class="controls">\r\n								<input type="text" name="User[city]" value="<?= $this->user->city ?>" class="span12" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><?= __(''Postal code'') ?></label> \r\n							<div class="controls">\r\n								<input type="text" name="User[zip]" value="<?= $this->user->zip ?>" class="span12" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class="row-fluid">\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><? __(''Country'') ?></label>\r\n							<div class="controls">\r\n								<?=form_dropdown(''User[country_id]'', \r\n									Location_Country::get_name_list(), \r\n									$this->user->country_id, \r\n									''id="popup_quote_''.$quote->id.''_country" class="span12"'', \r\n									 __(''-- Select --'', true)\r\n								 )?>\r\n							</div>\r\n						</div>\r\n					</div>\r\n					<div class="span6">\r\n						<div class="control-group">\r\n							<label class="control-label"><? __(''State'') ?></label>\r\n							<div class="controls">\r\n								<?=form_dropdown(''User[state_id]'', \r\n									Location_State::get_name_list($this->user->country_id), \r\n									$this->user->state_id, \r\n									''id="popup_quote_''.$quote->id.''_state" class="span12"'', \r\n									__(''-- Select --'', true)\r\n								)?>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<p class="terms"><?=__(''Accepting this quote is like a virtual handshake between you and %s. Help keep our community strong by honouring this quote.'', $quote->provider->business_name)?></p>\r\n		</div>\r\n		<div class="modal-footer">\r\n			<a href="javascript:;" class="popup-close pull-left"><?=__(''Review other quotes'')?></a>\r\n			<button name="done" class="btn btn-success btn-large btn-icon" id="button_quote_submit_''.$quote->id.''">\r\n				<i class="icon-ok"></i>\r\n				<?= __(''Create Booking!'') ?>\r\n			</button>\r\n		</div>\r\n		<script>\r\n\r\n			Page.requestQuotePanelFormFields = $.phpr.form().defineFields(function() {\r\n				this.defineField(''User[first_name]'').required("<?=__(''Please enter your first name'')?>");\r\n				this.defineField(''User[last_name]'').required("<?=__(''Please enter your surname'')?>");\r\n				this.defineField(''User[phone]'').required("<?=__(''Please enter your phone number'')?>");\r\n				this.defineField(''User[city]'').required("<?=__(''Please enter your city'')?>");\r\n				this.defineField(''User[zip]'').required("<?=__(''Please enter your zip or postal code'')?>");\r\n				this.defineField(''User[country_id]'').required("<?=__(''Please select your country'')?>");\r\n				this.defineField(''User[state_id]'').required("<?=__(''Please select your state'')?>");\r\n			});\r\n\r\n		</script>\r\n	<?=form_close()?>\r\n\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(70, 'request:quotes', 'request;quotes', '<div class="row-fluid">\r\n	<div class="span4 quote_list">\r\n		<ul>\r\n			<? foreach ($request->quotes as $quote): ?>\r\n			<? if (!$quote->provider) continue; ?>\r\n				<li id="quote_list_<?=$quote->id?>" data-quote-id="<?=$quote->id?>">\r\n					<div class="quote_item">\r\n						<? if ($quote->quote_type == Bluebell_Quote::quote_type_onsite): ?>\r\n							<div class="consult"><?=__("Consult req''d")?></div>\r\n						<? else: ?>\r\n							<div class="price"><?=format_currency($quote->price)?><span>00</span></div>\r\n						<? endif ?>\r\n						<div class="username"><a href="javascript:;"><?=$quote->provider->business_name?></a></div>\r\n						<div class="icons">\r\n							<? if ($quote->comment_html): ?><span class="note">Note</span><? endif ?>\r\n							<? if ($quote->messages->count > 0): ?><span class="chat">Chat</span><? endif ?>\r\n						</div>\r\n					</div>\r\n				</li>\r\n\r\n			<? endforeach ?>\r\n		</ul>\r\n	</div>\r\n	<div class="span8 quote_panel">\r\n\r\n		<? foreach ($request->quotes as $quote): ?>\r\n		<? if (!$quote->provider) continue; ?>\r\n			<div id="p_quote_panel_<?=$quote->id?>" class="quote" data-quote-id="<?=$quote->id?>" style="display:none">\r\n				<?=$this->display_partial(''request:quote_panel'', array(''quote''=>$quote))?>\r\n			</div>\r\n		<? endforeach ?>\r\n\r\n	</div>\r\n</div>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL);
INSERT INTO `cms_partials` (`id`, `name`, `file_name`, `content`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(71, 'request:request_form', 'request;request_form', '<!-- Who do you need? -->\n<div class="row-fluid request-section">\n	<div class="span1 visible-desktop align-right">\n		<span class="ring-badge ring-badge-info"><i class="fa fa-user"></i></span>\n	</div>\n	<div class="span10">\n		<div class="control-group title">\n			<label for="request_title" class="control-label"><?=__(''Who do you need?'')?></label> \n			<div class="controls">\n				<input type="text" id="request_title" \n					name="Request[title]" \n					value="<?= $role_name ?>" \n					placeholder="<?= __(''Handyman, Personal Trainer, Plumber, Cleaner, etc.'') ?>" \n					class="span12" />\n			</div>\n		</div>\n	</div>\n</div>\n\n<!-- When should they start? -->\n<div class="row-fluid request-section">\n	<div class="span1 visible-desktop align-right">\n		<span class="ring-badge ring-badge-info"><i class="fa fa-clock-o"></i></span>\n	</div>\n	<div class="span10">\n		<div class="control-group required_by">\n			<label class="control-label"><?= __(''When should they start?'') ?></label>\n			<div class="controls">\n				<ul class="radio-group" id="required_by_group">\n					<li>\n						<!-- Flexible -->\n						<label for="request_required_by_flexible" class="radio">\n							<input type="radio" id="request_required_by_flexible" \n								name="Request[required_by]" \n								<?= radio_state($request->required_by, Bluebell_Request::required_by_flexible) ?> \n								value="<?= Bluebell_Request::required_by_flexible ?>" />\n							<?=__("I''m flexible", true)?>\n						</label>\n						<div class="radio-expand">\n							<?=form_dropdown(''Request[required_type]'', array(\n								Bluebell_Request::required_type_flexible => __(''anytime'',true), \n								Bluebell_Request::required_type_flexible_week => __(''this week'',true), \n								Bluebell_Request::required_type_flexible_month => __(''this month'',true)\n							), $request->required_type)?>\n						</div>\n					</li>\n					<li>\n						<!-- Urgent! -->\n						<label for="request_required_by_urgent" class="radio urgent">\n							<input type="radio" id="request_required_by_urgent" \n								name="Request[required_by]" \n								<?= radio_state($request->required_by, Bluebell_Request::required_by_urgent) ?> \n								value="<?= Bluebell_Request::required_by_urgent ?>" />\n							<?=__("Within 48 hours (It''s urgent!)", true)?>\n						</label>\n						<div class="radio-expand">\n							<p>\n								<span class="label label-important"><i class="fa fa-bullhorn"></i> <?=__(''Request providers available to respond today!'')?></span>\n							</p>\n						</div>\n					</li>\n					<li>\n						<!-- At a specific date and time -->\n						<label for="request_required_by_firm" class="radio">\n							<input type="radio" id="request_required_by_firm" \n								name="Request[required_by]" \n								<?= radio_state($request->required_by, Bluebell_Request::required_by_firm) ?> \n								value="<?= Bluebell_Request::required_by_firm ?>" />\n							<?=__(''At a specific date and time'', true)?>\n						</label>\n						<div class="radio-expand firm">\n							<div class="well">\n\n								<!-- Primary time -->\n								<div class="control-group">\n									<div class="controls">\n										<div class="firm-date-primary">\n											<?=form_widget(''firm_date'', array(\n												''class'' => ''Db_DatePicker_Widget'',\n												''css_class'' => ''firm-date span3'',\n												''field_id'' => ''request_firm_date'',\n												''field_name'' => ''firm_date'',\n												''allow_past_dates'' => false,\n												''on_select'' => ''$(this).closest("form").validate().element(this)''\n											))?>\n											<?=__(''between %s and %s'', array(\n												form_dropdown(''firm_time_start'', Phpr_DateTime::time_array(), ''09:00:00''),\n												form_dropdown(''firm_time_end'', Phpr_DateTime::time_array(), ''10:00:00'')\n											))?> \n										</div>\n									</div>\n								</div>\n\n								<!-- Add an alternative time -->\n								<div class="firm-date-add">\n									<i class="fa fa-plus" data-toggle-class="fa fa-minus"></i>\n									<a href="javascript:;" id="link_request_alt_time" data-toggle-text="<?=__(''%s alternative time'', __(''Remove'',true))?>">\n										<?=__(''%s alternative time'', __(''Add'',true))?>\n									</a> \n									<?=__(''to increase the number of quotes'')?>\n								</div>\n\n								<!-- Secondary time -->\n								<div class="control-group">\n									<div class="controls">\n										<div class="firm-date-secondary" style="display:none">\n											<?=form_widget(''firm_date_alt'', array(\n												''class'' => ''Db_DatePicker_Widget'',\n												''css_class'' => ''firm-date span3'',\n												''field_id'' => ''request_firm_date_alt'',\n												''field_name'' => ''firm_date_alt'',\n												''allow_past_dates'' => false,\n												''on_select'' => ''$(this).closest("form").validate().element(this)''\n											))?>\n											<?=__(''between %s and %s'', array(\n												form_dropdown(''firm_time_alt_start'', Phpr_DateTime::time_array(), ''09:00:00''),\n												form_dropdown(''firm_time_alt_end'', Phpr_DateTime::time_array(), ''10:00:00'')\n											))?> \n										</div>\n									</div>\n								</div>\n\n							</div>\n						</div>\n\n					</li>\n				</ul>\n			</div>\n		</div>\n\n	</div>\n</div>\n\n<!-- Where do you need them? -->\n<div class="row-fluid request-section">\n	<div class="span1 visible-desktop align-right">\n		<span class="ring-badge ring-badge-info"><i class="fa fa-map-marker"></i></span>\n	</div>\n	<div class="span10">\n\n		<div class="control-group location">\n			<label for="request_location" class="control-label"><?= __(''Where do you need them?'') ?></label>\n			<div class="controls">\n				<input type="text" id="request_location" \n					name="Request[address]" \n					value="<?= (!empty($_POST[''zipcode'']) ? $_POST[''zipcode''] : $request->address_string) ?>"\n					placeholder="<?= __(''Enter your postal/zip code or full address'') ?>"\n					class="span12" />\n\n				<label class="checkbox location-remote" for="location_remote">\n					<input type="checkbox" id="location_remote"\n						name="Request[is_remote]" \n						<?= checkbox_state($request->is_remote) ?>\n						value="1" />\n					<?=__(''Job can be performed remotely'')?>\n				</label>\n			</div>\n		</div>\n\n	</div>\n</div>\n\n<!-- What do you need done? / Custom builder form -->\n<div id="p_request_custom_description">\n	<?= $this->display_partial(''request:custom_description'') ?>\n</div>\n\n<script>\n\nPage.requestSimpleFormFields = $.phpr.form().defineFields(function() {\n	this.defineField(''Request[title]'').required("<?=__(''Please enter a skill or occupation'',true)?>");\n	this.defineField(''Request[address]'').required("<?=__(''Please provide a valid location'',true)?>").action(''location:on_validate_address'', "<?=__(''Please provide a valid location'', true)?>");\n	this.defineField(''Request[description]'').required("<?=__(''Please describe your request'',true)?>").minWords(5, "<?=__(''Can you be more descriptive?'', true)?>");\n	this.defineField(''firm_date'').required("<?=__(''Please select a date'',true)?>").date("<?=__(''Please enter a valid date'',true)?>");\n	this.defineField(''firm_date_alt'').required("<?=__(''Please select a date'',true)?>").date("<?=__(''Please enter a valid date'',true)?>");\n});\n\n</script>\n', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-08-14 23:12:18'),
(72, 'request:request_form_files', 'request;request_form_files', '<div id="panel_photos" class="well" style="display:none">\r\n	<ul class="thumbnails"></ul>\r\n</div>\r\n<p><?=__(''%s about this job'', ''<a href="javascript:;" id="link_add_photos">''.__(''Attach photos'', true).''</a>'')?></p>\r\n<input id="input_add_photos" type="file" name="files[]" multiple>   \r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(73, 'request:review_form', 'request;review_form', '<div class="well">\r\n	<div class="row-fluid">\r\n		<div class="span4">\r\n\r\n			<!-- Basic information -->\r\n			<h6><?= __(''Who you are looking for'') ?></h6>\r\n			<p><?= $request->title ?></p>\r\n			<h6><?= __(''When do you need them'') ?></h6>\r\n			<p><?= Bluebell_Request::required_by($request) ?></p>\r\n			<h6><?= __(''Where do you need them'') ?></h6>\r\n			<p><?= Bluebell_Request::location($request) ?></p> \r\n\r\n		</div>\r\n		<div class="span6">\r\n\r\n			<? if ($custom_form_fields = Bluebell_Request::get_custom_form_fields($request)): ?>\r\n				\r\n				<!-- Custom form details -->\r\n				<? foreach ($custom_form_fields as $field): ?>\r\n					<h6><?= $field->label ?></h6>\r\n					<?= $field->current_value ?>\r\n				<? endforeach ?>\r\n				<h6><?= __(''Extra information'') ?></h6>\r\n				<?=$request->description_html?>\r\n\r\n			<? else: ?>\r\n\r\n				<!-- Regular form details -->\r\n				<h6><?= __(''What you need done'') ?></h6>\r\n				<?=$request->description_html?>\r\n\r\n			<? endif ?>\r\n\r\n		</div>\r\n		<div class="span2">\r\n\r\n			<!-- Edit request link -->\r\n			<a href="<?= root_url(''request/edit'') ?>" id="edit_request" \r\n				class="pull-right btn btn-primary btn-icon">\r\n				<i class="icon-pencil"></i> \r\n				<?= __(''Edit request'') ?>\r\n			</a>\r\n\r\n		</div>\r\n	</div>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(74, 'request:status_panel', 'request;status_panel', '<?\n	$can_edit = isset($can_edit) ? $can_edit : true;\n?>\n<div class="box-header">\n	<h6 class="pull-left"><?=__(''Request Status'', true)?></h6>\n	<div class="pull-right"><?=$request->status_name?></div>\n</div>\n<div class="box-content">\n	<? if ($can_edit): ?>\n		<ul>            \n			<? if ($request->status_code == Service_Status::status_expired): ?>\n				<li class="remaining"><?=__(''Quoting period has ended'')?>\n			<? else: ?>\n				<li class="remaining"><?=__(''%s remain'', $request->get_remaining_time())?>\n			<? endif ?>\n				<a href="javascript:;" id="link_extend_time" data-request-id="<?=$request->id?>" class="small"><?=__(''Extend time'')?></a>\n			</li>\n			<li class="quotes">\n				<? if ($request->total_quotes > 0): ?>\n					<a href="#quotes"><?=__(''%s quote(s)'', $request->total_quotes)?></a>\n				<? else: ?>\n					<?=__(''%s quote(s)'', $request->total_quotes)?>\n				<? endif ?>\n			</li>\n			<li class="questions">\n				<? if ($request->total_questions > 0): ?>\n					<a href="#questions"><?=__(''%s public question(s)'', $request->total_questions)?></a>\n				<? else: ?>\n					<?=__(''%s public question(s)'', $request->total_questions)?>\n				<? endif ?>\n			</li>\n			<li class="providers">\n				<a href="<?=$request->get_url(''request/providers'')?>"> Get Local Provider Suggestion </a>\n			</li>\n		</ul>\n	<? endif ?>\n</div>', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-08-16 22:40:30'),
(75, 'request:suggest_form', 'request;suggest_form', '<div class="row-fluid">\r\n	<fieldset class="form-horizontal">\r\n		<? if (!$this->user): ?>\r\n			<div class="control-group">\r\n				<label for="suggest_name" class="control-label"><?= __(''Your name'') ?></label>\r\n				<div class="controls">\r\n					<input type="text" name="name" id="suggest_name" class="span11" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class="control-group">\r\n				<label for="suggest_email" class="control-label"><?= __(''Your email'') ?></label>\r\n				<div class="controls">\r\n					<input type="text" name="email" id="suggest_email" class="span11" />\r\n				</div>\r\n			</div>\r\n		<? endif ?>\r\n\r\n		<div class="control-group">\r\n			<label for="suggest_category_name" class="control-label"><?= __(''Suggested category name'') ?></label>\r\n			<div class="controls">\r\n				<input type="text" name="category_name" id="suggest_category_name" class="span11" />\r\n			</div>\r\n		</div>\r\n	</fieldset>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.requestSuggestFormFields = $.phpr.form().defineFields(function() {\r\n	<? if (!$this->user): ?>\r\n		this.defineField(''email'').required("<?=__(''Please specify your email address'',true)?>").email("<?=__(''Please specify a valid email address'',true)?>");\r\n		this.defineField(''name'').required("<?=__(''Please specify your name'')?>");\r\n	<? endif ?>\r\n	this.defineField(''category_name'').required("<?=__(''Please specify a service name'')?>");\r\n});\r\n\r\n</script>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(76, 'site:404', 'site;404', '<?\r\n\r\n	$error_message = (isset($error_message)) ? $error_message : __(''Page not found'', true);\r\n\r\n?>\r\n<div class="page-header">\r\n	<h1><?=$error_message?></h1>\r\n	<h4 class="subheader"><?=__(''Something went wrong and we could not find what you were looking for...'', true)?></h4>\r\n</div>\r\n\r\n<?=global_content_block(''page_not_found'', ''Page not found'')?>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(77, 'site:about_menu', 'site;about_menu', '<?\r\n	$current_menu = isset($current_menu) ? $current_menu : '''';\r\n?>\r\n<ul class="nav nav-list">\r\n	<li class="nav-header"><?=__(''About'', true)?></li>\r\n	<li class="<?=$current_menu==''about''?''active'':''''?>"><a href="<?=root_url(''about'')?>"><?=__(''About %s'', c(''site_name''), true)?></a></li>\r\n	<li class="<?=$current_menu==''blog''?''active'':''''?>"><a href="<?=root_url(''blog'')?>"><?=__(''%s Blog'', c(''site_name''), true)?></a></li>\r\n	<li class="<?=$current_menu==''faq''?''active'':''''?>"><a href="<?=root_url(''faq'')?>"><?=__(''FAQ'', true)?></a></li>\r\n	<li class="divider"></li>\r\n	<li class="nav-header"><?=__(''Legal'', true)?></li>\r\n	<li class="<?=$current_menu==''terms''?''active'':''''?>"><a href="<?=root_url(''terms'')?>"><?=__(''Terms and Conditions'', true)?></a></li>\r\n	<li class="<?=$current_menu==''privacy''?''active'':''''?>"><a href="<?=root_url(''privacy'')?>"><?=__(''Privacy Policy'', true)?></a></li>\r\n	<li class="divider"></li>\r\n	<li class="nav-header"><?=__(''Contact'', true)?></li>\r\n	<li class="<?=$current_menu==''contact''?''active'':''''?>"><a href="<?=root_url(''contact'')?>"><?=__(''Contact Us'', true)?></a></li>\r\n</ul>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(78, 'site:footer', 'site;footer', '<div class="copyright">\r\n	&copy; <?=date(''Y'')?> <?=c(''site_name'')?>\r\n</div>				\r\n<ul class="links">\r\n	<li class="first"><a href="<?=root_url(''about'')?>"><?=__(''About'', true)?></a></li>\r\n	<li><a href="<?=root_url(''directory'')?>"><?=__(''Directory'', true)?></a></li>\r\n	<li><a href="<?=root_url(''blog'')?>"><?=__(''Blog'', true)?></a></li>\r\n	<li><a href="<?=root_url(''terms'')?>"><?=__(''Terms'', true)?></a></li>\r\n	<li><a href="<?=root_url(''privacy'')?>"><?=__(''Privacy'', true)?></a></li>\r\n	<li><a href="<?=root_url(''contact'')?>"><?=__(''Contact Us'', true)?></a></li>\r\n</ul>\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(79, 'site:head', 'site;head', '<meta name="description" content="<?= h($this->page->description) ?>" />\n<meta name="keywords" content="<?= h($this->page->keywords) ?>" />\n<meta name="author" content="Scripts Ahoy!" />\n<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->\n<?=$this->css_include(array(\n	// Framework\n		// Ahoy\n		''@/assets/stylesheets/css/shared/framework.css'',\n\n	// Extras\n	''@/assets/extras/font-awesome/css/font-awesome.css'',\n	''@/assets/extras/aristo/css/aristo.css'',\n	''@/assets/extras/carousel/css/skin.css'',\n\n	// Theme\n	''@/assets/stylesheets/css/layouts/application_layout/templates/default_template.css'',\n	''@/assets/stylesheets/css/layouts/application_layout/templates/home_template.css'',\n	''@/assets/stylesheets/css/layouts/application_layout/templates/content_template.css'',\n\n	// Shared / Controls\n	''@/assets/stylesheets/css/layouts/application_layout/shared/box.css'',\n	''@/assets/stylesheets/css/layouts/application_layout/shared/forms.css'',\n	''@/assets/stylesheets/css/layouts/application_layout/shared/badge_control.css'',\n	''@/assets/stylesheets/css/layouts/application_layout/shared/provider_form.css'',\n	''@/assets/stylesheets/css/layouts/application_layout/shared/offer_request_control.css'',\n	''@/assets/stylesheets/css/layouts/application_layout/shared/conversation_control.css'',\n), array(\n	''skip_cache'' => true,\n	''src_mode'' => true,\n)) ?>\n<?=$this->js_include(array(\n	// Framework\n		// Core\n		''jquery'',\n		''jquery-helper'',\n		''phpr-core'',\n		// Ahoy\n		''@/assets/scripts/js/shared/framework.js'',\n\n	// Extras\n	''@/assets/extras/carousel/js/jquery.jcarousel.js'',\n\n	// Theme\n	''@/assets/scripts/js/layouts/application_layout.js'',\n\n), array(\n	''skip_cache'' => true,\n	''src_mode'' => true,\n)) ?>\n\n<link rel="alternate" type="application/rss+xml" href="<?= root_url(''blog/feed'') ?>" title="<?= c(''site_name'') ?> RSS">\n\n<?=$this->display_head()?>\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(80, 'site:header', 'site;header', '<? \n	$logo = Cms_Config::get_logo();\n?>\n<? if ($logo): ?>\n	<div class="row-fluid">\n		<div class="span6 mobile-span2">\n			<div class="logo">\n				<a href="<?=root_url(''/'')?>"><img src="<?=$logo?>" alt="<?=c(''site_name'')?>" /></a>\n				<!--span class="hide-for-small motto"><?=__(''Get the job done your way!'', true)?></span-->\n			</div>\n		</div>\n		<div class="span6 mobile-span2">\n			<div id="site-user-menu">\n				<?=$this->display_partial(''site:user_menu'')?>\n			</div>\n		</div>\n	</div>\n<? else: ?>\n	<div class="logo">\n		<h1 class="text"><a href="<?=root_url(''/'')?>"><?=c(''site_name'')?></a></h1>\n	</div>\n	<div class="motto hidden-phone"><?=__(''Get the job done your way!'', true)?></div>\n	<div id="site-user-menu">\n		<?=$this->display_partial(''site:user_menu'')?>\n	</div>\n<? endif ?>', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-07-08 18:47:36'),
(81, 'site:login_form', 'site;login_form', '<div class="control-group">\r\n	<label for="user_login" class="control-label"><?= __(''Username or Email'', true) ?></label>\r\n	<div class="controls">\r\n		<input type="text" name="User[login]" value="" id="user_login" class="span12" />\r\n	</div>\r\n</div>\r\n\r\n<div class="control-group">\r\n	<label for="user_password" class="control-label"><?= __(''Password'', true) ?></label>\r\n	<div class="controls">\r\n		<input type="password" name="User[password]" value="" id="user_password" class="span12" />\r\n	</div>\r\n</div>\r\n\r\n<script>\r\n\r\nPage.siteLoginFormFields = $.phpr.form().defineFields(function() {\r\n	this.defineField(''User[login]'').required("<?=__(''Please specify your username or email address'',true)?>");\r\n	this.defineField(''User[password]'').required("<?=__(''Please specify your password'',true)?>").action(''user:on_validate_login'', "<?=__(''Password entered is invalid'',true)?>");\r\n});\r\n\r\n</script>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(82, 'site:main_menu', 'site;main_menu', '<?\n	$is_provider = ($this->user && $this->user->is_provider && !$this->user->is_requestor);\n	$is_requestor = ($this->user && $this->user->is_requestor && !$this->user->is_provider);\n	$is_newuser = !$this->user || ($this->user && !$this->user->is_requestor && !$this->user->is_provider);\n	$is_hybrid = ($this->user && $this->user->is_requestor && $this->user->is_provider);\n\n	// The $current_menu variable should be defined in the Pre Load Code field of all pages in the following way:\n	// $this->data[''current_menu''] = ''home'';\n	//\n	$current_menu = isset($current_menu) ? $current_menu : '''';\n?>\n<div class="navbar navbar-inverse">\n	<div class="navbar-inner">\n		\n		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">\n			<span class="icon-bar"></span>\n			<span class="icon-bar"></span>\n			<span class="icon-bar"></span>\n		</a>\n \n		<a class="brand visible-phone" href="#"><?=c(''site_name'')?></a>\n\n		<div class="nav-collapse collapse">\n\n			<ul class="nav">\n				<li class="<?=$current_menu==''dash''?''active'':''''?>">\n					<? if ($is_hybrid): ?>\n						<a href="<?=root_url(''/dashboard'')?>"><?=__(''Dashboard'', true)?></a>\n					<? elseif ($is_requestor): ?>\n						<a href="<?=root_url(''/dashboard'')?>"><?=__(''My Requests'', true)?></a>\n					<? elseif ($is_provider): ?>\n						<a href="<?=root_url(''/dashboard'')?>"><?=__(''My Jobs'', true)?></a>\n					<? elseif ($this->user): ?>\n						<a href="<?=root_url(''/dashboard'')?>"><?=__(''Dashboard'', true)?></a>\n					<? else: ?>\n						<a href="<?=root_url(''/'')?>"><?=__(''Home'', true)?></a>\n					<? endif ?>\n				</li>\n\n				<? if ($is_provider||$is_hybrid): ?>\n					<li class="<?=$current_menu==''provide''?''active'':''''?>"><a href="<?=root_url(''provide/profiles'')?>"><?=__(''Manage Profile'', true)?></a></li>\n				<? endif ?>\n\n				<? if ($is_requestor||$is_hybrid): ?>\n					<li class="<?=$current_menu==''request''?''active'':''''?>"><a href="<?=root_url(''request'')?>"><?=__(''Request Service'', true)?></a></li>\n				<? endif ?>\n\n				<? if ($is_newuser): ?>\n					<li class="<?=$current_menu==''intro''?''active'':''''?>"><a href="<?=root_url(''intro'')?>"><?=__(''How It Works'', true)?></a></li>\n					<li class="<?=$current_menu==''request''?''active'':''''?>"><a href="<?=root_url(''request'')?>"><?=__(''Request Service'', true)?></a></li>\n					<li class="<?=$current_menu==''provide''?''active'':''''?> visible-phone"><a href="<?=root_url(''provide'')?>"><?=__(''Provide a Service'', true)?></a></li>\n				<? endif?>\n			</ul>\n			<? if ($is_newuser): ?>\n				<ul class="nav pull-right">\n					<li class="hidden-phone"><a href="<?=root_url(''provide'')?>">\n						<?=__(''I want to provide my services'', true)?> \n					</a></li>\n				</ul>\n			<? endif?>\n\n		</div>\n\n	</div>\n</div>\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(83, 'site:pagination', 'site;pagination', '<?\r\n\r\n$current_page_index = $pagination->get_current_page_index();\r\n$page_number = $pagination->get_page_count();\r\n$suffix = isset($suffix) ? $suffix : null;\r\n$base_url = isset($base_url) ? $base_url : null;\r\n\r\n// Group anything over 10 pages\r\n$start_index = $current_page_index-5;\r\n$end_index = $current_page_index+5;\r\n$last_page_index = $page_number-1;\r\n\r\nif ($start_index < 0)\r\n	$start_index = 0;\r\n\r\nif ($end_index > $last_page_index)\r\n	$end_index = $last_page_index;\r\n\r\nif (($end_index - $start_index) < 11)\r\n	$end_index = $start_index + 11;\r\n\r\nif ($end_index > $last_page_index)\r\n	$end_index = $last_page_index;\r\n\r\nif (($end_index - $start_index) < 11)\r\n	$start_index = $end_index - 11;\r\n\r\nif ($start_index < 0)\r\n	$start_index = 0;\r\n\r\n?>\r\n<div class="pagination">\r\n	<ul>\r\n		<? if ($current_page_index): ?>\r\n			<li class="arrow"><a href="<?=($base_url) ? $base_url.''/''.($current_page_index).$suffix : ''javascript:;''?>" data-page="<?=$current_page_index?>">&laquo;</a></li>\r\n		<? else: ?>\r\n			<li class="arrow disabled"><a href="">&laquo;</a></li>    \r\n		<? endif ?>\r\n\r\n		<? if ($page_number < 11): ?>\r\n			\r\n			<? for ($i = 1; $i <= $page_number; $i++): ?>\r\n				<? if ($i != $current_page_index+1): ?>\r\n					<li><a href="<?=($base_url) ? $base_url.''/''.($i).$suffix : ''javascript:;''?>" data-page="<?=$i?>"><?=$i?></a></li>\r\n				<? else: ?>\r\n					<li class="active"><a href=""><?=$i?></a></li>\r\n				<? endif ?>\r\n			<? endfor ?>\r\n\r\n		<? else: ?>\r\n\r\n			<? if ($start_index > 0): ?>\r\n				<li><a href="<?=($base_url) ? $base_url.''/1''.$suffix : ''javascript:;''?>" data-page="1">1</a></li>\r\n				<? if ($start_index > 1): ?><li class="disabled"><a href="">&hellip;</a></li><? endif ?>\r\n			<? endif ?>\r\n\r\n			<? for ($i = $start_index+1; $i <= $end_index+1; $i++): ?>\r\n				<? if ($i != $current_page_index+1): ?>\r\n					<li><a href="<?=($base_url) ? $base_url.''/''.($i).$suffix : ''javascript:;''?>" data-page="<?=$i?>"><?=$i?></a></li>\r\n				<? else: ?>\r\n					<li class="active"><a href=""><?=$i?></a></li>\r\n				<? endif ?>\r\n			<? endfor ?>\r\n\r\n			<? if ($end_index < $last_page_index): ?>\r\n				<? if ($last_page_index - $end_index > 0): ?><li class="disabled"><a href="">&hellip;</a></li><? endif ?>\r\n				<li><a href="<?=($base_url) ? $base_url.''/''.($last_page_index+1).$suffix : ''javascript:;''?>" data-page="<?=($last_page_index+1)?>"><?=($last_page_index+1)?></a></li>\r\n			<? endif ?>\r\n			\r\n		<? endif ?>\r\n\r\n		<? if ($current_page_index < $page_number-1): ?>\r\n			<li class="arrow"><a href="<?=($base_url) ? $base_url.''/''.($current_page_index+2).$suffix : ''javascript:;''?>" data-page="<?=($current_page_index+2)?>">&raquo;</a></li>\r\n		<? else:?>\r\n			<li class="arrow disabled"><a href="">&raquo;</a></li>\r\n		<? endif ?>    \r\n	</ul>\r\n</div>\r\n<div>\r\n	<?=__(''<span class="interval">Showing <strong>%s - %s</strong> of </span><strong class="row_count">%s</strong> records'', array(\r\n		($pagination->get_first_page_row_index()+1),\r\n		($pagination->get_last_page_row_index()+1),\r\n		$pagination->get_row_count()\r\n	), true)?>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(84, 'site:register_form', 'site;register_form', '<!-- Auto Login Flag -->\n<input type="hidden" name="user_auto_login" value="1" />\n\n<div class="control-group">\n	<div class="controls">\n		<label for="user_email" class="control-label"><?= __(''Email Address'', true) ?></label>\n		<input type="text" name="User[email]" value="" id="user_email" class="span12" />\n	</div>\n</div>\n\n<div class="control-group">\n	<div class="controls">\n		<label for="user_username" class="control-label"><?= __(''Full name'', true) ?></label>\n		<input type="text" name="User[name]" value="" id="user_username" class="span12" />\n	</div>\n</div>\n\n<div class="control-group">\n	<div class="controls">\n		<label for="user_password" class="control-label"><?= __(''Create a Password'', true) ?></label>\n		<input type="password" name="User[password]" value="" id="user_password" class="span12" />\n	</div>\n</div>\n\n<div class="control-group">\n	<div class="controls">\n		<label for="user_password_confirm" class="control-label"><?= __(''Confirm Password'', true) ?></label>\n		<input type="password" name="User[password_confirm]" value="" id="user_password_confirm" class="span12" />\n	</div>\n</div>\n\n<script>\n\nPage.siteRegisterFormFields = $.phpr.form().defineFields(function() {\n	this.defineField(''User[email]'').required("<?=__(''Please specify your email address'',true)?>").email("<?=__(''Please specify a valid email address'',true)?>").action(''user:on_validate_email'', "<?=__(''Email address is already in use'', true)?>");\n	this.defineField(''User[name]'').required("<?=__(''Please specify Full name'',true)?>");\n	this.defineField(''User[password]'').required("<?=__(''Please specify a new password'',true)?>");\n	this.defineField(''User[password_confirm]'').required("<?=__(''Please specify a new password'',true)?>").matches(''#user_password'', "<?=__(''Password and confirmation password do not match.'',true)?>");\n});\n\n</script>', 'custom', NULL, NULL, 1, '2014-07-08 18:18:04', '2014-09-04 17:41:04'),
(143, 'site:location_bk', 'site;location_bk', '<div class="container">\n	<div class="wblocation">\n		<p class="ltitle">where is <span class="blue">wom</span><span class="green">Bids</span></p>\n	</div><!--.wblocation-->\n	<div class="locations">\n		<ul>\n			<li>California</li>\n			<li>Florida</li>\n			<li>Texas</li>\n			<li>Georgia</li>\n			<li>New Jersey</li>\n			<li>Pennsylvania</li>\n			<li>Virginia</li>\n			<li>Maryland</li>\n			<li>Colorado</li>\n			<li>Washington</li>\n			<li>New York</li>\n			<li>Illinois</li>\n			<li>Michigan</li>\n			<li>Minnesota</li>\n			<li>Massachusetts</li>\n			<li>North Carolina</li>\n			<li>Oregon</li>\n			<li>Alabama</li>\n			<li>Ohio</li>\n			<li>Arizona</li>\n			<li>Connecticut</li>\n			<li>Missouri</li>\n			<li>Rhode Island</li>\n			<li>Tennessee</li>\n			<li>Nevada</li>\n			<li>Wisconsin</li>\n			<li>South Carolina</li>\n			<li>Washington, DC</li>\n			<li>Delaware</li>\n			<li>Kansas</li>\n			<li>Indiana</li>\n			<li>Louisiana</li>\n			<li>Oklahoma</li>\n			<li>Utah</li>\n			<li>Nebraska</li>\n			<li>New Hampshire</li>\n			<li>Hawaii</li>\n			<li>Kentucky</li>\n			<li>Idaho</li>\n			<li>Iowa</li>\n		</ul>\n	</div>\n</div>', 'custom', NULL, 1, NULL, '2014-09-05 20:12:09', NULL),
(85, 'site:sub_menu', 'site;sub_menu', '<?\r\n	$categories = Service_Category::get_popular_categories()->where(''parent_id is null'')->limit(6)->find_all();\r\n?>\r\n<div class="tabs-below">\r\n	<ul class="nav nav-tabs">\r\n\r\n		<? foreach ($categories as $category): ?>\r\n		<?	\r\n			$children = $category->list_children();\r\n			$has_children = count($children);\r\n		?>\r\n			<li class="<?=$has_children?''dropdown'':''''?>">\r\n				<a href="<?=root_url(''directory/b/''.$category->url_name)?>" class="dropdown-toggle" data-toggle="dropdown">\r\n					<?=$category->name?> \r\n					<?=$has_children ? ''<b class="caret"></b>'' : '''' ?>\r\n				</a>\r\n\r\n				<? if ($has_children): ?>\r\n					<ul class="dropdown-menu">\r\n						<? foreach ($children as $role): ?>\r\n							<li><a href="<?=root_url(''directory/b/''.$category->url_name.''/''.$role->url_name)?>"><?=$role->name?></a></li>\r\n						<? endforeach ?>\r\n					</ul>\r\n				<? endif ?>\r\n			</li>\r\n		<? endforeach ?>\r\n		<li>\r\n			<a href="<?=root_url(''directory/browse'')?>"><?=__(''More'')?>&hellip;</a>\r\n		</li>\r\n	</ul>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(86, 'site:user_menu', 'site;user_menu', '<? if ($this->user) { ?>\r\n	<ul class="user">\r\n		<li>\r\n			<span><?=$this->user->username?></span>\r\n			<ul style="display:none">\r\n				<li><a href="<?=root_url(''account'')?>"><?=__(''Account Settings'', true)?></a></li>\r\n				<? if (!$this->user->is_requestor): ?><li><a href="<?=root_url(''request'')?>"><?=__(''Request Service'', true)?></a></li><? endif ?>\r\n				<? if (!$this->user->is_provider): ?><li><a href="<?=root_url(''provide'')?>"><?=__(''Provide Services'', true)?></a></li><? endif ?>\r\n				<li><a href="<?=root_url(''account/signout'')?>"><?=__(''Sign out'', true)?></a></li>\r\n			</ul>\r\n		</li>\r\n	</ul>\r\n	<script> jQuery(document).ready(Page.bindUserMenu); </script>\r\n<? } else { ?>\r\n	<ul class="guest">\r\n		<li><a href="<?=root_url(''account/signup'')?>"><?=__(''Register'', true)?></a></li>\r\n		<li><a href="<?=root_url(''account/signin'')?>"><?=__(''Sign in'', true)?></a></li>	\r\n	</ul>\r\n<? } ?>\r\n\r\n', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(87, 'widget:featured_provider_slideshow', 'widget;featured_provider_slideshow', '<? /* You can customise the homepage images with a featured group. \r\n		1. Select Service > Providers > Provider groups \r\n		2. Create a new group called Featured, name the API Code: featured\r\n		3. Assign providers who have good portfolios */ \r\n?>\r\n<div id="featured-providers" class="carousel slide">\r\n\r\n	<? if ($featured_providers): ?>\r\n\r\n		<!-- Featured providers -->\r\n		<ol class="carousel-indicators">\r\n			<?\r\n				$count = 0;\r\n			?>\r\n			<? foreach ($featured_providers as $portfolio): $count++; ?>\r\n				<li data-target="#featured-providers" data-slide-to="<?=$count?>" class="<?=$count==1?''active'':''''?>"></li>\r\n			<? endforeach ?>\r\n		</ol>\r\n		<div class="carousel-inner">\r\n			<?\r\n				$count = 0;\r\n			?>\r\n			<? foreach ($featured_providers as $portfolio): $count++; ?>\r\n				<div class="<?=$count==1?''active'':''''?> item">\r\n					<img src="<?=$portfolio->image?>" alt="" />\r\n					<div class="carousel-caption">\r\n						<h4><?=$portfolio->provider->business_name?></h4>\r\n						<p><?=$portfolio->provider->location_string?></p>\r\n					</div>\r\n				</div>\r\n			<? endforeach ?>\r\n		</div>\r\n\r\n	<? else: ?>\r\n\r\n		<!-- Sample content -->\r\n		<ol class="carousel-indicators">\r\n			<li data-target="#featured-providers" data-slide-to="0" class="active"></li>\r\n			<li data-target="#featured-providers" data-slide-to="1"></li>\r\n			<li data-target="#featured-providers" data-slide-to="2"></li>\r\n		</ol>\r\n		<div class="carousel-inner">\r\n			<div class="active item">\r\n				<img src="<?=theme_url(''assets/images/demo/plumber.jpg'')?>" alt="" />\r\n				<div class="carousel-caption">\r\n					<h4>Plumbers</h4>\r\n					<p><?=__(''Find a Plumber near you'')?></p>\r\n				</div>\r\n			</div>\r\n			<div class="item">\r\n				<img src="<?=theme_url(''assets/images/demo/chef.jpg'')?>" alt="" />\r\n				<div class="carousel-caption">\r\n					<h4>Chefs</h4>\r\n					<p><?=__(''How about a Chef for your next party'')?></p>\r\n				</div>\r\n			</div>\r\n			<div class="item">\r\n				<img src="<?=theme_url(''assets/images/demo/mechanic.jpg'')?>" alt="" />\r\n				<div class="carousel-caption">\r\n					<h4>Mechanics</h4>\r\n					<p><?=__(''Get a local Mechanic to fix your car'')?></p>\r\n				</div>\r\n			</div>\r\n		</div>	\r\n		\r\n	<? endif ?>\r\n	<a class="carousel-control left" href="#featured-providers" data-slide="prev">&lsaquo;</a>\r\n	<a class="carousel-control right" href="#featured-providers" data-slide="next">&rsaquo;</a>\r\n</div>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(88, 'widget:submit_request', 'widget;submit_request', '<?=form_open(array(''action''=>root_url(''request'')))?>\r\n	<label for="request_title"><?=__(''What service are you looking for?'')?></label>\r\n	<div class="input-append">\r\n		\r\n		<input type="text" name="role_name" placeholder="<?=__(''eg. Plumber, Gardener, Helper, etc.'')?>" id="request_title" class="span12" />\r\n		<input type="submit" value="<?=__(''Get a Quote'')?>" class="btn btn-primary" />\r\n	</div>\r\n	<script> Page.bindRoleSelect(''#request_title''); </script>\r\n<?=form_close()?>', 'custom', NULL, NULL, NULL, '2014-07-08 18:18:04', NULL),
(119, 'site:hmessage', 'site;hmessage', '<div class="mes-wrapper">\n	<div class="container">\n		<div class="row">\n			<div class="col-md-7 col-md-offset-1">\n				<div class="message">\n					<h3>Are you looking to grow your business?</h3>\n					<p>Every day thousands of professionals like you<br/>\n 						meet new customers through <span class="blue">wom</span><span class="green">Bids</span>.</p>\n					<button type="button" class="pull-right btn btn-default btn-lg btn-block">CREATE YOUR PROFESSIONAL PROFILE & START BIDDING <i class="fa fa-play-circle-o"></i> </button>\n				</div><!-- .message-->\n			</div>\n			<div class="col-md-3">\n				<img src="/themes/wombids/assets/images/hman.png">\n			</div>\n		</div>	\n	</div>\n</div><!--.mes-wrapper-->', 'custom', NULL, 1, NULL, '2014-07-26 00:17:47', NULL),
(120, 'site:location', 'site;location', '<div class="container">\n	<div class="wblocation">\n		<p class="ltitle">where is <span class="blue">wom</span><span class="green">Bids</span></p>\n	</div><!--.wblocation-->\n	<div class="locations">\n		<ul>\n			<li><a href="#" >California</a></li>\n			<li><a href="#" >Florida</a></li>\n			<li><a href="#" >Texas</a></li>\n			<li><a href="#" >Georgia</a></li>\n			<li><a href="#" >New Jersey</a></li>\n			<li><a href="#" >Pennsylvania</a></li>\n			<li><a href="#" >Virginia</a></li>\n			<li><a href="#" >California</a></li>\n			<li><a href="#" >Florida</a></li>\n			<li><a href="#" >Texas</a></li>\n			<li><a href="#" >Georgia</a></li>\n			<li><a href="#" >New Jersey</a></li>\n			<li><a href="#" >Pennsylvania</a></li>\n			<li><a href="#" >Virginia</a></li>\n			<li><a href="#" >California</a></li>\n			<li><a href="#" >Florida</a></li>\n			<li><a href="#" >Texas</a></li>\n			<li><a href="#" >Georgia</a></li>\n			<li><a href="#" >New Jersey</a></li>\n			<li><a href="#" >Pennsylvania</a></li>\n			<li><a href="#" >Virginia</a></li>\n			<li><a href="#" >California</a></li>\n			<li><a href="#" >Florida</a></li>\n			<li><a href="#" >Texas</a></li>\n			<li><a href="#" >Georgia</a></li>\n			<li><a href="#" >New Jersey</a></li>\n			<li><a href="#" >Pennsylvania</a></li>\n			<li><a href="#" >Virginia</a></li>\n			<li><a href="#" >California</a></li>\n			<li><a href="#" >Florida</a></li>\n			<li><a href="#" >Texas</a></li>\n			<li><a href="#" >Georgia</a></li>\n			<li><a href="#" >New Jersey</a></li>\n			<li><a href="#" >Pennsylvania</a></li>\n			<li><a href="#" >Virginia</a></li>\n			<li><a href="#" >California</a></li>\n			<li><a href="#" >Florida</a></li>\n			<li><a href="#" >Texas</a></li>\n			<li><a href="#" >Georgia</a></li>\n			<li><a href="#" >New Jersey</a></li>\n			<li><a href="#" >Pennsylvania</a></li>\n			<li><a href="#" >Virginia</a></li>\n		</ul>\n	</div>\n</div>', 'custom', NULL, 1, NULL, '2014-07-26 00:17:47', NULL),
(121, 'site:midmenu', 'site;midmenu', '<div class="midhead-top">&nbsp;</div>\n<div class="midhead">\n	<div class="midtext">\n		<span class="blue">wom</span><span class="green">Bids helps with</span>\n	</div>\n</div><!--.midhead-->\n\n<div class="midmenu">\n	<div class="container">\n		<ul class="menu-middle">\n			<li><a href="#">Air Conditioning & Cooling</a></li>\n			<li><a href="#">Cabinets & Countertops</a></li>\n			<li><a href="#">Carpentry</a></li>\n			<li><a href="#">Carpet</a></li>\n			<li><a href="#">Cleaning Services</a></li>\n			<li><a href="#">Concrete, Brick & Stone</a></li>\n			<li><a href="#">Decks, Porches, Gazebos & Play Equipment</a></li>\n			<li><a href="#">Decorators & Designers</a></li>\n			<li><a href="#">Driveways, Patios, Walks, Steps & Floors</a></li>\n			<li><a href="#">Drywall & Insulation</a></li>\n			<li><a href="#">Fences</a></li>\n			<li><a href="#">Flooring</a></li>\n			<li><a href="#">Garages, Doors, Openers</a></li>\n			<li><a href="#">Gutters</a></li>\n			<li><a href="#">Handyman Services</a></li>\n			<li><a href="#">Heating & Cooling</a></li>\n			<li><a href="#">Junk Removal</a></li>\n			<li><a href="#">Landscape, Decks & Fences</a></li>\n			<li><a href="#">Maintenance of Lawn, Trees & Shrubs</a></li>\n			<li><a href="#">Painting & Staining</a></li>\n			<li><a href="#">Plumbing</a></li>\n			<li><a href="#">Swimming Pools, Spas, Hot Tubs</a></li>\n			<li><a href="#">Tile & Stone</a></li>\n			<li><a href="#">Miscellaneous</a></li>\n		</ul>\n	</div>\n</div>	<!--.menu-middle-->', 'custom', NULL, 1, NULL, '2014-07-26 00:17:47', NULL),
(122, 'site:midtext', 'site;midtext', '<div class="container">\n	<div class="midtext">\n			<p>We’re making it easier and more affordable to find the best<br/>\n		contractors and home service pros in your area </p>\n	</div><!--.midtext-->\n</div>', 'custom', NULL, 1, NULL, '2014-07-26 00:17:47', NULL),
(123, 'site:slider', 'site;slider', '<div class="carousel slide">\n	<div class="box-wrapper col-md-7" style="margin:0px auto;">\n		<h3>Let''s get started choose an option</h3>\n		<div class="row">\n			<div class="col-md-5 sbox">\n				<h4 class="box-title">I need a home service pro</h4>\n				<p>Start your free search for contractors and professionals in your area</p>\n				<a href="<?=root_url(''/request'')?>" class="btn btn-search">Find a Pro</a>\n			</div>\n			<div class="mid-box col-md-1"></div>\n			<div class="col-md-5 rbox">\n				<h4 class="box-title">I want to bid on projects</h4>\n				<p>Contractors and home service pros can create a free profile and search for jobs</p>\n				<button type="button" class="btn btn-search">Find Projects</button>\n			</div>\n		</div><!--.row-->\n	</div><!--.box-wrapper-->\n</div><!--carousel-->', 'custom', NULL, 1, NULL, '2014-07-26 00:17:47', NULL),
(124, 'site:testimonials', 'site;testimonials', '<div class="speaks">\n	<div class="speak-title">\n		<span>client stories</span>\n	</div><!--.speak-title-->\n	<div class="container speak-items">\n		<div class="row">\n			<div class="col-md-4 col-md-offset-1">\n				<p class="speech-bubble"><i class="fa fa-quote-left fa-3"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>\n				<div class="item-writer col-md-8 col-md-offset-4">\n					<div class="media">\n						<a class="pull-right" href="#">\n						<img src="holder.js/50x50" alt="..." class="media-object img-circle"></a>\n						<p class="media-body">Charlotte Bronte<br/><small>jase Eyee</small></p>\n					</div>\n				</div><!--.item-writer-->\n			</div>\n			<div class="col-md-4 col-md-offset-1">\n				<p class="speech-bubble"><i class="fa fa-quote-left fa-3"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>\n				<div class="item-writer col-md-8 col-md-offset-4">\n					<div class="media">\n						<a class="pull-right" href="#">\n						<img src="holder.js/50x50" alt="..." class="media-object img-circle"></a>\n						<p class="media-body">Charlotte Bronte<br/><small>jase Eyee</small></p>\n					</div>\n				</div><!--.item-writer-->\n			</div>\n		</div>\n	</div>\n</div><!--.speaks-->', 'custom', NULL, 1, NULL, '2014-07-26 00:17:47', NULL),
(125, 'site:works', 'site;works', '<div class="container">\n	<div class="how-works">\n		<h3>SEE HOW IT WORKS</h3>\n	</div><!--.how-works-->\n	<div class="row">\n		<div class="col-md-5 hown col-md-offset-1">\n			<h4>I am a <span class="blue">Homeowner</span>?</h4>\n			<div class="row">\n				<span><i class="fa fa-laptop"></i> Sign up</span><br />\n				<small>Create an account. It''s easy</small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-pencil"></i> Post a Project</span><br />\n				<small>Professionals will contact<br/> you within 3 days, guaranteed</small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-star"></i> Review Profiles</span><br />\n				<small>Check out your candidates.<br/>\n				Request bids & additional info.</small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-check-circle"></i> Hire a Professional</span><br />\n				<small>Interview, check references<br/> and hire the one that work for you!</small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-dollar"></i> Pay for your project</span><br />\n				<small>Make and track payments<br/> in our Payments Center</small>\n			</div>\n			<a class="btn btn-default btn-block" href="<?=root_url(''/request'')?>">Post a Project</a>\n		</div><!--.hown-->\n		<div class="col-md-5 hser">\n			<h4>I am a <span class="green">Home Service Pro</span>?</h4>\n			<div class="row">\n				<span><i class="fa fa-laptop"></i> Sign up</span><br />\n				<small>Create an account. It''s easy</small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-users"></i> Create a Profile</span><br />\n				<small>List your skills and talents.<br/>Make it pop with photos and more.</small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-search"></i> Search for Jobs</span><br />\n				<small>New jobs are posted every minute!<br/><br/>    </small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-check-circle"></i> Get Hired</span><br />\n				<small>Apply to jobs,find the right match<br/> and voila-you''ve got a job!</small>\n			</div>\n			<div class="row">\n				<span><i class="fa fa-dollar"></i> Get Paid</span><br />\n				<small>Make payday a breeze by<br/> signing up for direct deposit.</small>\n			</div>\n			<button class="btn btn-default btn-block" type="button">Bid in Projects</button>\n		</div><!--.hser-->\n	</div>\n</div>', 'custom', NULL, 1, NULL, '2014-07-26 00:17:47', NULL),
(144, 'account:withdraw_profiles', 'account;withdraw_profiles', '<div class="block" id="block_ssn">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_ssn'',\n		''title'' => __(''SSN'',true),\n		''display_fields'' => array(\n			''ssn'' => array(''label''=>__(''Social Security Number''))\n		),\n		''edit_fields'' => array(\n			''ssn''=> array(\n				''label''=>__(''Social Security Number'',true),\n				''validate_rules''=>''{ required: true }'', \n				''validate_messages''=>''{ required: "''.__(''Please specify Social Security Number'', true).''" }''\n			),\n		)\n	))?>\n</div>\n<div class="block" id="block_femail">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_femail'',\n		''title'' => __(''Funding Email'',true),\n		''display_fields'' => array(\n			''funding_email'' => array(''label''=>__(''Your Funding Email''))\n		),\n		''edit_fields'' => array(\n			''funding_email''=> array(\n				''label''=>__(''Funding Email'',true),\n				''validate_rules''=>''{ required: true }'', \n				''validate_messages''=>''{ required: "''.__(''Please specify your Funding Email'', true).''" }''\n			),\n		)\n	))?>\n</div>\n<div class="block" id="block_accn">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_accn'',\n		''title'' => __(''Account No'',true),\n		''display_fields'' => array(\n			''account_no'' => array(''label''=>__(''Your Bank Account Number''))\n		),\n		''edit_fields'' => array(\n			''account_no''=> array(\n				''label''=>__(''Account no#'',true),\n				''validate_rules''=>''{ required: false }'', \n				''validate_messages''=>''{ required: "''.__(''Please specify your first name'', true).''" }''\n			),\n		)\n	))?>\n</div>\n<div class="block" id="block_routn">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_rountn'',\n		''title'' => __(''Rounting No'',true),\n		''display_fields'' => array(\n			''routing_no'' => array(''label''=>__(''Your Bank Routing Number''))\n		),\n		''edit_fields'' => array(\n			''routing_no''=> array(\n				''label''=>__(''Your Bank Routing Number'',true),\n				''validate_rules''=>''{ required: false }'', \n				''validate_messages''=>''{ required: "''.__(''Please specify your first name'', true).''" }''\n			),\n		)\n	))?>\n</div>\n<div class="block" id="block_txid">\n	<?=$this->display_partial(''account:detail_block'',  array(\n		''block_id'' => ''block_txid'',\n		''title'' => __(''Tax ID'',true),\n		''display_fields'' => array(\n			''tax_id'' => array(''label''=>__(''Your Tax ID''))\n		),\n		''edit_fields'' => array(\n			''tax_id''=> array(\n				''label''=>__(''Tax ID'',true),\n				''validate_rules''=>''{ required: false }'', \n				''validate_messages''=>''{ required: "''.__(''Please specify your first name'', true).''" }''\n			),\n		)\n	))?>\n</div>', 'custom', NULL, 1, 1, '2014-09-07 01:08:02', '2014-09-08 01:15:52'),
(142, 'payment:braintree', 'payment;braintree', '<p>Please provide your credit card information.</p>\r\n<script src="https://js.braintreegateway.com/v1/braintree.js"></script>\r\n <script>\r\n	  var braintree = Braintree.create("MIIBCgKCAQEAu7+hs/ZQsarQmKl7V4J+gUyPW7sOpkmfAgFgWAl2sZJlfAOo6LWT939WCcIY0M7YY4xnIbRHgCdA3Gwm03BCjIf19ayjfXYs13xoxozMAS/arDnMEeUkWCF6cFewitqBTjEwTcuAUy2zRvFWvareosBfEoT43OIky6SjqaB4ulew1i/8aw5/WYKmqG/ieB/BgRVPW+mY4X3vmOM3RXNFoXFxrh4FPYBlyBN1WkiaXb60pdstRft8Dt39/y0j5f3DUze37Kz8aQiUZyMsuiD7rUHSSFe82RS4tI2TaJNHQCVAXXzhGA01XB0JbX0iDNuTCoNAsXoMFETiD8JXj/Tb/QIDAQAB");\r\n      braintree.onSubmitEncryptForm(''braintree-payment-form'');\r\n  </script>\r\n<?= form_open(array(''id'' => ''braintree-payment-form'')) ?>\r\n	<?= flash_message() ?>\r\n	<div class="row-fluid">\r\n		<ul class="form braintreeForm">\r\n			<li class="field text fName">\r\n				<label for="FIRSTNAME">Cardholder First Name</label>\r\n				<div><input autocomplete="off" name="FIRSTNAME" value="" id="FIRSTNAME" type="text" class="text"/></div>\r\n			</li>		\r\n			\r\n			<li class="field text lName">\r\n				<label for="LASTNAME">Cardholder Last Name</label>\r\n				<div><input autocomplete="off" name="LASTNAME" value="" id="LASTNAME" type="text" class="text"/></div>\r\n			</li>		\r\n\r\n			<li class="field text cNumber">\r\n				<label for="ACCT">Credit Card Number</label>\r\n				<div><input autocomplete="off" name="ACCT" value="" id="ACCT" type="text" class="text"/></div>\r\n			</li>\r\n			\r\n			<li class="field select expMonth">\r\n				<label for="EXPDATE_MONTH">Expiration Date - Month</label>\r\n				<select autocomplete="off" name="EXPDATE_MONTH" id="EXPDATE_MONTH">\r\n					<? for ($month=1; $month <= 12; $month++): ?>\r\n						<option value="<?= $month ?>"><?= $month ?></option>\r\n					<? endfor ?> \r\n				</select>\r\n			</li>\r\n\r\n			<li class="field text expYear">\r\n				<label for="EXPDATE_YEAR">Expiration Date - Year</label>\r\n\r\n				<select autocomplete="off" name="EXPDATE_YEAR" id="EXPDATE_YEAR">\r\n					<?\r\n					  $start_year = Phpr_DateTime::now()->get_year();\r\n					  for ($year=$start_year; $year <= $start_year + 10; $year++): ?>\r\n						<option value="<?= $year ?>"><?= $year ?></option>\r\n					<? endfor ?> \r\n				</select>\r\n			</li>\r\n\r\n			<li class="field text cvv">\r\n				<label for="CVV2">\r\n					CVV2\r\n					<span class="comment">For MasterCard, Visa, and Discover, the CSC is the last three digits in the signature area on the back of your card. For American Express, it''s the four digits on the front of the card.</span>\r\n				</label>\r\n				\r\n				<div><input autocomplete="off" name="CVV2" value="" id="CVV2" type="text" class="text"/></div>\r\n			</li>\r\n		</ul>\r\n	</div>\r\n\r\n	<input type="button" onclick="return jQuery(this).phpr().post(''payment:on_pay'').send()" value="<?= __(''Submit Payment'') ?>" class="btn" />\r\n</form>', 'custom', NULL, 1, NULL, '2014-08-18 21:18:09', NULL);
INSERT INTO `cms_partials` (`id`, `name`, `file_name`, `content`, `theme_id`, `module_id`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(137, 'provide:description_short_form', 'provide;description_short_form', '<div class="row-fluid">\n	<div class="control-group">\n		<label for="provider_description_experience" class="control-label"><?= __(''Describe your work experience'') ?></label>\n		<textarea name="Provider[description_experience]" value="<?= form_value($provider, ''description_experience'') ?>" class="span12" id="provider_description_experience"></textarea>\n	</div>\n</div>', 'custom', NULL, 1, NULL, '2014-08-05 00:52:21', NULL),
(138, 'provide:membership_form', 'provide;membership_form', '<h4>Membership</h4>\n<?\n$has_membership = $provider->plan_id;\n\nif($has_membership){\n$pops = Service_Plan::get_object_list();\n$plan = $pops[$has_membership];\n\necho $plan->name;\n}\n?>\n<a href="javascript:;" id="link_update_membership"><?=__(''Change/Update Membership'')?></a>', 'custom', NULL, 1, 1, '2014-08-10 17:00:53', '2014-08-17 19:37:40'),
(139, 'payment:paypal_standard', 'payment;paypal_standard', '<?\n	$hidden_fields = $payment_type->get_hidden_fields($payment_type, $invoice);\n?>\n<p><?=__(''Click the button below to pay using PayPal'')?></p>\n<form action="<?=$payment_type->get_form_action($payment_type) ?>" method="post">\n	<? foreach ($hidden_fields as $name=>$value): ?>\n		<input type="hidden" name="<?=$name?>" value="<?=$value?>"/>\n	<? endforeach ?>\n	<input type="submit" class="btn" value="<?= __(''Pay with PayPal'') ?>" />\n</form>', 'custom', NULL, 1, NULL, '2014-08-13 16:42:02', NULL),
(140, 'request:providers', 'request;providers', '<div class="provider_list">\n	<? foreach ($providers as $provider): ?>\n	<div class="provider">\n		<div class="row-fluid">\n			<div class="span6">\n				<div class="badge-control">\n					<?=$this->display_partial(''control:badge'', array(''provider''=>$provider, ''badge_mode''=>''detailed''))?>\n				</div>\n			</div>\n			<div class="span6">\n				<? if ($provider->ratings->count): ?>\n				<? $rating = $provider->ratings->first(); ?>\n				<p>\n					<strong><?=__(''Recent job for a %s'', $rating->request_title)?>:</strong><br />\n						<?=$rating->comment?>\n				</p>\n					<? else: ?>\n					<p><?=__(''This provider has not been rated yet'')?></p>\n					<? endif ?>\n			</div>\n		</div>\n	</div>\n	<? endforeach ?>\n</div>', 'custom', NULL, 1, 1, '2014-08-16 00:47:43', '2014-08-16 22:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `cms_security`
--

CREATE TABLE IF NOT EXISTS `cms_security` (
  `id` varchar(25) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_security`
--

INSERT INTO `cms_security` (`id`, `name`, `description`) VALUES
('everyone', 'All', 'Page will be displayed to the public'),
('users', 'Registered users only', 'Only logged in users can access this page'),
('guests', 'Guests only', 'Only guests will be able to access this page');

-- --------------------------------------------------------

--
-- Table structure for table `cms_strings`
--

CREATE TABLE IF NOT EXISTS `cms_strings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `content` mediumtext,
  `original` mediumtext,
  `global` tinyint(4) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `theme_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `page_id` (`page_id`),
  KEY `theme_id` (`theme_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=996 ;

--
-- Dumping data for table `cms_strings`
--

INSERT INTO `cms_strings` (`id`, `code`, `content`, `original`, `global`, `page_id`, `theme_id`) VALUES
(1, 'happening_on_x_now', 'Happening on %s now', 'Happening on %s now', NULL, 16, 'custom'),
(2, 'reasons_to_use_x', 'Reasons to use %s', 'Reasons to use %s', NULL, 16, 'custom'),
(3, 'i_want_to_provide_my_services', 'I want to provide my services', 'I want to provide my services', 1, NULL, 'custom'),
(4, 'popular_categories', 'Popular categories', 'Popular categories', NULL, 16, 'custom'),
(5, 'from_the_blog', 'From the blog', 'From the blog', NULL, 16, 'custom'),
(6, 'get_the_job_done_your_way', 'Get the job done your way!', 'Get the job done your way!', 1, NULL, 'custom'),
(7, 'register', 'Register', 'Register', 1, NULL, 'custom'),
(8, 'sign_in', 'Sign in', 'Sign in', 1, NULL, 'custom'),
(9, 'home', 'Home', 'Home', 1, NULL, 'custom'),
(646, 'forgot_password', 'Forgot Password?', 'Forgot Password?', NULL, 27, 'custom'),
(11, 'request_service', 'Request Service', 'Request Service', 1, NULL, 'custom'),
(12, 'provide_a_service', 'Provide a Service', 'Provide a Service', 1, NULL, 'custom'),
(13, 'more', 'More', 'More', NULL, 16, 'custom'),
(14, 'we_connect_you_with_local_service_providers_who_are_ready_to_quote_on_your_job', 'We connect you with local service providers who are ready to quote on your job', 'We connect you with local service providers who are ready to quote on your job', NULL, 16, 'custom'),
(15, 'what_service_are_you_looking_for', 'What service are you looking for?', 'What service are you looking for?', NULL, 16, 'custom'),
(16, 'eg_plumber_gardener_helper_etc', 'eg. Plumber, Gardener, Helper, etc.', 'eg. Plumber, Gardener, Helper, etc.', NULL, 16, 'custom'),
(17, 'get_a_quote', 'Get a Quote', 'Get a Quote', NULL, 16, 'custom'),
(18, 'find_a_plumber_near_you', 'Find a Plumber near you', 'Find a Plumber near you', NULL, 16, 'custom'),
(19, 'how_about_a_chef_for_your_next_party', 'How about a Chef for your next party', 'How about a Chef for your next party', NULL, 16, 'custom'),
(20, 'get_a_local_mechanic_to_fix_your_car', 'Get a local Mechanic to fix your car', 'Get a local Mechanic to fix your car', NULL, 16, 'custom'),
(21, 'request_any_type_of_service', 'Request any type of service', 'Request any type of service', NULL, 16, 'custom'),
(22, 'compare_price_quotes', 'Compare price quotes', 'Compare price quotes', NULL, 16, 'custom'),
(23, 'schedule_the_job_online', 'Schedule the job online', 'Schedule the job online', NULL, 16, 'custom'),
(24, 'about', 'About', 'About', 1, NULL, 'custom'),
(25, 'directory', 'Directory', 'Directory', 1, NULL, 'custom'),
(26, 'blog', 'Blog', 'Blog', 1, NULL, 'custom'),
(27, 'terms', 'Terms', 'Terms', 1, NULL, 'custom'),
(28, 'privacy', 'Privacy', 'Privacy', 1, NULL, 'custom'),
(29, 'contact_us', 'Contact Us', 'Contact Us', 1, NULL, 'custom'),
(645, 'how_it_works', 'How it works', 'How it works', NULL, 27, 'custom'),
(31, 'quality_providers', 'Quality Providers', 'Quality Providers', NULL, 17, 'custom'),
(32, 'our_guarantee', 'Our Guarantee', 'Our Guarantee', NULL, 17, 'custom'),
(33, 'get_started', 'Get Started!', 'Get Started!', NULL, 17, 'custom'),
(34, 'already_have_an_account', 'Already have an account?', 'Already have an account?', NULL, 7, 'custom'),
(35, 'sign_in', 'Sign in', 'Sign in', NULL, 7, 'custom'),
(36, 'email_address', 'Email Address', 'Email Address', 1, NULL, 'custom'),
(37, 'username', 'Username', 'Username', 1, NULL, 'custom'),
(38, 'create_a_password', 'Create a Password', 'Create a Password', 1, NULL, 'custom'),
(39, 'confirm_password', 'Confirm Password', 'Confirm Password', 1, NULL, 'custom'),
(40, 'please_specify_your_email_address', 'Please specify your email address', 'Please specify your email address', 1, NULL, 'custom'),
(41, 'please_specify_a_valid_email_address', 'Please specify a valid email address', 'Please specify a valid email address', 1, NULL, 'custom'),
(42, 'email_address_is_already_in_use', 'Email address is already in use', 'Email address is already in use', 1, NULL, 'custom'),
(43, 'please_specify_a_username', 'Please specify a username', 'Please specify a username', 1, NULL, 'custom'),
(44, 'sorry_that_username_is_already_taken', 'Sorry that username is already taken!', 'Sorry that username is already taken!', 1, NULL, 'custom'),
(45, 'please_specify_a_new_password', 'Please specify a new password', 'Please specify a new password', 1, NULL, 'custom'),
(46, 'password_and_confirmation_password_do_not_match', 'Password and confirmation password do not match.', 'Password and confirmation password do not match.', 1, NULL, 'custom'),
(47, 'create_account', 'Create Account', 'Create Account', NULL, 7, 'custom'),
(48, 'terms_of_service', 'terms of service', 'terms of service', 1, NULL, 'custom'),
(49, 'by_pressing_create_account_you_agree_to_the_x', 'By pressing create account you agree to the %s.', 'By pressing create account you agree to the %s.', NULL, 7, 'custom'),
(50, 'why_x', 'Why %s', 'Why %s', NULL, 27, 'custom'),
(51, 'go_mobile', 'Go mobile', 'Go mobile', NULL, 27, 'custom'),
(644, 'how_it_works', 'How It Works', 'How It Works', 1, NULL, 'custom'),
(53, 'provide_services', 'Provide Services', 'Provide Services', NULL, 27, 'custom'),
(54, 'find_the_jobs_you_want_and_do_it_your_way', 'Find the jobs you want and do it your way!', 'Find the jobs you want and do it your way!', NULL, 27, 'custom'),
(55, 'find_jobs_on_the_go_with_your_mobile', 'Find jobs on the go with your mobile', 'Find jobs on the go with your mobile', NULL, 27, 'custom'),
(56, 'x_will_send_customer_requests_to_local_businesses_companies_and_professionals_who_can_do_t', '%s will send customer requests to local businesses, companies and professionals who can do the job at the location and time they request', '%s will send customer requests to local businesses, companies and professionals who can do the job at the location and time they request', NULL, 27, 'custom'),
(57, 'register_as_a_provider', 'Register as a Provider', 'Register as a Provider', NULL, 27, 'custom'),
(58, 'recieve_free_leads_from_customers_who_are_looking_for_your_services', 'Recieve free leads from customers who are looking for your services', 'Recieve free leads from customers who are looking for your services', NULL, 27, 'custom'),
(59, 'by_pressing_create_account_you_agree_to_the_x', 'By pressing create account you agree to the %s.', 'By pressing create account you agree to the %s.', NULL, 27, 'custom'),
(60, 'create_service_profile', 'Create Service Profile', 'Create Service Profile', NULL, 27, 'custom'),
(61, 'submit_your_request_to_our_pool_of_service_providers', 'Submit your request to our pool of service providers', 'Submit your request to our pool of service providers', NULL, 32, 'custom'),
(62, 'who_do_you_need', 'Who do you need?', 'Who do you need?', NULL, 32, 'custom'),
(63, 'handyman_personal_trainer_plumber_cleaner_etc', 'Handyman, Personal Trainer, Plumber, Cleaner, etc.', 'Handyman, Personal Trainer, Plumber, Cleaner, etc.', NULL, 32, 'custom'),
(64, 'when_should_they_start', 'When should they start?', 'When should they start?', NULL, 32, 'custom'),
(65, 'im_flexible', 'I''m flexible', 'I''m flexible', 1, NULL, 'custom'),
(66, 'anytime', 'anytime', 'anytime', 1, NULL, 'custom'),
(67, 'this_week', 'this week', 'this week', 1, NULL, 'custom'),
(68, 'this_month', 'this month', 'this month', 1, NULL, 'custom'),
(69, 'within_48_hours_its_urgent', 'Within 48 hours (It''s urgent!)', 'Within 48 hours (It''s urgent!)', 1, NULL, 'custom'),
(70, 'request_providers_available_to_respond_today', 'Request providers available to respond today!', 'Request providers available to respond today!', NULL, 32, 'custom'),
(71, 'at_a_specific_date_and_time', 'At a specific date and time', 'At a specific date and time', 1, NULL, 'custom'),
(72, 'between_x_and_x', 'between %s and %s', 'between %s and %s', NULL, 32, 'custom'),
(73, 'remove', 'Remove', 'Remove', 1, NULL, 'custom'),
(74, 'x_alternative_time', '%s alternative time', '%s alternative time', NULL, 32, 'custom'),
(75, 'add', 'Add', 'Add', 1, NULL, 'custom'),
(76, 'to_increase_the_number_of_quotes', 'to increase the number of quotes', 'to increase the number of quotes', NULL, 32, 'custom'),
(77, 'where_do_you_need_them', 'Where do you need them?', 'Where do you need them?', NULL, 32, 'custom'),
(78, 'enter_your_postalzip_code_or_full_address', 'Enter your postal/zip code or full address', 'Enter your postal/zip code or full address', NULL, 32, 'custom'),
(79, 'job_can_be_performed_remotely', 'Job can be performed remotely', 'Job can be performed remotely', NULL, 32, 'custom'),
(80, 'what_do_you_need_done', 'What do you need done?', 'What do you need done?', NULL, 32, 'custom'),
(81, 'please_describe_your_request_the_more_detail_you_provide_the_better_quality_quotes', 'Please describe your request. The more detail you provide, the better quality quotes.', 'Please describe your request. The more detail you provide, the better quality quotes.', NULL, 32, 'custom'),
(82, 'attach_photos', 'Attach photos', 'Attach photos', 1, NULL, 'custom'),
(83, 'x_about_this_job', '%s about this job', '%s about this job', NULL, 32, 'custom'),
(84, 'please_enter_a_skill_or_occupation', 'Please enter a skill or occupation', 'Please enter a skill or occupation', 1, NULL, 'custom'),
(85, 'please_provide_a_valid_location', 'Please provide a valid location', 'Please provide a valid location', 1, NULL, 'custom'),
(86, 'please_describe_your_request', 'Please describe your request', 'Please describe your request', 1, NULL, 'custom'),
(87, 'can_you_be_more_descriptive', 'Can you be more descriptive?', 'Can you be more descriptive?', 1, NULL, 'custom'),
(88, 'please_select_a_date', 'Please select a date', 'Please select a date', 1, NULL, 'custom'),
(89, 'please_enter_a_valid_date', 'Please enter a valid date', 'Please enter a valid date', 1, NULL, 'custom'),
(90, 'submit_request', 'Submit Request', 'Submit Request', NULL, 32, 'custom'),
(91, 'about_x', 'About %s', 'About %s', 1, NULL, 'custom'),
(92, 'x_blog', '%s Blog', '%s Blog', 1, NULL, 'custom'),
(93, 'faq', 'FAQ', 'FAQ', 1, NULL, 'custom'),
(94, 'legal', 'Legal', 'Legal', 1, NULL, 'custom'),
(95, 'terms_and_conditions', 'Terms and Conditions', 'Terms and Conditions', 1, NULL, 'custom'),
(96, 'privacy_policy', 'Privacy Policy', 'Privacy Policy', 1, NULL, 'custom'),
(97, 'contact', 'Contact', 'Contact', 1, NULL, 'custom'),
(98, 'username_or_email', 'Username or Email', 'Username or Email', 1, NULL, 'custom'),
(99, 'password', 'Password', 'Password', 1, NULL, 'custom'),
(100, 'please_specify_your_username_or_email_address', 'Please specify your username or email address', 'Please specify your username or email address', 1, NULL, 'custom'),
(101, 'please_specify_your_password', 'Please specify your password', 'Please specify your password', 1, NULL, 'custom'),
(102, 'password_entered_is_invalid', 'Password entered is invalid', 'Password entered is invalid', 1, NULL, 'custom'),
(103, 'log_in', 'Log in', 'Log in', NULL, 5, 'custom'),
(104, 'forgot_your_password', 'Forgot your password?', 'Forgot your password?', NULL, 5, 'custom'),
(105, 'register_a_new_account', 'Register a new account', 'Register a new account', NULL, 5, 'custom'),
(790, 'i_could_visit_onsite_as_soon_as_x_if_youd_like_a_price_quote_click_set_appointment_on_x_so', 'I could visit onsite as soon as %s. If you''d like a price quote, click ''Set Appointment'' on %s so we can share contact details and schedule a time.', 'I could visit onsite as soon as %s. If you''d like a price quote, click ''Set Appointment'' on %s so we can share contact details and schedule a time.', NULL, 18, 'custom'),
(789, 'please_enter_how_much_you_charge_for_travel', 'Please enter how much you charge for travel', 'Please enter how much you charge for travel', NULL, 18, 'custom'),
(788, 'please_enter_an_estimated_price', 'Please enter an estimated price', 'Please enter an estimated price', NULL, 18, 'custom'),
(786, 'how_much_do_you_charge_for_travel', 'How much do you charge for travel?', 'How much do you charge for travel?', NULL, 18, 'custom'),
(787, 'will_you_waive_the_travel_fee_if_you_are_picked_for_this_job', 'Will you waive the travel fee if you are picked for this job?', 'Will you waive the travel fee if you are picked for this job?', NULL, 18, 'custom'),
(785, 'do_you_charge_a_fee_to_travel_to_the_work_site', 'Do you charge a fee to travel to the work site?', 'Do you charge a fee to travel to the work site?', NULL, 18, 'custom'),
(784, 'what_is_the_estimated_price_range_for_this_work', 'What is the estimated price range for this work?', 'What is the estimated price range for this work?', NULL, 18, 'custom'),
(783, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarant', 'If this customer books you for this job, we will send you their contact details and guarantee you the job for a charge of 25% your price for labor.', 'If this customer books you for this job, we will send you their contact details and guarantee you the job for a charge of 25% your price for labor.', NULL, 18, 'custom'),
(782, 'submit_quote', 'Submit Quote', 'Submit Quote', NULL, 18, 'custom'),
(781, 'please_enter_a_message', 'Please enter a message', 'Please enter a message', NULL, 18, 'custom'),
(780, 'i_could_start_x_if_youd_like_to_move_forward_click_set_appointment_on_x_so_we_can_share_co', 'I could start %s. If you''d like to move forward, click ''Set Appointment'' on %s so we can share contact details and schedule a time.', 'I could start %s. If you''d like to move forward, click ''Set Appointment'' on %s so we can share contact details and schedule a time.', NULL, 18, 'custom'),
(779, 'use_this_as_an_opportunity_to_sell_your_services_tell_the_customer_some_reasons_they_shoul', 'Use this as an opportunity to sell your services. Tell the customer some reasons they should pick you.', 'Use this as an opportunity to sell your services. Tell the customer some reasons they should pick you.', NULL, 18, 'custom'),
(778, 'best_regards', 'Best regards', 'Best regards', NULL, 18, 'custom'),
(777, 'cheers', 'Cheers', 'Cheers', NULL, 18, 'custom'),
(775, 'thank_you', 'Thank you', 'Thank you', NULL, 18, 'custom'),
(776, 'sincerely', 'Sincerely', 'Sincerely', NULL, 18, 'custom'),
(773, 'in_two_weeks', 'in two weeks', 'in two weeks', NULL, 18, 'custom'),
(774, 'thanks', 'Thanks', 'Thanks', NULL, 18, 'custom'),
(772, 'next_week', 'next week', 'next week', NULL, 18, 'custom'),
(771, 'in_a_few_days', 'in a few days', 'in a few days', NULL, 18, 'custom'),
(766, 'dear_x', 'Dear %s', 'Dear %s', NULL, 18, 'custom'),
(767, 'hello_x', 'Hello %s', 'Hello %s', NULL, 18, 'custom'),
(768, 'hi_x', 'Hi %s', 'Hi %s', NULL, 18, 'custom'),
(769, 'today', 'today', 'today', NULL, 18, 'custom'),
(770, 'tomorrow', 'tomorrow', 'tomorrow', NULL, 18, 'custom'),
(765, 'personal_message', 'Personal Message', 'Personal Message', 1, NULL, 'custom'),
(764, 'minimum_cost_of_labor_must_be_x', 'Minimum cost of labor must be %s', 'Minimum cost of labor must be %s', NULL, 18, 'custom'),
(763, 'cost_of_labor_entered_must_be_a_number', 'Cost of labor entered must be a number', 'Cost of labor entered must be a number', NULL, 18, 'custom'),
(761, 'total_cost', 'Total Cost', 'Total Cost', NULL, 18, 'custom'),
(762, 'please_enter_your_cost_of_labor_min_x', 'Please enter your cost of labor (Min: %s)', 'Please enter your cost of labor (Min: %s)', NULL, 18, 'custom'),
(758, 'price_for_material', 'Price for material', 'Price for material', NULL, 18, 'custom'),
(759, 'list_any_materials_or_supplies_not_included_in_labor_price', 'List any materials or supplies not included in labor price', 'List any materials or supplies not included in labor price', NULL, 18, 'custom'),
(760, 'add_another_line_item', 'Add another line item', 'Add another line item', NULL, 18, 'custom'),
(757, 'item_or_material_description', 'Item or material description', 'Item or material description', NULL, 18, 'custom'),
(756, 'briefly_describe_what_is_and_is_not_included_in_your_labor_price', 'Briefly describe what is and is not included in your labor price', 'Briefly describe what is and is not included in your labor price', NULL, 18, 'custom'),
(755, 'price_for_labor', 'Price for labor', 'Price for labor', NULL, 18, 'custom'),
(754, 'description_of_work', 'Description of work', 'Description of work', NULL, 18, 'custom'),
(753, 'price', 'Price', 'Price', NULL, 18, 'custom'),
(751, 'flat_rate', 'Flat rate', 'Flat rate', NULL, 18, 'custom'),
(752, 'onsite_visit_required', 'Onsite visit required', 'Onsite visit required', NULL, 18, 'custom'),
(750, 'ignore_this_job_offer', 'Ignore this job offer', 'Ignore this job offer', NULL, 18, 'custom'),
(749, 'please_enter_your_question', 'Please enter your question', 'Please enter your question', NULL, 18, 'custom'),
(748, 'submit', 'Submit', 'Submit', 1, NULL, 'custom'),
(746, 'ask_a_question', 'Ask a question', 'Ask a question', NULL, 18, 'custom'),
(747, 'enter_your_question_here', 'Enter your question here', 'Enter your question here', NULL, 18, 'custom'),
(745, 'get_directions', 'Get directions', 'Get directions', NULL, 18, 'custom'),
(743, 'time', 'Time', 'Time', NULL, 18, 'custom'),
(744, 'location', 'Location', 'Location', NULL, 18, 'custom'),
(742, 'job_details', 'Job Details', 'Job Details', NULL, 18, 'custom'),
(741, 'x_request_from_x', '%s Request from %s', '%s Request from %s', 1, NULL, 'custom'),
(740, 'my_jobs', 'My Jobs', 'My Jobs', NULL, 18, 'custom'),
(738, 'view_profile', 'View Profile', 'View Profile', NULL, 26, 'custom'),
(739, 'quoting_closes_in', 'Quoting closes in', 'Quoting closes in', NULL, 3, 'custom'),
(737, 'this_provider_has_not_been_rated_yet', 'This provider has not been rated yet', 'This provider has not been rated yet', NULL, 70, 'custom'),
(736, 'view_profile', 'View Profile', 'View Profile', NULL, 70, 'custom'),
(734, 'get_a_free_quote', 'Get a free quote!', 'Get a free quote!', NULL, 14, 'custom'),
(735, 'get_quotes_from_a_x', 'Get Quotes from a %s', 'Get Quotes from a %s', NULL, 14, 'custom'),
(733, 'been_searching_high_and_low_for_the_best_x_to_meet_your_needs_look_no_further', 'Been searching high and low for the best %s to meet your needs? Look no further...', 'Been searching high and low for the best %s to meet your needs? Look no further...', NULL, 14, 'custom'),
(732, 'request_a_free_quote_from_a_x', 'Request a free quote from a %s', 'Request a free quote from a %s', NULL, 14, 'custom'),
(731, 'this_provider_has_not_been_rated_yet', 'This provider has not been rated yet', 'This provider has not been rated yet', NULL, 14, 'custom'),
(730, 'view_profile', 'View Profile', 'View Profile', NULL, 14, 'custom'),
(727, 'top_rated', 'Top Rated', 'Top Rated', NULL, 14, 'custom'),
(728, 'cheapest', 'Cheapest', 'Cheapest', NULL, 14, 'custom'),
(729, 'most_reliable', 'Most Reliable', 'Most Reliable', NULL, 14, 'custom'),
(724, 'search_for_providers_in_x_eg_plumber_gardener_etc', 'Search for providers in %s. Eg: Plumber, Gardener, etc.', 'Search for providers in %s. Eg: Plumber, Gardener, etc.', NULL, 14, 'custom'),
(725, 'browse_roles', 'Browse roles', 'Browse roles', NULL, 14, 'custom'),
(726, 'x_located_in_x', '%s located in %s', '%s located in %s', NULL, 14, 'custom'),
(723, 'anything_else_the_x_should_know', 'Anything else the %s should know?', 'Anything else the %s should know?', NULL, 32, 'custom'),
(722, 'sorry_that_profile_could_not_be_found', 'Sorry, that profile could not be found', 'Sorry, that profile could not be found', NULL, 29, 'custom'),
(721, 'services_requested', 'Services Requested', 'Services Requested', NULL, 3, 'custom'),
(720, 'jobs_offered', 'Jobs Offered', 'Jobs Offered', NULL, 3, 'custom'),
(719, 'enter_your_zip_code_or_full_address', 'Enter your zip code or full address.', 'Enter your zip code or full address.', NULL, 16, 'custom'),
(718, 'submit_1', 'SUBMIT', 'SUBMIT', NULL, 16, 'custom'),
(717, 'submit', 'Submit', 'Submit', NULL, 16, 'custom'),
(716, 'enter_zipcode_here', 'Enter zipcode here.', 'Enter zipcode here.', NULL, 16, 'custom'),
(715, 'what_service_do_you_need_', 'What service do you need ?', 'What service do you need ?', NULL, 16, 'custom'),
(192, 'has_a_new_request', 'has a new request', 'has a new request', NULL, 16, 'custom'),
(193, 'account_settings', 'Account Settings', 'Account Settings', 1, NULL, 'custom'),
(194, 'provide_services', 'Provide Services', 'Provide Services', 1, NULL, 'custom'),
(195, 'sign_out', 'Sign out', 'Sign out', 1, NULL, 'custom'),
(196, 'my_requests', 'My Requests', 'My Requests', 1, NULL, 'custom'),
(197, 'upcoming_appointments', 'Upcoming Appointments', 'Upcoming Appointments', NULL, 13, 'custom'),
(198, 'write_reviews', 'Write Reviews', 'Write Reviews', NULL, 13, 'custom'),
(199, 'no_reviews_required', 'No reviews required', 'No reviews required', 1, NULL, 'custom'),
(200, 'my_requests', 'My Requests', 'My Requests', NULL, 13, 'custom'),
(201, 'x_request', '%s Request', '%s Request', NULL, 13, 'custom'),
(202, 'less_than_a_minute', 'less than a minute', 'less than a minute', 1, NULL, 'custom'),
(203, 'to_get_quotes', 'to get quotes', 'to get quotes', NULL, 13, 'custom'),
(204, 'quote_received', 'quote received', 'quote received', NULL, 13, 'custom'),
(205, 'view_details', 'View Details', 'View Details', NULL, 13, 'custom'),
(206, 'view_my_service_requests', 'View my service requests', 'View my service requests', NULL, 13, 'custom'),
(207, 'account', 'Account', 'Account', NULL, 3, 'custom'),
(208, 'finances', 'Finances', 'Finances', NULL, 3, 'custom'),
(209, 'notifications', 'Notifications', 'Notifications', NULL, 3, 'custom'),
(210, 'work_history', 'Work History', 'Work History', NULL, 3, 'custom'),
(211, 'name', 'Name', 'Name', 1, NULL, 'custom'),
(212, 'your_real_name', 'Your real name', 'Your real name', NULL, 3, 'custom'),
(213, 'first_name', 'First Name', 'First Name', 1, NULL, 'custom'),
(214, 'please_specify_your_first_name', 'Please specify your first name', 'Please specify your first name', 1, NULL, 'custom'),
(215, 'last_name', 'Last Name', 'Last Name', 1, NULL, 'custom'),
(216, 'save', 'Save', 'Save', 1, NULL, 'custom'),
(217, 'cancel', 'Cancel', 'Cancel', 1, NULL, 'custom'),
(218, 'email', 'Email', 'Email', 1, NULL, 'custom'),
(219, 'your_email_address', 'Your email address', 'Your email address', NULL, 3, 'custom'),
(220, 'password', 'Password', 'Password', NULL, 3, 'custom'),
(221, 'your_password', 'Your password', 'Your password', NULL, 3, 'custom'),
(222, 'enter_current_password', 'Enter current password', 'Enter current password', 1, NULL, 'custom'),
(223, 'please_specify_the_old_password', 'Please specify the old password', 'Please specify the old password', 1, NULL, 'custom'),
(224, 'contact_details', 'Contact Details', 'Contact Details', NULL, 3, 'custom'),
(225, 'your_contact_phone', 'Your contact phone', 'Your contact phone', NULL, 3, 'custom'),
(226, 'your_mobile_phone', 'Your mobile phone', 'Your mobile phone', NULL, 3, 'custom'),
(227, 'your_address', 'Your address', 'Your address', NULL, 3, 'custom'),
(228, 'phone_number', 'Phone number', 'Phone number', 1, NULL, 'custom'),
(229, 'mobile_number', 'Mobile number', 'Mobile number', 1, NULL, 'custom'),
(230, 'address', 'Address', 'Address', 1, NULL, 'custom'),
(231, 'city', 'City', 'City', 1, NULL, 'custom'),
(232, 'zip__postal_code', 'Zip / Postal Code', 'Zip / Postal Code', 1, NULL, 'custom'),
(233, 'country', 'Country', 'Country', NULL, 3, 'custom'),
(234, 'state', 'State', 'State', NULL, 3, 'custom'),
(235, '--_select_--', '-- Select --', '-- Select --', 1, NULL, 'custom'),
(236, 'x_credits', '%s Credits', '%s Credits', NULL, 3, 'custom'),
(237, 'you_have_x_remaining_credits', 'You have %s remaining credits', 'You have %s remaining credits', NULL, 3, 'custom'),
(238, 'purchase_credits', 'Purchase credits', 'Purchase credits', NULL, 3, 'custom'),
(239, 'invoices', 'Invoices', 'Invoices', NULL, 3, 'custom'),
(240, 'general_notifications', 'General Notifications', 'General Notifications', NULL, 3, 'custom'),
(241, 'sms', 'SMS', 'SMS', 1, NULL, 'custom'),
(242, 'job_scheduled', 'Job scheduled', 'Job scheduled', NULL, 3, 'custom'),
(243, 'appointment_time_reminder', 'Appointment time reminder', 'Appointment time reminder', NULL, 3, 'custom'),
(244, 'job_completed', 'Job completed', 'Job completed', NULL, 3, 'custom'),
(245, 'ratingreview_left_about_you', 'Rating/review left about you', 'Rating/review left about you', NULL, 3, 'custom'),
(246, 'job_cancelled', 'Job cancelled', 'Job cancelled', NULL, 3, 'custom'),
(247, 'provide_services_notifications', 'Provide Services Notifications', 'Provide Services Notifications', NULL, 3, 'custom'),
(248, 'service_request_received', 'Service request received', 'Service request received', NULL, 3, 'custom'),
(249, 'question_about_your_request', 'Question about your request', 'Question about your request', NULL, 3, 'custom'),
(250, 'price_quote_on_your_request', 'Price quote on your request', 'Price quote on your request', NULL, 3, 'custom'),
(251, 'quoting_period_ended', 'Quoting period ended', 'Quoting period ended', NULL, 3, 'custom'),
(252, '12_hours_remaining_to_select_provider', '12 hours remaining to select Provider', '12 hours remaining to select Provider', NULL, 3, 'custom'),
(253, 'request_a_provider_notifications', 'Request a Provider Notifications', 'Request a Provider Notifications', NULL, 3, 'custom'),
(254, 'new_job_offer', 'New job offer', 'New job offer', NULL, 3, 'custom'),
(255, 'answer_to_your_question', 'Answer to your question', 'Answer to your question', NULL, 3, 'custom'),
(256, 'answer_to_another_providers_question', 'Answer to another Provider''s question', 'Answer to another Provider''s question', NULL, 3, 'custom'),
(257, 'stop_all_notifications', 'Stop All Notifications', 'Stop All Notifications', NULL, 3, 'custom'),
(258, 'block_all_notitications', 'Block all notitications', 'Block all notitications', NULL, 3, 'custom'),
(259, 'save_changes', 'Save changes', 'Save changes', NULL, 3, 'custom'),
(260, 'x_request', '%s Request', '%s Request', NULL, 3, 'custom'),
(261, 'to_get_quotes', 'to get quotes', 'to get quotes', NULL, 3, 'custom'),
(262, 'quote_received', 'quote received', 'quote received', NULL, 3, 'custom'),
(263, 'view_details', 'View Details', 'View Details', NULL, 3, 'custom'),
(264, 'span_classintervalshowing_strongx_-_xstrong_of_spanstrong_classrowcountxstrong_records', '<span class="interval">Showing <strong>%s - %s</strong> of </span><strong class="row_count">%s</strong> records', '<span class="interval">Showing <strong>%s - %s</strong> of </span><strong class="row_count">%s</strong> records', 1, NULL, 'custom'),
(265, 'page_not_found', 'Page not found', 'Page not found', 1, NULL, 'custom'),
(266, 'something_went_wrong_and_we_could_not_find_what_you_were_looking_for', 'Something went wrong and we could not find what you were looking for...', 'Something went wrong and we could not find what you were looking for...', 1, NULL, 'custom'),
(267, 'connect_using', 'Connect using', 'Connect using', NULL, 5, 'custom'),
(713, 'contact_information', 'Contact Information', 'Contact Information', NULL, 26, 'custom'),
(714, 'details_saved_successfully', 'Details saved successfully!', 'Details saved successfully!', 1, NULL, 'custom'),
(712, 'general_information', 'General Information', 'General Information', NULL, 26, 'custom'),
(710, 'pay_with_paypal', 'Pay with PayPal', 'Pay with PayPal', NULL, 24, 'custom'),
(711, 'leave_a_review_for_x', 'leave a review for %s', 'leave a review for %s', NULL, 26, 'custom'),
(709, 'click_the_button_below_to_pay_using_paypal', 'Click the button below to pay using PayPal', 'Click the button below to pay using PayPal', NULL, 24, 'custom'),
(708, 'browse_x_areas', 'Browse %s Areas', 'Browse %s Areas', NULL, 14, 'custom'),
(707, 'search', 'Search', 'Search', NULL, 14, 'custom'),
(705, 'send_request', 'Send request', 'Send request', NULL, 14, 'custom'),
(706, 'search_for_providers_enter_your_address_or_zippostal_code', 'Search for providers: Enter your address or zip/postal code', 'Search for providers: Enter your address or zip/postal code', NULL, 14, 'custom'),
(704, 'please_describe_your_request_the_more_detail_you_provide_the_better_quality_quotes', 'Please describe your request. The more detail you provide, the better quality quotes.', 'Please describe your request. The more detail you provide, the better quality quotes.', NULL, 14, 'custom'),
(703, 'what_do_you_need_done', 'What do you need done?', 'What do you need done?', NULL, 14, 'custom'),
(702, 'postalzip_code_or_full_address', 'Postal/zip code or full address', 'Postal/zip code or full address', NULL, 14, 'custom'),
(701, 'where_do_you_need_this_service', 'Where do you need this service?', 'Where do you need this service?', NULL, 14, 'custom'),
(700, 'handyman_plumber_cleaner_etc', 'Handyman, Plumber, Cleaner, etc.', 'Handyman, Plumber, Cleaner, etc.', NULL, 14, 'custom'),
(699, 'who_do_you_need', 'Who do you need?', 'Who do you need?', NULL, 14, 'custom'),
(698, 'request_a_free_quote', 'Request a free quote!', 'Request a free quote!', NULL, 14, 'custom'),
(697, 'providers_located_in_x', 'Providers located in %s', 'Providers located in %s', NULL, 14, 'custom'),
(696, 'your_comment_has_been_posted_thank_you', 'Your comment has been posted. Thank you!', 'Your comment has been posted. Thank you!', NULL, 10, 'custom'),
(694, 'comment', 'Comment', 'Comment', NULL, 10, 'custom'),
(695, 'submit_comment', 'Submit Comment', 'Submit Comment', NULL, 10, 'custom'),
(693, 'website_url', 'Website URL', 'Website URL', NULL, 10, 'custom'),
(692, 'comment_on_this_post', 'Comment on this post', 'Comment on this post', NULL, 10, 'custom'),
(691, 'no_comments_have_been_posted_yet', 'No comments have been posted yet', 'No comments have been posted yet', NULL, 10, 'custom'),
(690, 'comments', 'Comments', 'Comments', NULL, 10, 'custom'),
(688, 'show_more', 'Show more', 'Show more', 1, NULL, 'custom'),
(689, 'published_by_x_on_x_in_x', 'Published by %s on %s in %s', 'Published by %s on %s in %s', NULL, 10, 'custom'),
(687, 'comments', 'Comments', 'Comments', NULL, 8, 'custom'),
(686, 'published_by_x_on_x', 'Published by %s on %s.', 'Published by %s on %s.', NULL, 8, 'custom'),
(680, 'credits', 'Credits', 'Credits', NULL, 22, 'custom'),
(681, 'price', 'Price', 'Price', NULL, 22, 'custom'),
(682, 'xcredit', '(%s/credit)', '(%s/credit)', NULL, 22, 'custom'),
(683, 'purchase_credits', 'Purchase Credits', 'Purchase Credits', NULL, 22, 'custom'),
(684, 'not_specified', 'Not specified', 'Not specified', 1, NULL, 'custom'),
(685, 'read_more', 'Read more', 'Read more', 1, NULL, 'custom'),
(679, 'x_credits', '%s Credits', '%s Credits', NULL, 22, 'custom'),
(677, 'subtotal', 'Subtotal', 'Subtotal', NULL, 24, 'custom'),
(678, 'please_choose_a_payment_method', 'Please choose a payment method', 'Please choose a payment method', NULL, 24, 'custom'),
(676, 'total', 'Total', 'Total', NULL, 24, 'custom'),
(675, 'tax', 'Tax', 'Tax', NULL, 24, 'custom'),
(674, 'discount', 'Discount', 'Discount', NULL, 24, 'custom'),
(673, 'price', 'Price', 'Price', NULL, 24, 'custom'),
(672, 'item', 'Item', 'Item', NULL, 24, 'custom'),
(671, 'date_x', 'Date: %s', 'Date: %s', NULL, 24, 'custom'),
(670, 'invoice_x', 'Invoice %s', 'Invoice %s', NULL, 24, 'custom'),
(669, 'phone_number', 'Phone number', 'Phone number', NULL, 24, 'custom'),
(667, 'your_surname', 'Your surname', 'Your surname', NULL, 24, 'custom'),
(668, 'business_address', 'Business address', 'Business address', NULL, 24, 'custom'),
(666, 'your_name', 'Your name', 'Your name', NULL, 24, 'custom'),
(665, 'bill_to', 'Bill to', 'Bill to', NULL, 24, 'custom'),
(664, 'pay_this_invoice', 'Pay this Invoice', 'Pay this Invoice', NULL, 23, 'custom'),
(663, 'this_invoice_has_not_been_paid', 'This invoice has not been paid', 'This invoice has not been paid', NULL, 23, 'custom'),
(661, 'subtotal', 'Subtotal', 'Subtotal', NULL, 23, 'custom'),
(662, 'sorry_there_are_no_payment_gateways_available_to_use', 'Sorry there are no payment gateways available to use!', 'Sorry there are no payment gateways available to use!', NULL, 23, 'custom'),
(660, 'total', 'Total', 'Total', NULL, 23, 'custom'),
(657, 'price', 'Price', 'Price', NULL, 23, 'custom'),
(658, 'discount', 'Discount', 'Discount', NULL, 23, 'custom'),
(659, 'tax', 'Tax', 'Tax', NULL, 23, 'custom'),
(656, 'item', 'Item', 'Item', NULL, 23, 'custom'),
(655, 'date_x', 'Date: %s', 'Date: %s', NULL, 23, 'custom'),
(654, 'invoice_x', 'Invoice %s', 'Invoice %s', NULL, 23, 'custom'),
(653, 'bill_to', 'Bill to', 'Bill to', NULL, 23, 'custom'),
(652, 'changeupdate_membership', 'Change/Update Membership', 'Change/Update Membership', NULL, 29, 'custom'),
(651, 'update_membership', 'Update Membership', 'Update Membership', NULL, 29, 'custom'),
(648, 'why', 'Why?', 'Why?', NULL, 27, 'custom'),
(649, 'update_this_profile', 'update this profile', 'update this profile', NULL, 29, 'custom'),
(650, 'update_your_x_membership', 'Update your %s membership', 'Update your %s membership', NULL, 29, 'custom'),
(647, 'click_to_login_with', 'Click to login with', 'Click to login with', NULL, 27, 'custom'),
(643, 'sorry_that_provider_could_not_be_found', 'Sorry that provider could not be found', 'Sorry that provider could not be found', NULL, 28, 'custom'),
(642, 'add_photos_or_prior_work', 'Add photos or prior work', 'Add photos or prior work', NULL, 28, 'custom'),
(641, 'click_to_add_portfolio_photos', 'Click to add portfolio photos', 'Click to add portfolio photos', NULL, 28, 'custom'),
(639, 'x_to_your_portfolio', '%s to your portfolio', '%s to your portfolio', NULL, 28, 'custom'),
(640, 'add__remove_photos', 'Add / Remove Photos', 'Add / Remove Photos', NULL, 28, 'custom'),
(638, 'portfolio', 'Portfolio', 'Portfolio', NULL, 28, 'custom'),
(634, 'lost', 'Lost', 'Lost', NULL, 3, 'custom'),
(635, 'ignored', 'Ignored', 'Ignored', NULL, 3, 'custom'),
(636, 'cancelled', 'Cancelled', 'Cancelled', NULL, 3, 'custom'),
(637, 'no_job_offers_found', 'No job offers found', 'No job offers found', NULL, 3, 'custom'),
(633, 'performed', 'Performed', 'Performed', NULL, 3, 'custom'),
(632, 'open', 'Open', 'Open', NULL, 3, 'custom'),
(631, 'skill_profiles', 'Skill Profiles', 'Skill Profiles', NULL, 3, 'custom'),
(630, 'no_new_messages', 'No new messages', 'No new messages', 1, NULL, 'custom'),
(625, 'done', 'Done', 'Done', 1, NULL, 'custom'),
(626, 'view_top_profiles', 'View top profiles', 'View top profiles', NULL, 29, 'custom'),
(627, 'send', 'Send', 'Send', 1, NULL, 'custom'),
(628, 'you_have_no_upcoming_appointments', 'You have no upcoming appointments', 'You have no upcoming appointments', 1, NULL, 'custom'),
(629, 'messages', 'Messages', 'Messages', 1, NULL, 'custom'),
(624, 'why_do_your_customers_like_working_with_you', 'Why do your customers like working with you?', 'Why do your customers like working with you?', NULL, 28, 'custom'),
(623, 'what_kind_of_projects_do_you_work_on', 'What kind of projects do you work on?', 'What kind of projects do you work on?', NULL, 28, 'custom'),
(622, 'describe_your_work_experience', 'Describe your work experience', 'Describe your work experience', NULL, 28, 'custom'),
(620, 'about_your_company', 'About Your Company', 'About Your Company', NULL, 28, 'custom'),
(621, 'click_to_add_a_short_description_about_your_business', 'Click to add a short description about your business', 'Click to add a short description about your business', NULL, 28, 'custom'),
(619, 'complete_your_profile_and_connect_with_new_customers_today_research_shows_that_service_pro', 'Complete your profile and connect with new customers today, Research shows that service providers who fill out their profiles entirely are more likely to win bids.', 'Complete your profile and connect with new customers today, Research shows that service providers who fill out their profiles entirely are more likely to win bids.', NULL, 28, 'custom'),
(618, 'no_service_requests_found', 'No service requests found', 'No service requests found', NULL, 3, 'custom'),
(617, 'please_specify_a_valid_phone_number', 'Please specify a valid phone number', 'Please specify a valid phone number', 1, NULL, 'custom'),
(616, 'please_select_a_valid_service_form_the_list', 'Please select a valid service form the list', 'Please select a valid service form the list', 1, NULL, 'custom'),
(615, 'sorry_x_is_already_taken', 'Sorry %s is already taken!', 'Sorry %s is already taken!', 1, NULL, 'custom'),
(614, 'sign_up', 'Sign Up', 'Sign Up', NULL, 5, 'custom'),
(612, 'why', 'Why?', 'Why?', NULL, 7, 'custom'),
(613, 'dont_have_an_account_yet_', 'Don''t have an account yet? ', 'Don''t have an account yet? ', NULL, 5, 'custom'),
(611, 'click_to_login_with', 'Click to login with', 'Click to login with', NULL, 7, 'custom'),
(610, 'why', 'Why?', 'Why?', NULL, 35, 'custom'),
(609, 'click_to_login_with', 'Click to login with', 'Click to login with', NULL, 35, 'custom'),
(608, 'forgot_password', 'Forgot Password?', 'Forgot Password?', NULL, 35, 'custom'),
(605, 'log_in__submit_request', 'LOG IN & SUBMIT REQUEST', 'LOG IN & SUBMIT REQUEST', NULL, 5, 'custom'),
(606, 'forgot_password', 'Forgot Password?', 'Forgot Password?', NULL, 5, 'custom'),
(607, 'flexible_anytime', 'Flexible (anytime)', 'Flexible (anytime)', 1, NULL, 'custom'),
(603, 'why', 'Why?', 'Why?', NULL, 5, 'custom'),
(604, 'dont_have_an_account_yet_sign_up', 'Don''t have an account yet? Sign Up', 'Don''t have an account yet? Sign Up', NULL, 5, 'custom'),
(602, 'click_to_login_with', 'Click to login with', 'Click to login with', NULL, 5, 'custom'),
(601, 'job_can_be_performed_remotely', 'Job can be performed remotely', 'Job can be performed remotely', 1, NULL, 'custom'),
(384, 'has_a_new_service', 'has a new service', 'has a new service', NULL, 16, 'custom'),
(385, 'welcome_to_x', 'Welcome to %s', 'Welcome to %s', NULL, 13, 'custom'),
(386, 'profile', 'Profile', 'Profile', NULL, 13, 'custom'),
(387, 'account', 'Account', 'Account', NULL, 13, 'custom'),
(388, 'learn_more', 'Learn More', 'Learn More', NULL, 13, 'custom'),
(389, 'job_offers', 'Job Offers', 'Job Offers', NULL, 13, 'custom'),
(390, 'filter', 'Filter', 'Filter', 1, NULL, 'custom'),
(391, 'offers', 'Offers', 'Offers', NULL, 13, 'custom'),
(392, 'won', 'Won', 'Won', NULL, 13, 'custom'),
(393, 'lost', 'Lost', 'Lost', NULL, 13, 'custom'),
(394, 'job', 'Job', 'Job', 1, NULL, 'custom'),
(395, 'status', 'Status', 'Status', 1, NULL, 'custom'),
(396, 'there_are_no_active_service_requests', 'There are no active service requests', 'There are no active service requests', NULL, 13, 'custom'),
(397, 'my_jobs', 'My Jobs', 'My Jobs', 1, NULL, 'custom'),
(398, 'manage_profile', 'Manage Profile', 'Manage Profile', 1, NULL, 'custom'),
(399, 'profile_summaries', 'Profile summaries', 'Profile summaries', NULL, 30, 'custom'),
(400, 'create_skill_profile', 'Create Skill Profile', 'Create Skill Profile', NULL, 30, 'custom'),
(401, 'notification_preferences', 'Notification preferences', 'Notification preferences', NULL, 30, 'custom'),
(402, 'work_history', 'Work history', 'Work history', NULL, 30, 'custom'),
(403, 'profile', 'profile', 'profile', 1, NULL, 'custom'),
(404, 'earned', 'Earned', 'Earned', NULL, 30, 'custom'),
(405, 'jobs', 'Jobs', 'Jobs', NULL, 30, 'custom'),
(406, 'offers', 'offers', 'offers', NULL, 30, 'custom'),
(407, 'quotes', 'quotes', 'quotes', NULL, 30, 'custom'),
(408, 'wins', 'wins', 'wins', NULL, 30, 'custom'),
(409, 'rating', 'Rating', 'Rating', NULL, 30, 'custom'),
(410, 'no_ratings_yet', 'No ratings yet', 'No ratings yet', NULL, 30, 'custom'),
(411, 'viewedit_profile', 'View/Edit Profile', 'View/Edit Profile', NULL, 30, 'custom'),
(412, 'upcoming_appointments', 'Upcoming Appointments', 'Upcoming Appointments', NULL, 30, 'custom'),
(413, 'create_another_profile', 'Create another profile', 'Create another profile', NULL, 29, 'custom'),
(414, 'x_profile', '%s Profile', '%s Profile', NULL, 29, 'custom'),
(415, 'delete_this_profile', 'delete this profile', 'delete this profile', NULL, 29, 'custom'),
(416, 'below_is_how_x_customers_will_see_your_profile_research_shows_that_service_providers_who_f', 'Below is how %s customers will see your profile. Research shows that service providers who fill out their profiles entirely and more likely to win bids.', 'Below is how %s customers will see your profile. Research shows that service providers who fill out their profiles entirely and more likely to win bids.', NULL, 29, 'custom'),
(417, 'manage_your_profile', 'Manage your profile', 'Manage your profile', NULL, 29, 'custom'),
(418, 'click_here_to_add_your_business_logo', 'Click here to add your business logo', 'Click here to add your business logo', NULL, 29, 'custom'),
(419, 'remove', 'Remove', 'Remove', NULL, 29, 'custom'),
(420, 'add_image', 'Add Image', 'Add Image', NULL, 29, 'custom'),
(421, 'business_name_or_full_name', 'Business name or full name', 'Business name or full name', NULL, 29, 'custom'),
(422, 'skill_or_occupation', 'Skill or occupation', 'Skill or occupation', NULL, 29, 'custom'),
(423, 'phone_number', 'Phone number', 'Phone number', NULL, 29, 'custom'),
(424, 'mobile_number', 'Mobile number', 'Mobile number', NULL, 29, 'custom'),
(425, 'business_address', 'Business address', 'Business address', NULL, 29, 'custom'),
(426, 'website_link', 'Website link', 'Website link', NULL, 29, 'custom'),
(427, 'please_specify_your_business_name', 'Please specify your business name', 'Please specify your business name', NULL, 29, 'custom'),
(428, 'you_must_provide_your_business_address', 'You must provide your business address', 'You must provide your business address', NULL, 29, 'custom'),
(429, 'please_specify_a_zip__postal_code', 'Please specify a zip / postal code', 'Please specify a zip / postal code', NULL, 29, 'custom'),
(430, 'please_select_your_country', 'Please select your country', 'Please select your country', NULL, 29, 'custom'),
(431, 'please_select_your_state', 'Please select your state', 'Please select your state', NULL, 29, 'custom'),
(432, 'please_enter_a_valid_website_link', 'Please enter a valid website link', 'Please enter a valid website link', NULL, 29, 'custom'),
(433, 'add__remove_photos', 'Add / Remove Photos', 'Add / Remove Photos', NULL, 29, 'custom'),
(434, 'portfolio', 'Portfolio', 'Portfolio', NULL, 29, 'custom'),
(435, 'click_to_add_portfolio_photos', 'Click to add portfolio photos', 'Click to add portfolio photos', NULL, 29, 'custom'),
(436, 'about_x', 'About %s', 'About %s', NULL, 29, 'custom'),
(437, 'click_to_add_a_short_description_about_your_business', 'Click to add a short description about your business', 'Click to add a short description about your business', NULL, 29, 'custom'),
(438, 'get_testimonials', 'Get testimonials', 'Get testimonials', NULL, 29, 'custom'),
(439, 'testimonials', 'Testimonials', 'Testimonials', NULL, 29, 'custom'),
(440, 'modify', 'Modify', 'Modify', NULL, 29, 'custom'),
(441, 'my_hours', 'My hours', 'My hours', NULL, 29, 'custom'),
(442, 'my_work_area', 'My work area', 'My work area', NULL, 29, 'custom'),
(443, 'my_notifications', 'My notifications', 'My notifications', NULL, 29, 'custom'),
(444, 'view_my_profile_page', 'View my profile page', 'View my profile page', NULL, 29, 'custom'),
(445, 'delete_your_x_profile', 'Delete your %s profile', 'Delete your %s profile', NULL, 29, 'custom'),
(446, 'are_you_sure_you_want_to_delete_this_profile_this_action_cannot_be_reversed', 'Are you sure you want to delete this profile? This action cannot be reversed.', 'Are you sure you want to delete this profile? This action cannot be reversed.', NULL, 29, 'custom'),
(447, 'i_changed_my_mind', 'I changed my mind', 'I changed my mind', NULL, 29, 'custom'),
(448, 'delete_this_profile_1', 'Delete this profile', 'Delete this profile', NULL, 29, 'custom'),
(449, 'add_photos_or_prior_work', 'Add photos or prior work', 'Add photos or prior work', NULL, 29, 'custom'),
(450, 'x_to_your_portfolio', '%s to your portfolio', '%s to your portfolio', NULL, 29, 'custom'),
(451, 'about', 'About', 'About', NULL, 29, 'custom'),
(452, 'describe_your_work_experience', 'Describe your work experience', 'Describe your work experience', NULL, 29, 'custom'),
(453, 'what_kind_of_projects_do_you_work_on', 'What kind of projects do you work on?', 'What kind of projects do you work on?', NULL, 29, 'custom'),
(454, 'why_do_your_customers_like_working_with_you', 'Why do your customers like working with you?', 'Why do your customers like working with you?', NULL, 29, 'custom'),
(455, 'get_testimonials_1', 'Get Testimonials', 'Get Testimonials', NULL, 29, 'custom'),
(456, 'message_will_be_sent_from_x_and_is_not_a_bulk_email_the_recipient_will_only_see_their_emai', 'Message will be sent from %s and is not a bulk email, the recipient will only see their email address.', 'Message will be sent from %s and is not a bulk email, the recipient will only see their email address.', NULL, 29, 'custom'),
(457, 'to_email', 'To Email', 'To Email', NULL, 29, 'custom'),
(458, 'subject', 'Subject', 'Subject', NULL, 29, 'custom'),
(459, 'add_a_testimonial_for_me_on_x', 'Add a testimonial for me on %s', 'Add a testimonial for me on %s', NULL, 29, 'custom'),
(460, 'message', 'Message', 'Message', NULL, 29, 'custom'),
(461, 'would_you_add_a_brief_recommendation_of_my_work_for_my_x_profile_please_let_me_know_if_you', 'Would you add a brief recommendation of my work for my %s profile? Please let me know if you have any questions and thanks for your help!', 'Would you add a brief recommendation of my work for my %s profile? Please let me know if you have any questions and thanks for your help!', NULL, 29, 'custom'),
(462, 'please_specify_an_email_address', 'Please specify an email address', 'Please specify an email address', NULL, 29, 'custom'),
(463, 'please_specify_a_subject', 'Please specify a subject', 'Please specify a subject', NULL, 29, 'custom'),
(464, 'please_specify_message', 'Please specify message', 'Please specify message', NULL, 29, 'custom'),
(465, 'your_request_has_been_sent_successfully', 'Your request has been sent successfully', 'Your request has been sent successfully', NULL, 29, 'custom'),
(466, 'we_will_send_you_an_email_telling_you_when_someone_submits_a_testimonial_about_you', 'We will send you an email telling you when someone submits a testimonial about you.', 'We will send you an email telling you when someone submits a testimonial about you.', NULL, 29, 'custom'),
(467, 'how_far_are_you_willing_to_travel', 'How far are you willing to travel?', 'How far are you willing to travel?', NULL, 29, 'custom'),
(468, 'your_work_area_includes', 'Your work area includes...', 'Your work area includes...', NULL, 29, 'custom'),
(469, 'no_nearby_areas', '(No nearby areas)', '(No nearby areas)', NULL, 29, 'custom'),
(470, 'please_enter_the_address_for_your_business', 'Please enter the address for your business', 'Please enter the address for your business', NULL, 29, 'custom'),
(471, 'my_work_hours', 'My work hours', 'My work hours', NULL, 29, 'custom'),
(472, 'weekends_only', 'Weekends only', 'Weekends only', NULL, 29, 'custom'),
(473, 'specific_days', 'Specific days', 'Specific days', NULL, 29, 'custom'),
(474, 'from_x_to_x', 'from %s to %s', 'from %s to %s', NULL, 29, 'custom'),
(475, 'select_to_add_your_availability_for_x', 'Select to add your availability for %s', 'Select to add your availability for %s', NULL, 29, 'custom'),
(476, 'apply_to_all', 'Apply to all', 'Apply to all', NULL, 29, 'custom'),
(477, 'x_in_x', '%s in %s', '%s in %s', NULL, 26, 'custom'),
(478, 'about_x', 'About %s', 'About %s', NULL, 26, 'custom'),
(479, 'contact_this_provider', 'Contact this provider', 'Contact this provider', NULL, 26, 'custom'),
(480, 'rated', 'Rated', 'Rated', NULL, 26, 'custom'),
(481, 'average_rating', 'Average rating', 'Average rating', NULL, 26, 'custom'),
(482, 'no_ratings_yet', 'No ratings yet', 'No ratings yet', NULL, 26, 'custom'),
(483, 'more_x_found_in_x', 'More %s found in %s', 'More %s found in %s', NULL, 26, 'custom'),
(484, 'get_a_quote_from_x', 'Get a Quote from %s', 'Get a Quote from %s', NULL, 26, 'custom'),
(485, 'where_do_you_need_this_service', 'Where do you need this service?', 'Where do you need this service?', NULL, 26, 'custom'),
(486, 'postalzip_code_or_full_address', 'Postal/zip code or full address', 'Postal/zip code or full address', NULL, 26, 'custom'),
(487, 'what_do_you_need_done', 'What do you need done?', 'What do you need done?', NULL, 26, 'custom'),
(488, 'please_describe_your_request_the_more_detail_you_provide_the_better_quality_quotes', 'Please describe your request. The more detail you provide, the better quality quotes.', 'Please describe your request. The more detail you provide, the better quality quotes.', NULL, 26, 'custom'),
(489, 'send_this_to_other_x_in_x_and_let_them_provide_quotes_as_well', 'Send this to other %s in %s and let them provide quotes as well', 'Send this to other %s in %s and let them provide quotes as well', NULL, 26, 'custom'),
(490, 'send_request', 'Send request', 'Send request', NULL, 26, 'custom'),
(491, 'ratings', 'Ratings', 'Ratings', NULL, 26, 'custom'),
(492, 'create_your_profile', 'Create your profile', 'Create your profile', NULL, 28, 'custom'),
(493, 'click_here_to_add_your_business_logo', 'Click here to add your business logo', 'Click here to add your business logo', NULL, 28, 'custom'),
(494, 'remove', 'Remove', 'Remove', NULL, 28, 'custom'),
(495, 'add_image', 'Add Image', 'Add Image', NULL, 28, 'custom'),
(496, 'business_name_or_full_name', 'Business name or full name', 'Business name or full name', NULL, 28, 'custom'),
(497, 'skill_or_occupation', 'Skill or occupation', 'Skill or occupation', NULL, 28, 'custom'),
(498, 'phone_number', 'Phone number', 'Phone number', NULL, 28, 'custom'),
(499, 'mobile_number', 'Mobile number', 'Mobile number', NULL, 28, 'custom'),
(500, 'business_address', 'Business address', 'Business address', NULL, 28, 'custom'),
(501, 'website_link', 'Website link', 'Website link', NULL, 28, 'custom'),
(502, 'please_specify_your_business_name', 'Please specify your business name', 'Please specify your business name', NULL, 28, 'custom'),
(503, 'you_must_provide_your_business_address', 'You must provide your business address', 'You must provide your business address', NULL, 28, 'custom'),
(504, 'please_specify_a_zip__postal_code', 'Please specify a zip / postal code', 'Please specify a zip / postal code', NULL, 28, 'custom'),
(505, 'please_select_your_country', 'Please select your country', 'Please select your country', NULL, 28, 'custom'),
(506, 'please_select_your_state', 'Please select your state', 'Please select your state', NULL, 28, 'custom'),
(507, 'please_enter_a_valid_website_link', 'Please enter a valid website link', 'Please enter a valid website link', NULL, 28, 'custom'),
(508, 'your_hours', 'Your hours', 'Your hours', NULL, 28, 'custom');
INSERT INTO `cms_strings` (`id`, `code`, `content`, `original`, `global`, `page_id`, `theme_id`) VALUES
(509, 'show_less_options', 'Show less options', 'Show less options', NULL, 28, 'custom'),
(510, 'show_more_options', 'Show more options', 'Show more options', NULL, 28, 'custom'),
(511, 'weekends_only', 'Weekends only', 'Weekends only', NULL, 28, 'custom'),
(512, 'specific_days', 'Specific days', 'Specific days', NULL, 28, 'custom'),
(513, 'from_x_to_x', 'from %s to %s', 'from %s to %s', NULL, 28, 'custom'),
(514, 'select_to_add_your_availability_for_x', 'Select to add your availability for %s', 'Select to add your availability for %s', NULL, 28, 'custom'),
(515, 'apply_to_all', 'Apply to all', 'Apply to all', NULL, 28, 'custom'),
(516, 'your_work_area', 'Your work area', 'Your work area', NULL, 28, 'custom'),
(517, 'how_far_are_you_willing_to_travel', 'How far are you willing to travel?', 'How far are you willing to travel?', NULL, 28, 'custom'),
(518, 'your_work_area_includes', 'Your work area includes...', 'Your work area includes...', NULL, 28, 'custom'),
(519, 'no_nearby_areas', '(No nearby areas)', '(No nearby areas)', NULL, 28, 'custom'),
(520, 'please_enter_the_address_for_your_business', 'Please enter the address for your business', 'Please enter the address for your business', NULL, 28, 'custom'),
(521, 'i_agree_to_the_terms_and_conditions', 'I agree to the Terms and Conditions', 'I agree to the Terms and Conditions', NULL, 28, 'custom'),
(522, 'save_profile', 'Save profile', 'Save profile', NULL, 28, 'custom'),
(600, 'urgent_as_soon_as_possible', 'Urgent (As soon as possible)', 'Urgent (As soon as possible)', 1, NULL, 'custom'),
(599, 'close', 'Close', 'Close', 1, NULL, 'custom'),
(598, 'anywhere', 'Anywhere', 'Anywhere', 1, NULL, 'custom'),
(597, 'x_ago', '%s ago', '%s ago', 1, NULL, 'custom'),
(528, 'review_and_submit_your_request', 'Review and submit your request', 'Review and submit your request', NULL, 35, 'custom'),
(529, 'x_will_send_your_request_to_service_providers_available_to_do_your_work_at_the_time_you_sp', '%s will send your request to Service Providers available to do your work at the time you specified. Interested Providers will respond with their best price quotes.', '%s will send your request to Service Providers available to do your work at the time you specified. Interested Providers will respond with their best price quotes.', NULL, 35, 'custom'),
(530, 'who_you_are_looking_for', 'Who you are looking for', 'Who you are looking for', NULL, 35, 'custom'),
(531, 'when_do_you_need_them', 'When do you need them', 'When do you need them', NULL, 35, 'custom'),
(532, 'where_do_you_need_them', 'Where do you need them', 'Where do you need them', NULL, 35, 'custom'),
(533, 'what_you_need_done', 'What you need done', 'What you need done', NULL, 35, 'custom'),
(534, 'edit_request', 'Edit request', 'Edit request', NULL, 35, 'custom'),
(535, 'last_step_-_create_account_and_submit_request', 'Last step - Create account and submit request!', 'Last step - Create account and submit request!', NULL, 35, 'custom'),
(536, 'already_have_an_account_log_in', 'Already have an account? Log in', 'Already have an account? Log in', NULL, 35, 'custom'),
(537, 'last_step_-_log_in_and_submit_request', 'Last step - Log in and submit request!', 'Last step - Log in and submit request!', NULL, 35, 'custom'),
(538, 'dont_have_an_account_yet_sign_up', 'Don''t have an account yet? Sign up', 'Don''t have an account yet? Sign up', NULL, 35, 'custom'),
(539, 'by_pressing_submit_you_agree_to_the_x', 'By pressing submit you agree to the %s.', 'By pressing submit you agree to the %s.', NULL, 35, 'custom'),
(540, 'confirm_request', 'Confirm Request', 'Confirm Request', NULL, 35, 'custom'),
(541, 'x_will_send_your_request_to_home_service_professionals_available_to_do_your_work_at_the_ti', '%s will send your request to Home Service Professionals available to do your work at the time you specified. Interested Pros will respond with their best price quotes.', '%s will send your request to Home Service Professionals available to do your work at the time you specified. Interested Pros will respond with their best price quotes.', NULL, 35, 'custom'),
(542, 'blog_categories', 'Blog categories', 'Blog categories', NULL, 8, 'custom'),
(543, 'rss_channel', 'RSS channel', 'RSS channel', NULL, 8, 'custom'),
(544, 'rss_feed', 'RSS feed', 'RSS feed', NULL, 8, 'custom'),
(545, 'subscribe_to_our_x_to_stay_updated_with_our_latest_blog_news', 'Subscribe to our %s to stay updated with our latest blog news.', 'Subscribe to our %s to stay updated with our latest blog news.', NULL, 8, 'custom'),
(546, 'my_service_requests', 'My Service Requests', 'My Service Requests', NULL, 34, 'custom'),
(547, 'x_request_submitted', '%s request submitted', '%s request submitted', NULL, 34, 'custom'),
(548, 'your_request_has_been_submitted_what_happens_next', 'Your request has been submitted. What happens next?', 'Your request has been submitted. What happens next?', NULL, 34, 'custom'),
(549, 'requested_appointment_times', 'Requested appointment time(s)', 'Requested appointment time(s)', NULL, 34, 'custom'),
(550, 'location', 'Location', 'Location', 1, NULL, 'custom'),
(551, 'show_map', 'Show map', 'Show map', 1, NULL, 'custom'),
(552, 'what_you_need', 'What you need', 'What you need', NULL, 34, 'custom'),
(553, 'x_more_information', '%s more information', '%s more information', NULL, 34, 'custom'),
(554, 'please_enter_more_request_details', 'Please enter more request details', 'Please enter more request details', NULL, 34, 'custom'),
(555, 'photos', 'Photos', 'Photos', 1, NULL, 'custom'),
(556, 'x_about_this_job', '%s about this job', '%s about this job', NULL, 34, 'custom'),
(557, 'request_status', 'Request Status', 'Request Status', 1, NULL, 'custom'),
(558, 'x_remain', '%s remain', '%s remain', NULL, 34, 'custom'),
(559, 'extend_time', 'Extend time', 'Extend time', NULL, 34, 'custom'),
(560, 'x_quotes', '%s quote(s)', '%s quote(s)', NULL, 34, 'custom'),
(561, 'x_public_questions', '%s public question(s)', '%s public question(s)', NULL, 34, 'custom'),
(562, 'x_this_service_request', '%s this service request', '%s this service request', NULL, 34, 'custom'),
(563, 'cancel_this_service_request', 'Cancel this service request?', 'Cancel this service request?', NULL, 34, 'custom'),
(564, 'are_you_sure_you_want_to_cancel_this_request_this_request_and_all_quotes_will_be_lost', 'Are you sure you want to cancel this request? This request and all quotes will be lost!', 'Are you sure you want to cancel this request? This request and all quotes will be lost!', NULL, 34, 'custom'),
(565, 'cancel_this_request', 'Cancel this request', 'Cancel this request', NULL, 34, 'custom'),
(566, 'continue_request', 'Continue request', 'Continue request', NULL, 34, 'custom'),
(567, 'search_messages', 'Search messages', 'Search messages', NULL, 21, 'custom'),
(568, 'search', 'Search', 'Search', NULL, 21, 'custom'),
(569, 'about_us', 'ABOUT US', 'ABOUT US', 1, NULL, 'custom'),
(570, 'contact_1', 'CONTACT', 'CONTACT', 1, NULL, 'custom'),
(571, 'login', 'LOGIN', 'LOGIN', 1, NULL, 'custom'),
(572, 'browse_providers', 'Browse providers', 'Browse providers', NULL, 33, 'custom'),
(573, 'please_select_the_service_professional_who_can_best_meet_your_needs', 'Please select the service professional who can best meet your needs', 'Please select the service professional who can best meet your needs', NULL, 33, 'custom'),
(574, 'by_category', 'By Category', 'By Category', NULL, 33, 'custom'),
(575, 'browse_a-z', 'Browse A-Z', 'Browse A-Z', NULL, 33, 'custom'),
(576, 'select_a_category', 'Select a category:', 'Select a category:', NULL, 33, 'custom'),
(577, 'select_a_skill', 'Select a skill:', 'Select a skill:', NULL, 33, 'custom'),
(578, 'select_a_specific_skill', 'Select a specific skill:', 'Select a specific skill:', NULL, 33, 'custom'),
(579, 'still_not_finding_what_you_are_looking_for', 'Still not finding what you are looking for?', 'Still not finding what you are looking for?', NULL, 33, 'custom'),
(580, 'suggest_a_new_service_for_x', 'Suggest a new service for %s', 'Suggest a new service for %s', NULL, 33, 'custom'),
(581, 'continue_to_review', 'Continue to Review', 'Continue to Review', NULL, 33, 'custom'),
(582, 'thanks_your_suggestion_has_been_submitted_we_will_let_you_know_the_outcome_via_email', 'Thanks! Your suggestion has been submitted. We will let you know the outcome via Email.', 'Thanks! Your suggestion has been submitted. We will let you know the outcome via Email.', NULL, 33, 'custom'),
(583, 'your_name', 'Your name', 'Your name', NULL, 33, 'custom'),
(584, 'your_email', 'Your email', 'Your email', NULL, 33, 'custom'),
(585, 'suggested_category_name', 'Suggested category name', 'Suggested category name', NULL, 33, 'custom'),
(586, 'please_specify_your_name', 'Please specify your name', 'Please specify your name', NULL, 33, 'custom'),
(587, 'please_specify_a_service_name', 'Please specify a service name', 'Please specify a service name', NULL, 33, 'custom'),
(588, 'submit_suggestion', 'Submit suggestion', 'Submit suggestion', NULL, 33, 'custom'),
(589, 'email_x_is_already_in_use', 'Email %s is already in use', 'Email %s is already in use', 1, NULL, 'custom'),
(590, 'dashboard', 'Dashboard', 'Dashboard', 1, NULL, 'custom'),
(591, 'how_would_you_like_to_start_using_x', 'How would you like to start using %s?', 'How would you like to start using %s?', NULL, 13, 'custom'),
(592, 'request_services', 'Request Services', 'Request Services', NULL, 13, 'custom'),
(593, 'provide_services', 'Provide Services', 'Provide Services', NULL, 13, 'custom'),
(594, 'what_service_are_you_looking_for', 'What service are you looking for?', 'What service are you looking for?', NULL, 13, 'custom'),
(595, 'eg_web_developer_computer_programmer', 'eg: Web Developer, Computer Programmer', 'eg: Web Developer, Computer Programmer', NULL, 13, 'custom'),
(596, 'create_a_profile_and_get_free_job_leads', 'Create a profile and get free job leads!', 'Create a profile and get free job leads!', NULL, 13, 'custom'),
(791, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(792, 'ignore_this_job_offer_1', 'Ignore this job offer?', 'Ignore this job offer?', NULL, 18, 'custom'),
(793, 'this_job_offer_will_no_longer_be_available_to_you_this_can_not_be_undone', 'This job offer will no longer be available to you. This can not be undone.', 'This job offer will no longer be available to you. This can not be undone.', NULL, 18, 'custom'),
(794, 'i_changed_my_mind', 'I changed my mind', 'I changed my mind', NULL, 18, 'custom'),
(795, 'confirm_ignore', 'Confirm ignore', 'Confirm ignore', NULL, 18, 'custom'),
(796, 'quote_details', 'Quote Details', 'Quote Details', NULL, 18, 'custom'),
(797, 'modify_quote', 'Modify quote', 'Modify quote', NULL, 18, 'custom'),
(798, 'cancel_quote', 'Cancel quote', 'Cancel quote', NULL, 18, 'custom'),
(799, 'labor', 'Labor', 'Labor', NULL, 18, 'custom'),
(800, 'total', 'Total', 'Total', NULL, 18, 'custom'),
(801, 'quotes', 'Quotes', 'Quotes', NULL, 34, 'custom'),
(802, 'quote_from_x', 'Quote from %s', 'Quote from %s', NULL, 34, 'custom'),
(803, 'labor_x_materials_x', 'labor %s, materials %s', 'labor %s, materials %s', NULL, 34, 'custom'),
(804, 'no_appointment_time_yet', 'No appointment time yet', 'No appointment time yet', NULL, 34, 'custom'),
(805, 'suggest_a_time', 'Suggest a time', 'Suggest a time', NULL, 34, 'custom'),
(806, 'suggest', 'Suggest', 'Suggest', NULL, 34, 'custom'),
(807, 'please_provide_a_start_date', 'Please provide a start date', 'Please provide a start date', 1, NULL, 'custom'),
(808, 'please_provide_a_start_time', 'Please provide a start time', 'Please provide a start time', 1, NULL, 'custom'),
(809, 'accept_quote', 'Accept quote', 'Accept quote', NULL, 34, 'custom'),
(810, 'ask_a_question', 'Ask a question', 'Ask a question', NULL, 34, 'custom'),
(811, 'click_accept_quote_to_accept_the_quote_and_schedule_the_job', 'Click Accept Quote to accept the quote and schedule the job', 'Click Accept Quote to accept the quote and schedule the job', NULL, 34, 'custom'),
(812, 'type_your_message_or_question_and_click_send', 'Type your message or question and click Send', 'Type your message or question and click Send', NULL, 34, 'custom'),
(813, 'sharing_your_contact_details_is_against_our_terms_and_conditions', 'Sharing your contact details is against our terms and conditions', 'Sharing your contact details is against our terms and conditions', NULL, 34, 'custom'),
(814, 'please_specify_a_message', 'Please specify a message', 'Please specify a message', 1, NULL, 'custom'),
(815, 'portfolio', 'Portfolio', 'Portfolio', NULL, 34, 'custom'),
(816, 'reviews', 'Reviews', 'Reviews', NULL, 34, 'custom'),
(817, 'average_rating', 'Average rating', 'Average rating', NULL, 34, 'custom'),
(818, 'no_ratings_yet', 'No ratings yet', 'No ratings yet', NULL, 34, 'custom'),
(819, 'all_reviews', 'All reviews', 'All reviews', NULL, 34, 'custom'),
(820, 'view_5_star_reviews', 'View 5 star reviews', 'View 5 star reviews', NULL, 34, 'custom'),
(821, 'testimonials', 'Testimonials', 'Testimonials', NULL, 34, 'custom'),
(822, 'accept_this_quote', 'Accept this quote?', 'Accept this quote?', NULL, 34, 'custom'),
(823, 'provider', 'Provider', 'Provider', NULL, 34, 'custom'),
(824, 'quote', 'Quote', 'Quote', NULL, 34, 'custom'),
(825, 'note', 'Note', 'Note', NULL, 34, 'custom'),
(826, 'provide_your_contact_details', 'Provide your contact details', 'Provide your contact details', NULL, 34, 'custom'),
(827, 'first_name', 'First name', 'First name', NULL, 34, 'custom'),
(828, 'last_name', 'Last name', 'Last name', NULL, 34, 'custom'),
(829, 'phone_number', 'Phone number', 'Phone number', NULL, 34, 'custom'),
(830, 'mobile_phone', 'Mobile phone', 'Mobile phone', NULL, 34, 'custom'),
(831, 'street_address', 'Street Address', 'Street Address', NULL, 34, 'custom'),
(832, 'city', 'City', 'City', NULL, 34, 'custom'),
(833, 'postal_code', 'Postal code', 'Postal code', NULL, 34, 'custom'),
(834, 'country', 'Country', 'Country', NULL, 34, 'custom'),
(835, 'state', 'State', 'State', NULL, 34, 'custom'),
(836, 'accepting_this_quote_is_like_a_virtual_handshake_between_you_and_x_help_keep_our_community', 'Accepting this quote is like a virtual handshake between you and %s. Help keep our community strong by honouring this quote.', 'Accepting this quote is like a virtual handshake between you and %s. Help keep our community strong by honouring this quote.', NULL, 34, 'custom'),
(837, 'review_other_quotes', 'Review other quotes', 'Review other quotes', NULL, 34, 'custom'),
(838, 'create_booking', 'Create Booking!', 'Create Booking!', NULL, 34, 'custom'),
(839, 'please_enter_your_first_name', 'Please enter your first name', 'Please enter your first name', NULL, 34, 'custom'),
(840, 'please_enter_your_surname', 'Please enter your surname', 'Please enter your surname', NULL, 34, 'custom'),
(841, 'please_enter_your_phone_number', 'Please enter your phone number', 'Please enter your phone number', NULL, 34, 'custom'),
(842, 'please_enter_your_city', 'Please enter your city', 'Please enter your city', NULL, 34, 'custom'),
(843, 'please_enter_your_zip_or_postal_code', 'Please enter your zip or postal code', 'Please enter your zip or postal code', NULL, 34, 'custom'),
(844, 'please_select_your_country', 'Please select your country', 'Please select your country', NULL, 34, 'custom'),
(845, 'please_select_your_state', 'Please select your state', 'Please select your state', NULL, 34, 'custom'),
(846, 'my_appointments', 'My Appointments', 'My Appointments', NULL, 19, 'custom'),
(847, 'booking_with_x', 'Booking with %s', 'Booking with %s', NULL, 19, 'custom'),
(848, 'congrats_youve_booked_this_x_appointment', 'Congrats! You''ve booked this %s appointment', 'Congrats! You''ve booked this %s appointment', NULL, 19, 'custom'),
(849, 'start_time', 'Start time', 'Start time', NULL, 19, 'custom'),
(850, 'no_appointment_time_yet', 'No appointment time yet', 'No appointment time yet', NULL, 19, 'custom'),
(851, 'suggest_a_time', 'Suggest a time', 'Suggest a time', NULL, 19, 'custom'),
(852, 'suggest', 'Suggest', 'Suggest', NULL, 19, 'custom'),
(853, 'contact_info', 'Contact info', 'Contact info', NULL, 19, 'custom'),
(854, 'price_quoted', 'Price quoted', 'Price quoted', NULL, 19, 'custom'),
(855, 'labor_x_materials_x', 'labor %s, materials %s', 'labor %s, materials %s', NULL, 19, 'custom'),
(856, 'personal_note', 'Personal Note', 'Personal Note', NULL, 19, 'custom'),
(857, 'loading_map', 'Loading map...', 'Loading map...', NULL, 19, 'custom'),
(858, 'type_your_message_or_question_and_click_send', 'Type your message or question and click Send', 'Type your message or question and click Send', NULL, 19, 'custom'),
(859, 'sharing_your_contact_details_is_against_our_terms_and_conditions', 'Sharing your contact details is against our terms and conditions', 'Sharing your contact details is against our terms and conditions', NULL, 19, 'custom'),
(860, 'job_details', 'Job Details', 'Job Details', NULL, 19, 'custom'),
(861, 'cancel_this_appointment', 'Cancel this appointment', 'Cancel this appointment', NULL, 19, 'custom'),
(862, 'your_x_job_has_been_booked', 'Your %s job has been booked!', 'Your %s job has been booked!', NULL, 19, 'custom'),
(863, 'help_us_grow_by_sharing_this_with_your_friends', 'Help us grow by sharing this with your friends.', 'Help us grow by sharing this with your friends.', NULL, 19, 'custom'),
(864, 'just_booked_a_x_on_x', 'Just booked a %s on %s!', 'Just booked a %s on %s!', NULL, 19, 'custom'),
(865, 'share_on_facebook', 'Share on Facebook', 'Share on Facebook', NULL, 19, 'custom'),
(866, 'share_on_twitter', 'Share on Twitter', 'Share on Twitter', NULL, 19, 'custom'),
(867, 'no_thanks', 'No thanks', 'No thanks', NULL, 19, 'custom'),
(868, 'cancel_this_booking', 'Cancel this booking?', 'Cancel this booking?', NULL, 19, 'custom'),
(869, 'are_you_sure_you_want_to_cancel_this_booking_the_other_party_will_be_notified_of_your_deci', 'Are you sure you want to cancel this booking? The other party will be notified of your decision.', 'Are you sure you want to cancel this booking? The other party will be notified of your decision.', NULL, 19, 'custom'),
(870, 'i_changed_my_mind', 'I changed my mind', 'I changed my mind', NULL, 19, 'custom'),
(871, 'cancel_booking', 'Cancel booking', 'Cancel booking', NULL, 19, 'custom'),
(872, 'x_request', '%s Request', '%s Request', 1, NULL, 'custom'),
(873, 'quoting_closes_in', 'Quoting closes in', 'Quoting closes in', NULL, 13, 'custom'),
(874, 'view_more_job_offers', 'View more job offers', 'View more job offers', NULL, 13, 'custom'),
(875, 'quoting_closed', 'Quoting closed', 'Quoting closed', NULL, 13, 'custom'),
(876, 'suggest_another_time', 'Suggest another time', 'Suggest another time', NULL, 19, 'custom'),
(877, 'job_scheduled', 'Job scheduled', 'Job scheduled', NULL, 13, 'custom'),
(878, 'view_booking', 'View Booking', 'View Booking', NULL, 13, 'custom'),
(879, 'quoting_closed', 'Quoting closed', 'Quoting closed', NULL, 3, 'custom'),
(880, 'no_recipient_defined', 'No recipient defined', 'No recipient defined', 1, NULL, 'custom'),
(881, 'message_sent_successfully', 'Message sent successfully!', 'Message sent successfully!', 1, NULL, 'custom'),
(882, 'your_conversation_with_x', 'Your conversation with %s', 'Your conversation with %s', NULL, 19, 'custom'),
(883, 'you', 'You', 'You', 1, NULL, 'custom'),
(884, 'you_have_x_new_messages', 'You have %s new message(s)', 'You have %s new message(s)', 1, NULL, 'custom'),
(885, 'delete', 'Delete', 'Delete', 1, NULL, 'custom'),
(886, 'view_booking', 'View Booking', 'View Booking', NULL, 3, 'custom'),
(887, 'request_cancelled_by_consumer', 'Request cancelled by consumer', 'Request cancelled by consumer', NULL, 13, 'custom'),
(888, 'request_cancelled_by_consumer', 'Request cancelled by consumer', 'Request cancelled by consumer', NULL, 3, 'custom'),
(889, 'sorry_that_invoice_could_not_be_found', 'Sorry, that invoice could not be found', 'Sorry, that invoice could not be found', NULL, 23, 'custom'),
(890, 'conversation_with_x', 'Conversation with %s', 'Conversation with %s', NULL, 20, 'custom'),
(891, 'my_messages', 'My Messages', 'My Messages', NULL, 20, 'custom'),
(892, 'related_to_x_request', 'Related to %s request', 'Related to %s request', NULL, 20, 'custom'),
(893, 'your_message', 'Your Message', 'Your Message', NULL, 20, 'custom'),
(894, 'please_enter_a_reply_to_this_message', 'Please enter a reply to this message', 'Please enter a reply to this message', NULL, 20, 'custom'),
(895, 'send_message', 'Send message', 'Send message', NULL, 20, 'custom'),
(896, 'submit_payment', 'Submit Payment', 'Submit Payment', NULL, 24, 'custom'),
(897, 'this_invoice_has_been_paid', 'This invoice has been paid!', 'This invoice has been paid!', NULL, 23, 'custom'),
(898, 'thank_you_for_your_business', 'Thank you for your business.', 'Thank you for your business.', NULL, 23, 'custom'),
(899, 'click_to_add_company_name', 'Click to add company name', 'Click to add company name', NULL, 28, 'custom'),
(900, 'trade_speciality_or_skill', 'trade, speciality or skill', 'trade, speciality or skill', NULL, 28, 'custom'),
(901, 'license_', 'License #', 'License #', 1, NULL, 'custom'),
(902, 'license_', 'License #', 'License #', NULL, 28, 'custom'),
(903, 'state', 'State', 'State', 1, NULL, 'custom'),
(904, 'state', 'State', 'State', NULL, 28, 'custom'),
(905, 'type', 'Type', 'Type', 1, NULL, 'custom'),
(906, 'type', 'Type', 'Type', NULL, 28, 'custom'),
(907, 'date_issued', 'Date Issued', 'Date Issued', 1, NULL, 'custom'),
(908, 'date_issued', 'Date Issued', 'Date Issued', NULL, 28, 'custom'),
(909, 'bonded_yn_amount', 'Bonded (Y/N Amount)', 'Bonded (Y/N Amount)', 1, NULL, 'custom'),
(910, 'bonded_yn_amount', 'Bonded (Y/N Amount)', 'Bonded (Y/N Amount)', NULL, 28, 'custom'),
(911, 'click_to_add_company_name', 'Click to add company name', 'Click to add company name', NULL, 29, 'custom'),
(912, 'trade_speciality_or_skill', 'trade, speciality or skill', 'trade, speciality or skill', NULL, 29, 'custom'),
(913, 'license_', 'License #', 'License #', NULL, 29, 'custom'),
(914, 'state', 'State', 'State', NULL, 29, 'custom'),
(915, 'type', 'Type', 'Type', NULL, 29, 'custom'),
(916, 'date_issued', 'Date Issued', 'Date Issued', NULL, 29, 'custom'),
(917, 'bonded_yn_amount', 'Bonded (Y/N Amount)', 'Bonded (Y/N Amount)', NULL, 29, 'custom'),
(918, 'consultation_required', 'consultation required', 'consultation required', NULL, 19, 'custom'),
(919, 'job_photos', 'Job Photos', 'Job Photos', NULL, 19, 'custom'),
(920, 'submit_a_review_about_x', 'Submit a review about %s', 'Submit a review about %s', NULL, 19, 'custom'),
(921, 'write_something_about_x', 'Write something about %s', 'Write something about %s', NULL, 19, 'custom'),
(922, 'select_a_rating', 'Select a rating', 'Select a rating', NULL, 19, 'custom'),
(923, 'submit_this_rating', 'Submit this rating', 'Submit this rating', NULL, 19, 'custom'),
(924, 'please_write_a_short_review_comment', 'Please write a short review comment', 'Please write a short review comment', NULL, 19, 'custom'),
(925, 'review_of_x', 'Review of %s', 'Review of %s', NULL, 19, 'custom'),
(926, 'reviews', 'Reviews', 'Reviews', NULL, 26, 'custom'),
(927, 'x_reviews', '%s reviews', '%s reviews', NULL, 26, 'custom'),
(928, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(929, 'select_membership', 'Select Membership', 'Select Membership', NULL, 29, 'custom'),
(930, 'x_reviews', '%s reviews', '%s reviews', NULL, 30, 'custom'),
(931, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(932, 'sorry_that_payment_could_not_be_found', 'Sorry, that payment could not be found', 'Sorry, that payment could not be found', NULL, 24, 'custom'),
(933, 'extra_information', 'Extra information', 'Extra information', NULL, 35, 'custom'),
(934, 'extra_information', 'Extra information', 'Extra information', NULL, 34, 'custom'),
(935, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(936, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(937, 'job_questions', 'Job Questions', 'Job Questions', NULL, 18, 'custom'),
(938, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(939, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(940, 'questions', 'Questions', 'Questions', NULL, 34, 'custom'),
(941, 'all', 'All', 'All', NULL, 34, 'custom'),
(942, 'unanswered', 'Unanswered', 'Unanswered', NULL, 34, 'custom'),
(943, 'flagged', 'Flagged', 'Flagged', NULL, 34, 'custom'),
(944, 'posted_by', 'Posted by', 'Posted by', 1, NULL, 'custom'),
(945, 'enter_your_answer_here', 'Enter your answer here', 'Enter your answer here', NULL, 34, 'custom'),
(946, 'answer', 'Answer', 'Answer', 1, NULL, 'custom'),
(947, 'please_enter_an_answer_to_this_question', 'Please enter an answer to this question', 'Please enter an answer to this question', NULL, 34, 'custom'),
(948, 'update_answer', 'Update answer', 'Update answer', NULL, 34, 'custom'),
(949, 'flexible_within_30_days', 'Flexible (within 30 days)', 'Flexible (within 30 days)', 1, NULL, 'custom'),
(950, 'job_cancelled', 'Job cancelled', 'Job cancelled', NULL, 13, 'custom'),
(951, 'flexible_within_7_days', 'Flexible (within 7 days)', 'Flexible (within 7 days)', 1, NULL, 'custom'),
(952, 'log_in_1', 'LOG IN', 'LOG IN', NULL, 5, 'custom'),
(953, 'register_as_a_home_service_pro', 'Register as a Home Service Pro', 'Register as a Home Service Pro', NULL, 27, 'custom'),
(954, 'sorry_something_went_wrong_with_your_request_please_check_that_your_browser_has_cookies_en', 'Sorry, something went wrong with your request. Please check that your browser has cookies enabled or change your browser settings to default.', 'Sorry, something went wrong with your request. Please check that your browser has cookies enabled or change your browser settings to default.', NULL, 35, 'custom'),
(955, 'full_name', 'Full name', 'Full name', 1, NULL, 'custom'),
(956, 'please_specify_full_name', 'Please specify Full name', 'Please specify Full name', 1, NULL, 'custom'),
(957, 'recent_job_for_a_x', 'Recent job for a %s', 'Recent job for a %s', NULL, 70, 'custom'),
(958, 'if_this_customer_books_you_for_this_job_we_will_send_you_their_contact_details_and_guarantee_you_can', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', 'If this customer books you for this job, we will send you their contact details and guarantee you can meet the customer for a flat rate of $34.99.', NULL, 18, 'custom'),
(959, 'search_pros', 'Search Pro''s', 'Search Pro''s', NULL, 13, 'custom'),
(960, 'search_pros', 'Search Pro''s', 'Search Pro''s', NULL, 3, 'custom'),
(961, 'this_invoice_has_already_been_paid', 'This invoice has already been paid!', 'This invoice has already been paid!', NULL, 24, 'custom'),
(962, 'withdraw_profiles', 'Withdraw Profiles', 'Withdraw Profiles', NULL, 3, 'custom'),
(963, 'ssn', 'SSN', 'SSN', 1, NULL, 'custom'),
(964, 'social_security_number', 'Social Security Number', 'Social Security Number', NULL, 3, 'custom'),
(965, 'social_security_number', 'Social Security Number', 'Social Security Number', 1, NULL, 'custom'),
(966, 'please_specify_social_security_number', 'Please specify Social Security Number', 'Please specify Social Security Number', 1, NULL, 'custom'),
(967, 'funding_email', 'Funding Email', 'Funding Email', 1, NULL, 'custom'),
(968, 'your_funding_email', 'Your Funding Email', 'Your Funding Email', NULL, 3, 'custom'),
(969, 'please_specify_your_funding_email', 'Please specify your Funding Email', 'Please specify your Funding Email', 1, NULL, 'custom'),
(970, 'account_no', 'Account No', 'Account No', 1, NULL, 'custom'),
(971, 'your_bank_account_number', 'Your Bank Account Number', 'Your Bank Account Number', NULL, 3, 'custom'),
(972, 'account_no_1', 'Account no#', 'Account no#', 1, NULL, 'custom'),
(973, 'rounting_no', 'Rounting No', 'Rounting No', 1, NULL, 'custom'),
(974, 'your_banks_routing_number', 'Your Bank''s Routing Number', 'Your Bank''s Routing Number', NULL, 3, 'custom'),
(975, 'your_banks_routing_number', 'Your Bank''s Routing Number', 'Your Bank''s Routing Number', 1, NULL, 'custom'),
(976, 'tax_id', 'Tax ID', 'Tax ID', 1, NULL, 'custom'),
(977, 'your_tax_id', 'Your Tax ID', 'Your Tax ID', NULL, 3, 'custom'),
(978, 'eg_painter_carpenter_electrician', 'eg: Painter, Carpenter, Electrician', 'eg: Painter, Carpenter, Electrician', NULL, 13, 'custom'),
(979, 'create_a_profile_and_get_job_leads', 'Create a profile and get job leads!', 'Create a profile and get job leads!', NULL, 13, 'custom'),
(980, 'avg_rating', 'Avg rating', 'Avg rating', NULL, 26, 'custom'),
(981, 'avg_rating_', 'Avg rating ', 'Avg rating ', NULL, 26, 'custom'),
(982, 'your_bank_routing_number', 'Your Bank Routing Number', 'Your Bank Routing Number', NULL, 3, 'custom'),
(983, 'your_bank_routing_number', 'Your Bank Routing Number', 'Your Bank Routing Number', 1, NULL, 'custom'),
(984, 'close', 'Close', 'Close', NULL, 26, 'custom'),
(985, 'get_the_job_done_your_wayd', 'Get the job done your wayd!', 'Get the job done your wayd!', 1, NULL, 'custom'),
(986, 'blog', 'Blog', 'Blog', NULL, 11, 'custom'),
(987, 'news_and_updates', 'News and updates', 'News and updates', NULL, 11, 'custom'),
(988, 'no_problem_we_can_help_if_you_can_access_your_email', 'No problem! We can help if you can access your email...', 'No problem! We can help if you can access your email...', NULL, 4, 'custom'),
(989, 'enter_your_email_address', 'Enter your Email Address', 'Enter your Email Address', 1, NULL, 'custom'),
(990, 'reset_password', 'Reset Password', 'Reset Password', NULL, 4, 'custom'),
(991, 'check_your_mailbox', 'Check your mailbox', 'Check your mailbox', NULL, 4, 'custom'),
(992, 'instructions_have_been_sent_to_your_email_address_please_check_your_email_for_confirmation', 'Instructions have been sent to your email address, please check your Email for confirmation!', 'Instructions have been sent to your email address, please check your Email for confirmation!', NULL, 4, 'custom'),
(993, 'log_in', 'Log in', 'Log in', NULL, 4, 'custom'),
(994, 'published_by_x_on_x', 'Published by %s on %s.', 'Published by %s on %s.', NULL, 9, 'custom'),
(995, 'comments', 'Comments', 'Comments', NULL, 9, 'custom');

-- --------------------------------------------------------

--
-- Table structure for table `cms_templates`
--

CREATE TABLE IF NOT EXISTS `cms_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `theme_id` varchar(64) DEFAULT NULL,
  `unique_id` varchar(25) DEFAULT NULL,
  `is_default` tinyint(4) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `theme_id` (`theme_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `cms_templates`
--

INSERT INTO `cms_templates` (`id`, `name`, `file_name`, `content`, `theme_id`, `unique_id`, `is_default`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Content Template', 'content', '<!DOCTYPE html>\n<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->\n<head>\n	<meta charset="utf-8" />\n	<meta name="viewport" content="width=device-width">\n	<title><?=h($this->page->title_name) ?> - <?=c(''site_name'')?></title>\n	<?=$this->display_partial(''site:head'')?>\n</head>\n<body class="<?=$this->page->template_code?> <?=$this->page->page_code?>">\n\n	<div class="wrapper" id="site-header">\n		<div class="container">\n			<?=$this->display_partial(''site:header'')?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-mainnav">\n		<div class="container">\n			<?=$this->display_partial(''site:main_menu'')?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-content">\n		<div class="container">\n			<div class="row">\n				<div class="span3">\n					<div id="p_site_content_menu" class="about-menu">\n						<?=$this->display_partial(''site:about_menu'')?>\n					</div>					\n				</div>\n				<div class="span9">\n					<?=$this->display_page()?>\n				</div>\n			</div>\n		</div>\n	</div>\n	<div class="wrapper" id="site-footer">\n		<div class="container">\n			<?=$this->display_partial(''site:footer'')?>\n		</div>\n	</div>\n\n</body>\n</html>', 'custom', '4ff1208cb395c3.72209500', NULL, NULL, NULL, '2014-07-08 18:17:58', NULL),
(2, 'Default Template', 'default', '<!DOCTYPE html>\n<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->\n<head>\n	<meta charset="utf-8" />\n	<meta name="viewport" content="width=device-width">\n	<title><?=h($this->page->title_name) ?> - <?=c(''site_name'')?></title>\n	<?=$this->display_partial(''site:head'')?>\n</head>\n<body class="<?=$this->page->template_code?> <?=$this->page->page_code?>">\n\n	<div class="wrapper" id="site-header">\n		<div class="container">\n			<?=$this->display_partial(''site:header'')?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-mainnav">\n		<div class="container">\n			<?=$this->display_partial(''site:main_menu'')?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-content">\n		<div class="container">\n			<?=$this->display_page()?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-footer">\n		<div class="container">\n			<?=$this->display_partial(''site:footer'')?>\n		</div>\n	</div>\n\n</body>\n</html>', 'custom', '4f670f15565f26.27069914', NULL, NULL, NULL, '2014-07-08 18:17:58', NULL),
(3, 'Home Template', 'home', '<!DOCTYPE html>\n<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->\n<head>\n	<meta charset="utf-8" />\n	<meta name="viewport" content="width=device-width">\n	<title><?=h($this->page->title_name) ?> - <?=c(''site_name'')?></title>\n	<?=$this->display_partial(''site:head'')?>\n</head>\n<body class="<?=$this->page->template_code?> <?=$this->page->page_code?>">\n	\n	<div class="wrapper" id="site-header">\n		<div class="container">\n			<?=$this->display_partial(''site:header'')?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-mainnav">\n		<div class="container">\n			<?=$this->display_partial(''site:main_menu'')?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-subnav">\n		<div class="container">\n			<?=$this->display_partial(''site:sub_menu'')?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-title">\n		<div class="container">\n			<div class="banner">\n\n				<div class="row-fluid">\n					<div class="span6">\n						<h1><?=__(''Get the job done your way!'', true)?></h1>\n						<p class="lead">\n							<?=__(''We connect you with local service providers who are ready to quote on your job'')?>\n							<a href="<?=root_url(''intro'')?>" class="link-button radius">Learn more</a>\n						</p>\n						<div class="well">\n							<?=$this->display_partial(''widget:submit_request'')?>\n						</div>\n					</div>\n					<div class="span6">\n						<?=$this->display_partial(''widget:featured_provider_slideshow'')?>\n					</div>\n				</div>\n\n			</div>\n		</div>\n		<div class="container">\n			<div class="row hide-for-small" id="banners">\n				<div class="span4">\n					<div class="sub_banner" data-link="<?=root_url(''intro'')?>">\n						<i class="icon icon-comment"></i>\n						<h3><?=__(''Request any type of service'')?></h3>\n					</div>\n				</div>\n				<div class="span4">\n					<div class="sub_banner" data-link="<?=root_url(''intro'')?>">\n						<i class="icon icon-group"></i>\n						<h3><?=__(''Compare price quotes'')?></h3>\n					</div>\n				</div>\n				<div class="span4">\n					<div class="sub_banner" data-link="<?=root_url(''intro'')?>">\n						<i class="icon icon-thumbs-up"></i>\n						<h3><?=__(''Schedule the job online'')?></h3>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n	<div class="wrapper" id="site-content">\n		<div class="container">\n			<?=$this->display_page()?>\n		</div>\n	</div>\n	<div class="wrapper" id="site-footer">\n		<div class="container">\n			<?=$this->display_partial(''site:footer'')?>\n		</div>\n	</div>\n\n</body>\n</html>', 'custom', '4f68669a4ac537.45254800', NULL, NULL, NULL, '2014-07-08 18:17:58', NULL),
(7, 'Full Width Template', 'full_width', '<!DOCTYPE html>\n<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->\n<head>\n	<meta charset="utf-8" />\n	<meta name="viewport" content="width=device-width">\n	<title><?=h($this->page->title_name) ?> - <?=c(''site_name'')?></title>\n	<?=$this->display_partial(''site:head'')?>\n</head>\n<body class="<?=$this->page->template_code?> <?=$this->page->page_code?>">\n\n	<div class="wrapper" id="site-header">\n		<div class="container">\n			<?=$this->display_partial(''site:header'')?>\n		</div>\n	</div>\n	<!--div class="wrapper" id="site-mainnav">\n		<div class="container">\n			<?=$this->display_partial(''site:main_menu'')?>\n		</div>\n	</div-->\n	<div class="wrapper" id="site-content">\n		<div class="container">\n			<div class="row-fluid">\n				<div class="span12">\n					<?=$this->display_page()?>\n				</div>\n			</div>\n		</div>\n	</div>\n	<div class="wrapper" id="site-footer">\n		<div class="container">\n			<?=$this->display_partial(''site:footer'')?>\n		</div>\n	</div>\n\n</body>\n</html>', 'custom', '53e134949a8471.36960487', NULL, 1, 1, '2014-08-05 19:46:28', '2014-08-05 19:51:16'),
(8, 'Full Width Background Template', 'full_width_background', '<!DOCTYPE html>\n<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->\n<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->\n<head>\n	<meta charset="utf-8" />\n	<meta name="viewport" content="width=device-width">\n	<title><?=h($this->page->title_name) ?> - <?=c(''site_name'')?></title>\n	<?=$this->display_partial(''site:head'')?>\n</head>\n<body class="<?=$this->page->template_code?> <?=$this->page->page_code?>">\n\n	<div class="wrapper" id="site-header">\n		<div class="container">\n			<?=$this->display_partial(''site:header'')?>\n		</div>\n	</div>\n	<!--div class="wrapper" id="site-mainnav">\n		<div class="container">\n			<?=$this->display_partial(''site:main_menu'')?>\n		</div>\n	</div-->\n	<div class="wrapper" id="site-content">\n					<?=$this->display_page()?>\n	</div>\n	<div class="wrapper" id="site-footer">\n		<div class="container">\n			<?=$this->display_partial(''site:footer'')?>\n		</div>\n	</div>\n\n</body>\n</html>', 'custom', '53e4bdd28b8673.66507691', NULL, 1, NULL, '2014-08-08 12:08:50', NULL),
(9, 'response', 'response', '<? ?>', 'custom', '540a57df9b93d1.23793046', NULL, 1, 1, '2014-09-06 00:39:59', '2014-09-06 00:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `cms_themes`
--

CREATE TABLE IF NOT EXISTS `cms_themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` text,
  `author_name` varchar(255) DEFAULT NULL,
  `author_website` varchar(255) DEFAULT NULL,
  `is_default` tinyint(4) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_themes`
--

INSERT INTO `cms_themes` (`id`, `name`, `code`, `description`, `author_name`, `author_website`, `is_default`, `enabled`) VALUES
(1, 'Custom', 'custom', 'Default theme for womBids', 'Rajiv Banga', 'http://rajivbanga.com', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `core_settings`
--

CREATE TABLE IF NOT EXISTS `core_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_code` varchar(50) DEFAULT NULL,
  `config_data` text,
  PRIMARY KEY (`id`),
  KEY `record_code` (`record_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `core_settings`
--

INSERT INTO `core_settings` (`id`, `record_code`, `config_data`) VALUES
(1, 'cms_config', '<?xml version="1.0"?>\n<settings><field><id>development_mode</id><value>s:1:"1";</value></field></settings>\n'),
(2, 'core_config', '<?xml version="1.0"?>\n<settings><field><id>site_name</id><value>s:7:"womBids";</value></field><field><id>locale_code</id><value>s:5:"en_US";</value></field></settings>\n'),
(3, 'social_config', '<?xml version="1.0"?>\n<settings><field><id>allow_auth</id><value>s:1:"1";</value></field></settings>\n'),
(4, 'location_config', '<?xml version="1.0"?>\n<settings><field><id>default_country</id><value>s:1:"1";</value></field><field><id>default_state</id><value>s:1:"6";</value></field><field><id>default_unit</id><value>s:2:"mi";</value></field></settings>\n');

-- --------------------------------------------------------

--
-- Table structure for table `db_deferred_bindings`
--

CREATE TABLE IF NOT EXISTS `db_deferred_bindings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_class_name` varchar(100) DEFAULT NULL,
  `detail_class_name` varchar(100) DEFAULT NULL,
  `master_relation_name` varchar(100) DEFAULT NULL,
  `is_bind` int(11) DEFAULT NULL,
  `detail_key_value` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `session_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `db_deferred_bindings`
--

INSERT INTO `db_deferred_bindings` (`id`, `master_class_name`, `detail_class_name`, `master_relation_name`, `is_bind`, `detail_key_value`, `created_at`, `session_key`) VALUES
(71, 'Service_Provider', 'Service_Rating', 'ratings', 1, 4, '2014-09-08 06:05:26', 'Service_Provider540d46fb15b144.86220166'),
(67, 'Service_Provider', 'Db_File', 'logo', 1, 51, '2014-09-06 22:29:22', 'phpr540b8a4c5f653');

-- --------------------------------------------------------

--
-- Table structure for table `db_files`
--

CREATE TABLE IF NOT EXISTS `db_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `disk_name` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `field` varchar(100) DEFAULT NULL,
  `master_object_class` varchar(255) DEFAULT NULL,
  `master_object_id` int(11) DEFAULT NULL,
  `is_public` tinyint(4) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `field` (`field`),
  KEY `created_user_id` (`created_user_id`),
  KEY `master_index` (`master_object_class`,`master_object_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `db_files`
--

INSERT INTO `db_files` (`id`, `name`, `title`, `description`, `disk_name`, `mime_type`, `size`, `field`, `master_object_class`, `master_object_id`, `is_public`, `sort_order`, `created_at`, `created_user_id`) VALUES
(1, 'logo.png', NULL, NULL, '540e050ae89dd6.30868911.png', 'image/png', 4678, 'logo', 'Cms_Config', 1, 1, 1, '2014-09-08 19:35:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `db_model_logs`
--

CREATE TABLE IF NOT EXISTS `db_model_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_object_class` varchar(255) DEFAULT NULL,
  `master_object_id` int(11) DEFAULT NULL,
  `type` char(20) DEFAULT NULL,
  `param_data` text,
  `record_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `master_object_id` (`master_object_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `db_model_logs`
--

INSERT INTO `db_model_logs` (`id`, `master_object_class`, `master_object_id`, `type`, `param_data`, `record_datetime`) VALUES
(1, 'Service_Quote', 1, 'create', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="status" display_name="Status" type=""><new>New</new></field><field name="moderation_status" display_name="Moderation Status" type="varchar"><new>new</new></field><field name="id" display_name="#" type="number"><new>1</new></field><field name="request" display_name="Request" type=""><new>Painter</new></field><field name="user" display_name="User" type=""><new>provider</new></field><field name="provider" display_name="Provider" type=""><new>15</new></field><field name="comment" display_name="Comment" type="text"><new>Dear client1,\n\nGreat\n\nI could start today. If you''d like to move forward, click ''Set Appointment'' on womBids so we can share contact details and schedule a time.\n\nBest regards, \nComidie Entertainment Ltd</new></field><field name="price" display_name="Price" type="float"><new>4500.00</new></field><field name="quote_type" display_name="Quote Type" type="varchar"><new>flat_rate</new></field><field name="flat_items" display_name="Price of Materials" type="text"><new>[{"description":"Paint","price":"4000"}]</new></field><field name="flat_labor_description" display_name="Labour Description" type="text"><new>Test Desc</new></field><field name="flat_labor_price" display_name="Labour Price" type="float"><new>500.00</new></field></record>\n', '2014-08-17 02:20:50'),
(2, 'Service_Quote', 1, 'update', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="status" display_name="Status" type=""><old>New</old><new>Accepted</new></field></record>\n', '2014-08-17 02:24:58'),
(3, 'Service_Quote', 1, 'update', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="start_at" display_name="Available to Start" type="datetime"><old/><new>08/16/2014 09:00:00 AM</new></field></record>\n', '2014-08-17 02:26:21'),
(4, 'Service_Quote', 2, 'create', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="status" display_name="Status" type=""><new>New</new></field><field name="moderation_status" display_name="Moderation Status" type="varchar"><new>new</new></field><field name="id" display_name="#" type="number"><new>2</new></field><field name="request" display_name="Request" type=""><new>Painter</new></field><field name="user" display_name="User" type=""><new>alpha007</new></field><field name="provider" display_name="Provider" type=""><new>18</new></field><field name="comment" display_name="Comment" type="text"><new>ghf jghj gh</new></field><field name="price" display_name="Price" type="float"><new>675.00</new></field><field name="start_at" display_name="Available to Start" type="datetime"><new>08/19/2014 02:40:00 AM</new></field><field name="flat_labor_description" display_name="Labour Description" type="text"><new>ghj ghj gh</new></field><field name="flat_labor_price" display_name="Labour Price" type="float"><new>65756.00</new></field></record>\n', '2014-08-19 04:02:45'),
(5, 'Service_Quote', 2, 'update', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="start_at" display_name="Available to Start" type="datetime"><old>08/19/2014 02:40:00 AM</old><new>08/20/2014 09:00:00 AM</new></field></record>\n', '2014-08-19 04:03:38'),
(6, 'Service_Quote', 2, 'update', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="moderation_status" display_name="Moderation Status" type="varchar"><old>new</old><new>approved</new></field></record>\n', '2014-08-19 04:04:14'),
(7, 'Service_Quote', 3, 'create', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="status" display_name="Status" type=""><new>New</new></field><field name="moderation_status" display_name="Moderation Status" type="varchar"><new>new</new></field><field name="id" display_name="#" type="number"><new>3</new></field><field name="request" display_name="Request" type=""><new>Painter</new></field><field name="user" display_name="User" type=""><new>ElectricianUSA</new></field><field name="provider" display_name="Provider" type=""><new>23</new></field><field name="comment" display_name="Comment" type="text"><new>Dear client2,\n\nPick me. \n\nI could start in a few days. If you''d like to move forward, click ''Set Appointment'' on womBids so we can share contact details and schedule a time.\n\nThanks, \nElectricianUSA</new></field><field name="price" display_name="Price" type="float"><new>250.00</new></field><field name="quote_type" display_name="Quote Type" type="varchar"><new>flat_rate</new></field><field name="flat_items" display_name="Price of Materials" type="text"><new>[{"description":"overhead","price":"50"}]</new></field><field name="flat_labor_price" display_name="Labour Price" type="float"><new>200.00</new></field></record>\n', '2014-08-20 23:57:53'),
(8, 'Service_Quote', 4, 'create', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="status" display_name="Status" type=""><new>New</new></field><field name="moderation_status" display_name="Moderation Status" type="varchar"><new>new</new></field><field name="id" display_name="#" type="number"><new>4</new></field><field name="request" display_name="Request" type=""><new>Painter</new></field><field name="user" display_name="User" type=""><new>test123</new></field><field name="provider" display_name="Provider" type=""><new>24</new></field><field name="comment" display_name="Comment" type="text"><new>Dear request123,\n\ndf gf hgfh gf fg gf ngfn fgnfdnnjdndxgfn fdgn\n\nI could start today. If you''d like to move forward, click ''Set Appointment'' on womBids so we can share contact details and schedule a time.\n\nThanks, \nNew Painter Business</new></field><field name="price" display_name="Price" type="float"><new>456.00</new></field><field name="quote_type" display_name="Quote Type" type="varchar"><new>flat_rate</new></field><field name="flat_items" display_name="Price of Materials" type="text"><new>[]</new></field><field name="flat_labor_description" display_name="Labour Description" type="text"><new>sd v vdf dgfb fg bfg</new></field><field name="flat_labor_price" display_name="Labour Price" type="float"><new>456.00</new></field></record>\n', '2014-09-06 23:11:36'),
(9, 'Service_Quote', 4, 'update', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="status" display_name="Status" type=""><old>New</old><new>Accepted</new></field></record>\n', '2014-09-06 23:13:54'),
(10, 'Service_Quote', 4, 'update', '<?xml version="1.0" encoding="utf-8"?>\n<record><field name="status" display_name="Status" type=""><old>Accepted</old><new>New</new></field></record>\n', '2014-09-06 23:17:03');

-- --------------------------------------------------------

--
-- Table structure for table `db_record_locks`
--

CREATE TABLE IF NOT EXISTS `db_record_locks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_id` int(11) DEFAULT NULL,
  `record_class` varchar(100) DEFAULT NULL,
  `non_db_hash` varchar(100) DEFAULT NULL,
  `last_ping` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `non_db_hash` (`non_db_hash`),
  KEY `record_index` (`record_id`,`record_class`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_saved_tickets`
--

CREATE TABLE IF NOT EXISTS `db_saved_tickets` (
  `ticket_id` varchar(50) NOT NULL,
  `ticket_data` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `db_session_data`
--

CREATE TABLE IF NOT EXISTS `db_session_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_ip` varchar(15) DEFAULT NULL,
  `session_id` varchar(100) DEFAULT NULL,
  `session_data` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`,`client_ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `location_countries`
--

CREATE TABLE IF NOT EXISTS `location_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `code_3` varchar(3) DEFAULT NULL,
  `code_iso_numeric` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=247 ;

--
-- Dumping data for table `location_countries`
--

INSERT INTO `location_countries` (`id`, `code`, `name`, `enabled`, `code_3`, `code_iso_numeric`) VALUES
(1, 'US', 'United States', 1, 'USA', '840'),
(2, 'CA', 'Canada', NULL, 'CAN', '124'),
(3, 'AU', 'Australia', NULL, 'AUS', '036'),
(4, 'FR', 'France', 0, 'FRA', '250'),
(5, 'DE', 'Germany', 0, 'DEU', '276'),
(6, 'IS', 'Iceland', 0, 'ISL', '352'),
(7, 'IE', 'Ireland', 0, 'IRL', '372'),
(8, 'IT', 'Italy', 0, 'ITA', '380'),
(9, 'ES', 'Spain', 0, 'ESP', '724'),
(10, 'SE', 'Sweden', 0, 'SWE', '752'),
(11, 'AT', 'Austria', 0, 'AUT', '040'),
(12, 'BE', 'Belgium', 0, 'BEL', '056'),
(13, 'FI', 'Finland', 0, 'FIN', '246'),
(14, 'CZ', 'Czech Republic', 0, 'CZE', '203'),
(15, 'DK', 'Denmark', 0, 'DNK', '208'),
(16, 'NO', 'Norway', 0, 'NOR', '578'),
(17, 'GB', 'United Kingdom', 0, 'GBR', '826'),
(18, 'CH', 'Switzerland', 0, 'CHE', '756'),
(19, 'NZ', 'New Zealand', 0, 'NZL', '554'),
(20, 'RU', 'Russian Federation', 0, 'RUS', '643'),
(21, 'PT', 'Portugal', 0, 'PRT', '620'),
(22, 'NL', 'Netherlands', 0, 'NLD', '528'),
(23, 'IM', 'Isle of Man', 0, 'IMN', '833'),
(24, 'AF', 'Afghanistan', 0, 'AFG', '004'),
(25, 'AX', 'Aland Islands ﻿ Åland Islands', 0, 'ALA', '248'),
(26, 'AL', 'Albania', 0, 'ALB', '008'),
(27, 'DZ', 'Algeria', 0, 'DZA', '012'),
(28, 'AS', 'American Samoa', 0, 'ASM', '016'),
(29, 'AD', 'Andorra', 0, 'AND', '020'),
(30, 'AO', 'Angola', 0, 'AGO', '024'),
(31, 'AI', 'Anguilla', 0, 'AIA', '660'),
(32, 'AQ', 'Antarctica', 0, 'ATA', '010'),
(33, 'AG', 'Antigua and Barbuda', 0, 'ATG', '028'),
(34, 'AR', 'Argentina', 0, 'ARG', '032'),
(35, 'AM', 'Armenia', 0, 'ARM', '051'),
(36, 'AW', 'Aruba', 0, 'ABW', '533'),
(37, 'AZ', 'Azerbaijan', 0, 'AZE', '031'),
(38, 'BS', 'Bahamas', 0, 'BHS', '044'),
(39, 'BH', 'Bahrain', 0, 'BHR', '048'),
(40, 'BD', 'Bangladesh', 0, 'BGD', '050'),
(41, 'BB', 'Barbados', 0, 'BRB', '052'),
(42, 'BY', 'Belarus', 0, 'BLR', '112'),
(43, 'BZ', 'Belize', 0, 'BLZ', '084'),
(44, 'BJ', 'Benin', 0, 'BEN', '204'),
(45, 'BM', 'Bermuda', 0, 'BMU', '060'),
(46, 'BT', 'Bhutan', 0, 'BTN', '064'),
(47, 'BO', 'Bolivia, Plurinational State of', 0, 'BOL', '068'),
(48, 'BA', 'Bosnia and Herzegovina', 0, 'BIH', '070'),
(49, 'BW', 'Botswana', 0, 'BWA', '072'),
(50, 'BV', 'Bouvet Island', 0, 'BVT', '074'),
(51, 'BR', 'Brazil', 0, 'BRA', '076'),
(52, 'IO', 'British Indian Ocean Territory', 0, 'IOT', '086'),
(53, 'BN', 'Brunei Darussalam', 0, 'BRN', '096'),
(54, 'BG', 'Bulgaria', 0, 'BGR', '100'),
(55, 'BF', 'Burkina Faso', 0, 'BFA', '854'),
(56, 'BI', 'Burundi', 0, 'BDI', '108'),
(57, 'KH', 'Cambodia', 0, 'KHM', '116'),
(58, 'CM', 'Cameroon', 0, 'CMR', '120'),
(59, 'CV', 'Cape Verde', 0, 'CPV', '132'),
(60, 'KY', 'Cayman Islands', 0, 'CYM', '136'),
(61, 'CF', 'Central African Republic', 0, 'CAF', '140'),
(62, 'TD', 'Chad', 0, 'TCD', '148'),
(63, 'CL', 'Chile', 0, 'CHL', '152'),
(64, 'CN', 'China', 0, 'CHN', '156'),
(65, 'CX', 'Christmas Island', 0, 'CXR', '162'),
(66, 'CC', 'Cocos (Keeling) Islands', 0, 'CCK', '166'),
(67, 'CO', 'Colombia', 0, 'COL', '170'),
(68, 'KM', 'Comoros', 0, 'COM', '174'),
(69, 'CG', 'Congo', 0, 'COG', '178'),
(70, 'CD', 'Congo, the Democratic Republic of the', 0, 'COD', '180'),
(71, 'CK', 'Cook Islands', 0, 'COK', '184'),
(72, 'CR', 'Costa Rica', 0, 'CRI', '188'),
(73, 'CI', 'Cote d''Ivoire', 0, 'CIV', '384'),
(74, 'HR', 'Croatia', 0, 'HRV', '191'),
(75, 'CU', 'Cuba', 0, 'CUB', '192'),
(76, 'CY', 'Cyprus', 0, 'CYP', '196'),
(77, 'DJ', 'Djibouti', 0, 'DJI', '262'),
(78, 'DM', 'Dominica', 0, 'DMA', '212'),
(79, 'DO', 'Dominican Republic', 0, 'DOM', '214'),
(80, 'EC', 'Ecuador', 0, 'ECU', '218'),
(81, 'EG', 'Egypt', 0, 'EGY', '818'),
(82, 'SV', 'El Salvador', 0, 'SLV', '222'),
(83, 'GQ', 'Equatorial Guinea', 0, 'GNQ', '226'),
(84, 'ER', 'Eritrea', 0, 'ERI', '232'),
(85, 'EE', 'Estonia', 0, 'EST', '233'),
(86, 'ET', 'Ethiopia', 0, 'ETH', '231'),
(87, 'FK', 'Falkland Islands (Malvinas)', 0, 'FLK', '238'),
(88, 'FO', 'Faroe Islands', 0, 'FRO', '234'),
(89, 'FJ', 'Fiji', 0, 'FJI', '242'),
(90, 'GF', 'French Guiana', 0, 'GUF', '254'),
(91, 'PF', 'French Polynesia', 0, 'PYF', '258'),
(92, 'TF', 'French Southern Territories', 0, 'ATF', '260'),
(93, 'GA', 'Gabon', 0, 'GAB', '266'),
(94, 'GM', 'Gambia', 0, 'GMB', '270'),
(95, 'GE', 'Georgia', 0, 'GEO', '268'),
(96, 'GH', 'Ghana', 0, 'GHA', '288'),
(97, 'GI', 'Gibraltar', 0, 'GIB', '292'),
(98, 'GR', 'Greece', 0, 'GRC', '300'),
(99, 'GL', 'Greenland', 0, 'GRL', '304'),
(100, 'GD', 'Grenada', 0, 'GRD', '308'),
(101, 'GP', 'Guadeloupe', 0, 'GLP', '312'),
(102, 'GU', 'Guam', 0, 'GUM', '316'),
(103, 'GT', 'Guatemala', 0, 'GTM', '320'),
(104, 'GG', 'Guernsey', 0, 'GGY', '831'),
(105, 'GN', 'Guinea', 0, 'GIN', '324'),
(106, 'GW', 'Guinea-Bissau', 0, 'GNB', '624'),
(107, 'GY', 'Guyana', 0, 'GUY', '328'),
(108, 'HT', 'Haiti', 0, 'HTI', '332'),
(109, 'HM', 'Heard Island and McDonald Islands', 0, 'HMD', '334'),
(110, 'VA', 'Holy See (Vatican City State)', 0, 'VAT', '336'),
(111, 'HN', 'Honduras', 0, 'HND', '340'),
(112, 'HK', 'Hong Kong', 0, 'HKG', '344'),
(113, 'HU', 'Hungary', 0, 'HUN', '348'),
(114, 'IN', 'India', 0, 'IND', '356'),
(115, 'ID', 'Indonesia', 0, 'IDN', '360'),
(116, 'IR', 'Iran, Islamic Republic of', 0, 'IRN', '364'),
(117, 'IQ', 'Iraq', 0, 'IRQ', '368'),
(118, 'IL', 'Israel', 0, 'ISR', '376'),
(119, 'JM', 'Jamaica', 0, 'JAM', '388'),
(120, 'JP', 'Japan', 0, 'JPN', '392'),
(121, 'JE', 'Jersey', 0, 'JEY', '832'),
(122, 'JO', 'Jordan', 0, 'JOR', '400'),
(123, 'KZ', 'Kazakhstan', 0, 'KAZ', '398'),
(124, 'KE', 'Kenya', 0, 'KEN', '404'),
(125, 'KI', 'Kiribati', 0, 'KIR', '296'),
(126, 'KP', 'Korea, Democratic People''s Republic of', 0, 'PRK', '408'),
(127, 'KR', 'Korea, Republic of', 0, 'KOR', '410'),
(128, 'KW', 'Kuwait', 0, 'KWT', '414'),
(129, 'KG', 'Kyrgyzstan', 0, 'KGZ', '417'),
(130, 'LA', 'Lao People''s Democratic Republic', 0, 'LAO', '418'),
(131, 'LV', 'Latvia', 0, 'LVA', '428'),
(132, 'LB', 'Lebanon', 0, 'LBN', '422'),
(133, 'LS', 'Lesotho', 0, 'LSO', '426'),
(134, 'LR', 'Liberia', 0, 'LBR', '430'),
(135, 'LY', 'Libyan Arab Jamahiriya', 0, 'LBY', '434'),
(136, 'LI', 'Liechtenstein', 0, 'LIE', '438'),
(137, 'LT', 'Lithuania', 0, 'LTU', '440'),
(138, 'LU', 'Luxembourg', 0, 'LUX', '442'),
(139, 'MO', 'Macao', 0, 'MAC', '446'),
(140, 'MK', 'Macedonia', 0, 'MKD', '807'),
(141, 'MG', 'Madagascar', 0, 'MDG', '450'),
(142, 'MW', 'Malawi', 0, 'MWI', '454'),
(143, 'MY', 'Malaysia', 0, 'MYS', '458'),
(144, 'MV', 'Maldives', 0, 'MDV', '462'),
(145, 'ML', 'Mali', 0, 'MLI', '466'),
(146, 'MT', 'Malta', 0, 'MLT', '470'),
(147, 'MH', 'Marshall Islands', 0, 'MHL', '584'),
(148, 'MQ', 'Martinique', 0, 'MTQ', '474'),
(149, 'MR', 'Mauritania', 0, 'MRT', '478'),
(150, 'MU', 'Mauritius', 0, 'MUS', '480'),
(151, 'YT', 'Mayotte', 0, 'MYT', '175'),
(152, 'MX', 'Mexico', 0, 'MEX', '484'),
(153, 'FM', 'Micronesia, Federated States of', 0, 'FSM', '583'),
(154, 'MD', 'Moldova, Republic of', 0, 'MDA', '498'),
(155, 'MC', 'Monaco', 0, 'MCO', '492'),
(156, 'MN', 'Mongolia', 0, 'MNG', '496'),
(157, 'ME', 'Montenegro', 0, 'MNE', '499'),
(158, 'MS', 'Montserrat', 0, 'MSR', '500'),
(159, 'MA', 'Morocco', 0, 'MAR', '504'),
(160, 'MZ', 'Mozambique', 0, 'MOZ', '508'),
(161, 'MM', 'Myanmar', 0, 'MMR', '104'),
(162, 'NA', 'Namibia', 0, 'NAM', '516'),
(163, 'NR', 'Nauru', 0, 'NRU', '520'),
(164, 'NP', 'Nepal', 0, 'NPL', '524'),
(165, 'AN', 'Netherlands Antilles', 0, 'ANT', '530'),
(166, 'NC', 'New Caledonia', 0, 'NCL', '540'),
(167, 'NI', 'Nicaragua', 0, 'NIC', '558'),
(168, 'NE', 'Niger', 0, 'NER', '562'),
(169, 'NG', 'Nigeria', 0, 'NGA', '566'),
(170, 'NU', 'Niue', 0, 'NIU', '570'),
(171, 'NF', 'Norfolk Island', 0, 'NFK', '574'),
(172, 'MP', 'Northern Mariana Islands', 0, 'MNP', '580'),
(173, 'OM', 'Oman', 0, 'OMN', '512'),
(174, 'PK', 'Pakistan', 0, 'PAK', '586'),
(175, 'PW', 'Palau', 0, 'PLW', '585'),
(176, 'PS', 'Palestinian Territory, Occupied', 0, 'PSE', '275'),
(177, 'PA', 'Panama', 0, 'PAN', '591'),
(178, 'PG', 'Papua New Guinea', 0, 'PNG', '598'),
(179, 'PY', 'Paraguay', 0, 'PRY', '600'),
(180, 'PE', 'Peru', 0, 'PER', '604'),
(181, 'PH', 'Philippines', 0, 'PHL', '608'),
(182, 'PN', 'Pitcairn', 0, 'PCN', '612'),
(183, 'PL', 'Poland', 0, 'POL', '616'),
(184, 'PR', 'Puerto Rico', 0, 'PRI', '630'),
(185, 'QA', 'Qatar', 0, 'QAT', '634'),
(186, 'RE', 'Reunion', 0, 'REU', '638'),
(187, 'RO', 'Romania', 0, 'ROU', '642'),
(188, 'RW', 'Rwanda', 0, 'RWA', '646'),
(189, 'BL', 'Saint Barthélemy', 0, 'BLM', '652'),
(190, 'SH', 'Saint Helena', 0, 'SHN', '654'),
(191, 'KN', 'Saint Kitts and Nevis', 0, 'KNA', '659'),
(192, 'LC', 'Saint Lucia', 0, 'LCA', '662'),
(193, 'MF', 'Saint Martin (French part)', 0, 'MAF', '663'),
(194, 'PM', 'Saint Pierre and Miquelon', 0, 'SPM', '666'),
(195, 'VC', 'Saint Vincent and the Grenadines', 0, 'VCT', '670'),
(196, 'WS', 'Samoa', 0, 'WSM', '882'),
(197, 'SM', 'San Marino', 0, 'SMR', '674'),
(198, 'ST', 'Sao Tome and Principe', 0, 'STP', '678'),
(199, 'SA', 'Saudi Arabia', 0, 'SAU', '682'),
(200, 'SN', 'Senegal', 0, 'SEN', '686'),
(201, 'RS', 'Serbia', 0, 'SRB', '688'),
(202, 'SC', 'Seychelles', 0, 'SYC', '690'),
(203, 'SL', 'Sierra Leone', 0, 'SLE', '694'),
(204, 'SG', 'Singapore', 0, 'SGP', '702'),
(205, 'SK', 'Slovakia', 0, 'SVK', '703'),
(206, 'SI', 'Slovenia', 0, 'SVN', '705'),
(207, 'SB', 'Solomon Islands', 0, 'SLB', '090'),
(208, 'SO', 'Somalia', 0, 'SOM', '706'),
(209, 'ZA', 'South Africa', 0, 'ZAF', '710'),
(210, 'GS', 'South Georgia and the South Sandwich Islands', 0, 'SGS', '239'),
(211, 'LK', 'Sri Lanka', 0, 'LKA', '144'),
(212, 'SD', 'Sudan', 0, 'SDN', '736'),
(213, 'SR', 'Suriname', 0, 'SUR', '740'),
(214, 'SJ', 'Svalbard and Jan Mayen', 0, 'SJM', '744'),
(215, 'SZ', 'Swaziland', 0, 'SWZ', '748'),
(216, 'SY', 'Syrian Arab Republic', 0, 'SYR', '760'),
(217, 'TW', 'Taiwan, Province of China', 0, 'TWN', '158'),
(218, 'TJ', 'Tajikistan', 0, 'TJK', '762'),
(219, 'TZ', 'Tanzania, United Republic of', 0, 'TZA', '834'),
(220, 'TH', 'Thailand', 0, 'THA', '764'),
(221, 'TL', 'Timor-Leste', 0, 'TLS', '626'),
(222, 'TG', 'Togo', 0, 'TGO', '768'),
(223, 'TK', 'Tokelau', 0, 'TKL', '772'),
(224, 'TO', 'Tonga', 0, 'TON', '776'),
(225, 'TT', 'Trinidad and Tobago', 0, 'TTO', '780'),
(226, 'TN', 'Tunisia', 0, 'TUN', '788'),
(227, 'TR', 'Turkey', 0, 'TUR', '792'),
(228, 'TM', 'Turkmenistan', 0, 'TKM', '795'),
(229, 'TC', 'Turks and Caicos Islands', 0, 'TCA', '796'),
(230, 'TV', 'Tuvalu', 0, 'TUV', '798'),
(231, 'UG', 'Uganda', 0, 'UGA', '800'),
(232, 'UA', 'Ukraine', 0, 'UKR', '804'),
(233, 'AE', 'United Arab Emirates', 0, 'ARE', '784'),
(234, 'UM', 'United States Minor Outlying Islands', 0, 'UMI', '581'),
(235, 'UY', 'Uruguay', 0, 'URY', '858'),
(236, 'UZ', 'Uzbekistan', 0, 'UZB', '860'),
(237, 'VU', 'Vanuatu', 0, 'VUT', '548'),
(238, 'VE', 'Venezuela, Bolivarian Republic of', 0, 'VEN', '862'),
(239, 'VN', 'Viet Nam', 0, 'VNM', '704'),
(240, 'VG', 'Virgin Islands, British', 0, 'VGB', '092'),
(241, 'VI', 'Virgin Islands, U.S.', 0, 'VIR', '850'),
(242, 'WF', 'Wallis and Futuna', 0, 'WLF', '876'),
(243, 'EH', 'Western Sahara', 0, 'ESH', '732'),
(244, 'YE', 'Yemen', 0, 'YEM', '887'),
(245, 'ZM', 'Zambia', 0, 'ZMB', '894'),
(246, 'ZW', 'Zimbabwe', 0, 'ZWE', '716');

-- --------------------------------------------------------

--
-- Table structure for table `location_states`
--

CREATE TABLE IF NOT EXISTS `location_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=421 ;

--
-- Dumping data for table `location_states`
--

INSERT INTO `location_states` (`id`, `country_id`, `code`, `name`) VALUES
(1, 1, 'AL', 'Alabama'),
(2, 1, 'AK', 'Alaska'),
(3, 1, 'AS', 'American Samoa'),
(4, 1, 'AZ', 'Arizona'),
(5, 1, 'AR', 'Arkansas'),
(6, 1, 'CA', 'California'),
(7, 1, 'CO', 'Colorado'),
(8, 1, 'CT', 'Connecticut'),
(9, 1, 'DE', 'Delaware'),
(10, 1, 'DC', 'Dist. of Columbia'),
(11, 1, 'FL', 'Florida'),
(12, 1, 'GA', 'Georgia'),
(13, 1, 'GU', 'Guam'),
(14, 1, 'HI', 'Hawaii'),
(15, 1, 'ID', 'Idaho'),
(16, 1, 'IL', 'Illinois'),
(17, 1, 'IN', 'Indiana'),
(18, 1, 'IA', 'Iowa'),
(19, 1, 'KS', 'Kansas'),
(20, 1, 'KY', 'Kentucky'),
(21, 1, 'LA', 'Louisiana'),
(22, 1, 'ME', 'Maine'),
(23, 1, 'MD', 'Maryland'),
(24, 1, 'MH', 'Marshall Islands'),
(25, 1, 'MA', 'Massachusetts'),
(26, 1, 'MI', 'Michigan'),
(27, 1, 'FM', 'Micronesia'),
(28, 1, 'MN', 'Minnesota'),
(29, 1, 'MS', 'Mississippi'),
(30, 1, 'MO', 'Missouri'),
(31, 1, 'MT', 'Montana'),
(32, 1, 'NE', 'Nebraska'),
(33, 1, 'NV', 'Nevada'),
(34, 1, 'NH', 'New Hampshire'),
(35, 1, 'NJ', 'New Jersey'),
(36, 1, 'NM', 'New Mexico'),
(37, 1, 'NY', 'New York'),
(38, 1, 'NC', 'North Carolina'),
(39, 1, 'ND', 'North Dakota'),
(40, 1, 'MP', 'Northern Marianas'),
(41, 1, 'OH', 'Ohio'),
(42, 1, 'OK', 'Oklahoma'),
(43, 1, 'OR', 'Oregon'),
(44, 1, 'PW', 'Palau'),
(45, 1, 'PA', 'Pennsylvania'),
(46, 1, 'PR', 'Puerto Rico'),
(47, 1, 'RI', 'Rhode Island'),
(48, 1, 'SC', 'South Carolina'),
(49, 1, 'SD', 'South Dakota'),
(50, 1, 'TN', 'Tennessee'),
(51, 1, 'TX', 'Texas'),
(52, 1, 'UT', 'Utah'),
(53, 1, 'VT', 'Vermont'),
(54, 1, 'VA', 'Virginia'),
(55, 1, 'VI', 'Virgin Islands'),
(56, 1, 'WA', 'Washington'),
(57, 1, 'WV', 'West Virginia'),
(58, 1, 'WI', 'Wisconsin'),
(59, 1, 'WY', 'Wyoming'),
(60, 2, 'AB', 'Alberta'),
(61, 2, 'BC', 'British Columbia'),
(62, 2, 'MB', 'Manitoba'),
(63, 2, 'NB', 'New Brunswick'),
(64, 2, 'NL', 'Newfoundland and Labrador'),
(65, 2, 'NT', 'Northwest Territories'),
(66, 2, 'NS', 'Nova Scotia'),
(67, 2, 'NU', 'Nunavut'),
(68, 2, 'ON', 'Ontario'),
(69, 2, 'PE', 'Prince Edward Island'),
(70, 2, 'QC', 'Quebec'),
(71, 2, 'SK', 'Saskatchewan'),
(72, 2, 'YT', 'Yukon'),
(73, 3, 'NSW', 'New South Wales'),
(74, 3, 'QLD', 'Queensland'),
(75, 3, 'SA', 'South Australia'),
(76, 3, 'TAS', 'Tasmania'),
(77, 3, 'VIC', 'Victoria'),
(78, 3, 'WA', 'Western Australia'),
(79, 3, 'NT', 'Northern Territory'),
(80, 3, 'ACT', 'Australian Capital Territory'),
(81, 5, 'BW', 'Baden-Württemberg'),
(82, 5, 'BY', 'Bavaria'),
(83, 5, 'BE', 'Berlin'),
(84, 5, 'BB', 'Brandenburg'),
(85, 5, 'HB', 'Bremen'),
(86, 5, 'HH', 'Hamburg'),
(87, 5, 'HE', 'Hesse'),
(88, 5, 'MV', 'Mecklenburg-Vorpommern'),
(89, 5, 'NI', 'Lower Saxony'),
(90, 5, 'NW', 'North Rhine-Westphalia'),
(91, 5, 'RP', 'Rhineland-Palatinate'),
(92, 5, 'SL', 'Saarland'),
(93, 5, 'SN', 'Saxony'),
(94, 5, 'ST', 'Saxony-Anhalt'),
(95, 5, 'SH', 'Schleswig-Holstein'),
(96, 5, 'TH', 'Thuringia'),
(97, 7, 'D', 'Dublin'),
(98, 7, 'WW', 'Wicklow'),
(99, 7, 'WX', 'Wexford'),
(100, 7, 'CW', 'Carlow'),
(101, 7, 'KE', 'Kildare'),
(102, 7, 'MH', 'Meath'),
(103, 7, 'LH', 'Louth'),
(104, 7, 'MN', 'Monaghan'),
(105, 7, 'CN', 'Cavan'),
(106, 7, 'LD', 'Longford'),
(107, 7, 'WH', 'Westmeath'),
(108, 7, 'OY', 'Offaly'),
(109, 7, 'LS', 'Laois'),
(110, 7, 'KK', 'Kilkenny'),
(111, 7, 'WD', 'Waterford'),
(112, 7, 'C', 'Cork'),
(113, 7, 'KY', 'Kerry'),
(114, 7, 'LK', 'Limerick'),
(115, 7, 'TN', 'North Tipperary'),
(116, 7, 'TS', 'South Tipperary'),
(117, 7, 'CE', 'Clare'),
(118, 7, 'G', 'Galway'),
(119, 7, 'MO', 'Mayo'),
(120, 7, 'RN', 'Roscommon'),
(121, 7, 'SO', 'Sligo'),
(122, 7, 'LM', 'Leitrim'),
(123, 7, 'DL', 'Donegal'),
(124, 8, 'abruzzo', 'Abruzzo'),
(125, 8, 'aosta_valley', 'Aosta Valley'),
(126, 8, 'apulia', 'Apulia'),
(127, 8, 'basilicata', 'Basilicata'),
(128, 8, 'calabria', 'Calabria'),
(129, 8, 'campania', 'Campania'),
(130, 8, 'emilia_romagna', 'Emilia-Romagna'),
(131, 8, 'friuli_venezia_giulia', 'Friuli-VeneziaGiulia'),
(132, 8, 'lazio', 'Lazio'),
(133, 8, 'liguria', 'Liguria'),
(134, 8, 'lombardy', 'Lombardy'),
(135, 8, 'marche', 'Marche'),
(136, 8, 'molise', 'Molise'),
(137, 8, 'piedmont', 'Piedmont'),
(138, 8, 'sardinia', 'Sardinia'),
(139, 8, 'sicily', 'Sicily'),
(140, 8, 'tuscany', 'Tuscany'),
(141, 8, 'trentino_alto_adige', 'Trentino-Alto Adige'),
(142, 8, 'umbria', 'Umbria'),
(143, 10, 'stockholm', 'Stockholm County'),
(144, 10, 'skåne', 'Skåne County'),
(145, 10, 'uppsala', 'Uppsala County'),
(146, 10, 'västmanland', 'Västmanland County'),
(147, 10, 'örebro', 'Örebro County'),
(148, 10, 'jönköping', 'Jönköping County'),
(149, 10, 'östergötland', 'Östergötland County'),
(150, 10, 'västerbotten', 'Västerbotten County'),
(151, 10, 'gävleborg', 'Gävleborg County'),
(152, 10, 'västra_götaland', 'Västra Götaland County'),
(153, 10, 'södermanland', 'Södermanland County'),
(154, 10, 'värmland', 'Värmland County'),
(155, 10, 'halland', 'Halland County'),
(156, 10, 'kronoberg', 'Kronoberg County'),
(157, 11, 'burgenland', 'Burgenland'),
(158, 11, 'carinthia', 'Carinthia'),
(159, 11, 'lower_austria', 'Lower Austria'),
(160, 11, 'upper_austria', 'Upper Austria'),
(161, 11, 'salzburg', 'Salzburg'),
(162, 11, 'styria', 'Styria'),
(163, 11, 'tyrol', 'Tyrol'),
(164, 11, 'vorarlberg', 'Vorarlberg'),
(165, 11, 'vienna', 'Vienna'),
(166, 14, 'PRAGUE', 'Prague'),
(167, 14, 'ST', 'Central Bohemian'),
(168, 14, 'JC', 'South Bohemian '),
(169, 14, 'PL', 'Plzeň'),
(170, 14, 'KA', 'Karlovy Vary'),
(171, 14, 'US', 'Ústí nad Labem'),
(172, 14, 'LI', 'Liberec'),
(173, 14, 'KR', 'Hradec Králové'),
(174, 14, 'PA', 'Pardubice'),
(175, 14, 'OL', 'Olomouc'),
(176, 14, 'MO', 'Moravian-Silesian'),
(177, 14, 'JM', 'South Moravian'),
(178, 14, 'ZL', 'Zlín'),
(179, 14, 'VY', 'Vysočina'),
(180, 16, 'finnmark', 'Northern Norway/Finnmark'),
(181, 16, 'troms', 'Northern Norway/Troms'),
(182, 16, 'nordland', 'Northern Norway/Nordland'),
(183, 16, 'nord_trøndelag', 'Trøndelag/Nord-Trøndelag'),
(184, 16, 'sør_trøndelag', 'Trøndelag/Sør-Trøndelag'),
(185, 16, 'møre_og_romsdal', 'Western Norway/Møre og Romsdal'),
(186, 16, 'sogn_og_fjordane', 'Western Norway/Sogn og Fjordane'),
(187, 16, 'hordaland', 'Western Norway/Hordaland'),
(188, 16, 'rogaland', 'Western Norway/Rogaland'),
(189, 16, 'vest_agder', 'Southern Norway/Vest-Agder'),
(190, 16, 'aust_agder', 'Southern Norway/Aust-Agder'),
(191, 16, 'telemark', 'Eastern Norway/Telemark'),
(192, 16, 'buskerud', 'Eastern Norway/Buskerud'),
(193, 16, 'hedmark', 'Eastern Norway/Hedmark'),
(194, 16, 'oppland', 'Eastern Norway/Oppland'),
(195, 16, 'akershus', 'Eastern Norway/Akershus'),
(196, 16, 'oslo', 'Eastern Norway/Oslo'),
(197, 16, 'vestfold', 'Eastern Norway/Vestfold'),
(198, 16, 'østfold', 'Eastern Norway/Østfold'),
(199, 17, 'england_avon', 'England/Avon'),
(200, 17, 'england_bedfordshire', 'England/Bedfordshire'),
(201, 17, 'england_berkshire', 'England/Berkshire'),
(202, 17, 'england_buckinghamshire', 'England/Buckinghamshire'),
(203, 17, 'england_cambridgeshire', 'England/Cambridgeshire'),
(204, 17, 'england_cheshire', 'England/Cheshire'),
(205, 17, 'england_cleveland', 'England/Cleveland'),
(206, 17, 'england_cornwall', 'England/Cornwall'),
(207, 17, 'england_cumbria', 'England/Cumbria'),
(208, 17, 'england_derbyshire', 'England/Derbyshire'),
(209, 17, 'england_devon', 'England/Devon'),
(210, 17, 'england_dorset', 'England/Dorset'),
(211, 17, 'england_durham', 'England/Durham'),
(212, 17, 'england_east_sussex', 'England/East Sussex'),
(213, 17, 'england_essex', 'England/Essex'),
(214, 17, 'england_gloucestershire', 'England/Gloucestershire'),
(215, 17, 'england_hampshire', 'England/Hampshire'),
(216, 17, 'england_herefordshire', 'England/Herefordshire'),
(217, 17, 'england_hertfordshire', 'England/Hertfordshire'),
(218, 17, 'england_isle_of_wight', 'England/Isle of Wight'),
(219, 17, 'england_kent', 'England/Kent'),
(220, 17, 'england_lancashire', 'England/Lancashire'),
(221, 17, 'england_leicestershire', 'England/Leicestershire'),
(222, 17, 'england_lincolnshire', 'England/Lincolnshire'),
(223, 17, 'england_london', 'England/London'),
(224, 17, 'england_merseyside', 'England/Merseyside'),
(225, 17, 'england_middlesex', 'England/Middlesex'),
(226, 17, 'england_norfolk', 'England/Norfolk'),
(227, 17, 'england_northamptonshire', 'England/Northamptonshire'),
(228, 17, 'england_northumberland', 'England/Northumberland'),
(229, 17, 'england_north_humberside', 'England/North Humberside'),
(230, 17, 'england_north_yorkshire', 'England/North Yorkshire'),
(231, 17, 'england_nottinghamshire', 'England/Nottinghamshire'),
(232, 17, 'england_oxfordshire', 'England/Oxfordshire'),
(233, 17, 'england_rutland', 'England/Rutland'),
(234, 17, 'england_shropshire', 'England/Shropshire'),
(235, 17, 'england_somerset', 'England/Somerset'),
(236, 17, 'england_south_humberside', 'England/South Humberside'),
(237, 17, 'england_south_yorkshire', 'England/South Yorkshire'),
(238, 17, 'england_staffordshire', 'England/Staffordshire'),
(239, 17, 'england_suffolk', 'England/Suffolk'),
(240, 17, 'england_surrey', 'England/Surrey'),
(241, 17, 'england_tyne_and_wear', 'England/Tyne and Wear'),
(242, 17, 'england_warwickshire', 'England/Warwickshire'),
(243, 17, 'england_west_midlands', 'England/West Midlands'),
(244, 17, 'england_west_sussex', 'England/West Sussex'),
(245, 17, 'england_west_yorkshire', 'England/West Yorkshire'),
(246, 17, 'england_wiltshire', 'England/Wiltshire'),
(247, 17, 'england_worcestershire', 'England/Worcestershire'),
(248, 17, 'wales_clwyd', 'Wales/Clwyd'),
(249, 17, 'wales_dyfed', 'Wales/Dyfed'),
(250, 17, 'wales_gwent', 'Wales/Gwent'),
(251, 17, 'wales_gwynedd', 'Wales/Gwynedd'),
(252, 17, 'wales_mid_glamorgan', 'Wales/Mid Glamorgan'),
(253, 17, 'wales_powys', 'Wales/Powys'),
(254, 17, 'wales_south_glamorgan', 'Wales/South Glamorgan'),
(255, 17, 'wales_west_glamorgan', 'Wales/West Glamorgan'),
(256, 17, 'scotland_aberdeenshire', 'Scotland/Aberdeenshire'),
(257, 17, 'scotland_angus', 'Scotland/Angus'),
(258, 17, 'scotland_argyll', 'Scotland/Argyll'),
(259, 17, 'scotland_ayrshire', 'Scotland/Ayrshire'),
(260, 17, 'scotland_banffshire', 'Scotland/Banffshire'),
(261, 17, 'scotland_berwickshire', 'Scotland/Berwickshire'),
(262, 17, 'scotland_bute', 'Scotland/Bute'),
(263, 17, 'scotland_caithness', 'Scotland/Caithness'),
(264, 17, 'scotland_clackmannanshire', 'Scotland/Clackmannanshire'),
(265, 17, 'scotland_dumfriesshire', 'Scotland/Dumfriesshire'),
(266, 17, 'scotland_dunbartonshire', 'Scotland/Dunbartonshire'),
(267, 17, 'scotland_east_lothian', 'Scotland/East Lothian'),
(268, 17, 'scotland_fife', 'Scotland/Fife'),
(269, 17, 'scotland_inverness-shire', 'Scotland/Inverness-shire'),
(270, 17, 'scotland_kincardineshire', 'Scotland/Kincardineshire'),
(271, 17, 'scotland_kinross-shire', 'Scotland/Kinross-shire'),
(272, 17, 'scotland_kirkcudbrightshire', 'Scotland/Kirkcudbrightshire'),
(273, 17, 'scotland_lanarkshire', 'Scotland/Lanarkshire'),
(274, 17, 'scotland_midlothian', 'Scotland/Midlothian'),
(275, 17, 'scotland_moray', 'Scotland/Moray'),
(276, 17, 'scotland_nairnshire', 'Scotland/Nairnshire'),
(277, 17, 'scotland_orkney', 'Scotland/Orkney'),
(278, 17, 'scotland_peeblesshire', 'Scotland/Peeblesshire'),
(279, 17, 'scotland_perthshire', 'Scotland/Perthshire'),
(280, 17, 'scotland_renfrewshire', 'Scotland/Renfrewshire'),
(281, 17, 'scotland_ross-shire', 'Scotland/Ross-shire'),
(282, 17, 'scotland_roxburghshire', 'Scotland/Roxburghshire'),
(283, 17, 'scotland_selkirkshire', 'Scotland/Selkirkshire'),
(284, 17, 'scotland_shetland', 'Scotland/Shetland'),
(285, 17, 'scotland_stirlingshire', 'Scotland/Stirlingshire'),
(286, 17, 'scotland_sutherland', 'Scotland/Sutherland'),
(287, 17, 'scotland_west_lothian', 'Scotland/West Lothian'),
(288, 17, 'scotland_wigtownshire', 'Scotland/Wigtownshire'),
(289, 17, 'northern_ireland_antrim', 'Northern Ireland/Antrim'),
(290, 17, 'northern_ireland_armagh', 'Northern Ireland/Armagh'),
(291, 17, 'northern_ireland_down', 'Northern Ireland/Down'),
(292, 17, 'northern_ireland_fermanagh', 'Northern Ireland/Fermanagh'),
(293, 17, 'northern_ireland_londonderry', 'Northern Ireland/Londonderry'),
(294, 17, 'northern_ireland_tyrone', 'Northern Ireland/Tyrone'),
(295, 19, 'northland', 'Northland'),
(296, 19, 'auckland', 'Auckland'),
(297, 19, 'waikato', 'Waikato'),
(298, 19, 'bay_of_plenty', 'Bay of Plenty'),
(299, 19, 'east_cape', 'East Cape'),
(300, 19, 'hawkes_bay', 'Hawke''s Bay'),
(301, 19, 'taranaki', 'Taranaki'),
(302, 19, 'manawatu-wanganui', 'Manawatu-Wanganui'),
(303, 19, 'wellington', 'Wellington'),
(304, 19, 'tasman', 'Tasman'),
(305, 19, 'nelson', 'Nelson'),
(306, 19, 'marlborough', 'Marlborough'),
(307, 19, 'west_coast', 'West Coast'),
(308, 19, 'canterbury', 'Canterbury'),
(309, 19, 'otago', 'Otago'),
(310, 19, 'southland', 'Southland'),
(311, 20, 'amur', 'Amur'),
(312, 20, 'arkhangelsk', 'Arkhangelsk'),
(313, 20, 'astrakhan', 'Astrakhan'),
(314, 20, 'belgorod', 'Belgorod'),
(315, 20, 'bryansk', 'Bryansk'),
(316, 20, 'chelyabinsk', 'Chelyabinsk'),
(317, 20, 'chita', 'Chita'),
(318, 20, 'irkutsk', 'Irkutsk'),
(319, 20, 'ivanovo', 'Ivanovo'),
(320, 20, 'kaliningrad', 'Kaliningrad'),
(321, 20, 'kaluga', 'Kaluga'),
(322, 20, 'kemerovo', 'Kemerovo'),
(323, 20, 'kirov', 'Kirov'),
(324, 20, 'kostroma', 'Kostroma'),
(325, 20, 'kurgan', 'Kurgan'),
(326, 20, 'kursk', 'Kursk'),
(327, 20, 'leningrad', 'Leningrad'),
(328, 20, 'lipetsk', 'Lipetsk'),
(329, 20, 'magadan', 'Magadan'),
(330, 20, 'moscow', 'Moscow'),
(331, 20, 'murmansk', 'Murmansk'),
(332, 20, 'nizhny_novgorod', 'Nizhny Novgorod'),
(333, 20, 'novgorod', 'Novgorod'),
(334, 20, 'novosibirsk', 'Novosibirsk'),
(335, 20, 'omsk', 'Omsk'),
(336, 20, 'orenburg', 'Orenburg'),
(337, 20, 'oryol', 'Oryol'),
(338, 20, 'penza', 'Penza'),
(339, 20, 'pskov', 'Pskov'),
(340, 20, 'rostov', 'Rostov'),
(341, 20, 'ryazan', 'Ryazan'),
(342, 20, 'sakhalin', 'Sakhalin'),
(343, 20, 'samara', 'Samara'),
(344, 20, 'saratov', 'Saratov'),
(345, 20, 'smolensk', 'Smolensk'),
(346, 20, 'sverdlovsk', 'Sverdlovsk'),
(347, 20, 'tambov', 'Tambov'),
(348, 20, 'tomsk', 'Tomsk'),
(349, 20, 'tver', 'Tver'),
(350, 20, 'tula', 'Tula'),
(351, 20, 'tyumen', 'Tyumen'),
(352, 20, 'ulyanovsk', 'Ulyanovsk'),
(353, 20, 'vladimir', 'Vladimir'),
(354, 20, 'volgograd', 'Volgograd'),
(355, 20, 'vologda', 'Vologda'),
(356, 20, 'voronezh', 'Voronezh'),
(357, 20, 'yaroslavl', 'Yaroslavl'),
(358, 21, 'lisbon', 'Lisbon'),
(359, 21, 'leiria', 'Leiria'),
(360, 21, 'santarém', 'Santarém'),
(361, 21, 'setúbal', 'Setúbal'),
(362, 21, 'beja', 'Beja'),
(363, 21, 'faro', 'Faro'),
(364, 21, 'évora', 'Évora'),
(365, 21, 'portalegre', 'Portalegre'),
(366, 21, 'castelo_branco', 'Castelo Branco'),
(367, 21, 'guarda', 'Guarda'),
(368, 21, 'coimbra', 'Coimbra'),
(369, 21, 'aveiro', 'Aveiro'),
(370, 21, 'viseu', 'Viseu'),
(371, 21, 'bragança', 'Bragança'),
(372, 21, 'vila_real', 'Vila Real'),
(373, 21, 'porto', 'Porto'),
(374, 21, 'braga', 'Braga'),
(375, 21, 'viana_do_castelo', 'Viana do Castelo'),
(376, 22, 'drenthe', 'Drenthe'),
(377, 22, 'flevoland', 'Flevoland'),
(378, 22, 'frisia', 'Frisia'),
(379, 22, 'gelderland', 'Gelderland'),
(380, 22, 'groningen', 'Groningen'),
(381, 22, 'limburg', 'Limburg'),
(382, 22, 'north brabant', 'North Brabant'),
(383, 22, 'north holland', 'North Holland'),
(384, 22, 'overijssel', 'Overijssel'),
(385, 22, 'utrecht', 'Utrecht'),
(386, 22, 'zealand', 'Zealand'),
(387, 22, 'south holland', 'South Holland'),
(388, 51, 'AC', 'Acre'),
(389, 51, 'AL', 'Alagoas'),
(390, 51, 'AP', 'Amapá'),
(391, 51, 'AM', 'Amazonas'),
(392, 51, 'BA', 'Bahia'),
(393, 51, 'CE', 'Ceará'),
(394, 51, 'DF', 'Distrito Federal'),
(395, 51, 'ES', 'Espírito Santo'),
(396, 51, 'GO', 'Goiás'),
(397, 51, 'MA', 'Maranhão'),
(398, 51, 'MT', 'Mato Grosso'),
(399, 51, 'MS', 'Mato Grosso do Sul'),
(400, 51, 'MG', 'Minas Gerais'),
(401, 51, 'PA', 'Pará'),
(402, 51, 'PB', 'Paraíba'),
(403, 51, 'PR', 'Paraná'),
(404, 51, 'PE', 'Pernambuco'),
(405, 51, 'PI', 'Piauí'),
(406, 51, 'RJ', 'Rio de Janeiro'),
(407, 51, 'RN', 'Rio Grande do Norte'),
(408, 51, 'RS', 'Rio Grande do Sul'),
(409, 51, 'RO', 'Rondônia'),
(410, 51, 'RR', 'Roraima'),
(411, 51, 'SC', 'Santa Catarina'),
(412, 51, 'SP', 'São Paulo'),
(413, 51, 'SE', 'Sergipe'),
(414, 51, 'TO', 'Tocantins'),
(415, 17, 'england_greater_manchester', 'England/Greater Manchester'),
(416, 15, 'CAPITAL', 'Capital'),
(417, 15, 'CENTRAL', 'Central Denmark'),
(418, 15, 'NORTH-DENMARK', 'North Denmark '),
(419, 15, 'ZEALAND', 'Zealand'),
(420, 15, 'SOUTHERN-DENMARK', 'Southern Denmark');

-- --------------------------------------------------------

--
-- Table structure for table `notify_providers`
--

CREATE TABLE IF NOT EXISTS `notify_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `class_name` varchar(100) DEFAULT NULL,
  `config_data` text,
  `is_enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `class_name` (`class_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `notify_providers`
--

INSERT INTO `notify_providers` (`id`, `code`, `class_name`, `config_data`, `is_enabled`) VALUES
(1, 'user_emailer', 'Notify_User_Email_Provider', 'q1prAU6nL/EGWlQMxKoLMTLY0S4N0MZfVfWy/r6IOA6bp5vEhcU0MX3ZEO8ZDSmKyxgmo7/WaSqL+8JI0eZpOydC+6MAZBl5sVXtF/BtSe5zR8qU1mALxXEI3aWZn6NQarjspQ/eB5qCvBw1vccLH4PNB4wweq7+YyLm7iS/IAe7e8XORAUSa016Z4HWmKOLKjijW3SQb1bcD/ZZXACh5QBYaH8hrg8UIBnPj1zWi9kgsBk1os9Ic4q854ZCGJoUw3DdvLbUDuKTkLBzflSmGTp5hF6u3LkpkRfyIJwkHEdCA1TW1DbpBonXVsQm61LujUM7Q+0FlO/BFbcgnTzFq84mCqoha/0R7nKwAUNriKD0+Bs6u69tGfFsCgGYV6LGe2AyStbszIbye9uPWPbJwHAKFNriB6NHbdWVj/UtU5lSKHdK8BFj1bz+/tgTgYCzRYa6UKjpp4W1K1yUs6uy1HySlvoz8oNWo1eb0aBqv9UIYOu+kZMb25BddldInPPtu2P6+Qa/TL9lePhWnDKLnIPUEU1iNrMFAEiV0dGW92FGP0fuGxfaFFsobUM4rFQh6st5Wn8vcS8QNYtmvI68CFab3iH+Du3LfnPQjUwUhc9/GEvrlRxabgzF8DhrrdGCiOHxWCCYP3mpMcmfwt61QOLnYZc4HuN/w30IEukopGY7Q0qYdenrtJPReMWiuwgUwXoodsivr26uxK1Lma6nVFIXGsOqCOGM6wxuRjtFWVqTLtuI3qzIOHtqxNvz4dUHMOFzmf29C9Y9/eDHs5pTktZkeJKTArBo4jFYCF37Y6e7SBUn8URu4Ng+pkoFEs9LiRrgJehtyx4OzLmdF4QtSDkF+qlpAOzU1dNJpXMQ/HmygWvb5Ao78F0P+a+38xKNxl3AzvfJdMz/oeOWKTUFvEDPN9QNcdRi9HQXTGC9NkW2XXP+pgyrKb7oC8b1+vUm0r51JqvZpe505DIUAEK/AigByMNPqdcxsmmHtf0+ysifDiYoRqq+B1f6pZ1n+90gUlzi5OdIlisUZdjxgQKzqBdFIZ22teAzxXUrkpg2g1bfjmIeydM+08fukSQlXEH1BPMzfIK61eOBHnKAonKn4VSZ3azTHIQThJxL1rfoPs1FbaGav1edQ7D4yruTMy5gBdI7Ird4Kbhs2wkFxjDnVhKl+SrJBpd+DfGf+Ff9giIJDwPD3a2l4ECKvdJZs1UD6htksu2t73hTa+xkh8t1Ab4z6fCUqpw0FFx0u68rkCfWknVmBuLUmFxAwGnU99lw', 1),
(2, 'admin_emailer', 'Notify_Admin_Email_Provider', 'MhzvhSo+wIgoS3YAsBUz9YMCDf9xgjiBocnLjxVslhGsFy1uBqSktGEgZHf0pMrUjWin7/XjNLegaTW6zAl734Nm67Fa0p8vpJ6TIoqqLVwPmdQtG7Hsuclg/4W6CJN9UAT9C3ynExHsjyUaTL88pAbF1o3S4J0rDsvlLLwuLj737cvxqF95HAsP3phL9oULUOaA6bBUKA/TDwbUV+tRR00bN+g1S5FDDwKdgRt/skcTPsoFjXQIYqngez1dRtFcM6/uw0cNDE+M5flCSn5fwRj7i4toXLHATKhNOW7ejjzgfO7J68kH13CnEPEaqko9NKMH9vLswnW4MXC5w3ex9HWCd8uDUqDOUWIijfw6qC7DZHtJToEEQOw1VFyi7TkAB/4TW2oAVsY0PUa1lWpai7eUSCHAZ998PH1uJ1SG8l/mgPc1NPWHR+6EU2yc9MkyFA7mPQtetVYNAK1Rpf5mbF8iUb+m2KYfkGE85sgdXaOjQKrINsBE/d1ksUZrJReuREN9vkxJ/UTzT5Z8DQvKZA22TbUGalpupJknVyyiFXX14Wc2IYo6ySlBi5xa6hePHTaMLaEZgkPyN7M0AJpZHxBxSDpdsspeBpoW5KKAiny0l0a41vJkmD2Si84op/m0CKfiDM5rI3piQCSFostn785foAh6uHRjQc757e15gMB4BbKDfpcxbIHYV6w/REqUtLa89smebEI37N6VB915lb5b1M30nEw1xG2qZ4vpw1x5vlrcSAG0b62QiGKvSQdGOMymDXmZ3ghh+rsqj9IfUEtNqUFr9m5MQWmaXt886GnFSx75TXGikXFGnuoO7KOsnytRQC5IniWHw8JCiKNm4xIB2u14MIeeVrhK0dLMF/bepw11ISGbtuqpcA+oVEeNRL8IrbaZx+m9+z10TfQS0yJBuFsUmcm9MF99Z06Z6ulszfqPaBCjNo31y2Ef609CTl9NzM0NvTW9KyoJpM51Q/RewSnZ9al2sNJz2SpdpSOigLMqWbFpg5W4bQdvEoUyEn8a4vHLkO3LotKlEhyfjECcP9qhnS5QHBrckRIHsRGSNMVceNdznyc/ngQSnbmpHCrQdX/5SAhur25C4olHQcI1jS+dBsWRkUBcMUE3U42S4mACMXCL/aktVKEZV6X5sGfvkRMaxiv53DvoEyWZUA9sge+l90vxJmBSevpCNI0SUSblmhLKhVtaRA6k6y8tfIAeqoLiiZWoS25cZmO2Ik4iIjis+qV1K5Q6JkvvP2D8joSj4NK9HWJwrBuNgJJncB2xlxvgCW+KkYUezYl/iMZlH4ZzD2ICavYxjnFao+h0sWduXMeBJRigtTU1FC+wFY0Xhatf137wrowgJjoah3+I6ExQjanRz3JX674+ymgrfjKXuHfWLG0Cy0cPIQurv1pXrVAz5QBnWbS5wrHzqbnWn/KoRwKMhuptXVKFdiFZMTLPzECTG7/4cLcrjaOfTLuaC6NXXwrS0VK4CZlDeavTXOZ774aKY/2DtfioyeOyAseiRiTxkz2UEz2p6M0ovvostDwqg0fLmjpBduuzEw6qs+MyJMb+981CkpOmU8dyeNfKMc3iKvgrJM8tnRZiWbQSkFMInKOp6w46KN4Kv1Nh/35NpJxdjJs9+6VpPT4LOSzV2S7VxfySBU233jnKGI1eKT0OTf9zwws5ge8RThE7TGv3ulWHTMOQgo2noXRlSgxV41LSAODb1nt6iqo6pxjyVuUyNwcOK8YnwuL/+Xf8khFIGf3nGG6OLQ+NHXskut18TrCkz70VBdjBfBBoZXCwEOKbOz7VNMmr8J2EMN60r9BQuL05HLMrRfShukGB/uRoIaNJq/p6IR41mJ6o7xVSEJz//QLbTGcL4fcUAgdCbknwcFFV71KapfYUxRx7x3PdU9ZwKIxpNreGdF8b+gHK40T8xkC+Vr5VR+y+WV+7e2aishnIMjsSeDzyrVtj8DaCssy8+fXHUrBI31dfGzFzsC2/CP6a+Lq5JjfVuQ==', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notify_templates`
--

CREATE TABLE IF NOT EXISTS `notify_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `class_name` varchar(100) DEFAULT NULL,
  `description` text,
  `is_enabled` tinyint(4) DEFAULT NULL,
  `config_data` text,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `class_name` (`class_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `notify_templates`
--

INSERT INTO `notify_templates` (`id`, `name`, `code`, `class_name`, `description`, `is_enabled`, `config_data`) VALUES
(1, 'User Registration Confirmation', 'user:register_confirm', 'User_Register_Confirm_Template', 'Sent after a user successfully registers.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(2, 'New Request', 'service:new_request', 'Service_New_Request_Template', 'Sent to a user when they submit a new service request.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(3, 'New Customer Invoice', 'payment:new_invoice', 'Payment_New_Invoice_Template', 'Sent to a user when an invoice is created for them.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(4, 'New Blog Comment', 'blog:new_comment', 'Blog_New_Comment_Template', 'Sent to visitors who subscribe to a blog post comments.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(5, 'Admin User Invitation', 'admin:user_invite', 'Admin_User_Invite_Template', 'Sent to administrators when their account is first created.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(6, 'Admin Password Reset', 'admin:password_reset', 'Admin_Password_Reset_Template', 'Sent to administrators when they reset their own password.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(7, 'Blog Comment Alert', 'blog:comment_alert', 'Blog_Comment_Alert_Template', 'Sent to visitors who subscribe to a blog post comments.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(8, 'New Customer Booking', 'bluebell:new_customer_booking', 'Bluebell_New_Customer_Booking_Template', 'Sent to a customer when their request has been booked.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(9, 'New Provider Booking', 'bluebell:new_provider_booking', 'Bluebell_New_Provider_Booking_Template', 'Sent to a provider when a job has been booked.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(10, 'Remind Provider Booking', 'bluebell:remind_provider_booking', 'Bluebell_Remind_Provider_Booking_Template', 'Sent to a provider 24 hours before booking time.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(11, 'Cancel Provider Booking', 'bluebell:cancel_provider_booking', 'Bluebell_Cancel_Provider_Booking_Template', 'Sent to the provider if a booking has been cancelled.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(12, 'Quote time suggestion', 'bluebell:suggest_time', 'Bluebell_Suggest_Time_Template', 'Sent to other user if a different appointment time is suggested.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(13, 'Cancel Customer Booking', 'bluebell:cancel_customer_booking', 'Bluebell_Cancel_Customer_Booking_Template', 'Sent to the customer if a booking has been cancelled.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(14, 'Remind Provider Booking', 'bluebell:remind_customer_booking', 'Bluebell_Remind_Customer_Booking_Template', 'Sent to a customer 24 hours before booking time.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(15, 'Testimonial Complete', 'service:testimonial_complete', 'Service_Testimonial_Complete_Template', 'Message template used when a person has provided a testimonial for a provider.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(16, 'Provider Alert', 'service:job_alert', 'Service_Job_Alert_Template', 'Notification sent to a provider when a request matches their skills.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(17, 'Update Request Status', 'service:request_status', 'Service_Request_Status_Template', 'Sent to a user when their service request is marked as complete.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(18, 'New Question', 'service:new_question', 'Service_New_Question_Template', 'Sent to a user when a provider asks a question about their request.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(19, 'New Answer to Other Question', 'service:new_answer_other', 'Service_New_Answer_Other_Template', 'Sent to a provider when a customer answers another provider''s question.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(20, 'Suggest a Category', 'service:suggest_category', 'Service_Suggest_Category_Template', 'When a user suggests a category.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(21, 'New Answer to Question', 'service:new_answer', 'Service_New_Answer_Template', 'Sent to a provider when their question has been answered.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(22, 'Remind Request', 'service:remind_request', 'Service_Remind_Request_Template', 'Sent to a user 24 hours before their request closes.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(23, 'New Rating', 'service:new_rating', 'Service_New_Rating_Template', 'Sent to a user when a rating has been submitted about them.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(24, 'Provider Won a Job', 'service:job_won', 'Service_Job_Won_Template', 'Notification sent to a provider when they have won a request.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(25, 'Provider Invitation', 'service:job_invite', 'Service_Job_Invite_Template', 'Notification sent to a provider when a user invites them to their request.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(26, 'New Quote', 'service:new_quote', 'Service_New_Quote_Template', 'Sent to a user when a provider submits a price quote.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(27, 'Provider Lost a Job', 'service:job_lost', 'Service_Job_Lost_Template', 'Notification sent to a provider when they have lost a request.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(28, 'Testimonial Request', 'service:testimonial_ask', 'Service_Testimonial_Ask_Template', 'Message template used when requesting a testimonial.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(29, 'Expired Request', 'service:expire_request', 'Service_Expire_Request_Template', 'Sent to a user when their request expires.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(30, 'Confirm User Provider Linkage', 'social:confirm_provider', 'Social_Confirm_Provider_Template', 'An email confirmation sent when a customer associats a login provider such as Twitter with their existing account.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(31, 'Reset User Password', 'user:password_reset', 'User_Password_Reset_Template', 'Notifies the user when they reset their password.', 1, '<?xml version="1.0"?>\n<data/>\n'),
(32, 'Message Received', 'user:new_message', 'User_New_Message_Template', 'Notifies the user when they receive a private message.', 1, '<?xml version="1.0"?>\n<data/>\n');

-- --------------------------------------------------------

--
-- Table structure for table `payment_escrow`
--

CREATE TABLE IF NOT EXISTS `payment_escrow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `request_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `total_price` decimal(15,2) DEFAULT '0.00',
  `commission` decimal(15,2) DEFAULT '0.00',
  `funded_at` datetime DEFAULT NULL,
  `released_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `escrow_status` varchar(100) DEFAULT NULL,
  `is_requested` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `quote_id` (`quote_id`),
  KEY `request_id` (`request_id`),
  KEY `provider_id` (`provider_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_fees`
--

CREATE TABLE IF NOT EXISTS `payment_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `enabled` tinyint(4) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `event_class_name` varchar(100) DEFAULT NULL,
  `action_class_name` varchar(100) DEFAULT NULL,
  `config_data` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `payment_fees`
--

INSERT INTO `payment_fees` (`id`, `date_start`, `date_end`, `name`, `description`, `enabled`, `sort_order`, `event_class_name`, `action_class_name`, `config_data`) VALUES
(1, NULL, NULL, 'Membership Fee', NULL, 1, 1, 'Service_Provider_Update_Event', 'Service_Provider_Membership_Action', '<?xml version="1.0"?>\n<data><field><id>target_user</id><value><![CDATA[s:11:"active_user";]]></value></field></data>\n'),
(3, NULL, '2019-07-14', 'Final Value Fee', '10% Service Fee over Final Amount', 1, 3, 'Service_Quote_Status_Event', 'Service_Quote_Escrow_Action', '<?xml version="1.0"?>\n<data><field><id>status_id</id><value><![CDATA[s:1:"2";]]></value></field><field><id>service_percentage</id><value><![CDATA[s:2:"10";]]></value></field><field><id>target_user</id><value><![CDATA[s:8:"customer";]]></value></field><field><id>pay_page</id><value><![CDATA[s:2:"24";]]></value></field></data>\n');

-- --------------------------------------------------------

--
-- Table structure for table `payment_fees_user_groups`
--

CREATE TABLE IF NOT EXISTS `payment_fees_user_groups` (
  `payment_fee_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_fee_id`,`user_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_fee_promises`
--

CREATE TABLE IF NOT EXISTS `payment_fee_promises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `command_name` varchar(100) DEFAULT NULL,
  `param_data` text,
  `mode` varchar(40) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fee_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `invoice_item_id` int(11) DEFAULT NULL,
  `hash` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `fee_id` (`fee_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `invoice_item_id` (`invoice_item_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_invoices`
--

CREATE TABLE IF NOT EXISTS `payment_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_ip` varchar(15) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `billing_first_name` varchar(100) DEFAULT NULL,
  `billing_last_name` varchar(100) DEFAULT NULL,
  `billing_email` varchar(50) DEFAULT NULL,
  `billing_phone` varchar(100) DEFAULT NULL,
  `billing_company` varchar(100) DEFAULT NULL,
  `billing_street_addr` varchar(255) DEFAULT NULL,
  `billing_city` varchar(100) DEFAULT NULL,
  `billing_zip` varchar(20) DEFAULT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_country_id` int(11) DEFAULT NULL,
  `total` decimal(15,2) DEFAULT '0.00',
  `subtotal` decimal(15,2) DEFAULT '0.00',
  `discount` int(11) DEFAULT '0',
  `tax` decimal(15,2) DEFAULT '0.00',
  `tax_discount` decimal(15,2) DEFAULT '0.00',
  `tax_exempt` tinyint(4) DEFAULT NULL,
  `tax_data` text,
  `payment_type_id` int(11) DEFAULT NULL,
  `payment_processed` datetime DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `status_updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `due_at` datetime DEFAULT NULL,
  `hash` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `payment_type_id` (`payment_type_id`),
  KEY `status_id` (`status_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_invoice_items`
--

CREATE TABLE IF NOT EXISTS `payment_invoice_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT '0.00',
  `total` decimal(15,2) DEFAULT '0.00',
  `discount` int(11) DEFAULT '0',
  `subtotal` decimal(15,2) DEFAULT '0.00',
  `tax_exempt` tinyint(4) DEFAULT NULL,
  `tax` decimal(15,2) DEFAULT '0.00',
  `tax_discount` decimal(15,2) DEFAULT '0.00',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_invoice_log`
--

CREATE TABLE IF NOT EXISTS `payment_invoice_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `comment` text,
  `created_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `status_id` (`status_id`),
  KEY `created_user_id` (`created_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_invoice_statuses`
--

CREATE TABLE IF NOT EXISTS `payment_invoice_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `notify_user` tinyint(4) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `notify_recipient` tinyint(4) DEFAULT NULL,
  `user_message_template_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `payment_invoice_statuses`
--

INSERT INTO `payment_invoice_statuses` (`id`, `code`, `name`, `notify_user`, `enabled`, `notify_recipient`, `user_message_template_id`) VALUES
(1, 'new', 'Unpaid', NULL, NULL, NULL, 3),
(2, 'paid', 'Paid', 1, NULL, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `payment_taxes`
--

CREATE TABLE IF NOT EXISTS `payment_taxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `rates` mediumtext,
  `code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `payment_taxes`
--

INSERT INTO `payment_taxes` (`id`, `name`, `description`, `rates`, `code`) VALUES
(1, 'Default', 'Default tax class', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE IF NOT EXISTS `payment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `class_name` varchar(100) DEFAULT NULL,
  `is_enabled` tinyint(4) DEFAULT NULL,
  `is_default` tinyint(4) DEFAULT NULL,
  `config_data` text,
  `code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`id`, `name`, `description`, `class_name`, `is_enabled`, `is_default`, `config_data`, `code`) VALUES
(7, 'BrainTree', NULL, 'Payment_Braintree_Gateway', 1, 1, 'ILY50gtWmZFIScaHMTPQ7PeCmgCLi+nTw/3v/cMai7IxHlr/2n/CnfjXNwkVrPCAHVzFKp+VnDuP14wiEabtkdbOok2ukaX0gkWwZHKk2WfDu6QL4MA18PUAqKms7nivdL7Wji9KeFq7IvFV1YOwPodrGRyV1WIo3++zdqnvfowpvXhxpGpeSVi4PI39fsO12S9FFqpeRjc5e0OOPDVeA8RsFVKXUTI+Pqjy1E6ywlrL5OhIvn3moC9mV4xm13gfkJCLWWcAu2E1AUitUySC9Jl6GVq+94wzRoiIx870RQSUhwlkVwkOsHASatodMo9iVTUT+CvwPdc4r+Hj1DTHhc+CKeUWNm6LurJdYn+01falY+f31HWv5x0PSqky0HXCUJ3ECZiA4c4owLk6J/8rGczR9iwzBNjAJM0/BFmRF6NlwoL6dKSL9xzXTAk8kOZgofUyxjt+5/oCNBtWaU0M8e3zELN6KysXb8icX3DLa4Y6aurGyiPkAnaeSNgO8tkb6C/BAfei9vei3s+FCBOktu4Ynnb4XvSeoxu0itW8lbmYdOr2MrxKHyiXekKz2ewabmGUjg6Cl/Lwq+bKiwXgMaf+Gr1dUZ1QMw9GyEKXpdvKesi8dVuuT7K12J+CewQjPRpygMGVFW8EinnRVaMya6fnX2tOCH68anp4ONlur4cjOgA4Bqt30/B78btnThMMOh33k34IO37Xw2/4clNT9c9DrZjwrln1HEraOKXCYoX63+0WredUU7IgAgNfTVd44nlzWRNdy33b4zGZXpMHJHB3V/fvXuyPNGhwrLO+GJE=', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type_countries`
--

CREATE TABLE IF NOT EXISTS `payment_type_countries` (
  `payment_type_id` int(11) NOT NULL,
  `location_country_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_type_id`,`location_country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_type_countries`
--

INSERT INTO `payment_type_countries` (`payment_type_id`, `location_country_id`) VALUES
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type_log`
--

CREATE TABLE IF NOT EXISTS `payment_type_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `payment_type_name` varchar(255) DEFAULT NULL,
  `is_success` tinyint(4) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `raw_response` text,
  `request_data` text,
  `response_data` text,
  `ccv_response_code` varchar(20) DEFAULT NULL,
  `ccv_response_text` varchar(255) DEFAULT NULL,
  `avs_response_code` varchar(20) DEFAULT NULL,
  `avs_response_text` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `created_at` (`created_at`),
  KEY `created_user_id` (`created_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpr_cron_jobs`
--

CREATE TABLE IF NOT EXISTS `phpr_cron_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handler_name` varchar(100) DEFAULT NULL,
  `param_data` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `phpr_cron_jobs`
--

INSERT INTO `phpr_cron_jobs` (`id`, `handler_name`, `param_data`, `created_at`) VALUES
(1, 'Service_Request::geocode_address', 'a:1:{i:0;i:1;}', '2014-07-25 18:05:29'),
(2, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"1";}', '2014-07-25 18:05:29'),
(3, 'Service_Provider::geocode_address', 'a:1:{i:0;i:1;}', '2014-07-26 05:35:21'),
(4, 'Service_Request::geocode_address', 'a:1:{i:0;i:2;}', '2014-07-27 20:12:43'),
(5, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"2";}', '2014-07-27 20:12:43'),
(6, 'Service_Provider::geocode_address', 'a:1:{i:0;i:2;}', '2014-08-04 19:56:27'),
(7, 'Service_Provider::geocode_address', 'a:1:{i:0;i:3;}', '2014-08-04 20:39:48'),
(8, 'Service_Provider::geocode_address', 'a:1:{i:0;i:4;}', '2014-08-04 20:44:44'),
(9, 'Service_Provider::geocode_address', 'a:1:{i:0;i:5;}', '2014-08-04 20:49:08'),
(10, 'Service_Provider::geocode_address', 'a:1:{i:0;i:6;}', '2014-08-04 20:59:32'),
(11, 'Service_Provider::geocode_address', 'a:1:{i:0;i:7;}', '2014-08-04 21:03:28'),
(12, 'Service_Provider::geocode_address', 'a:1:{i:0;i:8;}', '2014-08-04 21:22:56'),
(13, 'Service_Provider::geocode_address', 'a:1:{i:0;i:9;}', '2014-08-04 21:27:54'),
(14, 'Service_Provider::geocode_address', 'a:1:{i:0;i:10;}', '2014-08-04 21:29:14'),
(15, 'Service_Provider::geocode_address', 'a:1:{i:0;i:11;}', '2014-08-04 21:34:38'),
(16, 'Service_Provider::geocode_address', 'a:1:{i:0;i:12;}', '2014-08-05 07:33:02'),
(17, 'Service_Provider::geocode_address', 'a:1:{i:0;i:13;}', '2014-08-10 11:57:53'),
(18, 'Service_Provider::geocode_address', 'a:1:{i:0;i:14;}', '2014-08-11 15:08:32'),
(19, 'Service_Provider::geocode_address', 'a:1:{i:0;i:15;}', '2014-08-13 16:23:55'),
(20, 'Service_Request::geocode_address', 'a:1:{i:0;i:3;}', '2014-08-15 13:23:16'),
(21, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"3";}', '2014-08-15 13:23:16'),
(22, 'Service_Request::geocode_address', 'a:1:{i:0;i:4;}', '2014-08-15 13:24:17'),
(23, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"4";}', '2014-08-15 13:24:18'),
(24, 'Service_Provider::geocode_address', 'a:1:{i:0;i:16;}', '2014-08-15 14:12:36'),
(25, 'Service_Request::geocode_address', 'a:1:{i:0;i:5;}', '2014-08-16 20:43:16'),
(26, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"5";}', '2014-08-16 20:43:16'),
(27, 'Service_Request::geocode_address', 'a:1:{i:0;i:6;}', '2014-08-16 21:12:58'),
(28, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"6";}', '2014-08-16 21:12:59'),
(29, 'Service_Request::geocode_address', 'a:1:{i:0;i:7;}', '2014-08-16 21:13:11'),
(30, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"7";}', '2014-08-16 21:13:11'),
(31, 'Service_Request::geocode_address', 'a:1:{i:0;i:8;}', '2014-08-17 01:49:51'),
(32, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"8";}', '2014-08-17 01:49:51'),
(33, 'Service_Provider::geocode_address', 'a:1:{i:0;i:17;}', '2014-08-17 01:59:45'),
(34, 'Service_Request::geocode_address', 'a:1:{i:0;i:9;}', '2014-08-17 02:35:05'),
(35, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:1:"9";}', '2014-08-17 02:35:05'),
(36, 'Service_Request::geocode_address', 'a:1:{i:0;i:10;}', '2014-08-17 02:35:41'),
(37, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"10";}', '2014-08-17 02:35:41'),
(38, 'Service_Provider::geocode_address', 'a:1:{i:0;i:18;}', '2014-08-17 13:32:38'),
(39, 'Service_Provider::geocode_address', 'a:1:{i:0;i:19;}', '2014-08-17 13:51:02'),
(40, 'Service_Provider::geocode_address', 'a:1:{i:0;s:2:"18";}', '2014-08-17 14:38:46'),
(41, 'Service_Provider::geocode_address', 'a:1:{i:0;i:20;}', '2014-08-19 13:38:37'),
(42, 'Service_Request::geocode_address', 'a:1:{i:0;i:11;}', '2014-08-19 13:44:24'),
(43, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"11";}', '2014-08-19 13:44:24'),
(44, 'Service_Provider::geocode_address', 'a:1:{i:0;s:2:"20";}', '2014-08-19 15:15:42'),
(45, 'Service_Provider::geocode_address', 'a:1:{i:0;s:2:"20";}', '2014-08-19 15:16:11'),
(46, 'Service_Request::geocode_address', 'a:1:{i:0;i:12;}', '2014-08-19 16:19:21'),
(47, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"12";}', '2014-08-19 16:19:23'),
(48, 'Service_Provider::geocode_address', 'a:1:{i:0;i:21;}', '2014-08-19 16:50:44'),
(49, 'Service_Request::geocode_address', 'a:1:{i:0;s:2:"12";}', '2014-08-19 17:15:58'),
(50, 'Service_Request::geocode_address', 'a:1:{i:0;i:13;}', '2014-08-20 18:14:11'),
(51, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"13";}', '2014-08-20 18:14:13'),
(52, 'Service_Provider::geocode_address', 'a:1:{i:0;i:22;}', '2014-08-20 18:29:21'),
(53, 'Service_Provider::geocode_address', 'a:1:{i:0;i:23;}', '2014-08-20 18:49:04'),
(54, 'Service_Request::geocode_address', 'a:1:{i:0;i:14;}', '2014-08-20 23:54:16'),
(55, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"14";}', '2014-08-20 23:54:17'),
(56, 'Service_Request::geocode_address', 'a:1:{i:0;s:2:"13";}', '2014-08-22 10:26:21'),
(57, 'Service_Request::geocode_address', 'a:1:{i:0;s:1:"3";}', '2014-08-25 09:44:58'),
(58, 'Service_Request::geocode_address', 'a:1:{i:0;i:15;}', '2014-08-26 11:11:39'),
(59, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"15";}', '2014-08-26 11:11:40'),
(60, 'Service_Request::geocode_address', 'a:1:{i:0;i:16;}', '2014-08-26 11:21:32'),
(61, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"16";}', '2014-08-26 11:21:33'),
(62, 'Service_Request::geocode_address', 'a:1:{i:0;i:17;}', '2014-08-29 19:27:19'),
(63, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"17";}', '2014-08-29 19:27:20'),
(64, 'Service_Request::geocode_address', 'a:1:{i:0;i:18;}', '2014-08-31 18:42:39'),
(65, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"18";}', '2014-08-31 18:42:40'),
(66, 'Service_Request::geocode_address', 'a:1:{i:0;i:19;}', '2014-08-31 18:47:33'),
(67, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"19";}', '2014-08-31 18:47:33'),
(68, 'Service_Request::geocode_address', 'a:1:{i:0;i:20;}', '2014-09-01 17:20:55'),
(69, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"20";}', '2014-09-01 17:20:55'),
(70, 'Service_Request::geocode_address', 'a:1:{i:0;i:21;}', '2014-09-04 00:32:42'),
(71, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"21";}', '2014-09-04 00:32:43'),
(72, 'Service_Provider::geocode_address', 'a:1:{i:0;i:24;}', '2014-09-06 17:31:28'),
(73, 'Service_Request::geocode_address', 'a:1:{i:0;i:22;}', '2014-09-06 17:56:20'),
(74, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"22";}', '2014-09-06 17:56:21'),
(75, 'Service_Request::geocode_address', 'a:1:{i:0;s:2:"22";}', '2014-09-06 18:04:13'),
(76, 'Service_Request::geocode_address', 'a:1:{i:0;i:23;}', '2014-09-06 18:05:53'),
(77, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"23";}', '2014-09-06 18:05:54'),
(78, 'Service_Request::geocode_address', 'a:1:{i:0;i:24;}', '2014-09-06 18:07:43'),
(79, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"24";}', '2014-09-06 18:07:43'),
(80, 'Service_Request::geocode_address', 'a:1:{i:0;s:2:"23";}', '2014-09-06 18:13:54'),
(81, 'Service_Request::geocode_address', 'a:1:{i:0;s:2:"23";}', '2014-09-06 18:16:35'),
(82, 'Service_Request::geocode_address', 'a:1:{i:0;s:2:"23";}', '2014-09-06 18:16:43'),
(83, 'Service_Request::geocode_address', 'a:1:{i:0;i:25;}', '2014-09-06 21:56:46'),
(84, 'Bluebell_Request_Manager::notify_providers', 'a:1:{i:0;s:2:"25";}', '2014-09-06 21:56:47'),
(85, 'Service_Request::geocode_address', 'a:1:{i:0;s:2:"25";}', '2014-09-06 21:57:37');

-- --------------------------------------------------------

--
-- Table structure for table `phpr_cron_table`
--

CREATE TABLE IF NOT EXISTS `phpr_cron_table` (
  `record_code` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`record_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phpr_generic_binds`
--

CREATE TABLE IF NOT EXISTS `phpr_generic_binds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `primary_id` int(11) DEFAULT NULL,
  `secondary_id` int(11) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `class_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `primary_id` (`primary_id`),
  KEY `secondary_id` (`secondary_id`),
  KEY `field_name` (`field_name`),
  KEY `class_name` (`class_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpr_module_applied_updates`
--

CREATE TABLE IF NOT EXISTS `phpr_module_applied_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(255) DEFAULT NULL,
  `update_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `phpr_module_applied_updates`
--

INSERT INTO `phpr_module_applied_updates` (`id`, `module_id`, `update_id`, `created_at`) VALUES
(1, 'admin', '2', '2014-07-08 06:17:55'),
(2, 'cms', '2', '2014-07-08 06:17:55'),
(3, 'user', '2', '2014-07-08 06:17:55'),
(4, 'user', '3', '2014-07-08 06:17:55'),
(5, 'user', '8', '2014-07-08 06:17:55'),
(6, 'payment', '2', '2014-07-08 06:17:55'),
(7, 'payment', '3', '2014-07-08 06:17:55'),
(8, 'service', '2', '2014-07-08 06:17:55'),
(9, 'service', '3', '2014-07-08 06:17:55'),
(10, 'blog', '1', '2014-07-08 06:17:55'),
(11, 'blog', '2', '2014-07-08 06:17:55'),
(12, 'bluebell', '2', '2014-07-08 06:17:55'),
(13, 'location', '2', '2014-07-08 06:17:55'),
(14, 'notify', '2', '2014-07-08 06:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `phpr_module_params`
--

CREATE TABLE IF NOT EXISTS `phpr_module_params` (
  `module_id` varchar(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`module_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phpr_module_params`
--

INSERT INTO `phpr_module_params` (`module_id`, `name`, `value`) VALUES
('core', 'birthmark', 's:32:"A6823B8853337B41D097888A435D2230";'),
('core', 'license_name', 's:88:"ma5jAZGeJx1IMzpDeVIT2+c7jqs2rHonB1FScnrgXvt2qMCdlQPtNbWSAUQEhkK9A1MTN3VkkefTPVZ1BBYkvw==";'),
('core', 'license_key', 's:128:"LapmLB7P26OqoCpCQEWWpizsU6DfI8bGFh+9kY7s9GbgBTuAoj7YF6u5UIMbLU7INa7CKb1ijCA0JH56eO7r8xnoiNDo6XAeqjP1ulwiXji91XxStKohUayW9YiEDuBj";'),
('core', 'license_hash', 's:128:"FGA9MBuhWz75dyyhgc6YRs6XssAfgUZGKds9Zqur8UkZ2a+nXKUHaxGIw7UlHTWYvubWpqkXfRCfNKpcavW9jj6dRP5zfSHb2KUYjOnbmzYljefBM8z/90Ohxt5F3GSr";'),
('core', 'app_name', 's:8:"Bluebell";'),
('core', 'app_image', 's:4:"null";'),
('core', 'vendor_name', 's:13:"Scripts Ahoy!";'),
('core', 'vendor_url', 's:22:"http://scriptsahoy.com";'),
('admin', 'updates_available', 'i:0;');

-- --------------------------------------------------------

--
-- Table structure for table `phpr_module_update_history`
--

CREATE TABLE IF NOT EXISTS `phpr_module_update_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `module_id` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `description` text,
  `version_str` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=122 ;

--
-- Dumping data for table `phpr_module_update_history`
--

INSERT INTO `phpr_module_update_history` (`id`, `date`, `module_id`, `version`, `description`, `version_str`) VALUES
(1, '2014-07-08', 'db', 1, 'Add Active Record deferred relations binding feature', '1.0.1'),
(2, '2014-07-08', 'db', 2, 'Add user parameters table', '1.0.2'),
(3, '2014-07-08', 'db', 3, 'Add files table', '1.0.3'),
(4, '2014-07-08', 'db', 4, 'Add module parameters table', '1.0.4'),
(5, '2014-07-08', 'db', 5, 'Add trace log table', '1.0.5'),
(6, '2014-07-08', 'db', 6, 'Add field column to the db_files table', '1.0.6'),
(7, '2014-07-08', 'db', 7, 'Add version string feature', '1.0.7'),
(8, '2014-07-08', 'db', 8, 'Add record locking feature', '1.0.8'),
(9, '2014-07-08', 'db', 9, 'Add support for non-database objects to the locking feature', '1.0.9'),
(10, '2014-07-08', 'db', 10, 'Add lock pinging feature', '1.0.10'),
(11, '2014-07-08', 'db', 11, 'Add the public folder for the images', '1.0.11'),
(12, '2014-07-08', 'db', 12, 'Add the image sorting feature', '1.0.12'),
(13, '2014-07-08', 'db', 13, 'Improved session management features', '1.0.13'),
(14, '2014-07-08', 'db', 14, 'Minor improvements to security', '1.0.14'),
(15, '2014-07-08', 'db', 15, 'Add the title field to the file model', '1.0.15'),
(16, '2014-07-08', 'db', 16, 'Add extra database indexes', '1.0.16'),
(17, '2014-07-08', 'db', 17, 'Add the error details field to the trace log table', '1.0.17'),
(18, '2014-07-08', 'db', 18, 'Add table for cron worker', '1.0.18'),
(19, '2014-07-08', 'db', 19, 'Add table for model logging', '1.0.19'),
(20, '2014-07-08', 'core', 1, 'First version.', '1.0.1'),
(21, '2014-07-08', 'core', 2, 'Added better logic for generating URI codes, include Notification engine.', '1.0.2'),
(22, '2014-07-08', 'core', 3, 'Added common settings page with locale settings.', '1.0.3'),
(23, '2014-07-08', 'core', 4, 'Improvements to framework structure.', '1.0.4'),
(24, '2014-07-08', 'core', 5, 'Add centralized attachment behavior.', '1.0.5'),
(25, '2014-07-08', 'core', 6, 'Add generic binding table.', '1.0.6'),
(26, '2014-07-08', 'core', 7, 'Add site name to common settings.', '1.0.7'),
(27, '2014-07-08', 'core', 8, 'Add admin tray override to module base.', '1.0.8'),
(28, '2014-07-08', 'core', 9, 'Fix to cron table model.', '1.0.9'),
(29, '2014-07-08', 'core', 10, 'Bug fixes.', '1.0.10'),
(30, '2014-07-08', 'core', 11, 'Add Portuguese / Brazil locality setting.', '1.0.11'),
(31, '2014-07-08', 'admin', 1, 'Add user tables', '1.0.1'),
(32, '2014-07-08', 'admin', 2, 'Add greater control for permissions', '1.0.2'),
(33, '2014-07-08', 'admin', 3, 'Allow admin users to reset their preferences via My Settings', '1.0.3'),
(34, '2014-07-08', 'admin', 4, 'Add system tray and quick search', '1.0.4'),
(35, '2014-07-08', 'admin', 5, 'User Interface improvements', '1.0.5'),
(36, '2014-07-08', 'cms', 1, 'First version', '1.0.1'),
(37, '2014-07-08', 'cms', 2, 'Add page visitor statistics', '1.0.2'),
(38, '2014-07-08', 'cms', 3, 'Add SEO sitemap for pages, add block render types', '1.0.3'),
(39, '2014-07-08', 'cms', 4, 'Add Menu builder compatability', '1.0.4'),
(40, '2014-07-08', 'cms', 5, 'Fixes bug with long language strings', '1.0.5'),
(41, '2014-07-08', 'cms', 6, 'Minor security update', '1.0.6'),
(42, '2014-07-08', 'cms', 7, 'Allow partials to display when file is not present', '1.0.7'),
(43, '2014-07-08', 'cms', 8, 'Separate Content and Editor menu items', '1.0.8'),
(44, '2014-07-08', 'cms', 9, 'Implement module theme support', '1.0.9'),
(45, '2014-07-08', 'cms', 10, 'Fixes issue with more than 1 theme not showing any pages or partials', '1.0.10'),
(46, '2014-07-08', 'user', 1, 'First version', '1.0.1'),
(47, '2014-07-08', 'user', 2, 'Add user preferences', '1.0.2'),
(48, '2014-07-08', 'user', 3, 'Add user messaging', '1.0.3'),
(49, '2014-07-08', 'user', 4, 'Add user groups', '1.0.4'),
(50, '2014-07-08', 'user', 5, 'Improve messaging system', '1.0.5'),
(51, '2014-07-08', 'user', 6, 'Minor improvement', '1.0.6'),
(52, '2014-07-08', 'user', 7, 'Improve attachment handling', '1.0.7'),
(53, '2014-07-08', 'user', 8, 'Allow thread ID to be a string', '1.0.8'),
(54, '2014-07-08', 'user', 9, 'Usernames are no longer capitalized', '1.0.9'),
(55, '2014-07-08', 'user', 10, 'Fix bug where new users are flagged as guests', '1.0.10'),
(56, '2014-07-08', 'user', 11, 'Users should not be notified about their own message', '1.0.11'),
(57, '2014-07-08', 'payment', 1, 'First version of Payment Module.', '1.0.1'),
(58, '2014-07-08', 'payment', 2, 'Add invoice tables.', '1.0.2'),
(59, '2014-07-08', 'payment', 3, 'Add payment fee structures.', '1.0.3'),
(60, '2014-07-08', 'payment', 4, 'Add tax classes.', '1.0.4'),
(61, '2014-07-08', 'payment', 5, 'Implement user credit system.', '1.0.5'),
(62, '2014-07-08', 'payment', 6, 'Add logging for payment attempts.', '1.0.6'),
(63, '2014-07-08', 'payment', 7, 'Add support for multiple payment methods.', '1.0.7'),
(64, '2014-07-08', 'payment', 8, 'Add Moneybookers gateway.', '1.0.8'),
(65, '2014-07-08', 'payment', 9, 'Fixes to Fees Event and Action selection.', '1.0.9'),
(66, '2014-07-08', 'payment', 10, 'Refactor to meet latest framework changes.', '1.0.10'),
(67, '2014-07-08', 'payment', 11, 'Fixes bug with 2Checkout payment method.', '1.0.11'),
(68, '2014-07-08', 'payment', 12, 'Fixes bug with Skrill payment method.', '1.0.12'),
(69, '2014-07-08', 'service', 1, 'First version.', '1.0.1'),
(70, '2014-07-08', 'service', 2, 'Add statuses and quote tables.', '1.0.2'),
(71, '2014-07-08', 'service', 3, 'Add rating and question tables.', '1.0.3'),
(72, '2014-07-08', 'service', 4, 'Add URL name to Service Categories.', '1.0.4'),
(73, '2014-07-08', 'service', 5, 'Added privacy options to Requests.', '1.0.5'),
(74, '2014-07-08', 'service', 6, 'Improve UI on Service Requests, Providers.', '1.0.6'),
(75, '2014-07-08', 'service', 7, 'Add deferred geocoding for Providers.', '1.0.7'),
(76, '2014-07-08', 'service', 8, 'Improve attachment handling.', '1.0.8'),
(77, '2014-07-08', 'service', 9, 'Add skills.', '1.0.9'),
(78, '2014-07-08', 'service', 10, 'Add skills to requests.', '1.0.10'),
(79, '2014-07-08', 'service', 11, 'Update schema.', '1.0.11'),
(80, '2014-07-08', 'service', 12, 'Create new preview screen for Requests.', '1.0.12'),
(81, '2014-07-08', 'service', 13, 'Added Target user to Invoice Percentage of Quote fee action.', '1.0.13'),
(82, '2014-07-08', 'service', 14, 'Various bug fixes and code improvements.', '1.0.14'),
(83, '2014-07-08', 'service', 15, 'Automatically approve new ratings and quotes.', '1.0.15'),
(84, '2014-07-08', 'service', 16, 'Request expiry dates now include exact hours and minutes.', '1.0.16'),
(85, '2014-07-08', 'blog', 1, 'First version', '1.0.1'),
(86, '2014-07-08', 'blog', 2, 'Add comments and subscribers', '1.0.2'),
(87, '2014-07-08', 'blog', 3, 'Minor improvements to permissions and email notifications', '1.0.3'),
(88, '2014-07-08', 'blog', 4, 'Add category filter to blog post list', '1.0.4'),
(89, '2014-07-08', 'bluebell', 1, 'First version of Bluebell Module.', '1.0.1'),
(90, '2014-07-08', 'bluebell', 2, 'Extend service module.', '1.0.2'),
(91, '2014-07-08', 'bluebell', 3, 'Add email templates.', '1.0.3'),
(92, '2014-07-08', 'bluebell', 4, 'Include booking cancelled request status.', '1.0.4'),
(93, '2014-07-08', 'bluebell', 5, 'Various performance and functional improvements.', '1.0.5'),
(94, '2014-07-08', 'bluebell', 6, 'Improvements to location based lookups.', '1.0.6'),
(95, '2014-07-08', 'bluebell', 7, 'Numerous bug fixes and improvements.', '1.0.7'),
(96, '2014-07-08', 'bluebell', 8, 'Change match making logic to use geolocation instead of postal codes.', '1.0.8'),
(97, '2014-07-08', 'bluebell', 9, 'Providers can now use a single profile for multiple skills using related categories.', '1.0.9'),
(98, '2014-07-08', 'bluebell', 10, 'Winners and losers of jobs are now notified when a quote is accepted.', '1.0.10'),
(99, '2014-07-08', 'builder', 1, 'First version', '1.0.1'),
(100, '2014-07-08', 'builder', 2, 'Fix various bugs in menu builder', '1.0.2'),
(101, '2014-07-08', 'builder', 3, 'Complete rebuild of menu builder', '1.0.3'),
(102, '2014-07-08', 'location', 1, 'First version', '1.0.1'),
(103, '2014-07-08', 'location', 2, 'Improvements to postal code lookups', '1.0.2'),
(104, '2014-07-08', 'location', 3, 'Add map marker customisation', '1.0.3'),
(105, '2014-07-08', 'location', 4, 'Improve country/state drop down handling', '1.0.4'),
(106, '2014-07-08', 'location', 5, 'Fix to geolocation lookup from IP addresses', '1.0.5'),
(107, '2014-07-08', 'location', 6, 'Fix minor bug handling namespaces', '1.0.6'),
(108, '2014-07-08', 'notify', 1, 'First version.', '1.0.1'),
(109, '2014-07-08', 'notify', 2, 'Create standard drivers.', '1.0.2'),
(110, '2014-07-08', 'notify', 3, 'Add SMSGlobal notification provider.', '1.0.3'),
(111, '2014-07-08', 'notify', 4, 'Add SMS Matrix and Clickatell notification providers.', '1.0.4'),
(112, '2014-07-08', 'notify', 5, 'Minor bug fixes.', '1.0.5'),
(113, '2014-07-08', 'notify', 6, 'Add ability to disable notification templates individually.', '1.0.6'),
(114, '2014-07-08', 'social', 1, 'First version', '1.0.1'),
(115, '2014-07-08', 'social', 2, 'Add features for Twitter', '1.0.2'),
(116, '2014-07-08', 'social', 3, 'Add OAuth features', '1.0.3'),
(117, '2014-07-08', 'social', 4, 'Improve provider system for greater scalability', '1.0.4'),
(118, '2014-07-08', 'social', 5, 'Fix to Facebook authentication adapter', '1.0.5'),
(119, '2014-07-08', 'social', 6, 'Twitter now collects email address', '1.0.6'),
(120, '2014-07-08', 'profile', 1, 'First version', '1.0.1'),
(121, '2014-07-08', 'profile', 2, 'Add protocol detection for secure user avatars', '1.0.2');

-- --------------------------------------------------------

--
-- Table structure for table `phpr_module_versions`
--

CREATE TABLE IF NOT EXISTS `phpr_module_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `version_str` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `phpr_module_versions`
--

INSERT INTO `phpr_module_versions` (`id`, `module_id`, `version`, `date`, `version_str`) VALUES
(1, 'file', 0, '2014-07-08', NULL),
(2, 'db', NULL, '2014-07-08', '1.0.19'),
(3, 'phpr', 0, '2014-07-08', NULL),
(4, 'net', 0, '2014-07-08', NULL),
(5, 'core', NULL, '2014-07-08', '1.0.11'),
(6, 'admin', NULL, '2014-07-08', '1.0.5'),
(7, 'cms', NULL, '2014-07-08', '1.0.10'),
(8, 'user', NULL, '2014-07-08', '1.0.11'),
(9, 'payment', NULL, '2014-07-08', '1.0.12'),
(10, 'service', NULL, '2014-07-08', '1.0.16'),
(11, 'blog', NULL, '2014-07-08', '1.0.4'),
(12, 'bluebell', NULL, '2014-07-08', '1.0.10'),
(13, 'builder', NULL, '2014-07-08', '1.0.3'),
(14, 'location', NULL, '2014-07-08', '1.0.6'),
(15, 'notify', NULL, '2014-07-08', '1.0.6'),
(16, 'social', NULL, '2014-07-08', '1.0.6'),
(17, 'profile', NULL, '2014-07-08', '1.0.2'),
(18, 'file', 0, '2014-07-08', NULL),
(19, 'db', NULL, '2014-07-08', '1.0.19'),
(20, 'phpr', 0, '2014-07-08', NULL),
(21, 'net', 0, '2014-07-08', NULL),
(22, 'core', NULL, '2014-07-08', '1.0.11'),
(23, 'admin', NULL, '2014-07-08', '1.0.5'),
(24, 'cms', NULL, '2014-07-08', '1.0.10'),
(25, 'user', NULL, '2014-07-08', '1.0.11'),
(26, 'payment', NULL, '2014-07-08', '1.0.12'),
(27, 'service', NULL, '2014-07-08', '1.0.16'),
(28, 'blog', NULL, '2014-07-08', '1.0.4'),
(29, 'bluebell', NULL, '2014-07-08', '1.0.10'),
(30, 'builder', NULL, '2014-07-08', '1.0.3'),
(31, 'location', NULL, '2014-07-08', '1.0.6'),
(32, 'notify', NULL, '2014-07-08', '1.0.6'),
(33, 'social', NULL, '2014-07-08', '1.0.6'),
(34, 'profile', NULL, '2014-07-08', '1.0.2');

-- --------------------------------------------------------

--
-- Table structure for table `phpr_trace_log`
--

CREATE TABLE IF NOT EXISTS `phpr_trace_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(255) DEFAULT NULL,
  `message` text,
  `details` mediumtext,
  `record_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log` (`log`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phpr_user_params`
--

CREATE TABLE IF NOT EXISTS `phpr_user_params` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`user_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phpr_user_params`
--

INSERT INTO `phpr_user_params` (`user_id`, `name`, `value`) VALUES
(1, 'Cms_Pages_create_collapsible_visible_0', 'b:0;'),
(1, 'Cms_Content_index_list_settings', 'a:5:{s:12:"visible_list";a:4:{i:0;s:4:"page";i:1;s:4:"name";i:2;s:4:"code";i:3;s:9:"is_global";}s:14:"invisible_list";a:7:{i:0;s:4:"type";i:1;s:9:"file_name";i:2;s:7:"content";i:3;s:10:"created_at";i:4;s:17:"created_user_name";i:5;s:10:"updated_at";i:6;s:17:"updated_user_name";}s:12:"column_order";a:8:{i:0;s:4:"page";i:1;s:4:"name";i:2;s:4:"code";i:3;s:9:"is_global";i:4;s:10:"created_at";i:5;s:17:"created_user_name";i:6;s:10:"updated_at";i:7;s:17:"updated_user_name";}s:7:"sorting";O:8:"stdClass":2:{s:5:"field";N;s:9:"direction";N;}s:16:"records_per_page";s:2:"20";}'),
(1, 'Cms_Pages_edit_collapsible_visible_0', 'b:1;'),
(1, 'Service_Providers_index_list_settings', 'a:5:{s:12:"visible_list";a:0:{}s:14:"invisible_list";a:0:{}s:12:"column_order";a:0:{}s:7:"sorting";O:8:"stdClass":2:{s:5:"field";s:2:"id";s:9:"direction";s:3:"asc";}s:16:"records_per_page";i:20;}'),
(1, 'payment_fees_status', 'a:1:{i:1;s:1:"0";}'),
(1, 'Cms_Pages_index_list_treenodestatus_72', 'i:0;');

-- --------------------------------------------------------

--
-- Table structure for table `profile_friends`
--

CREATE TABLE IF NOT EXISTS `profile_friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `follower_id` int(11) DEFAULT NULL,
  `leader_id` int(11) DEFAULT NULL,
  `mutual` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `follower_id` (`follower_id`),
  KEY `leader_id` (`leader_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_wall`
--

CREATE TABLE IF NOT EXISTS `profile_wall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_answers`
--

CREATE TABLE IF NOT EXISTS `service_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moderation_status` varchar(20) DEFAULT NULL,
  `description` text,
  `is_public` tinyint(4) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `request_id` int(11) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `moderation_status` (`moderation_status`),
  KEY `user_id` (`user_id`),
  KEY `request_id` (`request_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_categories`
--

CREATE TABLE IF NOT EXISTS `service_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url_name` varchar(100) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `code` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_hidden` tinyint(4) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `url_name` (`url_name`),
  KEY `code` (`code`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`),
  KEY `form_id` (`form_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `service_categories`
--

INSERT INTO `service_categories` (`id`, `name`, `url_name`, `description`, `keywords`, `code`, `parent_id`, `is_hidden`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`, `form_id`) VALUES
(7, 'Electrician', 'electrician', NULL, NULL, NULL, 4, NULL, 1, NULL, '2014-08-21 00:26:13', NULL, 3),
(8, 'plumber', 'plumber', 'plumber', 'drains plumber toilet', NULL, 8, NULL, 2, 2, '2014-08-30 00:13:37', '2014-08-30 00:25:43', 5),
(4, 'Home Improvement', 'home-improvement', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2014-07-20 15:33:43', NULL, NULL),
(5, 'Handyman', 'handyman', NULL, NULL, NULL, 4, NULL, 1, 1, '2014-07-20 15:34:26', '2014-08-15 20:25:00', 2),
(6, 'Painter', 'painter', NULL, NULL, NULL, 4, NULL, 1, NULL, '2014-07-20 15:35:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_categories_categories`
--

CREATE TABLE IF NOT EXISTS `service_categories_categories` (
  `category_id` int(11) NOT NULL,
  `related_category_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`related_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_categories_categories`
--

INSERT INTO `service_categories_categories` (`category_id`, `related_category_id`) VALUES
(7, 6);

-- --------------------------------------------------------

--
-- Table structure for table `service_categories_providers`
--

CREATE TABLE IF NOT EXISTS `service_categories_providers` (
  `category_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`provider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_categories_requests`
--

CREATE TABLE IF NOT EXISTS `service_categories_requests` (
  `category_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`request_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_plan`
--

CREATE TABLE IF NOT EXISTS `service_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL,
  `credits` int(11) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=253 ;

--
-- Dumping data for table `service_plan`
--

INSERT INTO `service_plan` (`id`, `name`, `enabled`, `credits`, `price`, `description`) VALUES
(247, 'Bronze', 1, 3, '0.00', 'bronze'),
(248, 'Silver', 1, 13, '10.00', 'silver'),
(249, 'gold', 1, 23, '20.00', 'gold'),
(250, 'Unlimited', 1, 2500, '25.00', 'unlimited');

-- --------------------------------------------------------

--
-- Table structure for table `service_providers`
--

CREATE TABLE IF NOT EXISTS `service_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moderation_status` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `license` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `bonded` varchar(255) DEFAULT NULL,
  `licn_state` varchar(255) DEFAULT NULL,
  `date_issue` date DEFAULT NULL,
  `url_name` varchar(255) DEFAULT NULL,
  `description` text,
  `url` varchar(255) DEFAULT NULL,
  `established` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `street_addr` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `rating_all` int(11) DEFAULT NULL,
  `rating_num` decimal(15,2) DEFAULT NULL,
  `rating_all_num` decimal(15,2) DEFAULT NULL,
  `stat_offers` int(11) DEFAULT '0',
  `stat_quotes` int(11) DEFAULT '0',
  `stat_quote_average` decimal(15,2) DEFAULT '0.00',
  `stat_wins` int(11) DEFAULT '0',
  `stat_earned` decimal(15,2) DEFAULT '0.00',
  `config_data` text,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `description_experience` text,
  `description_speciality` text,
  `description_why_us` text,
  `service_codes` text,
  `service_radius` int(11) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL,
  `active_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `moderation_status` (`moderation_status`),
  KEY `user_id` (`user_id`),
  KEY `url_name` (`url_name`),
  KEY `country_id` (`country_id`),
  KEY `state_id` (`state_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_groups`
--

CREATE TABLE IF NOT EXISTS `service_provider_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_groups_providers`
--

CREATE TABLE IF NOT EXISTS `service_provider_groups_providers` (
  `provider_id` int(11) NOT NULL,
  `provider_group_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`provider_id`,`provider_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_questions`
--

CREATE TABLE IF NOT EXISTS `service_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moderation_status` varchar(20) DEFAULT NULL,
  `description` text,
  `is_public` tinyint(4) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `request_id` int(11) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `moderation_status` (`moderation_status`),
  KEY `provider_id` (`provider_id`),
  KEY `answer_id` (`answer_id`),
  KEY `request_id` (`request_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_quotes`
--

CREATE TABLE IF NOT EXISTS `service_quotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moderation_status` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `request_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `comment` text,
  `price` decimal(15,2) DEFAULT '0.00',
  `start_at` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `config_data` text,
  `deleted_at` datetime DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `quote_type` varchar(100) DEFAULT NULL,
  `flat_items` text,
  `flat_labor_description` text,
  `flat_labor_price` decimal(15,2) DEFAULT '0.00',
  `onsite_price_start` decimal(15,2) DEFAULT '0.00',
  `onsite_price_end` decimal(15,2) DEFAULT '0.00',
  `onsite_travel_required` tinyint(4) DEFAULT NULL,
  `onsite_travel_price` decimal(15,2) DEFAULT '0.00',
  `onsite_travel_waived` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `moderation_status` (`moderation_status`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  KEY `request_id` (`request_id`),
  KEY `provider_id` (`provider_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_quote_statuses`
--

CREATE TABLE IF NOT EXISTS `service_quote_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `service_quote_statuses`
--

INSERT INTO `service_quote_statuses` (`id`, `code`, `name`) VALUES
(1, 'new', 'New'),
(2, 'accepted', 'Accepted'),
(3, 'shortlist', 'Shortlisted'),
(4, 'eliminate', 'Eliminated'),
(5, 'deleted', 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `service_ratings`
--

CREATE TABLE IF NOT EXISTS `service_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moderation_status` varchar(20) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `comment` text,
  `user_from_id` int(11) DEFAULT NULL,
  `user_to_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `request_id` int(11) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `moderation_status` (`moderation_status`),
  KEY `user_from_id` (`user_from_id`),
  KEY `user_to_id` (`user_to_id`),
  KEY `provider_id` (`provider_id`),
  KEY `request_id` (`request_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_requests`
--

CREATE TABLE IF NOT EXISTS `service_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moderation_status` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url_name` varchar(255) DEFAULT NULL,
  `description` text,
  `description_extra` text,
  `city` varchar(100) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `config_data` text,
  `expired_at` datetime DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `required_by` varchar(100) DEFAULT NULL,
  `required_type` varchar(100) DEFAULT NULL,
  `firm_start` datetime DEFAULT NULL,
  `firm_end` datetime DEFAULT NULL,
  `firm_alt_start` datetime DEFAULT NULL,
  `firm_alt_end` datetime DEFAULT NULL,
  `is_remote` tinyint(4) DEFAULT NULL,
  `custom_form_data` text,
  PRIMARY KEY (`id`),
  KEY `moderation_status` (`moderation_status`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  KEY `url_name` (`url_name`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_requests_providers`
--

CREATE TABLE IF NOT EXISTS `service_requests_providers` (
  `request_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`request_id`,`provider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_skills`
--

CREATE TABLE IF NOT EXISTS `service_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url_name` varchar(100) DEFAULT NULL,
  `description` text,
  `code` varchar(255) DEFAULT NULL,
  `is_hidden` tinyint(4) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `url_name` (`url_name`),
  KEY `code` (`code`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `service_skills`
--

INSERT INTO `service_skills` (`id`, `name`, `url_name`, `description`, `code`, `is_hidden`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Painter', NULL, NULL, NULL, NULL, 1, NULL, '2014-08-16 00:48:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_skills_requests`
--

CREATE TABLE IF NOT EXISTS `service_skills_requests` (
  `skill_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  PRIMARY KEY (`skill_id`,`request_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_statuses`
--

CREATE TABLE IF NOT EXISTS `service_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `service_statuses`
--

INSERT INTO `service_statuses` (`id`, `code`, `name`) VALUES
(1, 'draft', 'Draft'),
(2, 'pending', 'Pending Approval'),
(3, 'active', 'Active'),
(4, 'expired', 'Expired'),
(5, 'closed', 'Closed'),
(6, 'cancelled', 'Cancelled'),
(7, 'archived', 'Archived'),
(8, 'booked', 'Booked'),
(9, 'booked_cancelled', 'Booking Cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `service_testimonials`
--

CREATE TABLE IF NOT EXISTS `service_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moderation_status` varchar(20) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `is_published` tinyint(4) DEFAULT NULL,
  `hash` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `comment` text,
  `invite_subject` varchar(255) DEFAULT NULL,
  `invite_message` text,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `moderation_status` (`moderation_status`),
  KEY `provider_id` (`provider_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `social_providers`
--

CREATE TABLE IF NOT EXISTS `social_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `class_name` varchar(100) DEFAULT NULL,
  `config_data` text,
  `is_enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `class_name` (`class_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `social_providers`
--

INSERT INTO `social_providers` (`id`, `code`, `class_name`, `config_data`, `is_enabled`) VALUES
(1, 'facebook', 'Social_Facebook_Provider', 'CsHXr31bEQ1CDmpbfyZn9D3+QYbP9anXtVIZ4tRiW8BSs4y50kw23cv7mq97UOYqUh3x8AQwv0Aukf60eqo+adaTuM3ivJdEvikkPcocgvdDpkOU0wxfQFrKvJbDZVeArmZN2SBvxNDnRqV7NcwRL7/Q2LUnjckp8Nw8Uf6KLqkZ94Z3I3yyVErM34H66Ve7SWIKL5nZcyvOaG1EcdnczUAn6QQqADezuHQ4esZK0Az497zHxgeOxpMYMtXnoLuyAYTVlMQKsC06eVmeD+svuGB28QfSBqivB34rmX/Vo7WZzrEgYkPYQPf4Q1YXU07dEljFCzA0n/zVCfsqOGSnvst4T8ggb4/nF/DDRo690ZHnGj+iAbDDnH6JmfXGuwYC', 1),
(2, 'google', 'Social_Google_Provider', 'WYLXsRSRmAEBabLyH2evHmp3v5O/98zihh/lbwqg4gsx6FO/5Ss9x8Uat7c1HbKP5HVXmfYRSgz/TzXlw2IfvOMoFyDZT4POPt9BbVW5BIJgWxet+vSN+oVkqNpeCUHKM3dT0wKcGLEUgZiIO9mLEqnqEXjmzu0gdoMc/XkrA5heNWmE5X5SrjBLi0DdQKpUCuyk2jartG0YuYngZt35YDF3BMjJ+TsDH8BE+N1Lu4TG/Jf15bMdDO92iIGP38u6Xu+QzSRhGhLfmkzpyOCnDgv0OLRjhyYV49PkRQ0oN1quubhMNC5uHs/JcIIxGy8tUZy4DCdaUqoAqPvpxWZADyytw9vRgMUY5aPetwt0zF20kVSko/PTrtZcbAa6tOFHlTlpKq+/mavs/w2ZE7B9KlVWWTa/4oxlmAOD39FeSY8=', 1);

-- --------------------------------------------------------

--
-- Table structure for table `social_provider_users`
--

CREATE TABLE IF NOT EXISTS `social_provider_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider_code` varchar(255) DEFAULT NULL,
  `provider_token` varchar(255) DEFAULT NULL,
  `sort_order` int(8) DEFAULT '0',
  `is_enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `provider_code` (`provider_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `guest` tinyint(4) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `signup_ip` varchar(15) DEFAULT NULL,
  `last_ip` varchar(15) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `ssn` varchar(11) DEFAULT NULL,
  `funding_email` varchar(50) DEFAULT NULL,
  `account_no` varchar(50) DEFAULT NULL,
  `routing_no` varchar(255) DEFAULT NULL,
  `tax_id` varchar(255) DEFAULT NULL,
  `submerchantAccountId` varchar(255) DEFAULT NULL,
  `submerchantStatus` varchar(255) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `street_addr` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `credits` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `updated_user_id` (`updated_user_id`),
  KEY `state_id` (`state_id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `code`, `description`) VALUES
(1, 'Guest', 'guest', NULL),
(2, 'Registered', 'registered', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_messages`
--

CREATE TABLE IF NOT EXISTS `user_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user_id` int(11) DEFAULT NULL,
  `thread_id` varchar(40) DEFAULT NULL,
  `is_latest` tinyint(4) DEFAULT NULL,
  `message` text,
  `subject` varchar(255) DEFAULT NULL,
  `master_object_class` varchar(255) DEFAULT NULL,
  `master_object_id` int(11) DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `thread_id` (`thread_id`),
  KEY `from_user_id` (`from_user_id`),
  KEY `master_index` (`master_object_class`,`master_object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_message_recipients`
--

CREATE TABLE IF NOT EXISTS `user_message_recipients` (
  `message_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `is_new` tinyint(4) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_preferences`
--

CREATE TABLE IF NOT EXISTS `user_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `module_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_preference` (`user_id`,`module_id`,`name`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
