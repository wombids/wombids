<div class="speaks">
	<div class="speak-title">
		<span>client stories</span>
	</div><!--.speak-title-->
	<div class="container speak-items">
		<div class="row-fluid">
			<div class="span4 offset1">
				<p class="speech-bubble"><i class="fa fa-quote-left fa-3"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
				<div class="item-writer span8 offset4">
					<div class="media">
						<a class="pull-right" href="#">
						<img src="/themes/custom/assets/images/testi-user.jpg" alt="..." class="media-object img-circle"></a>
						<p class="media-body">Charlotte Bronte<br/><small>jase Eyee</small></p>
					</div>
				</div><!--.item-writer-->
			</div>
			<div class="span4 offset2">
				<p class="speech-bubble"><i class="fa fa-quote-left fa-3"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
				<div class="item-writer span8 offset4">
					<div class="media">
						<a class="pull-right" href="#">
						<img src="/themes/custom/assets/images/testi-user.jpg" alt="..." class="media-object img-circle"></a>
						<p class="media-body">Charlotte Bronte<br/><small>jase Eyee</small></p>
					</div>
				</div><!--.item-writer-->
			</div>
		</div>
	</div>
</div><!--.speaks-->