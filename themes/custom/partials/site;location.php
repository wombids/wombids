<div class="container">
	<div class="locations">
		<ul>
			<li><div class="provider-icon air-condition"></div><a href="#">Air Conditioning</a></li>
			<li><div class="provider-icon cabinet"></div><a href="#">Cabinets & Countertops</a></li>
			<li><div class="provider-icon carpenter"></div><a href="#">Carpentry</a></li>
			<li><div class="provider-icon carpet"></div><a href="#">Carpet</a></li>
			<li><div class="provider-icon cleaning"></div><a href="#">Cleaning Services</a></li>
			<li><div class="provider-icon insulation"></div><a href="#">Drywall & Insulation</a></li>
			<li><div class="provider-icon fences"></div><a href="#">Fences</a></li>
			<li><div class="provider-icon flooring"></div><a href="#">Flooring</a></li>
			<li><div class="provider-icon garage"></div><a href="#">Garages & Doors</a></li>
			<li><div class="provider-icon junk"></div><a href="#">Junk Removal</a></li>
			<li><div class="provider-icon landscape"></div><a href="#">Landscape & Decks</a></li>
			<li><div class="provider-icon trees"></div><a href="#">Trees & Shrubs</a></li>
			<li><div class="provider-icon painting"></div><a href="#">Painting & Staining</a></li>
			<li><div class="provider-icon plumbing"></div><a href="#">Plumbing</a></li>
			<li><div class="provider-icon swimming"></div><a href="#">Swimming Pools & Spa</a></li>
		
		</ul>
	</div>
</div>