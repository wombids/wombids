<div class="row-fluid thead">
	<div class="span6" style="margin-top: 15px;">
		<span class="blue">wom</span><span class="green">Bids</span>
		<span class="slg"> “word of mouth” bidding for home projects &trade;</span>
	</div>
	<div class="span6" style="">
		<p class="pull-right">
			<a href="<?=root_url('/terms')?>">Terms & Use</a>
			<a href="<?=root_url('/privacy')?>">Privacy Policy</a>
		</p>
	</div>
</div>
<div class="line"></div>
<div class="row-fluid">
	  <div class="span3">
	  	<div class="fmtitle">
		  	<img src="/themes/custom/assets/images/home-icon.png" alt="Home-icon" class="img-circle">
		  	Homeowners
	  	</div>
		  <ul class="foot-links">
			  <li class=""><a href="<?=root_url('/how-it-works-homeowner/')?>">How it Works</a></li>
			  <li class=""><a href="<?=root_url('/request')?>">Post a Job</a></li>
			  <li class=""><a href="<?=root_url('/advice-and-tips')?>">Advice and Tips</a></li>
		</ul>
	</div>  
	  <div class="span3">
	  	<div class="fmtitle">
		  	<img src="/themes/custom/assets/images/contr.png" alt="Home-icon" class="img-circle">
		  	Home Service Pro
	  	</div>
		  <ul class="foot-links">
			  <li class=""><a href="<?=root_url('/how-it-works-providers/')?>">How it Works</a></li>
			  <li class=""><a href="<?=root_url('/provide')?>">Sign Up</a></li>
			  <li class=""><a href="<?=root_url('/testimonials')?>">Testimonials</a></li>	
		</ul>
	</div> 
	<div class="span2">
		<div class="fmtitle">
		  	<img src="/themes/custom/assets/images/link.png" alt="Home-icon" class="img-circle">
		  	Everybody
	  	</div>
		  <ul class="foot-links">
			  <li class=""><a href="<?=root_url('/about')?>">About Us</a></li>
			  <li class=""><a href="<?=root_url('/blog')?>">Blog</a></li>
			  <li class=""><a href="<?=root_url('/contact')?>">Contact Us</a></li>		
		</ul>
	</div>							 
	<div class="span4">
		<div class="fcopy">
		  &copy; 2013 - 2014 womBids, Inc.<br /> All right reserved.
		</div> 
		<div class="fsocial">
				<div><a href="#"><span class="fbadge"><i class="fa fa-facebook"></i></span></a></div>
				<div><a href="#"><span class="gbadge"><i class="fa fa-google-plus"></i></span></a></div>
				<div><a href="#"><span class="tbadge"><i class="fa fa-twitter"></i></span></a></div>
				<div><a href="#"><span class="rbadge"><i class="fa fa-rss"></i></span></a></div>
				<div><a href="#"><span class="pbadge"><i class="fa fa-pinterest "></i></span></a></div>
		</div>
	</div> 
</div>
