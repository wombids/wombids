<div class="container">
	<div class="how-works">
		<h3>SEE HOW IT WORKS</h3>
	</div><!--.how-works-->
	<div class="row-fluid">
		<div class="span5 hown offset1">
			<h4>I am a <span class="green">Homeowner</span>?</h4>
			<div class="row">
				<span><i class="fa fa-laptop"></i> Sign up</span><br />
				<small>Create an account. It's easy</small>
			</div>
			<div class="row">
				<span><i class="fa fa-pencil"></i> Post a Project</span><br />
				<small>Professionals will contact<br/> you within 3 days, guaranteed</small>
			</div>
			<div class="row">
				<span><i class="fa fa-star"></i> Review Profiles</span><br />
				<small>Check out your candidates.<br/>
				Request bids & additional info.</small>
			</div>
			<div class="row">
				<span><i class="fa fa-check-circle"></i> Hire a Professional</span><br />
				<small>Interview, check references<br/> and hire the one that work for you!</small>
			</div>
			<div class="row">
				<span><i class="fa fa-dollar"></i> Pay for your project</span><br />
				<small>Make and track payments<br/> in our Payments Center</small>
			</div>
			<a class="btn btn-default btn-block" href="<?=root_url('/request')?>">Post a Project</a>
		</div><!--.hown-->
		<div class="span5 hser">
			<h4>I am a <span class="blue">Home Service Pro</span>?</h4>
			<div class="row">
				<span><i class="fa fa-laptop"></i> Sign up</span><br />
				<small>Create an account. It's easy</small>
			</div>
			<div class="row">
				<span><i class="fa fa-users"></i> Create a Profile</span><br />
				<small>List your skills and talents.<br/>Make it pop with photos and more.</small>
			</div>
			<div class="row">
				<span><i class="fa fa-search"></i> Search for Jobs</span><br />
				<small>New jobs are posted every minute!<br/><br/>    </small>
			</div>
			<div class="row">
				<span><i class="fa fa-check-circle"></i> Get Hired</span><br />
				<small>Apply to jobs,find the right match<br/> and voila-you've got a job!</small>
			</div>
			<div class="row">
				<span><i class="fa fa-dollar"></i> Get Paid</span><br />
				<small>Make payday a breeze by<br/> signing up for direct deposit.</small>
			</div>
			<a class="btn btn-default btn-block" href="<?=root_url('/provide')?>">Bid in Projects</a>
		</div><!--.hser-->
	</div>
</div>