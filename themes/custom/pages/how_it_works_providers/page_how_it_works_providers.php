<div class="page-header">
    <h1><?=$this->page->title_name?></h1>
    <h4 class="subheader"><?=$this->page->description?></h4>
</div>
<div class="row-fluid">
	<div class="span12">
	
		<div class="dynamic-row step-1">
			<div class="image">
			    <img src="/themes/custom/assets/images/comp_g.png">
			    <div class="imageText">
			    	<p>Create an account. It's easy.</p>
			    </div>
			</div>
			<div class="text">
			    <h3>1. Sign Up</h3>
			    <p>You can create a profile through womBids or use an existing Facebook or Google account. Don’t worry, it’s extremely easy.  By signing up, you represent that you have authority to claim the business account on behalf of this business. 
			    </p>
			</div>
		</div>
		
		<div class="dynamic-row step-2">
			<div class="image">
			    <img src="/themes/custom/assets/images/profile_g.png">
			    <div class="imageText">
			    	<p>List your skills and talents. Make it pop with photos and more.</p>
			    </div>
			</div>
			<div class="text">
			    <h3>2. Create a Profile</h3>
			    <p>Think of this as an opportunity to put your best foot forward. This webpage is solely designed to showcase your business’ portfolio, core competencies, experience level, licenses, insurance, bonding, and references. It also gives your past customers an opportunity to say something nice about you in the form a rating and short review.</p>
			</div>
		</div>
		
		<div class="dynamic-row step-3">
			<div class="image">
			    <img src="/themes/custom/assets/images/search_g.png">
			</div>
			<div class="text">
			    <h3>3. Search for Jobs</h3>
			    <p>After you’ve made your profile, you can search through potential projects. Millions homeowners search for contractors every year, here’s where you can find the jobs.  You’ll have an opportunity to talk to the homeowner regarding the project and if it’s a good fit put in a bid for the project.</p>
			</div>
		</div>
		
		<div class="dynamic-row step-4">
			<div class="image">
			    <img src="/themes/custom/assets/images/arrow_g.png">
			</div>
			<div class="text">
			    <h3>4. Get Hired</h3>
			    <p>
				After ironing the details, the homeowner will proceed with accepting a bid, hopefully, it’s your bid! This is homeowner picking a pro to proceed with the project.
			    </p>
			</div>
		</div>
		
		<div class="dynamic-row step-1">
			<div class="image">
			    <img src="/themes/custom/assets/images/dollar_g.png">
			</div>
			<div class="text">
			    <h3>5. Get Paid</h3>
			    <p>
				Don’t worry - it’s simple and easy. We have payment integration built in and it easily disburses to you. We use a safe and secure third party processor, Braintree (a division of Paypal, Inc. and the same company used by heavyweights such as Airbnb and Uber).
			    </p>
			</div>
		</div>
			
	</div>
</div>