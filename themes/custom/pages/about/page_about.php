<div class="image-container" style="background:url('http://dev1.wombids.com/themes/custom/assets/images/about/about-us-1.jpg');">
	<div class="inner-panel">
			<p>Founded in 2013 and based in San Francisco,<b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>is trusted marketplace where homeowners can find the most respected local
			contractors and home service pros. </p>
			
			<p>Our customers use <b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>to find small businesses that specialize in projects
			ranging from air conditioning to zinc roofing.</p>
	</div>
</div>
<div class="container">
	<div class="row-fluid">
		<div class="span12">
			<div class="page-header">
			    <h1><?=$this->page->title_name?></h1>
			    <h4 class="subheader"><?=$this->page->description?></h4>
			</div>
			<table class="contentTable"><tbody><tr><td>Air Conditioning &amp; Cooling</td><td>Driveways, Patios, Walks, Steps &amp; Floors</td><td>Junk Removal</td></tr><tr><td>Cabinets &amp; Countertops<br></td><td>Drywall &amp; Insulation<br></td><td>Landscape, Decks &amp; Fences<br></td></tr><tr><td>Carpentry<br></td><td>Fences<br></td><td>Maintenance of Lawn, Trees &amp; Shrubs<br></td></tr><tr><td>Carpet<br></td><td>Flooring<br></td><td>Painting &amp; Staining<br></td></tr><tr><td>Cleaning Services<br></td><td>Garages, Doors, Openers<br></td><td>Plumbing<br></td></tr><tr><td>Concrete, Brick &amp; Stone<br></td><td>Gutters<br></td><td>Swimming Pools, Spas, Hot Tubs<br></td></tr><tr><td>Decks, Porches, Gazebos &amp; Play Equipment<br></td><td>Handyman Services<br></td><td>Tile &amp; Stone<br></td></tr><tr><td>Decorators &amp; Designers<br></td><td>Heating &amp; Cooling<br></td><td>Miscellaneous<br></td></tr></tbody></table>

			<div class="sub-text">Whether it is an enormous remodel or a job as simple as removing debris, <b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>can help! Don’t
			believe us, give us a call toll free at 1(800)574-6501 and we’ll walk you
			through the process.</p></div>
			<div class="sub-header">At <b><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span> </b>we have two
			main missions:</div>
			
			<div class="dynamic-row step-1">
				<div class="image">
				    <img src="/themes/custom/assets/images/about/rating.jpg">
				</div>
				<div class="text">
				    <h4>1. Simpler & Transparent</h4>
				    <p>Make home projects simpler and more efficient.<br>We achieve this by providing transparency, as well as easier access to the best local pros.
				    </p>
				</div>
			</div>
			
			<div class="dynamic-row step-2">
				<div class="image">
				    <img src="/themes/custom/assets/images/about/american_dream.jpg">
				</div>
				<div class="text">
				    <h4>2. We <i class="fa fa-heart"></i> small businesses</h4>
				    <p>We want to do our part to help small businesses grow.<br>Not only are they the back bone of the economy, they are living proof that the American Dream is alive and well.<br>Whether it’s through finding new customers or providing a platform to showcase your amazing reputation, womBids is here to help.</p>
				</div>
			</div>
			
			<div class="pageLinks">Next steps? Start an account are you <a class="homeowner" href="/request">homeowner</a>&nbsp;or a <a class="pro" href="/provide">service pro</a></div>
			
			<?=content_block('about_us', 'About us')?>
		</div>
	</div>
</div>