<div class="page-header">
    <h1><?=$this->page->title_name?></h1>
    <h4 class="subheader"><?=$this->page->description?></h4>
</div>
<div class="row-fluid">
	<div class="span12">
	
		<div class="dynamic-row step-1">
			<div class="image">
			    <img src="/themes/custom/assets/images/comp_b.png">
			    <div class="imageText">
			    	<p>Create an account. It's easy.</p>
			    </div>
			</div>
			<div class="text">
			    <h3>1. Sign Up</h3>
			    <p>
			    	You can create a profile through womBids or use an existing Facebook or Google account. Once you’ve done this, you’ll be able to proceed with posting a project or you can search through local contractors in your area. You’ll be able to see contractor’s licenses, insurance and bonding limits, reputation and even a pictures from previous projects.
			    </p>
			</div>
		</div>
		
		<div class="dynamic-row step-2">
			<div class="image">
			    <img src="/themes/custom/assets/images/pencil_b.png">
			    <div class="imageText">
			    	<p>List your skills and talents. Make it pop with photos and more.</p>
			    </div>
			</div>
			<div class="text">
			    <h3>2. Post a Project</h3>
			    <p>Now this is the fun part, homeowners have the opportunity to post a project, otherwise known as a Request for Proposal. Don’t be discouraged if you don’t know the lingo, this is Step #1 in the process and contractors know that part of their job is to educate customers. The key to success is getting as much information as possible into the request form, i.e. in the form detailed descriptions, pictures, rough sketches, blue prints, etc. – whatever you have womBids and the contractors will appreciate it.</p>
			</div>
		</div>
		
		<div class="dynamic-row step-3">
			<div class="image">
			    <img src="/themes/custom/assets/images/star_b.png">
			</div>
			<div class="text">
			    <h3>3. Review Profiles</h3>
			    <p>We give customers the option to either wait for contractors to come to them or seek out contractors themselves on the womBids platform.</p>
			</div>
		</div>
		
		<div class="dynamic-row step-4">
			<div class="image">
			    <img src="/themes/custom/assets/images/arrow_b.png">
			</div>
			<div class="text">
			    <h3>4. Hire a Professional</h3>
			    <p>
				After ironing out the details of the project, sifting through reviews, checking references and agreeing on terms, customers can hire a professional simply by accepting the pro’s bid.
			    </p>
			</div>
		</div>
		
		<div class="dynamic-row step-1">
			<div class="image">
			    <img src="/themes/custom/assets/images/dollar_b.png">
			</div>
			<div class="text">
			    <h3>5. Pay for your Project</h3>
			    <p>
				Paying for the project is simple and easy as we have payment integration built into the site. We use a safe and secure third party processor, Braintree (a division of Paypal, Inc. and the same company used by heavyweights such as Airbnb and Uber).
			    </p>
			</div>
		</div>
			
	</div>
</div>