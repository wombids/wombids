<div class="image-container" style="background:url('http://dev1.wombids.com/themes/custom/assets/images/about/contact-us.jpg');">
	<div class="inner-panel">
			<h3>SEND US AN EMAIL</h3>
			<p>We love hearing from you. Whether its questions, suggestions, potential partnerships, or even just to say hi, the best way to get in contact with us is through email.</p>
	</div>
</div>
<div class="container">
	<div class="row-fluid">
		<div class="span12">
			<div class="page-header">
			    <h1><?=$this->page->title_name?></h1>
			    <h4 class="subheader"><?=$this->page->description?></h4>
			</div>

		<div class="row-fluid">
			<div class="span8">
				<p>If you have questions or need help with anything regarding our website, please email: <?=mailto_encode('help@wombids.com')?></p>

				<p>For non-help related inquiries, like suggestions, press inquiries, partnership opportunities, please direct all messages to: <?=mailto_encode('general@wombids.com')?></p>

				<p>If you are a contractor or home service professional and would like to speak to an account executive, please email: <?=mailto_encode('prosteam@wombids.com')?></p>
				</br>
				</br>
				<h3>GIVE US A CALL</h3>
				<p>We would be happy to hear from you over the phone as well. Give us a call toll free at 1(800)574-6501. Our hours of operation are Monday through Saturday 8 am to 5pm (PST).</p>
			</div>
			<div class="span4">
				<div class="well">
					<h2><span style="color: #548dd4;">wom</span><span style="color: #9bbb59;">Bids</span></h2>
					<p>
						Some street, some place <br />
						1111 Country
					</p>
					<h5>Support</h5>
					<p><?=mailto_encode('support@email.com')?></p>
		
					<h5>Sales</h5>
					<p><?=mailto_encode('sales@email.com')?></p>
				</div>
			</div>
			
		</div>
		</div>
	</div>
</div>