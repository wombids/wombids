/**
 * Scripts Ahoy! Software
 *
 * Copyright (c) 2012 Scripts Ahoy! (scriptsahoy.com)
 * All terms, conditions and copyrights as defined
 * in the Scripts Ahoy! License Agreement
 * http://www.scriptsahoy.com/license
 *
 */

/*

// Behaviors
@require application_layout/shared/role_select_behavior.js;
@require application_layout/shared/user_menu_behavior.js;
@require application_layout/shared/field_editor_behavior.js;
@require application_layout/shared/provide_radius_behavior.js;
@require application_layout/shared/provide_hours_behavior.js;
@require application_layout/shared/request_quotes_behavior.js;

*/