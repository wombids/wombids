<ul class="unstyled">
    <li>
        <h4 class="text-success"><span class="ring-badge ring-badge-success">1</span> We notify the right {title_plural} about your request</h4>
        <p>We send your request to qualified {title_plural} who are available to work on your job at the time and place you requested.</p>
    </li>
    <li>
        <h4 class="text-success"><span class="ring-badge ring-badge-success">2</span> You'll receive price quotes</h4>
        <p>You'll receive real-time price quotes from {title_plural}.</p>
    </li>
    <li>
        <h4 class="text-success"><span class="ring-badge ring-badge-success">3</span> Choose the right person for the job</h4>
        <p>Pick the {title} you like best and schedule the work to be completed.</p>
    </li>
</ul>
