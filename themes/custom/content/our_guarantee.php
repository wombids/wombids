<div class="row-fluid">
	<div class="span9 mobile-span9">
		<h3>Home Service Guarantee</h3>
		<p>We take your trust and safety very seriously. We know your home is your most valuable asset. More importantly, your personal safety and the safety of your family is priceless.</p>
		<p>Our Trust &amp; Safety team has developed some important features to ensure you find a trustworthy professional, and get your job done with peace of mind.</p>
	</div>
	<div class="span3 mobile-span3">
		<p><img src="{theme_url}assets/images/intro/our_guarantee.png" alt="Our Gaurantee" /></p>
	</div>
</div>