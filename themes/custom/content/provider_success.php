<h2>Congrats, Your {role_name} profile has been set up!</h2>
<p class="lead">We will now send you email or sms alerts with any requests during your work hours.</p>
<p><a href="/intro">Learn more about {site_name}</a></p>
<p><a href="/provide/create">Create another profile</a></p>
<p><a href="/account/notifications">Change notification preferences</a></p>
<p><a href="/dashboard">Go to your Dashboard</a></p>