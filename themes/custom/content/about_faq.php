<h3>Provider FAQ</h3>

<h5>How much does {site_name} cost?</h5>
<p>It is <em>FREE</em> to sign up and to receive job leads. To respond to jobs you're interested in, you will be charged a once off credit which cost as low as 75cents each. (credit amount varies by request type)</p>

<h5>When does {site_name} charge me for a job?</h5>
<p>When you find a job that you are interested in, you will need to buy some Pay-as-you-go credits to make contact with the client.</p>

<h5>How does {site_name} match me to a job?</h5>
<p>We match you based on the categories and skills you select in your freelance profile when you register. You can update this criteria at any time.</p>

<h5>How long do I have to submit a price quote for jobs I'm offered?</h5>
<p>It depends. Usually you have up to 30 hours from the time you receive the job offer. However, sometimes a client needs the service performed immediately. In that case, you'll have up to 24 hours to respond.</p>

<hr />

<h3>Customer FAQ</h3>

<h5>How much does {site_name} cost?</h5>
<p>It is <em>FREE</em> to sign up and to contact providers. To respond to providers you're interested in, you will be charged a once off credit which cost as low as 75cents each.</p>

<h5>When does {site_name} charge me to find a provider?</h5>
<p>When you find a provider that meets your requirements, you will need to buy some Pay-as-you-go credits to make contact with the provider.</p>

<h5>How does {site_name} match me to a provider?</h5>
<p>We match you based on the categories and skills you select in your freelance profile when you register. You can update this criteria at any time.</p>
