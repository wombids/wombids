<p></p><p><b style="line-height: 1.45em; background-color: initial;">Introduction</b><br></p>

<p></p>

<p>These terms of use govern your use of our website;
by using our website, you accept these terms of use in full.&nbsp; If you disagree with these terms of use or
any part of these terms of use, you must not use our website. </p>
<p>If you register with our website, we will ask you
to expressly agree to these terms of use.</p>
<p>We provide a forum for users to advertise, browse,
buy and sell products and services.&nbsp; We
are not party to the sales or purchases of products via this website, and
subject to the first paragraph of Section 18 we will not be liable to any
person in relation to any contract for the sale and purchase of products.&nbsp; See Section 11 for more details of our role.</p><p><br></p>
<p></p>

<p><b>Licence to use
website</b></p>
<p>Use of the Website
is available only to individuals who are at least 18 years old and can form
legally binding contracts under applicable law. You represent, acknowledge and
agree that you are at least 18 years of age, and that: </p>
<p>All registration
information that you submit is truthful and accurate, (b) you will maintain the
accuracy of such information, and (c) your use of the Website and Services
offered through this Website do not violate any applicable law or regulation.
Your username and account may be terminated without warning if we believe that
you are under the age of 18 or that you are not complying with any applicable
federal, state or local laws, rules or regulations.</p><p><br></p>
<p><b>Acceptable use and legal compliance</b></p>
<p>You must comply with all applicable laws in
relation to any proposed or actual sale or purchase of products or service via
our website.&nbsp; </p><p><br></p>
<p><b>Homeowner and / or
Consumer registration</b></p>
<p>Providing you are at least 18 years of age and
legally allowed to do business, you may register with the websites as a buyer
by: setting up a consumer profile. </p>
<p>You will have the opportunity to identify and
correct input errors prior to registering.</p><p><br></p>
<p><b>Contractor and / or
Service Provider registration</b></p>
<p>Providing you are at least 18 years of age, are
acting in the course of a business and you represent that you have authority to claim and
represent the business’s womBids account. </p>
<p>You will have the opportunity to identify and
correct input errors prior to registering. </p><p><br></p>
<p><b>Listing rules</b></p>

<p></p>

<p>Be truthful, be ethical. </p><p><br></p>
<p><b>Product and service rules</b></p>

<p></p>
<p>You must not use this website to advertise, buy,
sell or supply services or downloads, or other intangible products.</p>
<p>You must not sell or offer to sell via our website
any product that:</p>
<p>breaches any applicable laws, regulations or codes,
or infringes any third party intellectual property rights or other rights, or
gives rise to a cause of action against any person in any jurisdiction;</p><p><br></p>
<p><b>The selling and
buying process</b></p>
<p>You agree that a contract for the sale and purchase
of a product or products will come into force between you and another
registered website user (and accordingly that you commit to buying or selling
the relevant product or products) in the following circumstances: </p>
<p>(a) &nbsp; you
must add the products you wish to purchase to the shopping cart, and then
proceed to the checkout; </p>
<p>(b) &nbsp; if
you are a new user, you must create a buyer account with the website and log
in; if you already have a buyer account with the website, your must enter your
login details; </p>
<p>(f) &nbsp; once
the seller has checked whether it is able to meet the buyer's order, the seller
will send to the buyer an order confirmation (at which point the order will
become a binding contract between the seller and the buyer) or the seller will
confirm by email that it is unable to meet the buyer's order.</p><p><br></p>
<p><b>Terms
and conditions of sale</b></p>
<p>A seller must use the website interface to create
legal notices that will apply to the relationship between that seller and that
seller's customers.&nbsp; Sellers are
responsible for ensuring that such legal notices are sufficient to meet the
seller's legal disclosure obligations and other legal obligations.&nbsp; Sellers must comply with all laws applicable
to their product listings and sales, including where applicable the Consumer
Protection (Distance Selling) Regulations 2000 and the Electronic Commerce (EC
Directive) Regulations 2002.</p>
<p>Except to the extent that a buyer and seller
expressly agree otherwise / Notwithstanding any terms agreed between a buyer
and a seller, the following provisions will be incorporated into the contract
of sale and purchase between the buyer and the seller: </p>
<p>(a)&nbsp; the
price for a product will be as stated in the relevant product listing;</p>
<p>(b)&nbsp; delivery
charges, packaging charges, handling charges, administrative charges, insurance
costs, other ancillary costs and charges, and VAT and other taxes will only be
payable by the buyer if this is expressly and clearly stated in the product
listing;</p>
<p>(c)&nbsp; deliveries
of products must be made within 30 days of date the contract of sale coming
into force; </p><p><br></p>
<p><b>Payments</b></p>
<p>Sellers must pay to us the charges specified in
this Section 12.</p>
<p>We will process all payments in respect of products
made by buyers to sellers, and will account to sellers for such payments in
respect of a calendar month on or before the end of the following calendar
month.&nbsp; Payments to sellers will be made
by Braintree in the form of direct deposit or Venmo transaction. </p>
<p>The applicable seller charges in respect of stores
/ product listings / product sales will be as set out on our website from time to
time.&nbsp; </p>
<p>We will be entitled to charge interest on any
overdue amount under these terms of use at the annual rate of 8% above the base
rate of HSBC Bank Plc from time to time.&nbsp;
Such interest will accrue daily and be compounded quarterly.</p>
<p>If a buyer makes any credit card, debit card or
other charge-back then the seller will be liable to pay us, within 7 days
following the date we give notice to the buyer, an amount equal to the
commission due to us in respect of the transaction, plus any third party costs
incurred by us arising directly out of the charge-back.</p><p><br></p>
<p><b>User IDs and passwords</b></p>
<p>It is the accountholder’s responsibility to protect
all login credentials. </p><p><br></p>
<p><b>User content</b></p>

<p></p>

<p>Our Right
to Use Your Content<b>: &nbsp;</b>You acknowledge and agree that any
Content you post or provide may be viewed by the general public and will not be
treated as private, proprietary or confidential. You authorize us and our
affiliates, licensees and sub-licensees, without compensation to you or others,
to copy, adapt, create derivative works of, reproduce, incorporate, distribute,
publicly display or otherwise use or exploit such Content throughout the world
in any format or media (whether now known or hereafter created) for the
duration of any copyright or other rights in such Content, and such permission
shall be perpetual and may not be revoked for any reason. Further, to the
extent permitted under applicable law, you waive and release and covenant not
to assert any moral rights that you may have in any Content posted or provided
by you. Grant of License<b>:&nbsp;</b>You
hereby grant WomBids and its users a perpetual, non-exclusive, royalty-free,
transferable, assignable, sub-licensable, worldwide license to use, store,
display, reproduce, modify, create derivative works, perform, distribute,
print, publish, disseminate and place advertising near and adjacent to your
Content in any format or media (whether now know or hereafter created) on the
WomBids and apps in any manner that we deem appropriate or necessary,
including, if submitted, your name, voice and likeness throughout the world,
and such permission shall be perpetual and cannot be revoked for any reason. Representation of Ownership and Right to Use
Content: By posting or providing any Content to WomBids,
you represent and warrant to WomBids that you own or have all necessary rights
to use the Content, and grant to WomBids the rights granted below. The forgoing
representation includes, but is not limited to a representation and warranty
that you own or have the necessary rights (including any necessary releases) to
grant all rights granted below in relation to any persons, places or
intellectual property pictured in any photographic Content that you provide. In
addition, if you post or otherwise provide any Content that is protected by
copyright, you represent that you have obtained any necessary permissions or
releases from the applicable copyright owner. Content Guidelines:&nbsp;WomBids reserves the right, but not the
obligation, to edit or abridge, or to refuse to post, or to remove any content
that you or any other users post on any WomBids owned or operated websites or
apps if WomBids determines (in its sole discretion) that such content contains
or features any of the following:</p>

<p>Offensive, harmful and/or abusive
language, including without limitation: expletives, profanities, obscenities,
harassment, vulgarities, sexually explicit language and hate speech (e.g.,
racist/discriminatory speech.) References to illegal activity. Language that
violates the standards of good taste or the standards of this Site. Statements
that are or appear to be false. Comments that disparage WomBids.</p>

<p>With respect to Ratings and Reviews
of service professionals, all of the above and in addition the following:</p>

<p>Reviews that do not address the
goods and services of the business or reviews with no qualitative value (e.g.,
"work has not started yet"). Comments concerning a different Service
Professional: Information not related to work requested in the service request.
If a dispute arises between a consumer and professional, the rating submitted
may be held in pending status until resolution is reached. You represent and
warrant that any Rating and Review provided by you is accurate and truthful,
and that your will only provide a Rating and Review for a service professional
that has performed services for you pursuant to your applicable service
requests.</p>

<p></p>
<p>Upon using
WomBids, you will be prompted to disclose certain information about yourself and your
service requirements, and you will be able to store information, such as home services
records, on our website or apps. Some of this information will be sent to
service professionals who will need this information to respond to your
request. By providing this information to us, or by submitting a service
request, you are requesting, and you expressly consent to being contacted by us
and by our service professionals and providers via phone, fax, email, mail or
other reasonable means, at any of your contact numbers or addresses, even if
you are listed on any federal, state, provincial or other applicable "Do
Not Call" list, in order that we may provide the services set forth on our
site, to service your account, to reasonably address matters pertaining to your
account or for other purposes reasonably related to your service request and our
business, including marketing related emails. For complete details on our use
of your information, please see our privacy policy. You agree that by
completing a service request, you are entering into a business relationship
with WomBids and its partners and thus agree to be contacted by WomBids and/or
its partners. You promise that all information you provide (including but not
limited to your contact information, and any Ratings and Reviews of service
professionals that you provide) will be accurate, current and truthful to the
best of your knowledge. If you provide any information that is untrue, not
current or incomplete, or WomBids has reasonable grounds to suspect that such
information is untrue, inaccurate, not current or incomplete, WomBids has the
right to refuse any current or future use of the WomBids services (or any
portion thereof) by you. You are responsible for any use of the WomBids
services by persons to whom you intentionally or negligently allow access to
your password.</p>
<p>TO KNOWINGLY INPUT FALSE
INFORMATION, INCLUDING BUT NOT LIMITED TO NAME, PHONE NUMBER, ADDRESS OR E-MAIL
ADDRESS IS A VERY SERIOUS AND FRAUDULENT MATTER THAT COULD RESULT IN
SIGNIFICANT COSTS AND DAMAGES INCLUDING INVASION OF PRIVACY RIGHTS, TO WOMBIDS
AND ITS SERVICE PROFESSIONAL PARTNERS, AND TO CONSUMERS, AS WELL AS THE LOSS OF
TIME, EFFORT AND EXPENSE RESPONDING TO AND PURSUING SUCH FALSE INFORMATION AND
REQUEST, AND FURTHER, COULD RESULT IN REGULATORY FINES AND PENALTIES.
ACCORDINGLY, IF YOU KNOWINGLY INPUT FALSE INFORMATION IN A SERVICE REQUEST,
INCLUDING BUT NOT LIMITED TO SOME ONE ELSE'S NAME, E-MAIL ADDRESS, PHYSICAL
ADDRESS OR PHONE NUMBER OR A RANDOM OR MADE UP NAME, ADDRESS, E-MAIL OR PHONE
NUMBER YOU AGREE TO FULLY INDEMNIFY AND BE LIABLE TO WomBids AND EACH WomBids
SERVICE PROVIDER WHO ACCEPTS SUCH SERVICE REQUESTS, FOR THE GREATER OF: (1) A
MINIMUM AMOUNT OF $11,000 TO EACH OF WomBids AND EACH OF THE AFFECTED SERVICE
PROFESSIONALS AND FOR EACH OF THE ACTUAL PERSON(S) AFFECTED BY ANY OF THE
IMPROPER, INCORRECT OR FRAUDULENT INFORMATION YOU ENTER (FOR EXAMPLE THE ACTUAL
OWNER OF THE E-MAIL ADDRESS OR PHONE NUMBER, ETC.), PER IMPROPER SUBMISSION,
PLUS ANY ATTORNEYS FEES COSTS AND EXPENSE RELATING THERETO, IF APPLICABLE, OR
(2) THE ACTUAL DAMAGES, DIRECT, PUNITIVE AND CONSEQUENTIAL, AND ANY REGULATORY
OR JUDICIAL FINES OR PENALTIES THAT MAY ARISE FROM SUCH INTENTIONAL,
MISLEADING, HARMFUL AND FRAUDULENT ACTIVITY, PLUS REASONABLE LEGAL FEES, COST
AND EXPENSES RELATING THERETO, WHICH EVER IS GREATER.</p><p><br></p>
<p><b>Limitations and exclusions of liability</b></p>
<p>No Guarantees or
Endorsements:&nbsp;Although we take certain steps to examine the credentials of our
listed service professionals, we make no guarantees or representations
regarding the skills or representations of such service professional or the
quality of the job that he or she may perform for you if you elect to retain
their services.&nbsp;WomBids does not
endorse or recommend the services of any particular service professional.&nbsp;It
is entirely up to you to evaluate a service professional's qualifications, and
to enter into a direct contract or otherwise reach agreement with a service
professional. We do not guarantee or warrant any service professional's
performance on the job or the outcome or quality of the services performed. The
service professionals are not employees or agents of WomBids, nor is WomBids an
agent of the service professionals.</p>

<p></p>

<p>No Contracting via
the WomBids Website:&nbsp;WomBids may inform you of certain offers or discounts provided
by a service professional. Such offers or discounts are made solely by the
service professional, and WomBids does not guarantee or warrant the pricing or
discounts that a service professional may offer you. Any quotes provided by
Service Professionals via the WomBids or apps, or which you find on the WomBids
or apps, are not contractually binding offers, are for informational purposes
only, and cannot be accepted on or via WomBids. No contractual arrangement is
created based upon the quotes provided to you from Service Professionals (or
your scheduling of an appointment with a Service Professional) via WomBids. To
contract with a Service Professional, you must work directly with the Service
Professional. WomBids does not perform, and is not responsible for, any of the
services requested by you in your service request. Your rights under contracts
you enter into with service professionals are governed by the terms of such
contracts and by applicable federal, state, provincial and local laws.</p>

<p>Release from Damages
or Claims:&nbsp;Should you have a dispute with any service professional, you must
address such dispute with the service professional directly. YOU HEREBY AGREE
TO RELEASE WomBids (AND OUR OFFICERS, DIRECTORS, AFFILIATES, EMPLOYEES AND
AGENTS) FROM ANY DAMAGES OR CLAIMS (INCLUDING CONSEQUENTIAL AND INCIDENTAL
DAMAGES) OF EVERY KIND OR NATURE, SUSPECTED AND UNSUSPECTED, KNOWN AND UNKNOWN,
AND DISCLOSED OR UNDISCLOSED, ARISING OUT OF OR IN ANY WAY CONNECTED WITH SUCH
DISPUTES AND YOUR DEALINGS WITH SERVICE PROFESSIONALS.</p><p><br></p>

<p></p>

<p><b>Indemnity</b></p>

<p></p>

<p>Anyone who operates on
WOMBIDS AGREEs TO INDEMNIFY WOMBIDS, AND its AFFILIATES, OFFICERS, EMPLOYEES,
AGENTS, CO-BRANDERS, AND OTHER PARTNERS AND HOLD THEM EACH HARMLESS FROM ANY
AND ALL CLAIMS OR DEMANDS, INCLUDING ATTORNEY'S FEES, MADE BY ANY THIRD PARTY
DUE TO OR ARISING FROM YOUR USE OF THE WOMBIDS SERVICES, IN CONNECTION WITH THE
WOMBIDS WEB SITE, WITH REGARD TO ANY DISPUTE BETWEEN YOU AND A SERVICE
PROFESSIONAL, OR YOUR VIOLATION OF THESE TERMS AND CONDITIONS, OR ARISING FROM
YOUR VIOLATION OF ANY RIGHTS OF A THIRD PARTY.</p><p><br></p>

<p></p>
<p><b>Termination</b></p>
<p>You may terminate the Terms
at any time by closing your account, discontinuing your use of the Site, and
providing Wombids with a notice of termination. </p>
<p>We may close your account,
suspend your ability to use certain portions of the Site, and/or ban you
altogether from the Site for any or no reason, and without notice or liability
of any kind. Any such action could prevent you from accessing your account, the
Site, Your Content, Site Content, or any other related information.</p>
