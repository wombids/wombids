<p>
	Please check the link and try again, or alternatively navigate to one of the links below:
	<ul class="disc">
		<li><a href="{root_url}">Return to the homepage</a></li>
		<li><a href="{root_url}contact">Contact us for help</a></li>
	</ul>
</p>