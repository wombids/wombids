<div class="span6">
<h5>Why get testimonials?</h5>
<ul class="circle">
<li>Helps build your credibility and repuatation</li>
<li>Displays for talents and work ethic</li>
<li>Assists if you have little or no ratings</li>
</ul>
</div>
<div class="span6">
<h5>Who should I ask?</h5>
<ul class="circle">
<li>Anyone you have worked with before</li>
<li>Anyone who hired you for a job</li>
</ul>
</div>