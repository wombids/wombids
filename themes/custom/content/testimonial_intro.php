<h3>Testimonials will help improve {business_name}'s chance of winning jobs!</h3>
<p>{site_name} is a website that helps connect providers, like {business_name} to local customers.</p>
<p>By adding your testimonial you help {business_name} attract more customers and book more jobs.</p>