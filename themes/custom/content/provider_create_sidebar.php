<h3>Advice and Tips</h3>
<ul>
<li><a href="/diy-guides">DIY Guides</a></li>
<li><a href="/cost-guides">Cost Guides</a></li>
<li><a href="/trade-insights">Trade Insights</a></li>
<li><a href="/regulations">Regulations</a></li>
<li><a href="/ask-an-expert">Ask an Expert</a></li>
<li><a href="/blog">Blog</a></li>
<li><a href="/social-media">Social Media</a></li>
</ul>