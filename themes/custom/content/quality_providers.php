
<div class="row-fluid">
    <div class="span3 mobile-span3">
        <p><img src="{theme_url}assets/images/intro/quality_providers.png" alt="Quality Providers" /></p>
    </div>
    <div class="span9 mobile-span9">
        <h3>Target the best providers</h3>
        <p>We pre-screen and pre-approve all the pros in our community of providers so you can be sure your job will get done right. During our pre-screening process we:</p>
        <ul class="circle">
            <li>Interview each professional</li>
            <li>Verify their profile information</li>
            <li>Check licenses, background, and service history</li>
            <li>Look at reviews by customers like you, to see results from completed projects</li>
        </ul>
        <p>When a pro wows us during this process, we'll ask them to take an oath.</p>
    </div>
</div>